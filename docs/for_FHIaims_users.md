As CC-aims was originally developed to interface FHI-aims and CC4S, users of FHI-aims can
directly use CC-aims after linking it to FHI-aims during compilation.

## Compiling FHI-aims with CC-aims

**Currently, in order to utilize the CC-aims interface, you need to use the development version
of FHI-aims available on its [GitLab repository](https://aims-git.rz-berlin.mpg.de). Running CC-aims with the current official release
(210716_2) will not work. This will change with the next major release of FHI-aims.**

Upon compilation of FHI-aims, include the path to CC-aims. If compiled via cmake-cache file 
(as is recommended), include the path to CC-aims in the `LIB_PATHS` and `INC_PATHS` environment variables:

```
set(LIB_PATHS "/path/to/CC-aims" CACHE STRING "")
set(INC_PATHS "/path/to/CC-aims" CACHE STRING "")
```

Add `ccaims` to the LIBS environment variable:

```
set(LIBS "ccaims name-of-other-lib-1 name-of-other-lib-2" CACHE STRING "")
```

Finally, enable the linking to CC-aims:

```
set(USE_CC4AIMS ON CACHE BOOL "")
```

## Running CC-aims in FHI-aims

In order to generate CC4S input after an FHI-aims run, two things should be added to `control.in`:

```
output cc4s
KS_method parallel
```
The `output cc4s` will make *CC-aims* calculate and output all the data and yaml files required for a CC4S calculation. Since CC-aims uses the *ScaLAPACK* library, `KS_method parallel` must be set. This forces FHI-aims to use the ScaLAPACK-parallelized eigensolver.

In order to reduce the size of the Coulomb vertex, which can become quite big, 
additionally the following keyword can be added to `control.in`:
```
cc4s_screen_thresh <thr>
``` 

where `<thr>` is a decimal number. This will invoke a principal component analysis (PCA) 
of the Coulomb vertex, during which eigenvalues smaller than `<thr>` will be discarded, 
thus reducing the size of the Coulomb vertex. 
By choosing the threshold carefully, it has been found that often over 50% of 
the eigenvalues can be thrown out without a relevant loss in accuracy.

Alternatively, the PCA can be enabled by adding the following keyword to `control.in`.

```
cc4s_screen_num <n>
```

Here, `n` is an integer and will be the absolute number of eigenvalues kept during the PCA. 
If both, `cc4s_screen_thresh` and `cc4s_screen_num` are specified in `control.in`, `cc4s_screen_num` will have precedence and `cc4s_screen_thresh` will be ignored.

Finally, if one provides the `cc4s_debug`-keyword, more verbose output will be produced. Moreover, the 
files containing the Coulomb vertex and (if applicable) the Fock matrix, which would usually be only provided via 
binary/non-formatted files, will additionally be printed in a human-readable format.
Please use this keyword with caution, especially for bigger systems, as performing formatted I/O is extremely slow
and generates significantly larger files.

In FHI-aims, one can perform both periodic (using RI-LVL) and molecular (using RI-V) Hartree-Fock and Kohn-Sham 
density functional theory calculations in combination with the CC-aims interface.

## Example walkthrough

For convenience we will use an environment variable to the CC-aims root directory. For this, please type the following (assuming you have cloned the project already):

```
export CC_AIMS=<path/to/fhi-cc4s>
```
where you have to adapt the path to the root directory. 


As summarized in the previous section the calculation of quantities via CC4S starting from an (any) ab-initio codes
is carried out in 2 steps:

1. Calculate and write CC4S-input files via an ab-initio code (here: FHI-aims) + the CC-aims interface.
2. Perform the CC4S calculation using these generated files.

Hence, it is reasonable to create two directories, one for each step. We will define a root folder for this tutorial and, two subdirectories:

```
mkdir cc-aims-tutorial
mkdir aims CC4S
```

### Preparing the input

In order to complete the first step FHI-aims should have been compiled linking CC-aims as is explained on the [**Installation**-page](/installation).
Now, it's time to create the input for FHI-aims, which consists of 2 files: `control.in` and `geometry.in`.
The former contains the selection of the used method, convergence thresholds, basis set and many more settings,
which are listed in the FHI-aims manual. The former, `geometry.in`, in most cases only contains the structure
information of the system, which consists either of mere atomic coordinates or also includes unit cell information,
depending if you perform a molecular or periodic calculation. An introduction to FHI-aims you can find in the tutorial [Basics of Running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/)

For the most part, we will focus on the periodic LiH-bulk Hartree-Fock calculation with a 2x1x1 k-point mesh.
Other systems will be briefly examined to demonstrate the use of additional CC-aims specific keywords, which have
been introduced above.

You can find the `control.in` and `geometry.in` files for the LiH calculation in the follwing subdirectory of CC-aims: 
```
tests/test-inputs
```
 
Let's first copy them into our `aims` directory:
```
cd aims
cp $CC_AIMS/tests/test-inputs/LiH-2x1x1/control.in .
cp $CC_AIMS/tests/test-inputs/LiH-2x1x1/geometry.in .
```

Have a look at the input files:

=== "control.in"

```
xc                       hf
frozen_core_postSCF      0
RI_method                LVL
k_grid                   2  1  1
restart                  restart
symmetry_reduced_k_grid  .false.
output cc4s
relativistic             none
occupation_type          gaussian 0.001
mixer                    pulay
KS_method                parallel
n_max_pulay              10
charge_mix_param         0.2
sc_accuracy_rho          1E-6
sc_accuracy_eev          1E-4
sc_accuracy_etot         1E-6
sc_iter_limit            200
override_illconditioning .true.
charge                  0
```

=== "geometry.in"

```
lattice_vector 0.0000000000000000 1.9896000000000000 1.9896000000000000 
lattice_vector 1.9896000000000000 0.0000000000000000 1.9896000000000000 
lattice_vector 1.9896000000000000 1.9896000000000000 0.0000000000000000 
atom_frac 0.0000000000000000 0.0000000000000000 -0.0000000000000000 H
atom_frac 0.5000000000000000 0.5000000000000000 0.4999999999999999 Li
```


While the `geometry.in` lists concisely the lattice vectors and relative atomic coordinates of the LiH-system,
the `control.in` is slightly more convoluted. 

If you have a look at the copied `control.in` file yourself, you will
notice, that only the first half of the file is displayed here. This is because the second part of `control.in`
contains basis set information, which for our particular purpose is unimportant - in principle any basis set can be used.
In short, the basis set information, which is excluded here, can be copied and pasted from the species_defaults sub-directory
of FHI-aims. For a more in-depth explanation on basis sets, please consult the manual of FHI-aims.

The shown first half of `control.in` features the calculation settings, including for example:

* `xc hf`: a Hartree-Fock SCF-calculation will be performed
* `k_grid 2 1 1`: a 2x1x1 k-point grid will be used
* `KS_method parallel`: the ScaLAPACK-parallelized version of the SCF-loop will be run

and a couple of more or less self-explanatory and mostly optional keywords, which are all listed and explained 
in the FHI-aims manual.

As pointed out in the beginning of this chapter, the crucial keyword here, which invokes CC-aims, is `output cc4s`.
That's all that is needed to perform the most basic generation of input for CC4S. If one was to leave out this line,
FHI-aims would only perform the Hartree-Fock SCF-calculation and terminate. By adding the `output cc4s`-keyword,
a post-processing step after the SCF-calculation will be invoked, which in turn calls the CC-aims library.

The other CC-aims-specific keywords introduced in the beginning can be used in similar fashion and should be listed
in addition to `output cc4s`. Complete example inputs, where these are used, can be found in the `tests` subdirectory.
For instance, if you have a look at the test case in `tests/test-inputs/LiH-2x1x1-frozen-core-OAF-200`, you will find
a slightly changed `control.in`: 

=== "control.in"

```
xc                       hf
frozen_core_postSCF      1
RI_method                LVL
k_grid                   2  1  1
restart                  restart
symmetry_reduced_k_grid  .false.
output cc4s
cc4s_screen_num 200
relativistic             none
occupation_type          gaussian 0.001
mixer                    pulay
KS_method                parallel
n_max_pulay              10
charge_mix_param         0.2
sc_accuracy_rho          1E-6
sc_accuracy_eev          1E-4
sc_accuracy_etot         1E-6
sc_iter_limit            200
override_illconditioning .true.
charge                  0
```

In this test case LiH with a 2x1x1 k-point grid is calculated again, but now with additional settings: 

* `frozen_core_postSCF 1`: excluding ("freezing") the states belonging to the lowest shell
* `cc4s_screen_num 200`: compressing the Coulomb vertex by performing a PCA, so that only the 200 most relevant singular values
are taken into account. If one instead wishes to specify the PCA by a decimal threshold,
as explained in the previous section, `cc4s_screen_num 200` can be replaced by e.g `cc4s_screen_thresh 0.01`. 

This has been done in the test case in `tests/test-inputs/LiH-2x1x1-frozen-core-OAF-E-2`.

### Calculating input for CC4S

Now, launch the FHI-aims calculation in the `aims`-directory, where `control.in` and `geometry.in` can be found, e.g via

```
mpiexec -np 4 /path/to/fhi-aims/executable/aims.xxx.scalapack.mpi.x
```

Please make sure, that the executable has the `scalapack.mpi.x` at the end. If this is not the case, you have probably
compiled FHI-aims without ScaLAPACK support.

After the calculation has successfully terminated, you will find a set of files ending with `.elements` and `.yaml`.
These are the CC4S-input files. For a spin-restricted, Hartree-Fock based calculation, like the present 
LiH-calculation, you should find the following files in the `aims`-directory:

```
EigenEnergies.yaml
EigenEnergies.elements
CoulombVertex.yaml
CoulombVertex.elements
AuxiliaryField.yaml
State.yaml
```

These files constitute the basic input of any CC4S calculation. Depending on the type of SCF-calculation you are performing
4 more files may be generated:

* `Spin.yaml` and `Spin.elements`, if the system has been chosen to be spin-polarized (via the `spin collinear`-keyword in `control.in`)
* `FockOperator.yaml` and `FockOperator.elements`, if a non-canonical, i.e non-Hartree-Fock, SCF-calculation has been performed.

Currently, the only method in CC4S, which offers spin-polarized and/or non-canonical calculations, is the MP2 method.
This will, however, be extended to all other methods in the newer versions.

## Performing the CC4S calculation
A complete description of the input, the algorithms and additional information of CC4S can be found in its 
[manual](https://manuals.cc4s.org/user-manual/index.html).
Therefore, this section will only contain a brief instruction, how a CC4S-calculation is run.

1. Leave the `aims`-directory and enter the `CC4S`-directory.

    ```
    cd ../CC4S
    ```

2. Either copy or create links to the ".elements"- and ".yaml"-files generated previously.

    ```
    cp ../aims/*yaml .
    cp ../aims/*elements .
    ```

3. Write a `cc4s.in`-file. This yaml-file instructs CC4S, which algorithm with which settings
should be performed (a more in-depth explanation can be found [here](https://manuals.cc4s.org/user-manual/tutorials/Rbn_tutorial.html)). You can call the file any name you want, `cc4s.in`, however, is the default name. For this example, the following input file called `mp2.yaml` will be used, which will instruct CC4S to perform a basic MP2 calculation:

    === "mp2.yaml"

    ```
    - name: Read
      in:
        fileName: "EigenEnergies.yaml"
      out:
        destination: EigenEnergies

    - name: Read
      in:
        fileName: "CoulombVertex.yaml"
      out:
        destination: CoulombVertex


    - name: DefineHolesAndParticles
      in:
        eigenEnergies: EigenEnergies
      out:
        slicedEigenEnergies: EigenEnergies

    - name: SliceOperator
      in:
        slicedEigenEnergies: EigenEnergies
        operator: CoulombVertex
      out:
        slicedOperator: CoulombVertex

    - name: VertexCoulombIntegrals
      in:
        slicedCoulombVertex: CoulombVertex
      out:
        coulombIntegrals: CoulombIntegrals

    - name: SecondOrderPerturbationTheory
      in:
        slicedEigenEnergies: EigenEnergies
        coulombIntegrals: CoulombIntegrals
      out:
        energy: Mp2Energy
    ```

4. Launch the CC4S-calculation, e.g via 
    ```
    mpiexec -np 4 /path/to/CC4S/Cc4s
    ``` 
    or via 
    ```
    mpiexec -np 4 /path/to/CC4S/Cc4s -i mp2.yaml
    ``` 
    (in case you chose a custom name for the `cc4s.in`-file)


The calculation progress will be printed to standard output and to the output file `cc4s.out.yaml`.
The latter contains a more detailed account of the calculation.
In the case of a MP2-calculation the correlation energy can be found

- in the last paragraph of `cc4s.out.yaml`. After the  LiH calculation of the presented example has
finished, the bottom of that file should contain a line similar to 

`correlation: -0.084407956741432685`

This is the correlation energy of the calculated 
system in Hartree units. It is very likely that the number you have obtained will differ 
slightly due to numerical precision and other factors including the use of different compilers
or libraries. As can be seen in the image below, CC4S additionally provides a number of decompositions
of the correlation energy, e.g the direct and exchange-like contribution of the correlation energy.

=== "cc4s.out.yaml"

```
correlation: -0.084407956741432685
direct: -0.16052632454054014
exchange: 0.076118367799107453
secondOrder: -0.084407956741432685
secondOrderDirect: -0.16052632454054014
secondOrderExchange: 0.076118367799107453
secondOrderSingles: 0
singles: 0
unit: 1
```

- A briefer report is printed by CC4S to the standard output. There, the most relevant results, like 
the correlation energy are listed. However, it should be noted that the correlation energy there is
shown with a reduced number of decimal places.

=== "stdout"

```
step 6: SecondOrderPerturbationTheory
contracting second order energy...
correlation energy: -0.084408
  singles:  0
  direct:   -0.160526
  exchange: 0.0761184
realtime 0.136715133 s
```

