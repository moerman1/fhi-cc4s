Before contributing to CC-aims, please get in touch with the maintainer
of the code. This can always be done by [opening an issue](https://gitlab.com/moerman1/fhi-cc4s/-/issues?sort=created_date&state=opened), but an email is
also welcome. 

If reporting an issue, please make sure that your bug/request/idea has not been
posted in a prior issue.

## Bug reports
If you have a bug to report, please provide a detailed explanation how the
code execution deviates from the exepected behavior. Error backtraces, 
debugger output  and similar additional information are greatly appreciated
and will likely accelerate the resolution of the bug.
In addition, please always provide a minimal example including 
the specification of the compiler and the dependencies you used, to ensure
reproducibility of the bug.

## Feature requests
You have a good idea for a new functionality which is not yet implemented?
Open an issue and let's discuss. If you already have an idea, how to 
implement the feature or even wish to implement it yourself, even better.

## Interfacing a new ab-initio code
You want to use CC-aims to interface your favourite ab-initio software
package but you are not entirely sure how?
That's great! First, please make sure that the code you want to interface
fullfils the minimal requirements for using CC-aims, which are listed
on the **Home** page. 
If these are not a problem, please have a look on the **Other codes** 
and the **Interface subroutines** page, where
the subroutines and the utilization of the API are summarized.

While CC-aims was designed to be used with any
ab-initio code, which complies with the basic requirements, it is possible,
that your particular ab-initio code requires some specific feature in CC-aims,
which has not been accounted for. In that case, please get in touch with the
maintainer via the aforementioned channels to discuss, how CC-aims can be
extended to accommodate your code as well.

If questions with respect to interfacing your ab-initio code remain, don't
hesitate to ask.

## Merge requests
Code contributions to CC-aims should be done via [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html). To ensure the correct integration of the added feature, 
please make sure that the changes you have made pass the CI/CD test pipeline. Also, if applicable, you should provide one
or more integration tests yourself, which cover the functionality you added or changed. Please keep the size of each test as small as possible.

With respect to the formatting of your code contribution, it is only requested that you use expressive names for
functions and variables. Long names are not a problem, as long as you don't reach the compiler-imposed limits.
