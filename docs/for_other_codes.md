Codes other than FHI-aims, which employ a localized atomic basis set and the RI formalism can use the main
routines provided by CC-aims to obtain CC4S input after a HF- or KS-SCF calculation.
To do that, the 2 central subroutines of CC-aims `CC4S_init_interface` and `CC4S_interface_driver`
are necessary. 

After having obtained the converged ab-initio quantities from a HF- or KS-SCF calculation, 
`CC4S_init_interface` can be called. This routine takes all relevant quantities, which describe
the calculated system and the calculation settings, including the number of k-points, the number 
of basis functions etc...
From these quantities `CC4S_init_interface` calculates how the matrix- and tensor-quantities, from which
CC-aims computes the Coulomb vertex, have to be distributed over the MPI tasks. 
This information is returned by the subroutine in the form of task-local index-bounds, and 
Scalapack-specific quantities.

The returned information should suffice to (re-)distribute the relevant matrix- and tensor-quantities
appropriately for CC-aims. 

After the distributed quantities, including HF/KS-eigenvectors, RI-expansion coefficients etc., have been
obtained, they can be passed to the `CC4S_interface_driver` subroutine. This subroutine will perform the
calculation of the Coulomb vertex and generate all the files necessary for CC4S.

In summary, the calling algorithm should look like this (pseudo-code):

---
prepare quantities which describe the calculation setting (number of states, number of k-points etc.) <br>
... <br>
`call CC4S_init_interface(...)` <br>
... <br>
calculate + distribute or just re-distribute matrix-/tensor-quantities (RI-coefficients, KS/HF-eigenvectors etc.) <br>
... <br>
`call CC4S_interface_driver(...)` <br>
... <br>
finish up/clean up/deallocate

---

