# CC-aims

This project constitutes the interface between the Fritz Haber Institute ab initio molecular simulations (FHI-aims) 
and the 'Coupled Cluster for Solids' (CC4S) code developed by Felix Hummel and Adreas Grueneis at the TU Wien. 
While developed in connection to FHI-aims, `CC-aims` can be interface by any quantum chemistry software
that

* uses a localized basis sets (NAOs, GTOs, STOs etc.) and
* employs a "resolution of identity" scheme (RI-LVL, RI-V, RI-SVS...).

## Compilation

* `cd src`
* `make` or `make CONFIG=INTEL`

generates a static library (`libccaims.a`) and a shared library (`libccaims.so`)

## Documentation

Details about the theoretical basis of `CC-aims`, its application in conjunction with FHI-aims
and its API for other ab-initio codes, which want to interface to CC4S via CC-aims as well, 
can be found in [the documentation right here](https://moerman1.gitlab.io/fhi-cc4s).

In particular, if you have ideas how to improve CC-aims, please read first the [contributing guidelines](https://moerman1.gitlab.io/fhi-cc4s/contributing_guidelines/).
