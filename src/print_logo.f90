subroutine print_logo
  implicit none

  write(*,*) "   ____________           _               "
  write(*,*) "  / ____/ ____/    ____ _(_)___ ___  _____"
  write(*,*) " / /   / /  ______/ __ `/ / __ `__ \/ ___/"
  write(*,*) "/ /___/ /__/_____/ /_/ / / / / / / (__  ) "
  write(*,*) "\____/\____/     \__,_/_/_/ /_/ /_/____/  "
end subroutine print_logo
