module CC4S_bin2format


contains

  subroutine bin2format(binfile_name, formatfile_name, comm, size_vertex, taskID)
        use mpi
        implicit none

        !input
        character(len=*) :: binfile_name, formatfile_name
        integer :: comm
        integer :: size_vertex
        integer,optional :: taskID
  
        !local
        integer :: fh
        integer :: ierr
        integer :: myid
        integer :: i_line
        double complex :: val
  
        integer(kind=MPI_OFFSET_KIND) :: offset
  
        integer :: writingTaskID

        if(present(taskID)) then
          writingTaskID = taskID
        else
          writingTaskID = 0 
        end if 
 
        call MPI_Comm_rank(comm, myid, ierr)

        if(myid .eq. 0) then
          !write(*,'(A,1X,A)') 'Write Coulomb vertex to', formatfile_name
          write(*,'(A,1X,A,1X,A,1X,A)') 'Converting binary', trim(binfile_name), 'to formatted', trim(formatfile_name)
        end if 

        if(myid .eq. writingTaskID) then
          call MPI_File_open(MPI_COMM_SELF, binfile_name, MPI_MODE_RDONLY, &
                  &MPI_INFO_NULL, fh, ierr)
  
          offset=0
  
          call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, MPI_DOUBLE_COMPLEX, &
                  &'native', MPI_INFO_NULL, ierr)
  
          open(unit=551, file=formatfile_name, iostat=ierr)
          do i_line=1, size_vertex
            call MPI_File_read(fh, val, 1, MPI_DOUBLE_COMPLEX, MPI_STATUS_IGNORE, &
                    &ierr)
            write(551,'("(",(E22.15),",",(E22.15),")")') val
          end do
          close(551)
          call MPI_File_close(fh, ierr)
  
        end if

  end subroutine bin2format
  !*******************************************
  subroutine bin2format_real(binfile_name, formatfile_name, comm, size_vertex, taskID)
        use mpi
        implicit none
  
        !input
        character(len=*) :: binfile_name, formatfile_name
        integer :: comm
        integer :: size_vertex
        integer,optional :: taskID
  
        !local
        integer :: fh
        integer :: ierr
        integer :: myid
        integer :: i_line
        double precision :: val
  
        integer(kind=MPI_OFFSET_KIND) :: offset
  
        integer :: writingTaskID
 
        if(present(taskID)) then
          writingTaskID = taskID
        else
          writingTaskID = 0 
        end if

        
        call MPI_Comm_rank(comm, myid, ierr)

        if(myid .eq. 0) then
          !write(*,'(A,1X,A)') 'Write Coulomb vertex to', formatfile_name
          write(*,'(A,1X,A,1X,A,1X,A)') 'Converting binary', trim(binfile_name), 'to formatted', trim(formatfile_name)
        end if 

        if(myid .eq. writingTaskID) then
          call MPI_File_open(MPI_COMM_SELF, binfile_name, MPI_MODE_RDONLY, &
                  &MPI_INFO_NULL, fh, ierr)
  
          offset=0
  
          call MPI_File_set_view(fh, offset, MPI_DOUBLE_PRECISION, MPI_DOUBLE_PRECISION, &
                  &'native', MPI_INFO_NULL, ierr)
  
          open(unit=551, file=formatfile_name, iostat=ierr)
          do i_line=1, size_vertex
            call MPI_File_read(fh, val, 1, MPI_DOUBLE_PRECISION, MPI_STATUS_IGNORE, &
                    &ierr)
            write(551,'(E22.15)') val
          end do
          close(551)
          call MPI_File_close(fh, ierr)
  
        end if

  end subroutine bin2format_real
 
end module CC4S_bin2format
