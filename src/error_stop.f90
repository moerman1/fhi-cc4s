subroutine error_stop(routine, err_message)
      use mpi
      implicit none

      !input
      character(len=*) :: routine
      character(len=*) :: err_message

      !local
      integer :: ierr

      write(*,"(1X, 'Error occurred in', A30, /, A300)") routine, err_message 

      call MPI_Abort(MPI_COMM_WORLD, 0, ierr)

end subroutine error_stop
