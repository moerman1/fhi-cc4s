module CC4S_OAF_transformation
      use mpi
      use CC4S_blacs_environment, only: CC4S_lbb_row, CC4S_ubb_row, CC4S_lbb_col, CC4S_ubb_col, &
                                        &CC4S_lstates2_col,CC4S_ustates2_col, &
                                        &CC4S_states2xbb_desc, CC4S_bb_desc, &
                                        &CC4S_n_tasks, &
                                        &CC4S_init_blacsOAF_distribution, CC4S_states2xbb_desc, &
                                        &CC4S_nprow, CC4S_npcol, &
                                        &CC4S_myid_col, CC4S_myid_row

      use calculation_data, only: CC4S_n_basbas, CC4S_nuf_states, CC4S_n_spin

      double precision, dimension(:,:), allocatable :: OAF_sum_squared_blocks_r
      double complex, dimension(:,:), allocatable :: OAF_sum_squared_blocks_c

      double precision, dimension(:,:), allocatable :: OAF_trafo_matrix_r
      double complex, dimension(:,:), allocatable :: OAF_trafo_matrix_c

      integer :: oaf_trafo_matrix_fh

      interface CC4S_add_up_squared_block
              module procedure CC4S_add_up_squared_block_real
              module procedure CC4S_add_up_squared_block_cmplx
      end interface CC4S_add_up_squared_block

!      interface CC4S_compute_OAF_trafo_matrix
!              module procedure CC4S_compute_OAF_trafo_matrix_real
!              module procedure CC4S_compute_OAF_trafo_matrix_cmplx
!      end interface CC4S_compute_OAF_trafo_matrix

      interface U_times_Gamma
              module procedure U_times_Gamma_real
              module procedure U_times_Gamma_cmplx
      end interface U_times_Gamma

contains
subroutine initialize_oaf_trafo_matrix_as_identity_real()
  implicit none

  !local
  integer :: i_bb_1, i_bb_2

  if(allocated(OAF_trafo_matrix_r)) then
    deallocate(OAF_trafo_matrix_r)
  end if

  allocate(OAF_trafo_matrix_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
  OAF_trafo_matrix_r(:,:) = 0.0d0

  !Go through all the diagonal elements and set them to 1
  do i_bb_1 = CC4S_lbb_row, CC4S_ubb_row
    do i_bb_2 = CC4S_lbb_col, CC4S_ubb_col
      if(i_bb_1 == i_bb_2) then
        OAF_trafo_matrix_r(i_bb_1,i_bb_2) = 1.0d0
      end if
    end do
  end do
end subroutine initialize_oaf_trafo_matrix_as_identity_real
!************************************************************************
subroutine initialize_oaf_trafo_matrix_as_identity_cmplx()
  implicit none

  !local
  integer :: i_bb_1, i_bb_2

  if(allocated(OAF_trafo_matrix_c)) then
    deallocate(OAF_trafo_matrix_c)
  end if

  allocate(OAF_trafo_matrix_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
  OAF_trafo_matrix_c(:,:) = (0.0d0,0.0d0)

  !Go through all the diagonal elements and set them to 1
  do i_bb_1 = CC4S_lbb_row, CC4S_ubb_row
    do i_bb_2 = CC4S_lbb_col, CC4S_ubb_col
      if(i_bb_1 == i_bb_2) then
        OAF_trafo_matrix_c(i_bb_1,i_bb_2) = (1.0d0,0.0d0)
      end if
    end do
  end do
end subroutine initialize_oaf_trafo_matrix_as_identity_cmplx
!************************************************************************
subroutine open_oaf_trafo_matrix_filehandle()
  use CC4S_blacs_environment, only : CC4S_member, CC4S_comm
  use outputfile_names
  use mpi
  implicit none

  integer :: ierr

  if(CC4S_member) then
    call MPI_File_open(CC4S_comm, oaf_trafo_matrix_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, oaf_trafo_matrix_fh, ierr)
  end if

end subroutine open_oaf_trafo_matrix_filehandle
!*************************************************************************
subroutine close_oaf_trafo_matrix_filehandle(debug)
  use CC4S_blacs_environment, only : CC4S_member, CC4S_comm
  use outputfile_names
  use CC4S_adapt_data_file_suffix
  use mpi
  implicit none

  logical, intent(in) :: debug

  integer :: ierr

  if(CC4S_member) then
    call MPI_File_close(oaf_trafo_matrix_fh, ierr)

    !In the new (release-version) of CC4S, data files carry the suffix ".elements"
    !instead of ".dat" or ".bin".
    !The following function will select the chosen data file 
    !(the .bin- or .dat-file depending on debug-setting)
    !and change the respective suffix to ".elements"
    call adapt_data_file_suffix(debug, oaf_trafo_matrix_filename_dat, &
                                &oaf_trafo_matrix_filename_bin, MPI_COMM_WORLD)
  end if

end subroutine close_oaf_trafo_matrix_filehandle
!*************************************************************************
subroutine CC4S_parallel_write_oaf_trafo_matrix_block_to_file(curr_kq, isreal, oaf_reduced_basbas)

  use calculation_data, only: CC4S_n_basbas, CC4S_nuf_states, CC4S_n_spin
  use CC4S_blacs_environment, only : CC4S_lstates2_col, CC4S_lbb_row, CC4S_lredbb_col
  use CC4S_parallel_io, only: CC4S_parallel_write, CC4S_parallel_write_oaf_singular_vectors
  implicit none

  integer, intent(in) :: curr_kq
  logical, intent(in) :: isreal
  integer, optional, intent(in) :: oaf_reduced_basbas

  !local
  integer, dimension(2) :: oaf_trafo_matrix_sizes
  integer, dimension(2) :: oaf_trafo_matrix_subsizes
  integer, dimension(2) :: oaf_trafo_matrix_starts
  integer :: internal_oaf_reduced_basbas
  integer :: internal_lower_bound_bb

  if(present(oaf_reduced_basbas)) then
    internal_oaf_reduced_basbas = oaf_reduced_basbas
    internal_lower_bound_bb = CC4S_lredbb_col
  else
    internal_oaf_reduced_basbas = CC4S_n_basbas
    internal_lower_bound_bb = CC4S_lbb_col
    !If we are here, there is no OAF-trafo, i.e
    !the trafo matrix is a CC4S_n_basbas x CC4S_n_basbas
    !identity matrix
    if(isreal) then
      call initialize_oaf_trafo_matrix_as_identity_real()
    else
      call initialize_oaf_trafo_matrix_as_identity_cmplx()
    end if
  end if

  oaf_trafo_matrix_sizes = [CC4S_n_basbas, internal_oaf_reduced_basbas]

  if(isreal) then
    oaf_trafo_matrix_subsizes=shape(OAF_trafo_matrix_r)
  else
    oaf_trafo_matrix_subsizes=shape(OAF_trafo_matrix_c)
  endif

  oaf_trafo_matrix_starts=[CC4S_lbb_row-1, internal_lower_bound_bb-1]

  if(isreal) then
    call CC4S_parallel_write_oaf_singular_vectors(OAF_trafo_matrix_r, oaf_trafo_matrix_fh, oaf_trafo_matrix_sizes, &
                          &oaf_trafo_matrix_subsizes, oaf_trafo_matrix_starts, internal_oaf_reduced_basbas, curr_kq)
  else
    call CC4S_parallel_write_oaf_singular_vectors(OAF_trafo_matrix_c, oaf_trafo_matrix_fh, oaf_trafo_matrix_sizes, &
                          &oaf_trafo_matrix_subsizes, oaf_trafo_matrix_starts, internal_oaf_reduced_basbas, curr_kq)
  endif

end subroutine CC4S_parallel_write_oaf_trafo_matrix_block_to_file
!**********************************************************************
      subroutine initiate_sum_squared_blocks_real()
        implicit none
        
        if(.not. allocated(OAF_sum_squared_blocks_r)) then
          allocate(OAF_sum_squared_blocks_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
        end if 

      end subroutine initiate_sum_squared_blocks_real
!**********************************************************************
      subroutine initiate_sum_squared_blocks_cmplx()
        implicit none

        if(.not. allocated(OAF_sum_squared_blocks_c)) then
          allocate(OAF_sum_squared_blocks_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
        end if

      end subroutine initiate_sum_squared_blocks_cmplx
!**********************************************************************
      subroutine CC4S_add_up_squared_block_real(coulomb_vertex_block)
        implicit none
        
        !input
        double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates2_col:CC4S_ustates2_col, &
              &CC4S_n_spin), intent(in) :: coulomb_vertex_block

        !local
        integer :: i_spin

        !make sure that sun_squared_blocks is allocated
        if(.not. allocated(OAF_sum_squared_blocks_r)) then
          allocate(OAF_sum_squared_blocks_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
          OAF_sum_squared_blocks_r(:,:) = 0.d0
        end if 

        !Compute the square of the current Coulomb vertex block and immediately
        !add it to the sum of squared blocks
        do i_spin = 1,CC4S_n_spin
          call pdgemm('N', 'C', CC4S_n_basbas, CC4S_n_basbas, CC4S_nuf_states**2, 1.0D0, &
                     &coulomb_vertex_block(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, &
                     &coulomb_vertex_block(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, &
                     &1.0D0, OAF_sum_squared_blocks_r(:,:), 1, 1, CC4S_bb_desc)
        end do
      end subroutine CC4S_add_up_squared_block_real
!**********************************************************************
      subroutine CC4S_add_up_squared_block_cmplx(coulomb_vertex_block)
        implicit none
        
        !input
        double complex, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates2_col:CC4S_ustates2_col, &
              &CC4S_n_spin), intent(in) :: coulomb_vertex_block

        !local
        integer :: i_spin

        !make sure that sun_squared_blocks is allocated
        if(.not. allocated(OAF_sum_squared_blocks_c)) then
          allocate(OAF_sum_squared_blocks_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
          OAF_sum_squared_blocks_c(:,:) = (0.0d0, 0.0d0)
        end if 

        !Compute the square of the current Coulomb vertex block and immediately
        !add it to the sum of squared blocks
        do i_spin = 1,CC4S_n_spin
          call pzgemm('N', 'C', CC4S_n_basbas, CC4S_n_basbas, CC4S_nuf_states**2, (1.0D0, 0.D0), &
                     &coulomb_vertex_block(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, &
                     &coulomb_vertex_block(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, &
                     &(1.0D0, 0.0D0), OAF_sum_squared_blocks_c(:,:), 1, 1, CC4S_bb_desc)
        end do
      end subroutine CC4S_add_up_squared_block_cmplx
!***********************************************************************
      subroutine CC4S_reset_squared_block_sum()
        implicit none

        !Intended to be used when all blocks for a given transfer momentum have been taken care of 
        !and new transfer momentum is going to be treated

        if(allocated(OAF_sum_squared_blocks_r)) then
          OAF_sum_squared_blocks_r(:,:) = 0.D0
        else if(allocated(OAF_sum_squared_blocks_c)) then
          OAF_sum_squared_blocks_c(:,:) = (0.0D0, 0.0D0)
        end if 

      end subroutine CC4S_reset_squared_block_sum
!**********************************************************************
      subroutine CC4S_compute_OAF_trafo_matrix_real(lredbb_row, max_singval, num_singvals, singval_thr)
        implicit none

        !input
        integer, intent(out) :: max_singval
        integer, optional, intent(in) :: num_singvals
        double precision, optional, intent(in) :: singval_thr
        integer, intent(out) :: lredbb_row

        !local
        double precision, dimension(:), allocatable :: work
        integer :: lwork
        integer :: info
        double precision, dimension(:), allocatable :: singular_values
        double precision, dimension(:,:), allocatable :: eigenvectors
        !integer :: max_singval
        integer :: uredbb_row
        integer :: lredbb_col, uredbb_col
        integer, dimension(9) :: bbxredbb_desc, states2xredbb_desc
        integer :: redbb_block_col
        integer :: bb_block_row
        double precision, dimension(:,:), allocatable :: proj_id
        integer :: i_basbas
        integer, external :: indxg2l
        integer :: loc_col, loc_row

        !Negate matrix to get eigenvalues from highest to lowest
        OAF_sum_squared_blocks_r(:,:) = -OAF_sum_squared_blocks_r(:,:)

        allocate(work(1))
        lwork = -1

        allocate(singular_values(CC4S_n_basbas))
        allocate(eigenvectors(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))

        if(CC4S_n_tasks .eq. 1) then
          call dsyev('V', 'U', CC4S_n_basbas, OAF_sum_squared_blocks_r(:,:), CC4S_n_basbas, &
                    &singular_values, work, lwork, info)

          lwork = int(work(1))
          deallocate(work)
          allocate(work(lwork))

          call dsyev('V', 'U', CC4S_n_basbas, OAF_sum_squared_blocks_r(:,:), CC4S_n_basbas, &
                    &singular_values, work, lwork, info)

          deallocate(work)
          eigenvectors(:,:) = OAF_sum_squared_blocks_r(:,:)
        else
          call pdsyev('V', 'U', CC4S_n_basbas, OAF_sum_squared_blocks_r(:,:), 1, 1, CC4S_bb_desc, &
                     &singular_values, eigenvectors(:,:), 1, 1, CC4S_bb_desc, work, lwork, info)

          lwork = int(work(1))
          deallocate(work)
          allocate(work(lwork))

          call pdsyev('V', 'U', CC4S_n_basbas, OAF_sum_squared_blocks_r(:,:), 1, 1, CC4S_bb_desc, &
                     &singular_values, eigenvectors(:,:), 1, 1, CC4S_bb_desc, work, lwork, info)

         
          deallocate(work)
        end if 

        !Compensate for prior negation of matrix
        singular_values(:) = -singular_values(:)

        !Truncate number of singular values (optional)
        if(present(num_singvals)) then
          if(abs(num_singvals) .le. size(singular_values)) then
            max_singval = abs(num_singvals)
          else
            max_singval = size(singular_values)
          end if 
        else if(present(singval_thr)) then
          !Will only enter here, if num_singval has not been provided
          max_singval = max(1, count(singular_values > singval_thr))
        else
          max_singval = size(singular_values)
        end if

        !Set up context and blacs-infrastructure for new reduced/truncated eigenvector matrix
        !write(*,*) "MAX_SINGVAL = ", max_singval
        call CC4S_init_blacsOAF_distribution(max_singval, lredbb_row, &
                &uredbb_row, lredbb_col, uredbb_col, &
                &bbxredbb_desc, states2xredbb_desc, redbb_block_col, bb_block_row)

        !Construct identity projection to reduce number of eigenvectors and uphold blacs-distribution
        allocate(proj_id(CC4S_lbb_row:CC4S_ubb_row, lredbb_col:uredbb_col))
        proj_id=0.0D0

        do i_basbas=1,max_singval
          if((mod((i_basbas-1)/bb_block_row, CC4S_nprow) .eq. CC4S_myid_row) &
                  &.and. (mod((i_basbas-1)/redbb_block_col, CC4S_npcol) &
                  &.eq. CC4S_myid_col)) then

            loc_row=indxg2l(i_basbas, bb_block_row, 0, 0, CC4S_nprow)
            loc_col=indxg2l(i_basbas, redbb_block_col, 1, 1, CC4S_npcol)

            proj_id(CC4S_lbb_row+loc_row-1, lredbb_col+loc_col-1) = 1.0D0
          end if
        end do

        !Truncate eigenvector matrix according to selected eigenvalues/singular values
        if(.not. allocated(OAF_trafo_matrix_r)) then
          allocate(OAF_trafo_matrix_r(CC4S_lbb_row:CC4S_ubb_row, lredbb_col:uredbb_col))
          OAF_trafo_matrix_r(:,:) = 0.0d0
        end if 

        call pdgemm('N', 'N', CC4S_n_basbas, max_singval, CC4S_n_basbas, 1.0D0, &
               &eigenvectors(:,:), 1, 1, CC4S_bb_desc, proj_id(:,:), 1, 1, &
               &bbxredbb_desc, 0.0D0, OAF_trafo_matrix_r(:,:), &
               &1, 1, bbxredbb_desc)

      end subroutine CC4S_compute_OAF_trafo_matrix_real
!**********************************************************************
      subroutine CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, max_singval, num_singvals, singval_thr)
        implicit none

        !input
        integer, intent(out) :: max_singval
        integer, optional, intent(in) :: num_singvals
        double precision, optional, intent(in) :: singval_thr
        integer, intent(out) :: lredbb_row

        !local
        double complex, dimension(:), allocatable :: work
        double complex, dimension(:), allocatable :: rwork
        integer :: lwork, lrwork
        integer :: info
        double precision, dimension(:), allocatable :: singular_values
        double complex, dimension(:,:), allocatable :: eigenvectors
        !integer :: max_singval
        integer :: uredbb_row
        integer :: lredbb_col, uredbb_col
        integer, dimension(9) :: bbxredbb_desc, states2xredbb_desc
        integer :: redbb_block_col
        integer :: bb_block_row
        double complex, dimension(:,:), allocatable :: proj_id
        integer :: i_basbas
        integer, external :: indxg2l
        integer :: loc_col, loc_row

        !Negate matrix to get eigenvalues from highest to lowest
        OAF_sum_squared_blocks_c(:,:) = -OAF_sum_squared_blocks_c(:,:)

        allocate(work(1))
        lwork = -1

        allocate(singular_values(CC4S_n_basbas))
        allocate(eigenvectors(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))

        if(CC4S_n_tasks .eq. 1) then
          allocate(rwork(3*CC4S_n_basbas-2))
          call zheev('V', 'U', CC4S_n_basbas, OAF_sum_squared_blocks_c(:,:), CC4S_n_basbas, &
                    &singular_values, work, lwork, rwork, info)

          lwork = int(work(1))
          deallocate(work)
          allocate(work(lwork))

          call zheev('V', 'U', CC4S_n_basbas, OAF_sum_squared_blocks_c(:,:), CC4S_n_basbas, &
                    &singular_values, work, lwork, rwork, info)

          deallocate(work, rwork)
          eigenvectors(:,:) = OAF_sum_squared_blocks_c(:,:)
        else
          allocate(rwork(1))
          lrwork=-1
          call pzheev('V', 'U', CC4S_n_basbas, OAF_sum_squared_blocks_c(:,:), 1, 1, CC4S_bb_desc, &
                     &singular_values, eigenvectors(:,:), 1, 1, CC4S_bb_desc, &
                     &work, lwork, rwork, lrwork, info)

          lwork = int(work(1))
          deallocate(work)
          allocate(work(lwork))

          lrwork = int(rwork(1))
          deallocate(rwork)
          allocate(rwork(lrwork))

          call pzheev('V', 'U', CC4S_n_basbas, OAF_sum_squared_blocks_c(:,:), 1, 1, CC4S_bb_desc, &
                     &singular_values, eigenvectors(:,:), 1, 1, CC4S_bb_desc, &
                     &work, lwork, rwork, lrwork, info)

         
          deallocate(work, rwork)
        end if 

        !Compensate for prior negation of matrix
        singular_values(:) = -singular_values(:)


        !Truncate number of singular values (optional)
        if(present(num_singvals)) then
          if(abs(num_singvals) .le. size(singular_values)) then
            max_singval = abs(num_singvals)
          else
            max_singval = size(singular_values)
          end if 
        else if(present(singval_thr)) then
          !Will only enter here, if num_singval has not been provided
          max_singval = max(1, count(singular_values > singval_thr))
        else
          max_singval = size(singular_values)
        end if

        !Set up context and blacs-infrastructure for new reduced/truncated eigenvector matrix
        write(*,*) "MAX_SINGVAL = ", max_singval
        call CC4S_init_blacsOAF_distribution(max_singval, lredbb_row, &
                &uredbb_row, lredbb_col, uredbb_col, &
                &bbxredbb_desc, states2xredbb_desc, redbb_block_col, bb_block_row)

        !Construct identity projection to reduce number of eigenvectors and uphold blacs-distribution
        allocate(proj_id(CC4S_lbb_row:CC4S_ubb_row, lredbb_col:uredbb_col))
        proj_id=(0.0D0, 0.0D0)

        do i_basbas=1,max_singval
          if((mod((i_basbas-1)/bb_block_row, CC4S_nprow) .eq. CC4S_myid_row) &
                  &.and. (mod((i_basbas-1)/redbb_block_col, CC4S_npcol) &
                  &.eq. CC4S_myid_col)) then

            loc_row=indxg2l(i_basbas, bb_block_row, 0, 0, CC4S_nprow)
            loc_col=indxg2l(i_basbas, redbb_block_col, 1, 1, CC4S_npcol)

            proj_id(CC4S_lbb_row+loc_row-1, lredbb_col+loc_col-1) = (1.0D0, 0.0D0)
          end if
        end do

        !Truncate eigenvector matrix according to selected eigenvalues/singular values
        if(.not. allocated(OAF_trafo_matrix_c)) then
          allocate(OAF_trafo_matrix_c(CC4S_lbb_row:CC4S_ubb_row, lredbb_col:uredbb_col))
          OAF_trafo_matrix_c(:,:) = (0.0D0, 0.0D0)
        end if 

        call pzgemm('N', 'N', CC4S_n_basbas, max_singval, CC4S_n_basbas, (1.0D0, 0.0D0), &
               &eigenvectors(:,:), 1, 1, CC4S_bb_desc, proj_id(:,:), 1, 1, &
               &bbxredbb_desc, (0.0D0, 0.0D0), OAF_trafo_matrix_c(:,:), &
               &1, 1, bbxredbb_desc)

      end subroutine CC4S_compute_OAF_trafo_matrix_cmplx
!**********************************************************************
      subroutine U_times_Gamma_real(coulomb_vertex_block, max_singval, OAF_block)

        use CC4S_blacs_environment, only: CC4S_bbxredbb_desc,  CC4S_states2xredbb_desc, &
                                          &CC4S_lredbb_row, CC4S_uredbb_row
        implicit none
        
        !input
        double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates2_col:CC4S_ustates2_col, &
              &CC4S_n_spin), intent(in) :: coulomb_vertex_block
        integer, intent(in) :: max_singval
        double precision, dimension(:,:,:), allocatable, intent(out) :: OAF_block

        !local
        integer :: i_spin

        allocate(OAF_block(CC4S_lredbb_row:CC4S_uredbb_row,CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
        OAF_block(:,:,:) = 0.0D0

        do i_spin = 1,CC4S_n_spin
          call pdgemm('T', 'N', max_singval, CC4S_nuf_states**2, CC4S_n_basbas, 1.0D0, &
                     &OAF_trafo_matrix_r(:,:), 1, 1, CC4S_bbxredbb_desc, &
                     &coulomb_vertex_block(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, 0.0D0, &
                     &OAF_block(:,:,i_spin), 1, 1, CC4S_states2xredbb_desc)
        end do
      end subroutine U_times_Gamma_real
!**********************************************************************
      subroutine U_times_Gamma_cmplx(coulomb_vertex_block, max_singval, OAF_block)

        use CC4S_blacs_environment, only: CC4S_bbxredbb_desc,  CC4S_states2xredbb_desc, &
                                          &CC4S_lredbb_row, CC4S_uredbb_row

        implicit none
        
        !input
        double complex, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates2_col:CC4S_ustates2_col, &
              &CC4S_n_spin), intent(in) :: coulomb_vertex_block
        integer, intent(in) :: max_singval
        double complex, dimension(:,:,:), allocatable, intent(out) :: OAF_block

        !local
        integer :: i_spin

        allocate(OAF_block(CC4S_lredbb_row:CC4S_uredbb_row,CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
        OAF_block(:,:,:) = (0.0D0, 0.0D0)

        do i_spin = 1,CC4S_n_spin
          call pzgemm('C', 'N', max_singval, CC4S_nuf_states**2, CC4S_n_basbas, (1.0D0, 0.0D0), &
                     &OAF_trafo_matrix_c(:,:), 1, 1, CC4S_bbxredbb_desc, &
                     &coulomb_vertex_block(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, (0.0D0, 0.0D0), &
                     &OAF_block(:,:,i_spin), 1, 1, CC4S_states2xredbb_desc)
        end do
      end subroutine U_times_Gamma_cmplx
end module CC4S_OAF_transformation
