module CC4S_shm_KS
  use mpi

  integer :: win_shm_KS
  interface CC4S_share_eigenvec
          module procedure CC4S_share_eigenvec_real
          module procedure CC4S_share_eigenvec_cmplx
  end interface CC4S_share_eigenvec

  double precision, dimension(:,:,:,:), allocatable, target :: KS_eigenv_shm
  double complex, dimension(:,:,:,:), allocatable, target :: KS_eigenv_cmplx_shm

  integer, dimension(:), allocatable :: all_0th_tasks
contains
  subroutine CC4S_setup_inter_comm(comm, mpi_comm_intra, n_tasks_intra, myid_intra, &
                  &mpi_comm_inter, n_tasks_inter, myid_inter)
          implicit none

          !input
          integer, intent(in) :: comm
          integer, intent(out) :: mpi_comm_intra
          integer, intent(out) :: n_tasks_intra
          integer, intent(out) :: myid_intra

          integer, intent(out) :: mpi_comm_inter
          integer, intent(out) :: n_tasks_inter
          integer, intent(out) :: myid_inter

          !local
          integer :: myid_world
          integer :: ierr
          integer, dimension(:), allocatable :: all_KS_shm_tasks

          !if(comm .ne. MPI_COMM_NULL) then
            call MPI_Comm_rank(comm, myid_world, ierr)
  
            !Communicator connecting all tasks on the same node/shared space
            call MPI_Comm_split_type(comm, MPI_COMM_TYPE_SHARED, myid_world, &
                    &MPI_INFO_NULL, mpi_comm_intra, ierr)
            call MPI_Comm_size(mpi_comm_intra, n_tasks_intra, ierr)
            call MPI_Comm_rank(mpi_comm_intra, myid_intra, ierr)
            !Communicator connecting the n-th task of ever node
            call MPI_Comm_split(comm, myid_intra, myid_world, mpi_comm_inter, &
                    &ierr)
            call MPI_Comm_size(mpi_comm_inter, n_tasks_inter, ierr)
            call MPI_Comm_rank(mpi_comm_inter, myid_inter, ierr)
          !end if 

  end subroutine CC4S_setup_inter_comm
!**************************************************************
  subroutine CC4S_setup_shm_KS_per_k(n_tasks_inter, myid_inter, n_tasks_shared, &
                  &myid_shared, myid_world, KS_kpt2local_rank, KS_shm_comm_k)

          use calculation_data, only: CC4S_n_k_points, CC4S_k_eigvec_loc 
          implicit none

          !input
          integer, intent(in) :: n_tasks_inter, myid_inter, n_tasks_shared, &
                  &myid_shared, myid_world
          !integer, dimension(:), allocatable, intent(out) :: KS_glob2shm_map
          !integer, intent(out) :: KS_shm_comm, myid_KS_shm
          integer, dimension(:), allocatable, intent(out) :: KS_kpt2local_rank
          integer, dimension(:), allocatable, intent(out) :: KS_shm_comm_k !Array of communicators

          !local
          integer :: world_group
          integer :: ierr
          integer :: send_size
          integer :: n_tasks
          integer, dimension(:), allocatable :: KSgroup_arr
          integer, dimension(:), allocatable :: all_KS_shm_tasks
          integer :: KSgroup
          integer, dimension(:), allocatable :: disp_arr, revcount
          integer :: disp_val
          integer :: n_tasks_KS_shm
          integer :: curr_task, i_task, i_k_point

          integer, dimension(:), allocatable :: all_0th_tasks_from_all
          integer :: j, i
          integer, dimension(:), allocatable :: KSgroup_k_arr
          integer, dimension(:), allocatable :: all_KS_shm_tasks_k
          integer :: KSgroup_k
          integer, dimension(:,:), allocatable :: KS_kpt2local_rank_temp
          integer :: KS_owners_comm
          integer :: myid_k_local
          integer :: i_map_entry
          integer :: root_task
          integer, dimension(:), allocatable :: all_global_k_ids
          integer :: number_k_owners, number_k_owned
          integer, dimension(:,:), allocatable :: KS_kpt2local_rank_temp_local
          integer, dimension(:), allocatable :: counts_recv, displacements
          integer :: i_owner
          integer :: local_k_counter

          integer :: debug_rank
          integer :: i_arr
          integer, dimension(:), allocatable :: sorted_KSgroup_k_arr
          logical, dimension(:), allocatable :: sorting_mask 

          call MPI_Comm_group(MPI_COMM_WORLD, world_group, ierr)
          call MPI_Comm_size(MPI_COMM_WORLD, n_tasks, ierr)


          !First: From every shared-memory region collect the first task-id/rank
          !in an array and communicate that to all tasks

          allocate(all_0th_tasks_from_all(n_tasks))
          all_0th_tasks_from_all(:) = -1
          if(myid_shared .eq. 0) then
            !If the current rank, is actually the 0th of a node
            call MPI_Allgather(myid_world, 1, MPI_INTEGER, &
                    &all_0th_tasks_from_all(:), 1, MPI_INTEGER, MPI_COMM_WORLD, ierr)
          else
            call MPI_Allgather(-1, 1, MPI_INTEGER, &
                    &all_0th_tasks_from_all(:), 1, MPI_INTEGER, MPI_COMM_WORLD, ierr)
          end if 

          !Build all_0th_tasks from all_0th_tasks_from_all by removing all '-1'-entries
          !num_0th_tasks = count(all_0th_tasks_from_all(:) == -1)

          allocate(all_0th_tasks(n_tasks_inter))
          j=1
          do i=1,n_tasks
            if(all_0th_tasks_from_all(i) /= -1) then
              all_0th_tasks(j) = all_0th_tasks_from_all(i)
              j = j + 1
            end if 
          end do

          !all_KS_shm_tasks_k is an array of shape (CC4S_n_k_points x n_tasks_inter+1)
          !The i-th row contains the global task-ids, which need to have a physical
          !(i.e not shared) copy of KS_eigenvector.
          !This is every first task of a shared memory region (= a node)
          !plus the task which initially allocates the particular KS_eigenvector
          !of the i-th k-point.

          !allocate(all_KS_shm_tasks_k(CC4S_n_k_points, n_tasks_inter+1))
          allocate(all_KS_shm_tasks_k(n_tasks_inter+1))
          all_KS_shm_tasks_k(1:n_tasks_inter) = all_0th_tasks(:)

          allocate(KS_shm_comm_k(CC4S_n_k_points))

          do i_k_point=1,CC4S_n_k_points
            !Create a shared-memory communicator for every k-point
            all_KS_shm_tasks_k(n_tasks_inter+1) = CC4S_k_eigvec_loc(1,i_k_point)  
            !Remove potential duplicates 
            !(i.e if task in CC4S_k_eigvec_loc is also a 0th task)
            call reduce_unique_vec_k(all_KS_shm_tasks_k(:), KSgroup_k_arr)
            !Make a group out of these tasks/ranks
            call MPI_Group_incl(world_group, size(KSgroup_k_arr), KSgroup_k_arr, &
                    &KSgroup_k, ierr)
            !Build a communicator from the group
            call MPI_Comm_create(MPI_COMM_WORLD, KSgroup_k, &
                    &KS_shm_comm_k(i_k_point), ierr)

            !Deallocate after it has been allocated in reduce_unique_vec()
            !if(allocated(KSgroup_k_arr)) then
            !  deallocate(KSgroup_k_arr)
            !end if 
          end do

          !Create array which maps i_k_point -> local rank in 
          !KS_shm_comm_k(i_k_point)-communicator
          allocate(KS_kpt2local_rank(CC4S_n_k_points))
          allocate(KS_kpt2local_rank_temp(2, CC4S_n_k_points))
          !Create communicator 'KS_owners_comm' which contains only tasks
          !which possess the KS_eigenvector of a k-point (reusing KSgroup_k_arr)
          allocate(all_global_k_ids(CC4S_n_k_points))
          all_global_k_ids(:) = CC4S_k_eigvec_loc(1,:)
          call reduce_unique_vec_k(all_global_k_ids(:), KSgroup_k_arr)

          if(myid_world == 0) then
            write(*,*) 'original KSgroup_k_arr = ', KSgroup_k_arr
          end if

          !Try: Sort KSgroup_k_arr from lowest to biggest
          allocate(sorted_KSgroup_k_arr(size(KSgroup_k_arr,1)))
          sorted_KSgroup_k_arr(:) = -1
          allocate(sorting_mask(size(KSgroup_k_arr,1)))
          sorting_mask = .true.
          do i_arr=1, size(KSgroup_k_arr,1)
            sorted_KSgroup_k_arr(i_arr) = minval(KSgroup_k_arr, sorting_mask)
            sorting_mask(minloc(KSgroup_k_arr,sorting_mask)) = .false.
          end do

          KSgroup_k_arr(:) = sorted_KSgroup_k_arr(:)

          if(myid_world == 0) then
            write(*,*) 'Sorted KSgroup_k_arr = ', KSgroup_k_arr
          end if

          call MPI_Group_incl(world_group, size(KSgroup_k_arr), KSgroup_k_arr, &
                  &KSgroup_k, ierr)
          call MPI_Comm_create(MPI_COMM_WORLD, KSgroup_k, &
                  &KS_owners_comm, ierr)

          !Determine smallest rank in CC4S_k_eigvec_loc(1,:) as root for gather and broadcast
          !According to documentation MPI_Comm_create is a special case of MPI_Comm_split
          !which means that it is guaranteed that root_task (the task with the smallest global rank)
          !will also have rank 0 within KS_owners_comm
          root_task = minval(CC4S_k_eigvec_loc(1,:))

!          do i_k_point=1,CC4S_n_k_points
!            write(*,*) "CC4S_k_eigvec_loc: ", CC4S_k_eigvec_loc
!            if(CC4S_k_eigvec_loc(1,i_k_point)==myid_world) then
!              call MPI_Comm_rank(KS_shm_comm_k(i_k_point), myid_k_local, ierr)
!              call MPI_Gather([i_k_point, myid_k_local], 2, MPI_INTEGER, &
!                      &KS_kpt2local_rank_temp(:,:), 2, MPI_INTEGER, &
!                      &0, KS_owners_comm, ierr)
!
!              call MPI_Barrier(KS_owners_comm, ierr)
!            end if 
!          end do

          number_k_owners = size(KSgroup_k_arr)
          do i_task=1,number_k_owners
            if(KSgroup_k_arr(i_task) == myid_world) then
              !Count number of k-points owned by i_task
              number_k_owned = count(CC4S_k_eigvec_loc(1,:)==myid_world)
              allocate(KS_kpt2local_rank_temp_local(2,number_k_owned))
    
              local_k_counter = 0
              do i_k_point=1,CC4S_n_k_points
                if(CC4S_k_eigvec_loc(1,i_k_point)==myid_world) then
                  call MPI_Comm_rank(KS_shm_comm_k(i_k_point), myid_k_local, ierr)
                  local_k_counter = local_k_counter + 1
                  KS_kpt2local_rank_temp_local(1,local_k_counter) = i_k_point
                  KS_kpt2local_rank_temp_local(2,local_k_counter) = myid_k_local
                  write(*,'(A,1X,I0, 1X, A, I0, A, 1X, I0, 1X, I0)') 'On rank', myid_world, &
                             &"KS_kpt2local_rank_temp_local(1:2,", local_k_counter, ") =", &
                             &i_k_point, myid_k_local 
                end if 
              end do

              !Because it can happen that one task_id owns multiple k-points
              !MPI_Gatherv instead of MPI_Gather has to be used.
              !Collect counts_recv and displacements for MPI_Gatherv
              allocate(counts_recv(number_k_owners))
              counts_recv(:) = -1
              allocate(displacements(number_k_owners))
              displacements(:) = 0

              call MPI_Gather(2*number_k_owned, 1, MPI_INTEGER, &
                      &counts_recv(:), 1, MPI_INTEGER, &
                      &0, KS_owners_comm, ierr)
              
              do i_owner=2,number_k_owners
                displacements(i_owner) = &
                  &displacements(i_owner-1) + counts_recv(i_owner-1)
              end do


              call MPI_Gatherv(KS_kpt2local_rank_temp_local(:,:), &
                      &2*number_k_owned, MPI_INTEGER, &
                      &KS_kpt2local_rank_temp(:,:), &
                      &counts_recv(:), displacements(:), MPI_INTEGER, &
                      &0, KS_owners_comm, ierr)
            end if 
          end do


          !debug
          !debug_rank = -1
          !call MPI_Comm_rank(KS_owners_comm, debug_rank, ierr)
          !if(debug_rank == 0) then
          !  write(*,*) '0th owners-comm rank has global rank', myid_world
          !end if

          if(myid_world == root_task) then
            write(*,*) 'On root-rank', myid_world, 'KS_kpt2local_rank_temp(:,:) =', &
                       &KS_kpt2local_rank_temp(:,:)
          end if

          
          if(myid_world == root_task) then
            !Use KS_kpt2local_rank_temp to properly
            !define KS_kpt2local_rank
            do i_map_entry=1,CC4S_n_k_points
              write(*,'(A, 1X, I0, 1X, A, I0, A, 1X, I0)') &
                &'On rank', myid_world, ": KS_kpt2local_rank_temp(1,", &
                &i_map_entry, ") = ", KS_kpt2local_rank_temp(1,i_map_entry)

              KS_kpt2local_rank(KS_kpt2local_rank_temp(1,i_map_entry)) = &
                      &KS_kpt2local_rank_temp(2,i_map_entry)
            end do
            !Now broadcast mapping array to all tasks
            call MPI_Bcast(KS_kpt2local_rank(:), CC4S_n_k_points, MPI_INTEGER, root_task, &
                    &MPI_COMM_WORLD, ierr)
          else
            !All others receive the mapping array
            call MPI_Bcast(KS_kpt2local_rank(:), CC4S_n_k_points, MPI_INTEGER, root_task, &
                    &MPI_COMM_WORLD, ierr)
          end if 




  contains
          subroutine reduce_unique_vec_k(redundant_vec, unique_vec)
                  implicit none
                  !input
                  integer, dimension(:), intent(in) :: redundant_vec
                  integer, dimension(:), allocatable ,intent(out) :: unique_vec
                  !local
                  logical, dimension(size(redundant_vec)) :: is_unique
                  integer :: i_elem, num

                  if(allocated(unique_vec)) then
                    deallocate(unique_vec)
                  end if 

                  is_unique(:)=.false. 
                  do i_elem=1,size(redundant_vec)
                    num=count(redundant_vec==redundant_vec(i_elem))
                    if(num==1) then
                      is_unique(i_elem)=.true.
                    else
                      if(.not. any(redundant_vec==redundant_vec(i_elem) &
                              &.and. is_unique)) then
                        is_unique(i_elem)=.true.
                      end if 
                    end if 
                  end do

                  allocate(unique_vec(count(is_unique)))
                  unique_vec=pack(redundant_vec, is_unique)

          end subroutine reduce_unique_vec_k
  end subroutine CC4S_setup_shm_KS_per_k
!***************************************************************
  subroutine CC4S_share_eigenvec_real(mpi_comm_shared,  n_tasks_inter, myid_inter, n_tasks_shared, &
                  &myid_shared, myid_world, KS_eigenv, KS_shm)

          use calculation_data, only: CC4S_n_basis, CC4S_n_states, CC4S_n_spin, &
                  &CC4S_n_k_points, CC4S_k_eigvec_loc

          use iso_c_binding
          implicit none

          !input
          integer, intent(in) :: mpi_comm_shared, n_tasks_inter, myid_inter, n_tasks_shared
          integer, intent(in) :: myid_shared, myid_world
          double precision, dimension(:,:,:,:), intent(in) :: KS_eigenv
          double precision, dimension(:,:,:,:), pointer, contiguous, intent(out) :: KS_shm 
          !local
          !integer, dimension(:), allocatable :: KS_glob2shm_map
          integer, dimension(:), allocatable :: KS_kpt2local_rank
          !integer :: KS_shm_comm, myid_KS_shm
          integer, dimension(:), allocatable :: KS_shm_comm_k !Array of communicators
          integer :: i_k_point
          integer :: ncount, empty_dp
          integer :: ierr
          !integer :: win_shm_KS
          type(c_ptr) :: cptr_shm_KS
          integer(kind=MPI_ADDRESS_KIND) :: wsize
          integer :: disp_unit
          integer, dimension(4) :: win_shape

          

          KS_shm => null()


          call CC4S_setup_shm_KS_per_k(n_tasks_inter, myid_inter, n_tasks_shared, &
                  &myid_shared, myid_world, KS_kpt2local_rank, KS_shm_comm_k)


          if(myid_shared .eq. 0) then
            allocate(KS_eigenv_shm(CC4S_n_basis, CC4S_n_states, CC4S_n_spin, CC4S_n_k_points))
            KS_eigenv_shm(:,:,:,:) = 0.0d0
          end if 

          do i_k_point=1,CC4S_n_k_points
            if(myid_world .eq. CC4S_k_eigvec_loc(1, i_k_point)) then
              !Sending task(s)
              ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin
              call MPI_Bcast(KS_eigenv(:,:,:,CC4S_k_eigvec_loc(2, i_k_point)), ncount, &
                        &MPI_DOUBLE_PRECISION, KS_kpt2local_rank(i_k_point), &
                        &KS_shm_comm_k(i_k_point), ierr)
              if(myid_shared .eq. 0) then
                KS_eigenv_shm(:,:,:,i_k_point) = KS_eigenv(:,:,:,CC4S_k_eigvec_loc(2,i_k_point))
              end if 
            else if(myid_shared .eq. 0) then
              !Receiving tasks
              ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin
              call MPI_Bcast(KS_eigenv_shm(:,:,:,i_k_point), ncount, MPI_DOUBLE_PRECISION, &
                        &KS_kpt2local_rank(i_k_point), KS_shm_comm_k(i_k_point), ierr)

            end if 
          end do

          !Share the few copies of the eigenvectors across all processes via shared memory
          if(myid_shared .eq. 0) then
            wsize=8*CC4S_n_basis*CC4S_n_states*CC4S_n_spin*CC4S_n_k_points
          else
            wsize=0
          end if  
              
          call MPI_Win_allocate_shared(wsize, 8, MPI_INFO_NULL, mpi_comm_shared, &
                  &cptr_shm_KS, win_shm_KS, ierr)
          call MPI_Win_shared_query(win_shm_KS, 0, wsize, disp_unit, cptr_shm_KS, ierr)
          win_shape=[CC4S_n_basis,CC4S_n_states,CC4S_n_spin,CC4S_n_k_points]
          call c_f_pointer(cptr_shm_KS,KS_shm,win_shape)
          call mpi_win_lock_all(MPI_MODE_NOCHECK,win_shm_KS,ierr)

          if(myid_shared .eq. 0) then
            KS_shm=KS_eigenv_shm(:,:,:,:)
          end if 

          call MPI_Win_sync(win_shm_KS, ierr)
          call MPI_Barrier(mpi_comm_shared, ierr)

  end subroutine CC4S_share_eigenvec_real
!***************************************************************
  subroutine CC4S_share_eigenvec_cmplx(mpi_comm_shared,  n_tasks_inter, myid_inter, n_tasks_shared, &
                  &myid_shared, myid_world, KS_eigenv, KS_shm)

          use calculation_data, only: CC4S_n_basis, CC4S_n_states, CC4S_n_spin, &
                  &CC4S_n_k_points, CC4S_k_eigvec_loc

          use iso_c_binding
          implicit none

          !input
          integer, intent(in) :: mpi_comm_shared, n_tasks_inter, myid_inter, n_tasks_shared
          integer, intent(in) :: myid_shared, myid_world
          double complex, dimension(:,:,:,:), intent(in) :: KS_eigenv
          double complex, dimension(:,:,:,:), pointer, contiguous, intent(out) :: KS_shm 
          !local
          !integer, dimension(:), allocatable :: KS_glob2shm_map
          integer, dimension(:), allocatable :: KS_kpt2local_rank
          !integer :: KS_shm_comm, myid_KS_shm
          integer, dimension(:), allocatable :: KS_shm_comm_k !Array of communicators
          integer :: i_k_point
          integer :: ncount, empty_dp
          integer :: ierr
          !integer :: win_shm_KS
          type(c_ptr) :: cptr_shm_KS
          integer(kind=MPI_ADDRESS_KIND) :: wsize
          integer :: disp_unit
          integer, dimension(4) :: win_shape

          

          KS_shm => null()


          call CC4S_setup_shm_KS_per_k(n_tasks_inter, myid_inter, n_tasks_shared, &
                  &myid_shared, myid_world, KS_kpt2local_rank, KS_shm_comm_k)


          if(myid_shared .eq. 0) then
            allocate(KS_eigenv_cmplx_shm(CC4S_n_basis, CC4S_n_states, CC4S_n_spin, CC4S_n_k_points))
            KS_eigenv_cmplx_shm(:,:,:,:) = (0.0d0, 0.0d0)
          end if 

          do i_k_point=1,CC4S_n_k_points
            if(myid_world .eq. CC4S_k_eigvec_loc(1, i_k_point)) then
              !Sending task(s)
              ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin
              call MPI_Bcast(KS_eigenv(:,:,:,CC4S_k_eigvec_loc(2, i_k_point)), ncount, &
                        &MPI_DOUBLE_COMPLEX, KS_kpt2local_rank(i_k_point), &
                        &KS_shm_comm_k(i_k_point), ierr)
              if(myid_shared .eq. 0) then
                KS_eigenv_cmplx_shm(:,:,:,i_k_point) = KS_eigenv(:,:,:,CC4S_k_eigvec_loc(2,i_k_point))
              end if 
            else if(myid_shared .eq. 0) then
              !Receiving tasks
              ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin
              call MPI_Bcast(KS_eigenv_cmplx_shm(:,:,:,i_k_point), ncount, MPI_DOUBLE_COMPLEX, &
                        &KS_kpt2local_rank(i_k_point), KS_shm_comm_k(i_k_point), ierr)

            end if 
          end do


          !Share the few copies of the eigenvectors across all processes via shared memory
          if(myid_shared .eq. 0) then
            wsize=16*CC4S_n_basis*CC4S_n_states*CC4S_n_spin*CC4S_n_k_points
          else
            wsize=0
          end if  
              
          call MPI_Win_allocate_shared(wsize, 16, MPI_INFO_NULL, mpi_comm_shared, &
                  &cptr_shm_KS, win_shm_KS, ierr)
          call MPI_Win_shared_query(win_shm_KS, 0, wsize, disp_unit, cptr_shm_KS, ierr)
          win_shape=[CC4S_n_basis,CC4S_n_states,CC4S_n_spin,CC4S_n_k_points]
          call c_f_pointer(cptr_shm_KS,KS_shm,win_shape)
          call mpi_win_lock_all(MPI_MODE_NOCHECK,win_shm_KS,ierr)

          if(myid_shared .eq. 0) then
            KS_shm=KS_eigenv_cmplx_shm(:,:,:,:)
          end if 

          call MPI_Win_sync(win_shm_KS, ierr)
          call MPI_Barrier(mpi_comm_shared, ierr)

  end subroutine CC4S_share_eigenvec_cmplx




































!**************************************************************
!  subroutine CC4S_setup_shm_KS(n_tasks_inter, myid_inter, n_tasks_shared, &
!                  &myid_shared, myid_world, KS_glob2shm_map, KS_shm_comm, myid_KS_shm)
!
!          use calculation_data, only: CC4S_n_k_points, CC4S_k_eigvec_loc 
!          implicit none
!
!          !input
!          integer, intent(in) :: n_tasks_inter, myid_inter, n_tasks_shared, &
!                  &myid_shared, myid_world
!          integer, dimension(:), allocatable, intent(out) :: KS_glob2shm_map
!          integer, intent(out) :: KS_shm_comm, myid_KS_shm
!          !local
!          integer :: world_group
!          integer :: ierr
!          integer :: send_size
!          integer :: n_tasks
!          integer, dimension(:), allocatable :: KSgroup_arr
!          integer, dimension(:), allocatable :: all_KS_shm_tasks
!          integer :: KSgroup
!          integer, dimension(:), allocatable :: disp_arr, revcount
!          integer :: disp_val
!          integer :: n_tasks_KS_shm
!          integer :: curr_task, i_task, i_k_point
!
!          call MPI_Comm_group(MPI_COMM_WORLD, world_group, ierr)
!          call MPI_Comm_size(MPI_COMM_WORLD, n_tasks, ierr)
!
!          !all_KS_shm_tasks is an array which saves task-ids of 
!          !tasks which have or need to have a real 
!          !(i.e not shared) copy of KS_eigenvector.
!          !Here this array is filled with the ids of the 0th task
!          !of every node (=of every shared communicator)
!          !and with the ids which originally contain the
!          !KS-eigenvector-array for a given k-point
!          allocate(all_KS_shm_tasks(n_tasks_inter+CC4S_n_k_points))
!          curr_task=0
!          do i_task=1, n_tasks_inter
!            if(curr_task < n_tasks) then
!              all_KS_shm_tasks(i_task)=curr_task
!              curr_task=curr_task+n_tasks_shared
!            else
!              exit
!            end if 
!          end do 
!
!          all_KS_shm_tasks(n_tasks_inter+1:)=CC4S_k_eigvec_loc(1,:)
!         
!          call reduce_unique_vec(all_KS_shm_tasks(:), KSgroup_arr)
!
!          call MPI_Group_incl(world_group, size(KSgroup_arr), KSgroup_arr, &
!                  &KSgroup, ierr) 
!
!          call MPI_Comm_create(MPI_COMM_WORLD, KSgroup, KS_shm_comm, ierr)
!
!          allocate(KS_glob2shm_map(n_tasks))
!          KS_glob2shm_map(:) = -1
!          allocate(revcount(0:n_tasks-1))
!          revcount(:) = 0
!          do i_k_point=1,CC4S_n_k_points
!            revcount(CC4S_k_eigvec_loc(1,:))=1
!          end do
!
!          allocate(disp_arr(0:n_tasks-1))
!          disp_arr(:)=0
!          disp_val=0
!          do i_task=0,n_tasks-1
!            disp_arr(i_task)=disp_val
!            disp_val=disp_val+1
!          end do
!
!          myid_KS_shm=-1
!
!          !Here the communicator is build which contains all tasks
!          !which have or should have a real (i.e not shared) copy of KS_eigenvector
!          if((myid_shared .eq. 0) .or. (any(CC4S_k_eigvec_loc(1,:)==myid_world))) then
!            call MPI_Comm_size(KS_shm_comm, n_tasks_KS_shm, ierr)
!            call MPI_Comm_rank(KS_shm_comm, myid_KS_shm, ierr)
!
!            call MPI_Allgatherv(myid_KS_shm, 1, MPI_INTEGER, KS_glob2shm_map, &
!                    &revcount, disp_arr, MPI_INTEGER, MPI_COMM_WORLD, ierr)
!          else
!            call MPI_Allgatherv(myid_KS_shm, 0, MPI_INTEGER, KS_glob2shm_map, &
!                    &revcount, disp_arr, MPI_INTEGER, MPI_COMM_WORLD, ierr)
!          end if 
!
!
!  contains
!          subroutine reduce_unique_vec(redundant_vec, unique_vec)
!                  implicit none
!                  !input
!                  integer, dimension(:), intent(in) :: redundant_vec
!                  integer, dimension(:), allocatable ,intent(out) :: unique_vec
!                  !local
!                  logical, dimension(size(redundant_vec)) :: is_unique
!                  integer :: i_elem, num
!
!                  is_unique(:)=.false. 
!                  do i_elem=1,size(redundant_vec)
!                    num=count(redundant_vec==redundant_vec(i_elem))
!                    if(num==1) then
!                      is_unique(i_elem)=.true.
!                    else
!                      if(.not. any(redundant_vec==redundant_vec(i_elem) &
!                              &.and. is_unique)) then
!                        is_unique(i_elem)=.true.
!                      end if 
!                    end if 
!                  end do
!
!                  allocate(unique_vec(count(is_unique)))
!                  unique_vec=pack(redundant_vec, is_unique)
!
!          end subroutine reduce_unique_vec
!  end subroutine CC4S_setup_shm_KS
!!***************************************************************
!  subroutine CC4S_share_eigenvec_real_OLD(mpi_comm_shared,  n_tasks_inter, myid_inter, n_tasks_shared, &
!                  &myid_shared, myid_world, KS_eigenv, KS_shm)
!
!          use calculation_data, only: CC4S_n_basis, CC4S_n_states, CC4S_n_spin, &
!                  &CC4S_n_k_points, CC4S_k_eigvec_loc
!
!          use iso_c_binding
!          implicit none
!
!          !input
!          integer, intent(in) :: mpi_comm_shared, n_tasks_inter, myid_inter, n_tasks_shared
!          integer, intent(in) :: myid_shared, myid_world
!          double precision, dimension(:,:,:,:), intent(in) :: KS_eigenv
!          double precision, dimension(:,:,:,:), pointer, contiguous, intent(out) :: KS_shm 
!          !local
!          integer, dimension(:), allocatable :: KS_glob2shm_map
!          integer :: KS_shm_comm, myid_KS_shm
!          integer :: i_k_point
!          integer :: ncount, empty_dp
!          integer :: ierr
!          !integer :: win_shm_KS
!          type(c_ptr) :: cptr_shm_KS
!          integer(kind=MPI_ADDRESS_KIND) :: wsize
!          integer :: disp_unit
!          integer, dimension(4) :: win_shape
!
!          
!
!          KS_shm => null()
!
!
!          call CC4S_setup_shm_KS(n_tasks_inter, myid_inter, n_tasks_shared, &
!                  &myid_shared, myid_world, KS_glob2shm_map, KS_shm_comm, myid_KS_shm)
!
!          if(myid_shared .eq. 0) then
!            allocate(KS_eigenv_shm(CC4S_n_basis, CC4S_n_states, CC4S_n_spin, CC4S_n_k_points))
!          end if 
!
!          !Give every relevant process a real copy of the scf eigenvectors
!          if(myid_KS_shm/=-1) then
!            do i_k_point=1,CC4S_n_k_points
!
!              if(myid_world .eq. CC4S_k_eigvec_loc(1, i_k_point)) then
!                ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin
!
!
!                call MPI_Bcast(KS_eigenv(:,:,:,CC4S_k_eigvec_loc(2, i_k_point)), ncount, &
!                        &MPI_DOUBLE_PRECISION, KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1), &
!                        &KS_shm_comm, ierr)
!                if(myid_shared .eq. 0) then
!                  KS_eigenv_shm(:,:,:,i_k_point) = KS_eigenv(:,:,:,CC4S_k_eigvec_loc(2,i_k_point))
!                end if 
!              elseif(myid_shared .eq. 0) then
!                ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin
!                call MPI_Bcast(KS_eigenv_shm(:,:,:,i_k_point), ncount, MPI_DOUBLE_PRECISION, &
!                        &KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1), KS_shm_comm, ierr)
!              else
!                ncount=0
!                empty_dp=0
!
!                call MPI_Bcast(empty_dp, ncount, MPI_DOUBLE_PRECISION, &
!                        &KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1), KS_shm_comm, ierr)
!              end if 
!            end do
!          end if
!
!          !Share the few copies of the eigenvectors across all processes via shared memory
!          if(myid_shared .eq. 0) then
!            wsize=8*CC4S_n_basis*CC4S_n_states*CC4S_n_spin*CC4S_n_k_points
!          else
!            wsize=0
!          end if  
!              
!          call MPI_Win_allocate_shared(wsize, 8, MPI_INFO_NULL, mpi_comm_shared, &
!                  &cptr_shm_KS, win_shm_KS, ierr)
!          call MPI_Win_shared_query(win_shm_KS, 0, wsize, disp_unit, cptr_shm_KS, ierr)
!          win_shape=[CC4S_n_basis,CC4S_n_states,CC4S_n_spin,CC4S_n_k_points]
!          call c_f_pointer(cptr_shm_KS,KS_shm,win_shape)
!          call mpi_win_lock_all(MPI_MODE_NOCHECK,win_shm_KS,ierr)
!
!          if(myid_shared .eq. 0) then
!            KS_shm=KS_eigenv_shm(:,:,:,:)
!          end if 
!
!          call MPI_Win_sync(win_shm_KS, ierr)
!          call MPI_Barrier(mpi_comm_shared, ierr)
!
!  end subroutine CC4S_share_eigenvec_real_OLD
!!*****************************************
!  subroutine CC4S_share_eigenvec_cmplx_OLD(mpi_comm_shared, n_tasks_inter, myid_inter, n_tasks_shared, &
!                  &myid_shared, myid_world, KS_eigenv, KS_shm)
!
!          use calculation_data, only: CC4S_n_basis, CC4S_n_states, CC4S_n_spin, &
!                  &CC4S_n_k_points, CC4S_k_eigvec_loc
!
!          use iso_c_binding
!          implicit none
!
!          !input
!          integer, intent(in) :: mpi_comm_shared, n_tasks_inter, myid_inter, n_tasks_shared
!          integer, intent(in) :: myid_shared, myid_world
!          double complex, dimension(:,:,:,:), intent(in) :: KS_eigenv
!          double complex, dimension(:,:,:,:), pointer, contiguous, intent(out) :: KS_shm 
!          !local
!          integer, dimension(:), allocatable :: KS_glob2shm_map
!          integer :: KS_shm_comm, myid_KS_shm
!          integer :: i_k_point
!          integer :: ncount
!          double complex :: empty_dp
!          integer :: ierr
!          !integer :: win_shm_KS
!          type(c_ptr) :: cptr_shm_KS
!          integer(kind=MPI_ADDRESS_KIND) :: wsize 
!          integer :: disp_unit
!          integer, dimension(4) :: win_shape
!
!          !debug
!          double complex, dimension(:,:,:,:), allocatable :: KS_eigenvector_dummy
!          KS_shm => null()
!
!          call CC4S_setup_shm_KS(n_tasks_inter, myid_inter, n_tasks_shared, &
!                  &myid_shared, myid_world, KS_glob2shm_map, KS_shm_comm, myid_KS_shm)
!
!          if(myid_shared .eq. 0) then
!            allocate(KS_eigenv_cmplx_shm(CC4S_n_basis, CC4S_n_states, CC4S_n_spin, CC4S_n_k_points))
!          end if 
!        
!          !debug 
!          allocate(KS_eigenvector_dummy(CC4S_n_basis, CC4S_n_states, CC4S_n_spin, CC4S_n_k_points))
!
!          !write(*,*) "On rank", myid_world, "k_eigvec_loc(1,:) is", CC4S_k_eigvec_loc(1,:)
!          !write(*,*) "On rank", myid_world, "k_eigvec_loc(2,:) is", CC4S_k_eigvec_loc(2,:)
!          !write(*,*) "Global rank", myid_world, "has KS_glob2shm_map: ", KS_glob2shm_map(:)
!
!          !Give every relevant process a real copy of the scf eigenvectors
!          if(myid_KS_shm/=-1) then
!            do i_k_point=1,CC4S_n_k_points
!              write(*,*) "Sharing KS_eigenvector for k-point", i_k_point
!              if(myid_world .eq. CC4S_k_eigvec_loc(1, i_k_point)) then
!
!                !write(*,*) "Rank", myid_world, "caught by FIRST condition"
!                write(*,"(A,1X,I0,1X,A,1X,I0,1X,A,1X,I0)") "For k-point", i_k_point, "rank", myid_KS_shm, &
!                        &"does Bcast with root", KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1)
!                ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin!*CC4S_n_k_points
!                call MPI_Bcast(KS_eigenv(:,:,:,CC4S_k_eigvec_loc(2, i_k_point)), ncount, &
!                        &MPI_DOUBLE_COMPLEX, KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1), &
!                        &KS_shm_comm, ierr)
!                if(myid_shared .eq. 0) then
!                  KS_eigenv_cmplx_shm(:,:,:,i_k_point) = KS_eigenv(:,:,:,CC4S_k_eigvec_loc(2,i_k_point))
!                end if
!
!                !write(*,*) "Rank", myid_world, "finalized by FIRST condition" 
!              elseif(myid_shared .eq. 0) then
!
!                !write(*,*) "Rank", myid_world, "caught by SECOND condition"
!                write(*,"(A,1X,I0,1X,A,1X,I0,1X,A,1X,I0)") "For k-point", i_k_point, "rank", myid_KS_shm, &
!                        &"does Bcast with root", KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1)
!                ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin!*CC4S_n_k_points
!                call MPI_Bcast(KS_eigenv_cmplx_shm(:,:,:,i_k_point), ncount, MPI_DOUBLE_COMPLEX, &
!                        &KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1), KS_shm_comm, ierr)
!
!                !write(*,*) "Rank", myid_world, "finalized by SECOND condition"
!              else
!                !write(*,*) "Rank", myid_world, "caught by THIRD condition"
!                write(*,"(A,1X,I0,1X,A,1X,I0,1X,A,1X,I0)") "For k-point", i_k_point, "rank", myid_KS_shm, &
!                        &"does Bcast with root", KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1)
!                !ncount=0
!                !empty_dp=(0.0D0, 0.0D0)
!
!                ncount=CC4S_n_basis*CC4S_n_states*CC4S_n_spin
!
!                !call MPI_Bcast(empty_dp, ncount, MPI_DOUBLE_COMPLEX, &
!                !        &KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1), KS_shm_comm, ierr)
!
!                call MPI_Bcast(KS_eigenvector_dummy(:,:,:,i_k_point), ncount, MPI_DOUBLE_COMPLEX, &
!                        &KS_glob2shm_map(CC4S_k_eigvec_loc(1,i_k_point)+1), KS_shm_comm, ierr)
!
!                !write(*,*) "Rank", myid_world, "finalized by THIRD condition"
!              end if
!
!              call MPI_Barrier(KS_shm_comm, ierr) 
!            end do
!          end if
!
!          write(*,*) "RANK", myid_world, "finished MPI_Bcasting"
!          !Share the few copies of the eigenvectors across all processes via shared memory
!          if(myid_shared .eq. 0) then
!            wsize=16*CC4S_n_basis*CC4S_n_states*CC4S_n_spin*CC4S_n_k_points
!          else
!            wsize=0
!          end if  
!              
!          call MPI_Win_allocate_shared(wsize, 16, MPI_INFO_NULL, mpi_comm_shared, &
!                  &cptr_shm_KS, win_shm_KS, ierr)
!          call MPI_Win_shared_query(win_shm_KS, 0, wsize, disp_unit, cptr_shm_KS, ierr)
!          win_shape=[CC4S_n_basis,CC4S_n_states,CC4S_n_spin,CC4S_n_k_points]
!          call c_f_pointer(cptr_shm_KS,KS_shm,win_shape)
!          call mpi_win_lock_all(MPI_MODE_NOCHECK,win_shm_KS,ierr)
!
!          if(myid_shared .eq. 0) then
!            KS_shm = KS_eigenv_cmplx_shm(:,:,:,:)
!          end if 
!
!          call MPI_Win_sync(win_shm_KS, ierr)
!          call MPI_Barrier(mpi_comm_shared, ierr)
!
!  end subroutine CC4S_share_eigenvec_cmplx_OLD
!******************************************************
  subroutine cleanup_shm_KS(shared_KS_real, shared_KS_cmplx)
          implicit none
         
          !input
          double precision, dimension(:,:,:,:), pointer, optional :: shared_KS_real
          double complex, dimension(:,:,:,:), pointer, optional :: shared_KS_cmplx
          !local
          integer :: ierr
          character(len=200) :: func
          character(len=200) :: err_msg

          func='cleanup_shm_KS'

          call MPI_Win_unlock_all(win_shm_KS, ierr)
          call MPI_Win_free(win_shm_KS, ierr)

          if(present(shared_KS_real) .and. present(shared_KS_cmplx)) then
            shared_KS_real => null()
            shared_KS_cmplx => null()
          elseif(present(shared_KS_real)) then
            shared_KS_real => null()
          elseif(present(shared_KS_cmplx)) then
            shared_KS_cmplx => null()
          else
            err_msg='No shared KS-pointer submitted to be deallocated'
            call error_stop(func, err_msg)
          end if


  end subroutine cleanup_shm_KS
end module CC4S_shm_KS
