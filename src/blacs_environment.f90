module CC4S_blacs_environment
  use mpi
  implicit none

  integer :: CC4S_n_tasks !Number of tasks used for interface
  integer :: CC4S_nprow, CC4S_npcol !Number of rows and columns in process grid 
  integer :: CC4S_comm !Communicator for process grid used by interface
  integer :: CC4S_myid !Rank in comm_cc4s
  logical :: CC4S_member !if .true. this process will be used in the interface
  integer :: CC4S_lbb_row, CC4S_ubb_row !local lower and upper row bound of (n_basbas x n_basbas)-matrix
  integer :: CC4S_lbb_col, CC4S_ubb_col !local lower and upper column bound of (n_basbas x n_basbas)-matrix
  integer :: CC4S_lstates_row, CC4S_ustates_row
  integer :: CC4S_lstates_col, CC4S_ustates_col
  integer :: CC4S_lstates2_row, CC4S_ustates2_row
  integer :: CC4S_lstates2_col, CC4S_ustates2_col
  integer :: CC4S_lbasis2_row, CC4S_ubasis2_row
  integer :: CC4S_lbasis2_col, CC4S_ubasis2_col
  integer :: CC4S_lredbb_row, CC4S_uredbb_row
  integer :: CC4S_lredbb_col, CC4S_uredbb_col
  integer :: CC4S_lbasis_row, CC4S_ubasis_row
  integer :: CC4S_lbasis_col, CC4S_ubasis_col
  integer :: CC4S_myid_row, CC4S_myid_col !process-coordinates in the process grid
  integer :: comm_row, comm_col !Sub-communicators for every row and every column
  integer :: CC4S_loc_bb_row, CC4S_loc_bb_col !Process local sub-matrix dimensions of (n_basbas x n_basbas)-matrix
  integer :: CC4S_loc_states_row, CC4S_loc_states_col !Process local sub-matrix dimensions of (n_states x n_states)-matrix
  integer :: CC4S_loc_basis_row, CC4S_loc_basis_col
  integer :: CC4S_loc_states2_row, CC4S_loc_states2_col !Process local sub-matrix dimension of (n_states**2 x n_states**2)- matrix
    integer :: CC4S_loc_basis2_row, CC4S_loc_basis2_col
  integer, dimension(9) :: CC4S_bb_desc !Array descriptor for (n_basbas x n_basbas)-matrix
  integer, dimension(9) :: CC4S_states2xbb_desc !Array descriptor for (n_states**2 x n_basbas)-matrix
  integer, dimension(9) :: CC4S_basis2xbb_desc !Array descriptor for (n_basis**2 x n_basbas)-matrix
  integer, dimension(9) :: CC4S_bbxstates_desc !Array descriptor for (n_basbas x n_states)-matrix
  integer, dimension(9) :: CC4S_states2xredbb_desc
  integer, dimension(9) :: CC4S_bbxredbb_desc
  integer, dimension(9) :: CC4S_bbxbasis_desc
  integer :: CC4S_blockbb_row, CC4S_blockbb_col !block dimension of n_basbas x n_basbas matrix
  integer, private :: CC4S_blockstates_row, CC4S_blockstates_col !block dimension of n_states x n_states
  integer, private :: CC4S_blockbasis_row, CC4S_blockbasis_col
  integer, private :: CC4S_states2_block_row, CC4S_states2_block_col !block dimension of n_states**2 x n_states**2
  integer, private :: CC4S_basis2_block_row, CC4S_basis2_block_col
  integer, private :: CC4S_cntxt

  !For finite-size correction only
  integer :: CC4S_lpw_row, CC4S_upw_row
  integer :: CC4S_lpw_col, CC4S_upw_col
  integer :: CC4S_loc_pw_row, CC4S_loc_pw_col
  integer, dimension(9) :: CC4S_bbxpws_desc
  integer, dimension(9) :: CC4S_pwsxbb_desc
  integer, dimension(9) :: CC4S_GxGprime_desc
  integer, private :: CC4S_blockpw_row, CC4S_blockpw_col
  integer :: CC4S_max_singval
  integer, dimension(9) :: CC4S_redbbxpws_desc
  integer, dimension(9) :: CC4S_pwsxredbb_desc
  integer :: CC4S_lr_row, CC4S_ur_row
  integer :: CC4S_lr_col, CC4S_ur_col
  integer :: CC4S_loc_r_row, CC4S_loc_r_col
  integer, dimension(9) :: CC4S_bbxr_desc
  integer, dimension(9) :: CC4S_rxr_desc
  integer, private :: CC4S_blockr_row, CC4S_blockr_col

  !"Optimal" quadratic distribution of matrices for Scalapack's SVD
  integer :: CC4S_loc_pw_row_opt, CC4S_loc_pw_col_opt
  integer :: CC4S_loc_bb_row_opt, CC4S_loc_bb_col_opt
  integer, dimension(9) :: CC4S_pwsxbb_opt_desc
  integer, dimension(9) :: CC4S_bb_opt_desc  
  integer, dimension(9) :: CC4S_GxGprime_opt_desc

  !PURPOSE: The algebraic operations of the interface are based on scalapack routines. This module initializes
  !         and governs the process grid and the distribution of the relevant arrays on that grid. 
  !         The distribution of the matrices of this interface is blocked, but non-cyclic so that every member
  !         of the process grid has a contiguous part of the matrix. 
contains 

  subroutine CC4S_init_blacs_distribution(n_states, n_basbas, n_basis, comm, n_pws, n_r)

          use calculation_data, only: CC4S_n_kq_points

          !input
          integer, intent(in) :: n_states !Number of states/maximal band index
          integer, intent(in) :: n_basbas !Number of aux. basis functions (RI-basis)
          integer, intent(in) :: n_basis  !Number of AO basis functions
          integer, intent(in) :: comm !General communicator to be used
          integer, optional, intent(in) :: n_pws !Number of PWs (only for finite-size correction) 
          integer, optional, intent(in) :: n_r   !Number of real-space grid points (only for FS)
          !(e.g MPI_COMM_WORLD)
    
          !local
          integer :: ierr
          integer, dimension(2) :: dims_cart_comm ! Shape-array of process grid 
          logical, dimension(2) :: periodic       ! Periodicity of process grid
          integer :: myid      !Rank in comm

          logical, dimension(2) :: remain !Which cart. dimensions to be conserved for MPI_Cart_sub

          !integer :: cntxt !dummy context variable for blacs grid
          integer :: info

          !blacs function: numroc
          integer, external :: numroc

          integer :: opt_block_size

          !Determine max. available number of tasks
          call MPI_Comm_size(comm, CC4S_n_tasks, ierr)
          call MPI_Comm_rank(comm, myid, ierr)
          !The interface only uses a square processor grid => Final n_tasks only subset of comm
          CC4S_n_tasks=floor(sqrt(real(CC4S_n_tasks)))
          CC4S_n_tasks=CC4S_n_tasks**2

          !Number of rows in process grid (nprow) shall be equal to number of columns in process grid (npcol)
          CC4S_nprow=int(sqrt(real(CC4S_n_tasks)))
          CC4S_npcol=CC4S_n_tasks/CC4S_nprow

          dims_cart_comm=(/CC4S_nprow, CC4S_npcol/)
          periodic=(/.false., .false./)

          call MPI_Cart_create(comm, 2, dims_cart_comm, &
                  &periodic, .false., CC4S_comm, ierr)

          !Determine the process-local block-sizes for the matrices involved
          !The matrices which are distributed over the process grid for this interface have dimensions
          ! - n_basbas x n_basbas (the aux. Coulomb matrix)
          ! - n_basbas x n_states (slice of 3D RI-coefficient tensor)

          !For n_basbas
          if(mod(n_basbas, CC4S_nprow) .eq. 0) then
            CC4S_blockbb_row=n_basbas/CC4S_nprow
          else
            CC4S_blockbb_row=n_basbas/CC4S_nprow+1
          end if 

          if(mod(n_basbas, CC4S_npcol) .eq. 0) then
            CC4S_blockbb_col=n_basbas/CC4S_npcol
          else
            CC4S_blockbb_col=n_basbas/CC4S_npcol+1
          end if 

          !For n_states
          if(mod(n_states, CC4S_nprow) .eq. 0) then
            CC4S_blockstates_row=n_states/CC4S_nprow
          else
            CC4S_blockstates_row=n_states/CC4S_nprow+1
          end if 

          if(mod(n_states, CC4S_npcol) .eq. 0) then
            CC4S_blockstates_col=n_states/CC4S_npcol
          else
            CC4S_blockstates_col=n_states/CC4S_npcol+1
          end if 

          !For n_basis (only necessary for non-local RI-V coefficients)
          if(mod(n_basis, CC4S_nprow) .eq. 0) then
            CC4S_blockbasis_row = n_basis/CC4S_nprow
          else
            CC4S_blockbasis_row = n_basis/CC4S_nprow+1
          end if 

          if(mod(n_basis, CC4S_npcol) .eq. 0) then
            CC4S_blockbasis_col = n_basis/CC4S_npcol
          else
            CC4S_blockbasis_col = n_basis/CC4S_npcol + 1
          end if 

          !For n_pws (only necessary for periodic finite-size correction)
          if(present(n_pws)) then
            if(mod(n_pws, CC4S_nprow) .eq. 0) then
              CC4S_blockpw_row = n_pws/CC4S_nprow
            else
              CC4S_blockpw_row = n_pws/CC4S_nprow+1
            end if

            if(mod(n_pws, CC4S_npcol) .eq. 0) then
              CC4S_blockpw_col = n_pws/CC4S_npcol
            else
              CC4S_blockpw_col = n_pws/CC4S_npcol + 1
            end if
          else
            CC4S_blockpw_row = 0
            CC4S_blockpw_col = 0
          end if

          !For n_r (only necessary for periodic finite-size correction)
          if(present(n_r)) then
            if(mod(n_r, CC4S_nprow) .eq. 0) then
              CC4S_blockr_row = n_r/CC4S_nprow
            else
              CC4S_blockr_row = n_r/CC4S_nprow+1
            end if

            if(mod(n_r, CC4S_npcol) .eq. 0) then
              CC4S_blockr_col = n_r/CC4S_npcol
            else
              CC4S_blockr_col = n_r/CC4S_npcol + 1
            end if
          else
            CC4S_blockr_row = 0
            CC4S_blockr_col = 0
          end if

          CC4S_states2_block_row=CC4S_blockstates_row*n_states
          CC4S_states2_block_col=CC4S_blockstates_col*n_states

          CC4S_basis2_block_row=CC4S_blockbasis_row*n_basis
          CC4S_basis2_block_col=CC4S_blockbasis_col*n_basis

          CC4S_myid=-1
          CC4S_myid_col=-1
          CC4S_myid_row=-1
          if(myid .ge. CC4S_n_tasks) then
            CC4S_member=.false. 

            CC4S_lbb_row=1
            CC4S_ubb_row=1
            CC4S_lbb_col=1
            CC4S_ubb_col=1
            CC4S_lstates_row=1
            CC4S_ustates_row=1
            CC4S_lstates_col=1
            CC4S_ustates_col=1
            CC4S_lbasis_row=1
            CC4S_ubasis_row=1
            CC4S_lbasis_col=1
            CC4S_ubasis_col=1
            CC4S_n_kq_points=0
            !Test: For redistribution via pdgemr2d
            CC4S_bbxbasis_desc(2) = -1
            CC4S_bb_desc(2) = -1

            CC4S_loc_bb_row=1
            CC4S_loc_bb_col=1

            CC4S_lpw_row = 1
            CC4S_upw_row = 1
            CC4S_lpw_col = 1
            CC4S_upw_col = 1

            CC4S_lr_row = 1
            CC4S_ur_row = 1
            CC4S_lr_col = 1
            CC4S_ur_col = 1
          else
            CC4S_member=.true. 

            !Get ranks of reduced communicator
            call MPI_Comm_rank(CC4S_comm, CC4S_myid, ierr)
            !Sub-divide interface-process to assign them to specific process-rows and process-columns of the
            !process grid

            !Every process-row gets a communicator
            remain=(/.true., .false./)
            call MPI_Cart_sub(CC4S_comm, remain, comm_row, ierr)
            !And every process-column gets a communicator
            remain=(/.false., .true./)
            call MPI_Cart_sub(CC4S_comm, remain, comm_col, ierr)

            CC4S_cntxt=CC4S_comm
            call blacs_gridinit(CC4S_cntxt, 'r', CC4S_nprow, CC4S_npcol)
            call blacs_gridinfo(CC4S_cntxt, CC4S_nprow, CC4S_npcol, CC4S_myid_row, CC4S_myid_col)           

            CC4S_loc_bb_row=numroc(n_basbas, CC4S_blockbb_row, CC4S_myid_row, 0, CC4S_nprow)   
            CC4S_loc_bb_col=numroc(n_basbas, CC4S_blockbb_col, CC4S_myid_col, 0, CC4S_npcol)

            CC4S_loc_states_row=numroc(n_states, CC4S_blockstates_row, CC4S_myid_row, 0, CC4S_nprow)
            CC4S_loc_states_col=numroc(n_states, CC4S_blockstates_col, CC4S_myid_col, 0, CC4S_npcol)

            CC4S_loc_basis_row=numroc(n_basis, CC4S_blockbasis_row, CC4S_myid_row, 0, CC4S_nprow)
            CC4S_loc_basis_col=numroc(n_basis, CC4S_blockbasis_col, CC4S_myid_col, 0, CC4S_npcol)

            if(present(n_pws)) then
              CC4S_loc_pw_row = numroc(n_pws, CC4S_blockpw_row, CC4S_myid_row, 0, CC4S_nprow)
              CC4S_loc_pw_col = numroc(n_pws, CC4S_blockpw_col, CC4S_myid_col, 0, CC4S_npcol)

              !The "_opt"-quantities are required to use ScaLapack's SVD for non-square matrices
              opt_block_size = min(128, CC4S_blockbb_row, CC4S_blockpw_row)
              write(*,*) "opt_block_size =", opt_block_size
              CC4S_loc_pw_row_opt = numroc(n_pws, opt_block_size, CC4S_myid_row, 0, CC4S_nprow)
              CC4S_loc_pw_col_opt = numroc(n_pws, opt_block_size, CC4S_myid_col, 0, CC4S_npcol)
 
              CC4S_loc_bb_row_opt = numroc(n_basbas, opt_block_size, CC4S_myid_row, 0, CC4S_nprow)
              CC4S_loc_bb_col_opt = numroc(n_basbas, opt_block_size, CC4S_myid_col, 0, CC4S_npcol)

              call descinit(CC4S_pwsxbb_opt_desc, n_pws, n_basbas, opt_block_size, opt_block_size, 0, 0, &
                      &CC4S_cntxt, max(1,CC4S_loc_pw_row_opt), info)
              call descinit(CC4S_bb_opt_desc, n_basbas, n_basbas, opt_block_size, opt_block_size, 0, 0, &
                      &CC4S_cntxt, max(1,CC4S_loc_bb_row_opt), info) 
              call descinit(CC4S_GxGprime_opt_desc, n_pws, n_pws, opt_block_size, opt_block_size, 0, 0, &
                      &CC4S_cntxt, max(1,CC4S_loc_pw_row_opt), info)
            else
              CC4S_loc_pw_row = 1
              CC4S_loc_pw_col = 1
              CC4S_loc_pw_row_opt = 1
              CC4S_loc_pw_col_opt = 1
              CC4S_loc_bb_row_opt = 1
              CC4S_loc_bb_col_opt = 1
              CC4S_pwsxbb_opt_desc = -1
              CC4S_bb_opt_desc = -1
              CC4S_GxGprime_opt_desc = -1
            end if

            if(present(n_r)) then
              CC4S_loc_r_row = numroc(n_r, CC4S_blockr_row, CC4S_myid_row, 0, CC4S_nprow)
              CC4S_loc_r_col = numroc(n_r, CC4S_blockr_col, CC4S_myid_col, 0, CC4S_npcol)
            else
              CC4S_loc_r_row = 1
              CC4S_loc_r_col = 1
            end if

            CC4S_loc_states2_row=CC4S_loc_states_row*n_states
            CC4S_loc_states2_col=CC4S_loc_states_col*n_states

            CC4S_loc_basis2_row=CC4S_loc_basis_row*n_basis
            CC4S_loc_basis2_col=CC4S_loc_basis_col*n_basis

            !Initialize description array for (n_basbas x n_basbas)-matrix
            call descinit(CC4S_bb_desc, n_basbas, n_basbas, CC4S_blockbb_row, CC4S_blockbb_col, &
                    &0, 0, CC4S_cntxt, max(1, CC4S_loc_bb_row), info)

            call descinit(CC4S_states2xbb_desc, n_basbas, n_states**2, CC4S_blockbb_row, &
                    &CC4S_states2_block_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_bb_row), info)

            call descinit(CC4S_basis2xbb_desc, n_basbas, n_basis**2, CC4S_blockbb_row, &
                    &CC4S_basis2_block_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_bb_row), info)

            call descinit(CC4S_bbxstates_desc, n_basbas, n_states, CC4S_blockbb_row, &
                    &CC4S_blockstates_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_bb_row), info)

            !ONLY for RI-V (non-local RI-coefficients, TODO: maybe switch order of dimensions)
            call descinit(CC4S_bbxbasis_desc, n_basbas, n_basis, CC4S_blockbb_row, &
                    &CC4S_blockbasis_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_bb_row), info)

            if(present(n_pws)) then
              !descriptor-array for RI->PW trafo matrix
              call descinit(CC4S_bbxpws_desc, n_basbas, n_pws, CC4S_blockbb_row, &
                    &CC4S_blockpw_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_bb_row), info)

              !descriptor-array for PW->RI trafo matrix
              call descinit(CC4S_pwsxbb_desc, n_pws, n_basbas, CC4S_blockpw_row, &
                    &CC4S_blockbb_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_pw_row), info)

              !descriptor-array for non-diagonal Coulomb matrix in PWs
              call descinit(CC4S_GxGprime_desc, n_pws, n_pws, CC4S_blockpw_row, &
                    &CC4S_blockpw_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_pw_row), info)
            else
              CC4S_bbxpws_desc = -1
              CC4S_pwsxbb_desc = -1
              CC4S_GxGprime_desc = -1
            end if

            if(present(n_r)) then
              !descriptor-array for RI->PW trafo matrix
              call descinit(CC4S_bbxr_desc, n_basbas, n_r, CC4S_blockbb_row, &
                    &CC4S_blockr_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_bb_row), info)

              !!descriptor-array for PW->RI trafo matrix
              !call descinit(CC4S_pwsxbb_desc, n_pws, n_basbas, CC4S_blockpw_row, &
              !      &CC4S_blockbb_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_pw_row), info)

              !descriptor-array for non-diagonal Coulomb matrix in real-space
              call descinit(CC4S_rxr_desc, n_r, n_r, CC4S_blockr_row, &
                    &CC4S_blockr_col, 0, 0, CC4S_cntxt, max(1, CC4S_loc_r_row), info)
            else
              CC4S_bbxr_desc = -1
              !CC4S_pwsxbb_desc = -1
              !CC4S_GxGprime_desc = -1
            end if


            !Specify lower and upper bounds for local blocks of (n_basbas x n_basbas)-matrix
            CC4S_lbb_row=CC4S_myid_row*CC4S_blockbb_row+1
            CC4S_ubb_row=min(CC4S_myid_row*CC4S_blockbb_row+CC4S_loc_bb_row, n_basbas)

            CC4S_lbb_col=CC4S_myid_col*CC4S_blockbb_col+1
            CC4S_ubb_col=min(CC4S_myid_col*CC4S_blockbb_col+CC4S_loc_bb_col, n_basbas)

            CC4S_lstates_row=CC4S_myid_row*CC4S_blockstates_row+1
            CC4S_ustates_row=min(CC4S_myid_row*CC4S_blockstates_row+CC4S_loc_states_row, n_states)

            CC4S_lstates_col=CC4S_myid_col*CC4S_blockstates_col+1
            CC4S_ustates_col=min(CC4S_myid_col*CC4S_blockstates_col+CC4S_loc_states_col, n_states)

            CC4S_lstates2_row=CC4S_myid_row*CC4S_states2_block_row+1
            CC4S_ustates2_row=min(CC4S_myid_row*CC4S_states2_block_row+&
                    &CC4S_loc_states2_row, n_states**2)

            CC4S_lstates2_col=CC4S_myid_col*CC4S_states2_block_col+1
            CC4S_ustates2_col=min(CC4S_myid_col*CC4S_states2_block_col+&
                    &CC4S_loc_states2_col, n_states**2)

            CC4S_lbasis2_row=CC4S_myid_row*CC4S_basis2_block_row+1
            CC4S_ubasis2_row=min(CC4S_myid_row*CC4S_basis2_block_row+&
                    &CC4S_loc_basis2_row, n_basis**2)

            CC4S_lbasis2_col=CC4S_myid_col*CC4S_basis2_block_col+1
            CC4S_ubasis2_col=min(CC4S_myid_col*CC4S_basis2_block_col+&
                    &CC4S_loc_basis2_col, n_basis**2)

            CC4S_lbasis_row=CC4S_myid_row*CC4S_blockbasis_row+1
            CC4S_ubasis_row=min(CC4S_myid_row*CC4S_blockbasis_row+CC4S_loc_basis_row, n_basis)

            CC4S_lbasis_col=CC4S_myid_col*CC4S_blockbasis_col+1
            CC4S_ubasis_col=min(CC4S_myid_col*CC4S_blockbasis_col+CC4S_loc_basis_col, n_basis)

            if(present(n_pws)) then
              CC4S_lpw_row=CC4S_myid_row*CC4S_blockpw_row+1
              CC4S_upw_row=min(CC4S_myid_row*CC4S_blockpw_row+CC4S_loc_pw_row, n_pws)

              CC4S_lpw_col=CC4S_myid_col*CC4S_blockpw_col+1
              CC4S_upw_col=min(CC4S_myid_col*CC4S_blockpw_col+CC4S_loc_pw_col, n_pws)
            else
              CC4S_lpw_row = 1
              CC4S_upw_row = 1

              CC4S_lpw_col = 1
              CC4S_upw_col = 1
            end if

            if(present(n_r)) then
              CC4S_lr_row=CC4S_myid_row*CC4S_blockr_row+1
              CC4S_ur_row=min(CC4S_myid_row*CC4S_blockr_row+CC4S_loc_r_row, n_r)

              CC4S_lr_col=CC4S_myid_col*CC4S_blockr_col+1
              CC4S_ur_col=min(CC4S_myid_col*CC4S_blockr_col+CC4S_loc_r_col, n_r)
            else
              CC4S_lr_row = 1
              CC4S_ur_row = 1

              CC4S_lr_col = 1
              CC4S_ur_col = 1
            end if
          end if 
  end subroutine CC4S_init_blacs_distribution
!******************************************************************************
  subroutine CC4S_init_blacsOAF_distribution(max_singval, lredbb_row, uredbb_row, lredbb_col, &
                  &uredbb_col, bbxredbb_desc, states2xredbb_desc, &
                  &redbb_block_col, blockbb_row)

          use calculation_data, only: CC4S_nuf_states, CC4S_n_basbas
          implicit none

          !PURPOSE: After the Coulomb vertex has been calculated, it is essential
          !- especially for bigger calculations - to perform a principal component analysis
          !of the vertex. Hence, one performs an eigenvalue decomposition and discards of the 
          !lowest eigenvalues and corresponding eigenvectors. This routine initializes the new
          !matrix block sizes and descriptor arrays resulting from this rank reduction.

          !input
          !integer, intent(in) :: cntxt
          integer, intent(in) :: max_singval
          integer, intent(out) :: lredbb_row, uredbb_row
          integer, intent(out) :: lredbb_col, uredbb_col
          integer, dimension(9), intent(out) :: bbxredbb_desc, states2xredbb_desc
          integer, intent(out) :: redbb_block_col, blockbb_row

          !local
          integer :: nprow, npcol
          integer :: myid_row, myid_col
          integer :: redbb_block_row
          integer :: redbb_row_proc, redbb_col_proc
          integer :: info
          integer, external :: numroc

          !Save the number of singular values accepted for OAF transformation here.
          !NOTE: There may be a better place to store this variable
          CC4S_max_singval = max_singval

          blockbb_row= CC4S_blockbb_row
          call blacs_gridinfo(CC4S_cntxt, nprow, npcol, myid_row, myid_col)

          if(mod(max_singval,nprow) .eq. 0) then
            redbb_block_row=max_singval/nprow
          else
            redbb_block_row=max_singval/nprow + 1
          end if 

          if(mod(max_singval, npcol) .eq. 0) then
            redbb_block_col=max_singval/npcol
          else
            redbb_block_col=max_singval/npcol+1
          end if 


          redbb_row_proc=numroc(max_singval, redbb_block_row, myid_row, 0, nprow)
          redbb_col_proc=numroc(max_singval, redbb_block_col, myid_col, 0, npcol)

          call descinit(states2xredbb_desc, max_singval, CC4S_nuf_states**2, redbb_block_row, &
                  &CC4S_states2_block_col, 0, 0, CC4S_cntxt, max(1, redbb_row_proc), info)

          CC4S_states2xredbb_desc = states2xredbb_desc


          call descinit(bbxredbb_desc, CC4S_n_basbas, max_singval, CC4S_blockbb_row, redbb_block_col, &
                  &0, 0, CC4S_cntxt, max(1, CC4S_loc_bb_row), info)

          CC4S_bbxredbb_desc = bbxredbb_desc

          lredbb_row=myid_row*redbb_block_row+1
          uredbb_row=min(myid_row*redbb_block_row+redbb_row_proc, max_singval)

          CC4S_lredbb_row = lredbb_row
          CC4S_uredbb_row = uredbb_row

          lredbb_col=myid_col*redbb_block_col+1
          uredbb_col=min(myid_col*redbb_block_col+redbb_col_proc, max_singval)

          CC4S_lredbb_col = lredbb_col
          CC4S_uredbb_col = uredbb_col

  end subroutine CC4S_init_blacsOAF_distribution
!**********************************************************
  subroutine CC4S_init_blacsOAF_pws_distribution(max_singval_k)

    use calculation_data, only : CC4S_num_pws
    implicit none

    integer, intent(in) :: max_singval_k

    !local
    integer :: nprow, npcol
    integer :: myid_row, myid_col
    integer :: redbb_block_row, redbb_block_col
    integer :: redbb_row_proc, redbb_col_proc
    integer :: info
    integer, external :: numroc

    call blacs_gridinfo(CC4S_cntxt, nprow, npcol, myid_row, myid_col)

    if(mod(max_singval_k,nprow) .eq. 0) then
      redbb_block_row=max_singval_k/nprow
    else
      redbb_block_row=max_singval_k/nprow + 1
    end if 

    if(mod(max_singval_k, npcol) .eq. 0) then
      redbb_block_col=max_singval_k/npcol
    else
      redbb_block_col=max_singval_k/npcol+1
    end if

    redbb_row_proc=numroc(max_singval_k, redbb_block_row, myid_row, 0, nprow)
    redbb_col_proc=numroc(max_singval_k, redbb_block_col, myid_col, 0, npcol)

    call descinit(CC4S_redbbxpws_desc, max_singval_k, CC4S_num_pws, redbb_block_row, CC4S_blockpw_col, &
                  &0, 0, CC4S_cntxt, max(1, redbb_row_proc), info)

    call descinit(CC4S_pwsxredbb_desc, CC4S_num_pws, max_singval_k, CC4S_blockpw_row, redbb_block_col, &
                  &0, 0, CC4S_cntxt, max(1, redbb_col_proc), info)

  end subroutine CC4S_init_blacsOAF_pws_distribution
!**********************************************************
  subroutine CC4S_get_blacs_data(desc, cntxt, str)
    implicit none

    integer, dimension(9), intent(out) :: desc
    integer, intent(out) :: cntxt    
    character(len=*) :: str

    select case(trim(str))
      case('bbxbasis')
        desc = CC4S_bbxbasis_desc
      case('bbxbb')
        desc = CC4S_bb_desc
    end select

    cntxt = CC4S_cntxt 

  end subroutine CC4S_get_blacs_data
!***********************************************************
  subroutine get_cc4s_context(cntxt)
    implicit none

    integer, intent(out) :: cntxt

    cntxt = CC4S_cntxt
  end subroutine
!***********************************************************
end module CC4S_blacs_environment
