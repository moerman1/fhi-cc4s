module CC4S_M_k_q

        interface CC4S_compute_M_k_q
                module procedure CC4S_compute_M_k_q_real
                module procedure CC4S_compute_M_k_q_cmplx
        end interface CC4S_compute_M_k_q

  double precision, allocatable, dimension(:,:,:) :: co_densities_at_kq_real
  double complex, allocatable, dimension(:,:,:) :: co_densities_at_kq_cmplx
  integer :: co_densities_fh

contains
subroutine open_co_densities_filehandle()
  use CC4S_blacs_environment, only : CC4S_member, CC4S_comm
  use outputfile_names
  use mpi
  implicit none

  integer :: ierr

  if(CC4S_member) then
    call MPI_File_open(CC4S_comm, co_densities_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, co_densities_fh, ierr)
  end if

end subroutine open_co_densities_filehandle

subroutine close_co_densities_filehandle(debug)
  use CC4S_blacs_environment, only : CC4S_member, CC4S_comm
  use outputfile_names
  use CC4S_adapt_data_file_suffix
  use mpi
  implicit none

  logical, intent(in) :: debug

  integer :: ierr

  if(CC4S_member) then
    call MPI_File_close(co_densities_fh, ierr)

    !In the new (release-version) of CC4S, data files carry the suffix ".elements"
    !instead of ".dat" or ".bin".
    !The following function will select the chosen data file 
    !(the .bin- or .dat-file depending on debug-setting)
    !and change the respective suffix to ".elements"
    call adapt_data_file_suffix(debug, co_densities_filename_dat, &
                                &co_densities_filename_bin, MPI_COMM_WORLD)
  end if

end subroutine close_co_densities_filehandle

subroutine CC4S_parallel_write_co_densities_block_to_file(curr_kq, isreal, oaf_reduced_basbas)

  use calculation_data, only: CC4S_n_basbas, CC4S_nuf_states, CC4S_n_spin
  use CC4S_blacs_environment, only : CC4S_lstates2_col, CC4S_lbb_row, CC4S_lredbb_row
  use CC4S_parallel_io, only: CC4S_parallel_write
  implicit none

  integer, intent(in) :: curr_kq
  logical, intent(in) :: isreal
  integer, optional, intent(in) :: oaf_reduced_basbas

  !local
  integer, dimension(3) :: co_densities_sizes
  integer, dimension(3) :: co_densities_subsizes
  integer, dimension(3) :: co_densities_starts
  integer :: internal_oaf_reduced_basbas
  integer :: internal_lower_bound_bb

  if(present(oaf_reduced_basbas)) then
    internal_oaf_reduced_basbas = oaf_reduced_basbas
    internal_lower_bound_bb = CC4S_lredbb_row
  else
    internal_oaf_reduced_basbas = CC4S_n_basbas
    internal_lower_bound_bb = CC4S_lbb_row
  end if

  co_densities_sizes = [internal_oaf_reduced_basbas, CC4S_nuf_states**2, CC4S_n_spin]

  if(isreal) then
    co_densities_subsizes=shape(co_densities_at_kq_real)
  else
    co_densities_subsizes=shape(co_densities_at_kq_cmplx)
  endif

  co_densities_starts=[internal_lower_bound_bb-1, CC4S_lstates2_col-1, 0]

  if(isreal) then
    call CC4S_parallel_write(co_densities_at_kq_real, co_densities_fh, co_densities_sizes, &
                          &co_densities_subsizes, co_densities_starts, internal_oaf_reduced_basbas, curr_kq)
  else
    call CC4S_parallel_write(co_densities_at_kq_cmplx, co_densities_fh, co_densities_sizes, &
                          &co_densities_subsizes, co_densities_starts, internal_oaf_reduced_basbas, curr_kq)
  endif

end subroutine CC4S_parallel_write_co_densities_block_to_file


subroutine CC4S_compute_M_k_q_real(i_spin, i_k_point, i_q_point, &
                &KS_eigenvector_k, KS_eigenvector_q, lvl_k, lvl_q, M_k_q, save_co_densities)

      use calculation_data, only: CC4S_atom2basbas_off, CC4S_sp2n_basbas_sp, CC4S_basbas_atom, &
              &CC4S_n_states, CC4S_n_basis, CC4S_max_n_basis_sp, CC4S_n_atoms, &
              &CC4S_n_species, CC4S_n_basbas, CC4S_species, CC4S_n_spin, CC4S_lb_atom, &
              &CC4S_ub_atom, CC4S_n_low_state

      use CC4S_blacs_environment, only: CC4S_lstates_col, CC4S_ustates_col, &
              &CC4S_lbb_row, CC4S_ubb_row, CC4S_lstates2_col, CC4S_ustates2_col 

      use CC4S_OAF, only: CC4S_reshape_vertex_2D
      implicit none

      !INPUT:
      !        *i_spin             : spin-channel (1 or 2)
      !        *i_k_point,i_q_point: duple of k-points
      !        *KS_eigenvector_k   : HF/KS eigenvector matrix for k-point
      !        *KS_eigenvector_q   : HF/KS eigenvector matrix for q-point
      !        *lvl_k, lvl_q       : RI-LVL-coefficients in k-space 
      !        *lb_atom            : array mapping atom index to first basis function belonging to that atom
      !        *ub_atom            : array mapping atom index to last basis function belonging to that atom
      !        *atom2basbas_off    : array mapping atom index to first aux. basis function of that atom
      !        *sp2n_basbas_sp     : array mapping element to number of aux. basis functions of that atom
      !        *basbas_atom        : array mapping aux.basis function index to atom index
      !        *l/ustate_col       : local lower and upper bound of state-index in matrix columns
      !        *l/ubb_row          : local lower and upper bound of aux. basis function-index in matrix rows
      !        *n_states           : Total number of HF/KS-states (accounting for frozen core approx.)
      !        *n_basis            : Total number of AO basis functions
      !        *max_n_basis_sp     : Max. number of basis functions for an element
      !        *n_lowest_state     : First state to be counted (only relevant for frozen core calculations)
      !        *n_spin             : Number of spin channels
      !        *n_atoms            : Number of atoms
      !        *n_species          : Number of elements
      !        *species            : array mapping atom index to element index

      integer, intent(in) :: i_spin
      integer, intent(in) :: i_k_point, i_q_point
      double precision, dimension(CC4S_n_basis, CC4S_n_states), intent(in) :: &
              &KS_eigenvector_k, KS_eigenvector_q
      double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_max_n_basis_sp, &
              &CC4S_n_low_state:CC4S_n_states,CC4S_n_spin), intent(in) :: &
              &lvl_k, lvl_q
      double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
              &CC4S_n_low_state:CC4S_n_states, CC4S_n_spin), intent(out) :: M_k_q
      logical, optional, intent(in) :: save_co_densities


      !local
      integer :: i_state, i_state_2, i_state_2_glob
      integer :: i_atom, i_species
      integer :: lb, ub, brange
      integer :: lbb_atom, ubb_atom
      integer :: ind
      logical :: internal_save_co_densities

      !debug
      double precision, dimension(:), allocatable :: flat_lvl_k, flat_lvl_q
      character(len=30) :: file_name
      integer :: i

      !write(file_name, '(A10,I1,A4)') "flat_lvl_k", i_k_point, '.txt'
      !allocate(flat_lvl_k(size(lvl_k)))
      !flat_lvl_k = pack(lvl_k,.true.)
      !open(unit=137, file=file_name)
      !do i=1,size(flat_lvl_k)
      !  write(137, '(E22.15)') flat_lvl_k(i)
      !end do
      !close(137)

      do i_state=CC4S_n_low_state, CC4S_n_states
        do i_state_2=CC4S_lstates_col, CC4S_ustates_col

          i_state_2_glob=i_state_2+CC4S_n_low_state-1

          do i_atom=CC4S_basbas_atom(CC4S_lbb_row), CC4S_basbas_atom(CC4S_ubb_row)
            lb=CC4S_lb_atom(i_atom)
            ub=CC4S_ub_atom(i_atom)
            brange=ub-lb+1

            i_species=CC4S_species(i_atom)
            lbb_atom=max(CC4S_lbb_row, CC4S_atom2basbas_off(i_atom)+1)
            ubb_atom=min(CC4S_ubb_row, CC4S_atom2basbas_off(i_atom)+&
                    &CC4S_sp2n_basbas_sp(i_species))

            do ind=1, brange
              M_k_q(lbb_atom:ubb_atom, i_state_2, &
                      &i_state, i_spin) = &
             &M_k_q(lbb_atom:ubb_atom, i_state_2, &
                      &i_state, i_spin) + &
             &KS_eigenvector_q(lb+ind-1, i_state_2_glob)*&
             &lvl_k(lbb_atom:ubb_atom, ind, i_state, i_spin)+ &
             &KS_eigenvector_k(lb+ind-1, i_state)* &
             &lvl_q(lbb_atom:ubb_atom, ind, i_state_2_glob, i_spin)
            end do


          end do


        end do
      end do

      if(present(save_co_densities)) then
        internal_save_co_densities = save_co_densities

      else
        internal_save_co_densities = .false.
      endif

      if(internal_save_co_densities) then
        if(.not. allocated(co_densities_at_kq_real)) then
          allocate(co_densities_at_kq_real(CC4S_lbb_row:CC4S_ubb_row, &
                   &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
        end if
        co_densities_at_kq_real(:,:,:) = 0.0d0

        !Now the module-owned co_densities_at_kq_real should contain
        !M_k_q (the co-densities) in the save format as a Coulomb
        !vertex block before it is written to file.
        call CC4S_reshape_vertex_2D(M_k_q, co_densities_at_kq_real) 
      end if

end subroutine CC4S_compute_M_k_q_real
!***************************************************
subroutine CC4S_compute_M_k_q_cmplx(i_spin, i_k_point, i_q_point, &
                &KS_eigenvector_k, KS_eigenvector_q, lvl_k, lvl_q, M_k_q, save_co_densities)

      use calculation_data, only: CC4S_atom2basbas_off, CC4S_sp2n_basbas_sp, CC4S_basbas_atom, &
              &CC4S_n_states, CC4S_n_basis, CC4S_max_n_basis_sp, CC4S_n_atoms, &
              &CC4S_n_species, CC4S_n_basbas, CC4S_species, CC4S_n_spin, CC4S_lb_atom, &
              &CC4S_ub_atom, CC4S_n_low_state

      use CC4S_blacs_environment, only: CC4S_lstates_col, CC4S_ustates_col, &
              &CC4S_lbb_row, CC4S_ubb_row, CC4S_lstates2_col, CC4S_ustates2_col

      use CC4S_OAF, only: CC4S_reshape_vertex_2D
      implicit none

      integer, intent(in) :: i_spin
      integer, intent(in) :: i_k_point, i_q_point
      double complex, dimension(CC4S_n_basis, CC4S_n_states), intent(in) :: &
              &KS_eigenvector_k, KS_eigenvector_q
      double complex, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_max_n_basis_sp, &
              &CC4S_n_low_state:CC4S_n_states,CC4S_n_spin), intent(in) :: &
              &lvl_k, lvl_q
      double complex, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
              &CC4S_n_low_state:CC4S_n_states, CC4S_n_spin), intent(out) :: M_k_q
      logical, optional, intent(in) :: save_co_densities


      !local
      integer :: i_state, i_state_2, i_state_2_glob
      integer :: i_atom, i_species
      integer :: lb, ub, brange
      integer :: lbb_atom, ubb_atom
      integer :: ind
      logical :: internal_save_co_densities

      do i_state=CC4S_n_low_state, CC4S_n_states
        do i_state_2=CC4S_lstates_col, CC4S_ustates_col

          i_state_2_glob=i_state_2+CC4S_n_low_state-1

          do i_atom=CC4S_basbas_atom(CC4S_lbb_row), CC4S_basbas_atom(CC4S_ubb_row)
            lb=CC4S_lb_atom(i_atom)
            ub=CC4S_ub_atom(i_atom)
            brange=ub-lb+1

            i_species=CC4S_species(i_atom)
            lbb_atom=max(CC4S_lbb_row, CC4S_atom2basbas_off(i_atom)+1)
            ubb_atom=min(CC4S_ubb_row, CC4S_atom2basbas_off(i_atom)+&
                    &CC4S_sp2n_basbas_sp(i_species))

            do ind=1, brange
              M_k_q(lbb_atom:ubb_atom, i_state_2, &
                      &i_state, i_spin) = &
             &M_k_q(lbb_atom:ubb_atom, i_state_2, &
                      &i_state, i_spin) + &
             &conjg(KS_eigenvector_q(lb+ind-1, i_state_2_glob))*&
             &lvl_k(lbb_atom:ubb_atom, ind, i_state, i_spin)+ &
             &KS_eigenvector_k(lb+ind-1, i_state)* &
             &conjg(lvl_q(lbb_atom:ubb_atom, ind, i_state_2_glob, i_spin))
            end do


          end do


        end do
      end do

      if(present(save_co_densities)) then
        internal_save_co_densities = save_co_densities

      else
        internal_save_co_densities = .false.
      endif

      if(internal_save_co_densities) then
        if(.not. allocated(co_densities_at_kq_cmplx)) then
          allocate(co_densities_at_kq_cmplx(CC4S_lbb_row:CC4S_ubb_row, &
                   &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
        end if
        co_densities_at_kq_cmplx(:,:,:) = 0.0d0

        !Now the module-owned co_densities_at_kq_cmplx should contain
        !M_k_q (the co-densities) in the save format as a Coulomb
        !vertex block before it is written to file.
        call CC4S_reshape_vertex_2D(M_k_q, co_densities_at_kq_cmplx) 
      end if

end subroutine CC4S_compute_M_k_q_cmplx
end module CC4S_M_k_q
