module calculation_data
      implicit none

      integer :: CC4S_n_states !Number of SCF-states (including frozen states)
      integer :: CC4S_n_basbas !Number of aux. basis functions
      integer :: CC4S_n_basis  !Number of AO basis functions
      integer :: CC4S_n_atoms  !Number of atoms
      integer :: CC4S_n_species !Number of elements
      integer :: CC4S_n_k_points !Number of k-points
      integer :: CC4S_n_kq_points !Number of k-q-pairs
      integer :: CC4S_n_spin !Number of spin channels
      integer :: CC4S_max_n_basis_sp !Maximum number of AO basis funtions
      !per element
      integer :: CC4S_max_n_basbas_sp !Maximum number of aux. functions
      !per element
      integer :: CC4S_n_cells
      logical :: CC4S_isreal
      integer :: CC4S_n_low_state
      integer :: CC4S_nuf_states !Number of unfrozen states
      logical :: prepare_finite_size_correction !flag if quantities for finite-size correction are requested

      integer, dimension(:), pointer :: CC4S_basbas_atom => null()
      integer, dimension(:), pointer :: CC4S_species => null()
      integer, dimension(:), pointer :: CC4S_sp2n_basis_sp => null()
      integer, dimension(:), pointer :: CC4S_atom2basbas_off => null()
      integer, dimension(:), pointer :: CC4S_sp2n_basbas_sp => null()
      integer, dimension(:), pointer :: CC4S_atom2basis_off => null()
      integer, dimension(:,:), pointer :: CC4S_k_eigvec_loc => null()
      integer, dimension(:,:), pointer :: CC4S_kpq_point_list => null()
      double precision, dimension(:,:), pointer :: CC4S_k_minus_q_vecs => null()
      integer, dimension(:), pointer :: CC4S_lb_atom => null()
      integer, dimension(:), pointer :: CC4S_ub_atom => null()

      double precision :: CC4S_fermi_energy
      integer :: CC4S_num_pws
      double precision, allocatable, dimension(:,:) :: CC4S_reciprocal_lattice_vectors
      integer :: CC4S_num_r

      logical :: compute_left_coul_vertex = .false. !Enable (in driver) until fixed in cc4s

contains
        subroutine initialize_data(n_states, n_basbas, n_basis, n_atoms, n_species, &
                        &n_k_points, n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, &
                        &n_cells, isreal, n_low_state, basbas_atom, species, &
                        &sp2n_basis_sp, atom2basbas_off, sp2n_basbas_sp, &
                        &atom2basis_off, k_eigvec_loc, kpq_point_list, k_minus_q_vecs, lb_atom, ub_atom, &
                        &fermi_energy, num_pws, reciprocal_lattice_vectors, num_r)

                implicit none

                integer, intent(in) :: n_states, n_basbas, n_basis, n_atoms, n_species
                integer, intent(in) :: n_k_points, n_kq_points, n_spin
                integer, intent(in) :: max_n_basis_sp, max_n_basbas_sp
                integer, intent(in) :: n_cells
                logical, intent(in) :: isreal
                integer, intent(in) :: n_low_state
                integer, dimension(n_basbas), target, intent(in) :: basbas_atom
                integer, dimension(n_atoms), target, intent(in) :: species
                integer, dimension(n_species), target, intent(in) :: sp2n_basis_sp
                integer, dimension(n_atoms), target, intent(in) :: atom2basbas_off
                integer, dimension(n_species), target, intent(in) :: sp2n_basbas_sp
                integer, dimension(n_atoms), target, intent(in) :: atom2basis_off
                integer, dimension(2,n_k_points), target, intent(in) :: k_eigvec_loc
                integer, dimension(n_k_points,n_k_points), target, intent(in) :: kpq_point_list
                double precision, dimension(n_k_points, 3), target, intent(in) :: k_minus_q_vecs
                integer, dimension(n_atoms), target, intent(in) :: lb_atom, ub_atom
                double precision, intent(in) :: fermi_energy
                integer, optional, intent(in) :: num_pws
                double precision, dimension(3,3), optional, intent(in) :: reciprocal_lattice_vectors
                integer, optional, intent(in) :: num_r


                CC4S_n_states=n_states
                CC4S_n_basbas=n_basbas
                CC4S_n_basis=n_basis
                CC4S_n_atoms=n_atoms
                CC4S_n_species=n_species
                CC4S_n_k_points=n_k_points
                CC4S_n_kq_points=n_kq_points
                CC4S_n_spin=n_spin
                CC4S_max_n_basis_sp=max_n_basis_sp
                CC4S_max_n_basbas_sp=max_n_basbas_sp
                CC4S_n_cells=n_cells
                CC4S_isreal=isreal
                CC4S_n_low_state=n_low_state

                CC4S_basbas_atom => basbas_atom
                CC4S_species => species
                CC4S_sp2n_basis_sp => sp2n_basis_sp
                CC4S_atom2basbas_off => atom2basbas_off
                CC4S_sp2n_basbas_sp => sp2n_basbas_sp
                CC4S_atom2basis_off => atom2basis_off
                CC4S_k_eigvec_loc => k_eigvec_loc
                CC4S_kpq_point_list => kpq_point_list
                CC4S_k_minus_q_vecs => k_minus_q_vecs
                CC4S_lb_atom => lb_atom
                CC4S_ub_atom => ub_atom

                CC4S_nuf_states = CC4S_n_states-CC4S_n_low_state+1

                CC4S_fermi_energy = fermi_energy
                
                if(present(num_pws)) then
                  CC4S_num_pws = num_pws
                else
                  CC4S_num_pws = 0
                end if

                if(present(reciprocal_lattice_vectors)) then
                  allocate(CC4S_reciprocal_lattice_vectors(3,3))
                  CC4S_reciprocal_lattice_vectors(:,:) = reciprocal_lattice_vectors(:,:)
                end if

                if(present(num_r)) then
                  CC4S_num_r = num_r
                else
                  CC4S_num_r = 0
                end if

        end subroutine initialize_data
end module calculation_data
