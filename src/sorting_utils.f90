module sorting_utils
  implicit none

contains
  subroutine order_1d_array_real(array, ordered_array, ordered_index)
    implicit none

    !input
    double precision, dimension(:), intent(in) :: array
    double precision, dimension(:), intent(out) :: ordered_array
    integer, dimension(:), intent(out) :: ordered_index

    !local
    integer :: array_size
    integer :: i
    integer :: current_minval_position
    double precision :: temp_val_dp
    integer :: temp_val_int
    !Perform most elementary (and most inefficient) selection sort
    !Additional algorithms may be added in the future, but as the
    !array size (i.e the number of scf-eigenvalues) is expected to 
    !be on the order of O(1000), this simple algorithm should do just fine

    array_size = size(array)
    ordered_array(:) = array(:)
    ordered_index = (/(i, i=1,array_size, 1)/)

    do i=1,array_size
      !Find index of smallest value in unordered part of array
      current_minval_position = i + minloc(ordered_array(i:), dim=1) - 1
      !write(*,*) "Smallest unordered value: ", ordered_array(current_minval_position)
      !Swap that element with the element at i-th position
      temp_val_dp = ordered_array(i)
      ordered_array(i) = ordered_array(current_minval_position)
      ordered_array(current_minval_position) = temp_val_dp
      !Swap corresponding elements in index_array
      temp_val_int = ordered_index(i)
      ordered_index(i) = ordered_index(current_minval_position)
      ordered_index(current_minval_position) = temp_val_int
      !write(*,*) 'Swapped index ', i, 'with ', current_minval_position
    end do

  end subroutine order_1d_array_real

end module sorting_utils

!program test_sort
!  use sorting_utils
!  implicit none
!
!  double precision, dimension(48) :: energies_array
!  double precision, dimension(48) :: ordered_energies_array
!  integer, dimension(48) :: ordered_indices
!
!  energies_array = [-2.5657435322634621D0, -0.6888885966979743D0, 0.4468376841716761D0, &
!          &0.5106442560285324D0, 0.5320223042185676D0, 0.5320223044350390D0, &
!          &0.6833449329973007D0, 0.6978890464357774D0, 0.6978890465105018D0, &
!          &1.0484194519228447D0, 1.0484194520557859D0, 1.3004527549048532D0, &
!          &1.3004527549200047D0, 1.3025995090644071D0, 2.4251625811135660D0, &
!          &3.9144314359117400D0, -2.5636504679192376D0, -0.6133759311285877D0, &
!          &0.0262591456322636D0, 0.3748416551948821D0, 0.4625135195521633D0, &
!          &0.4625135196987163D0, 0.6219251678193061D0, 0.6219251678472477D0, &
!          &0.9394250277632614D0, 1.0838380964436889D0, 1.0838380965068362D0, &
!          &1.7143278204648673D0, 2.2494523223878038D0, 2.2494523224221088D0, &
!          &2.6259279067152410D0, 2.9529968165267610D0, -2.5636504679192385D0, &
!          &-0.6133759311285880D0, 0.0262591456322628D0, 0.3748416551948943D0, &
!          &0.4625135195521553D0, 0.4625135196987171D0, 0.6219251678193037D0, &
!          &0.6219251678472464D0, 0.9394250277633664D0, 1.0838380964436944D0, &
!          &1.0838380965068417D0, 1.7143278204648633D0, 2.2494523223878184D0, &
!          &2.2494523224220968D0, 2.6259279067152397D0, 2.9529968165270768D0]
!
!  !write(*,*) energies_array
!  call order_1d_array_real(energies_array, ordered_energies_array, ordered_indices)
!  write(*,'(F22.16)') ordered_energies_array
!end program test_sort
