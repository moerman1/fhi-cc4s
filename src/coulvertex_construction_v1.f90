module CC4S_coul_vertex
use mpi
contains
        subroutine CC4S_coulvertex_construction(aomo_lvl_r, aomo_lvl_c, &
                        &scf_eigvecs_real, scf_eigvecs_cmplx, &
                        &sqrt_coulmat_real, sqrt_coulmat_cmplx, CoulombVertex_fh, &
                        &singval_thr, singval_num)
          use calculation_data, only: CC4S_kpq_point_list, CC4S_isreal, &
                  &CC4S_nuf_states, CC4S_n_spin, CC4S_n_k_points, CC4S_n_kq_points, &
                  &CC4S_n_basbas
          use CC4S_blacs_environment, only: CC4S_lbb_row,CC4S_ubb_row, CC4S_comm, &
                  &CC4S_lstates_col,CC4S_ustates_col, CC4S_lstates2_col, CC4S_ustates2_col, CC4S_myid, &
                  &CC4S_lbb_col 
          use CC4S_VtimesM, only: CC4S_v_times_M_k_q
          use CC4S_OAF, only: CC4S_prune_vertex, CC4S_reshape_vertex_2D
          use CC4S_M_k_q, only: CC4S_compute_M_k_q
          use CC4S_parallel_io, only: CC4S_parallel_write, CC4S_debug_write_4d_real, &
                  &CC4S_debug_write_2d_real, CC4S_debug_write_3d_real
          use CC4S_bin2format
          implicit none

          !input
          double precision, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_r
          double complex, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_c
          double precision, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_real
          double complex, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_cmplx
          double precision, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_real
          double complex, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_cmplx
          integer, intent(in) :: CoulombVertex_fh
          double precision, optional :: singval_thr
          integer, optional :: singval_num

          !local
          integer :: curr_pair
          integer :: i_q_point, i_kq_pair, i_k_point, i_spin
          double precision, dimension(:,:,:,:), target, allocatable :: momo_lvl_r
          double precision, dimension(:,:,:,:), pointer :: momo_lvl_rptr => null()
          double complex, dimension(:,:,:,:), target, allocatable :: momo_lvl_c
          double complex, dimension(:,:,:,:), pointer :: momo_lvl_cptr => null()
          double precision, dimension(:,:,:), pointer :: sr_momo_lvl_rptr => null()
          double complex, dimension(:,:,:), pointer :: sr_momo_lvl_cptr => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_k => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_k => null() 
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_q => null()
          double precision, dimension(:,:), pointer :: KS_rptr_k => null()
          double precision, dimension(:,:), pointer :: KS_rptr_q => null()
          double complex, dimension(:,:), pointer :: KS_cptr_k => null()
          double complex, dimension(:,:), pointer :: KS_cptr_q => null()
          double precision, dimension(:,:), pointer :: sqrt_cm_rptr => null()
          double complex, dimension(:,:), pointer :: sqrt_cm_cptr => null()
          double precision, dimension(:,:,:), target, allocatable :: vertex_reshaped_r
          double precision, dimension(:,:,:), pointer :: vertex_reshaped_rptr => null()
          double complex, dimension(:,:,:), target, allocatable :: vertex_reshaped_c
          double complex, dimension(:,:,:), pointer :: vertex_reshaped_cptr => null()
          double precision, dimension(:,:,:), allocatable :: vertex_rOAF
          double complex, dimension(:,:,:), allocatable :: vertex_cOAF
          double precision, dimension(:,:,:,:), pointer :: coulomb_vertex_rptr => null()
          double complex, dimension(:,:,:,:), pointer :: coulomb_vertex_cptr => null()

          integer :: max_singval
          integer :: lredbb_row
          integer, dimension(3) :: OAF_sizes, OAF_subsizes, OAF_starts

          !Just for debugging, remove afterwards
          integer :: momo_size
          integer, dimension(4) :: momo_sizes, momo_subsizes, momo_starts
          integer :: reshape_vertex_size
          integer, dimension(3) :: reshape_vertex_sizes, reshape_vertex_subsizes, reshape_vertex_starts 
          integer :: cm_size
          integer, dimension(2) :: cm_sizes, cm_subsizes, cm_starts
          
          character(len=1) :: taskIdString
          logical :: skip_pruning

          if(CC4S_isreal) then
            allocate(momo_lvl_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                &CC4S_nuf_states, CC4S_n_spin))
            momo_lvl_r(:,:,:,:) = 0.0D0
            momo_lvl_rptr => momo_lvl_r(:,:,:,:)
          else
            allocate(momo_lvl_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                &CC4S_nuf_states, CC4S_n_spin))
            momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
            momo_lvl_cptr => momo_lvl_c(:,:,:,:)
          end if

          curr_pair=0

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p
              write(*,*) "(From CC4S_coulvertex_construction) RANK ", CC4S_myid, &
                      &" : Construction vertex for k-/q-/k-q-point", &
                      &i_q_point, i_k_point, i_kq_pair

              if(CC4S_isreal) then
                momo_lvl_r(:,:,:,:) = 0.0D0
                aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k_point)
                aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q_point)
                sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
              else
                momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
                aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k_point)
                aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q_point)
                sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
              end if 

              do i_spin=1,CC4S_n_spin
                if(CC4S_isreal) then
                  KS_rptr_k => scf_eigvecs_real(:,:,i_spin,i_k_point)
                  KS_rptr_q => scf_eigvecs_real(:,:,i_spin,i_q_point)
                  call CC4S_compute_M_k_q(i_spin, i_k_point, i_q_point, &
                          &KS_rptr_k, KS_rptr_q, aomo_lvl_rptr_k, aomo_lvl_rptr_q, &
                          &momo_lvl_r)

                  momo_size=CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
                  momo_sizes=[CC4S_n_basbas, CC4S_nuf_states, CC4S_nuf_states, CC4S_n_spin]
                  momo_subsizes=shape(momo_lvl_r)
                  momo_starts=[CC4S_lbb_row-1, CC4S_lstates_col-1, 0, 0]
                
                  !Convert task-ID to string to put in file name
                  !write(taskIdString,'(I1)') CC4S_myid

                  if (i_k_point == 1 .and. i_q_point == 1) then
                    call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
                        &momo_sizes, momo_subsizes, momo_starts, 'momo_lvl_r_1_1')
                  else if (i_k_point == 1 .and. i_q_point == 2) then
                    call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
                        &momo_sizes, momo_subsizes, momo_starts, 'momo_lvl_r_1_2')
                  else if (i_k_point == 2 .and. i_q_point == 1) then
                    call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
                        &momo_sizes, momo_subsizes, momo_starts, 'momo_lvl_r_2_1')
                  else if (i_k_point == 2 .and. i_q_point == 2) then
                    call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
                        &momo_sizes, momo_subsizes, momo_starts, 'momo_lvl_r_2_2')
                  else
                    write(*,*) "UNEXPECTED ITERATION", i_q_point, i_kq_pair
                  end if
!                  write(*,*) "(From momo_lvl_r) Shape of momo_lvl_r: ", shape(momo_lvl_r)
!                  write(*,*) "(From CC4S_coulvertex_construction) Writing momo_lvl to file"
 
!                  write(*,*) "(From CC4S_coulvertex_construction) Successfully written to file"
                  sr_momo_lvl_rptr => momo_lvl_r(:,:,:,i_spin)
                 
!                  cm_size = CC4S_n_basbas * CC4S_n_basbas
!                  cm_sizes = [CC4S_n_basbas, CC4S_n_basbas]
!                  cm_subsizes = shape(sqrt_cm_rptr)
!                  cm_starts = [CC4S_lbb_row-1, CC4S_lbb_col-1]
!                  if (i_q_point == 1 .and. i_kq_pair == 1) then
!                    call CC4S_debug_write_2d_real(CC4S_comm, sqrt_cm_rptr, cm_size, &
!                        &cm_sizes, cm_subsizes, cm_starts, 'CM_k_q_1_1')
!                  else if (i_q_point == 1 .and. i_kq_pair == 2) then
!                    call CC4S_debug_write_2d_real(CC4S_comm, sqrt_cm_rptr, cm_size, &
!                            &cm_sizes, cm_subsizes, cm_starts, 'CM_k_q_1_2')
!                  else if (i_q_point == 2 .and. i_kq_pair == 1) then
!                    call CC4S_debug_write_2d_real(CC4S_comm, sqrt_cm_rptr, cm_size, &
!                            &cm_sizes, cm_subsizes, cm_starts, 'CM_k_q_2_1')
!                  else if (i_q_point == 2 .and. i_kq_pair == 2) then
!                    call CC4S_debug_write_2d_real(CC4S_comm, sqrt_cm_rptr, cm_size, &
!                            &cm_sizes, cm_subsizes, cm_starts, 'CM_k_q_2_2')
!                  else
!                    write(*,*) "UNEXPECTED ITERATION", i_q_point, i_kq_pair
!                  end if



                  call CC4S_v_times_M_k_q(sqrt_cm_rptr, sr_momo_lvl_rptr)
                else
                  KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin,i_k_point)
                  KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin,i_q_point)
                  call CC4S_compute_M_k_q(i_spin, i_k_point, i_q_point, &
                          &KS_cptr_k, KS_cptr_q, aomo_lvl_cptr_k, aomo_lvl_cptr_q, &
                          &momo_lvl_c)

                  sr_momo_lvl_cptr => momo_lvl_c(:,:,:,i_spin)

                  call CC4S_v_times_M_k_q(sqrt_cm_cptr, sr_momo_lvl_cptr)
                end if                 
              end do !i_spin
              if(CC4S_isreal) then
                momo_size=CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
                momo_sizes=[CC4S_n_basbas, CC4S_nuf_states, CC4S_nuf_states, CC4S_n_spin]
                momo_subsizes=shape(momo_lvl_r)
                momo_starts=[CC4S_lbb_row-1, CC4S_lstates_col-1, 0, 0]

                !Divide the vertex by sqrt(N_k)
                !momo_lvl_r = momo_lvl_r/sqrt(real(CC4S_n_k_points))

                if (i_k_point == 1 .and. i_q_point == 1) then 
                  call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_1_1')
                else if (i_k_point == 1 .and. i_q_point == 2) then
                  call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_1_2')
                else if (i_k_point == 2 .and. i_q_point == 1) then
                  call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_2_1')
                else if (i_k_point == 2 .and. i_q_point == 2) then
                  call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_2_2')
                else
                  write(*,*) "UNEXPECTED ITERATION", i_q_point, i_kq_pair
                end if 


                
                coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
                allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_r(:,:,:) = 0.0D0
                vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

                call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

                !debug: check if the mere reshaping of the vertex, changes the result wrt. cc4s
                !the reshape applies to the two n-states-dimensions merging them to a superindex

                !YES IT DOES; Reason: Before the reshaping happens, the two state-dimensions
                !are swtiched/transposed. In blocks where k /= q, this leads to a swapping of parts of 
                !a given block.

                !Proposed solution: Re-define M_k_q (momo_lvl_r/momo_lvl_c)
                !to have the dimensions
                !CC4S_lbb_row:CC4S_ubb_row, CC4S_nuf_states, CC4S_lstates_col:CC4S_ustates_col, CC4S_n_spin
                !instead of 
                !CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, CC4S_nuf_states, CC4S_n_spin
                !This should merely mean, changing the dimensions of momo_lvl_r/c of the allocation in this
                !subroutine and in those using this quantity:
                ! - CC4S_compute_M_k_q()
                ! - CC4S_v_times_M_k_q()
                ! - CC4S_reshape_vertex_2D()
                ! - the various debug write-to-file routines

                reshape_vertex_size = CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
                reshape_vertex_sizes = [CC4S_n_basbas, CC4S_nuf_states**2, CC4S_n_spin]
                reshape_vertex_subsizes = shape(vertex_reshaped_r)
                reshape_vertex_starts = [CC4S_lbb_row-1, CC4S_lstates2_col-1, 0]

                if (i_k_point == 1 .and. i_q_point == 1) then 
                  call CC4S_debug_write_3d_real(CC4S_comm, vertex_reshaped_r, reshape_vertex_size, &
                          &reshape_vertex_sizes, reshape_vertex_subsizes, reshape_vertex_starts, 'reshaped_vertex_r_1_1')
                else if (i_k_point == 1 .and. i_q_point == 2) then
                  call CC4S_debug_write_3d_real(CC4S_comm, vertex_reshaped_r, reshape_vertex_size, &
                          &reshape_vertex_sizes, reshape_vertex_subsizes, reshape_vertex_starts, 'reshaped_vertex_r_1_2')
                else if (i_k_point == 2 .and. i_q_point == 1) then
                  call CC4S_debug_write_3d_real(CC4S_comm, vertex_reshaped_r, reshape_vertex_size, &
                          &reshape_vertex_sizes, reshape_vertex_subsizes, reshape_vertex_starts, 'reshaped_vertex_r_2_1')
                else if (i_k_point == 2 .and. i_q_point == 2) then
                  call CC4S_debug_write_3d_real(CC4S_comm, vertex_reshaped_r, reshape_vertex_size, &
                          &reshape_vertex_sizes, reshape_vertex_subsizes, reshape_vertex_starts, 'reshaped_vertex_r_2_2')
                else
                  write(*,*) "UNEXPECTED ITERATION", i_q_point, i_kq_pair
                end if 



                call CC4S_prune_vertex(vertex_reshaped_rptr, vertex_rOAF, max_singval, &
                        &lredbb_row, singval_thr, singval_num)

                if(allocated(vertex_reshaped_r)) then
                  deallocate(vertex_reshaped_r)
                end if 



                OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
                OAF_subsizes=shape(vertex_rOAF)
                OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]
                !write(*,*) "(From CC4S_coulvertex_construction) CC4S-rank ", CC4S_myid, "entering CC4S_parallel_write"
                if(i_k_point == 1 .and. i_q_point == 1) then
                  call CC4S_debug_write_3d_real(CC4S_comm, vertex_rOAF, &
                        &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin, &
                        &OAF_sizes, OAF_subsizes, OAF_starts, 'OAF_1_1')
                else if(i_k_point == 1 .and. i_q_point == 2) then
                  call CC4S_debug_write_3d_real(CC4S_comm, vertex_rOAF, &
                        &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin, &
                        &OAF_sizes, OAF_subsizes, OAF_starts, 'OAF_1_2')
                else if(i_k_point == 2 .and. i_q_point == 1) then
                  call CC4S_debug_write_3d_real(CC4S_comm, vertex_rOAF, &
                        &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin, &
                        &OAF_sizes, OAF_subsizes, OAF_starts, 'OAF_2_1')
                else if(i_k_point == 2 .and. i_q_point == 2) then
                  call CC4S_debug_write_3d_real(CC4S_comm, vertex_rOAF, &
                        &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin, &
                        &OAF_sizes, OAF_subsizes, OAF_starts, 'OAF_2_2')
                else
                  write(*,*) "UNEXPECTED ITERATION", i_q_point, i_kq_pair
                end if 

                call CC4S_parallel_write(vertex_rOAF, CoulombVertex_fh, OAF_sizes, &
                        &OAF_subsizes, OAF_starts, max_singval, curr_pair)

                !Test:Deallocate the OAF-Coulomb vertex already here, right after
                !it has been written to file
                deallocate(vertex_rOAF)

                !call bin2format('CoulombVertex.dat', 'CV_formatted.dat', &
                !        &MPI_COMM_WORLD, &
                !        &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin)
              else
                coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
                allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
                vertex_reshaped_cptr => vertex_reshaped_c(:,:,:) 

                call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

                call CC4S_prune_vertex(vertex_reshaped_cptr, vertex_cOAF, max_singval, &
                        &lredbb_row, singval_thr, singval_num) 

                if(allocated(vertex_reshaped_c)) then
                  deallocate(vertex_reshaped_c)
                end if 

                OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin]
                OAF_subsizes=shape(vertex_cOAF)
                OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]
                call CC4S_parallel_write(vertex_cOAF, CoulombVertex_fh, OAF_sizes, &
                        &OAF_subsizes, OAF_starts, max_singval, curr_pair)
                !call bin2format('CoulombVertex.dat', 'CV_formatted.dat', &
                !        &MPI_COMM_WORLD, &
                !        &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin)
              end if 

            curr_pair = curr_pair + 1 
            end do !i_kq_pair
          end do !i_q_point

         call bin2format('CoulombVertex.dat', 'CV_formatted.dat', &
           &MPI_COMM_WORLD, &
           &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin*CC4S_n_k_points*CC4S_n_k_points)

        end subroutine CC4S_coulvertex_construction
end module CC4S_coul_vertex
