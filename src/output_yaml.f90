module CC4S_yaml_output
  use outputfile_names
  use calculation_data, only: CC4S_n_states, CC4S_n_basbas, CC4S_n_k_points, &
          &CC4S_fermi_energy, CC4S_isreal, CC4S_n_low_state, CC4S_nuf_states, &
          &CC4S_num_pws, CC4S_n_kq_points, CC4S_reciprocal_lattice_vectors
  !use CC4S_blacs_environment, only : CC4S_max_singval
  use sorting_utils
  implicit none

contains
  subroutine output_yaml_files(scf_energies, momentum_triples, sparse_OAF_dim, n_spins, n_bb, is_binary, is_hf, do_fs, do_oaf, left_momentum_triples)
    implicit none
  
    double precision, dimension(:,:,:), intent(in) :: scf_energies
    integer, dimension(:,:), intent(in) :: momentum_triples
    integer, dimension(:), intent(in) :: sparse_OAF_dim
    integer, intent(in) :: n_spins
    integer, intent(in) :: n_bb
    logical, intent(in) :: is_binary
    logical, intent(in) :: is_hf
    logical, intent(in) :: do_fs
    logical, intent(in) :: do_oaf
    integer, optional, dimension(:,:), intent(in) :: left_momentum_triples

    !local
    double precision, dimension(:), allocatable :: flat_scf_energies
    double precision, dimension(:), allocatable :: ordered_energies
    integer, dimension(:), allocatable :: ordered_indices

    allocate(flat_scf_energies(size(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:))))
    flat_scf_energies = pack(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:), .true.)

    allocate(ordered_energies(size(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:))))
    ordered_energies = 0.0D0

    allocate(ordered_indices(size(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:))))
    ordered_indices = 0

    !Generate yaml-files which are always required
    call order_1d_array_real(flat_scf_energies, ordered_energies, ordered_indices)
    call CC4S_out_eigenenergies_yaml("100", &
                                    &1.0D0, &
                                    &CC4S_fermi_energy, &
                                    &ordered_energies, &
                                    &CC4S_n_k_points, &
                                    &n_spins)



    call CC4S_out_coulvertex_yaml("100", size(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:)), n_spins, &
            &sum(sparse_OAF_dim), momentum_triples, 1.0D0, is_binary, CC4S_isreal)

    if(present(left_momentum_triples)) then
      !If at this point, a list of left momentum-triples (k1,k2,-(k2-k1)) has been submitted,
      !generate yaml-files for LeftCoulombVertex as well.
      call CC4S_out_left_coulvertex_yaml("100", size(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:)), n_spins, &
              &sum(sparse_OAF_dim), left_momentum_triples, 1.0D0, is_binary, CC4S_isreal)
    end if

    call CC4S_out_orbital_properties_yaml(ordered_indices, &
            &CC4S_n_k_points, CC4S_n_states, n_spins)

    call CC4S_out_momentum_properties_yaml(sparse_OAF_dim)

    call CC4S_out_coulvertex_singular_vectors_yaml("100", n_bb, sum(sparse_OAF_dim), &
                  &CC4S_n_kq_points, 1.0d0, is_binary, CC4S_isreal)

    if(.not. is_hf) then
      !If a KS calculation has been performed, we additionally need the 
      !(non-diagonal) fock matrix
      !call CC4S_out_fock_yaml(datafile_name, version, n_states, bin, isreal)
      call CC4S_out_fock_yaml("100", size(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:)), &
              &CC4S_n_k_points, n_spins, 1.0D0, is_binary, CC4S_isreal)
      !continue
    end if 

    if(n_spins .eq. 2) then
      !If  a spin-polarized calculation has been performed, we may need the Sz-values
      call CC4S_out_spin_yaml("100", size(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:)), &
              &n_spins, CC4S_n_k_points, 1.0D0)
      continue
    end if

    !New: Output yaml file for inverse sqrt of Coulomb potential (in PWs)
    if(do_fs) then
      !if(do_oaf) then
      !  call CC4S_out_coulomb_inverse_sqrt("100", sum(sparse_OAF_dim), CC4S_num_pws*CC4S_n_kq_points, is_binary, 1.0d0, .false.)
      !else
      !  call CC4S_out_coulomb_inverse_sqrt("100", CC4S_n_basbas, CC4S_num_pws*CC4S_n_kq_points, is_binary, 1.0d0, .false.)
      !end if
      !call CC4S_out_V_G_diagonal("100", CC4S_num_pws*CC4S_n_kq_points, 1.0d0)

!      call CC4S_out_pw_transform_yaml("100", CC4S_n_basbas*CC4S_n_kq_points, &
!                         &CC4S_num_pws*CC4S_n_kq_points, is_binary, 1.0d0, .false.)
      call CC4S_out_co_densities_yaml("100", &
                         &size(scf_energies(CC4S_n_low_state:CC4S_n_states,:,:)), n_spins, &
                         &CC4S_n_basbas*CC4S_n_kq_points, momentum_triples, &
                         &1.0D0, is_binary, CC4S_isreal)

      call CC4S_out_grid_vectors_yaml("100", CC4S_num_pws*CC4S_n_kq_points, 1.0d0)

      call CC4S_out_coulomb_potential_yaml("100", CC4S_n_basbas, CC4S_n_kq_points, CC4S_isreal, 1.0d0)

      call CC4S_out_crystal_momentum_properties_yaml(CC4S_n_kq_points, CC4S_n_basbas)
    end if 
  
  end subroutine output_yaml_files
  !*******************************************************
  subroutine CC4S_out_left_coulvertex_yaml(version, n_states, n_spin, n_basbas_OAF, &
                  &left_momentum_triples_list, e_unit, bin, isreal)
          implicit none
  
          character(len=*), intent(in) :: version
          integer, intent(in) :: n_states
          integer, intent(in) :: n_spin
          integer, intent(in) :: n_basbas_OAF
          integer, dimension(:,:), intent(in) :: left_momentum_triples_list
          double precision, intent(in) :: e_unit
          logical, intent(in) :: bin
          logical, intent(in) :: isreal
  
          !local
          character(len=25) :: grid
          integer :: i, i_spin
  
          !if(isreal) then
          !  grid='halfGrid'
          !else
          !  grid='fullGrid'
          !end if 
  
          open(unit=489, file='LeftCoulombVertex.yaml')
          write(489,'(A8, 1X, A5)') "version:", version
          write(489,'(A12)') "type: Tensor"
          write(489,'(A21)') "scalarType: Complex64"
 
          write(489,'(A11)') "dimensions:"
          write(489,'(A9, 1X, I0)') "- length:", n_basbas_OAF
          write(489,'(2X, A20)') "type: AuxiliaryField"
          write(489,'(A9, 1X, I0)') "- length:", n_states
          write(489,'(2X, A11)') "type: State"
          write(489,'(A9, 1X, I0)') "- length:", n_states
          write(489,'(2X, A11)') "type: State"

          write(489,'(A9)') "elements:"
          if(bin) then
            write(489,'(2X, A20)') "type: IeeeBinaryFile"
          else
            write(489,'(2X, A14)') "type: TextFile" 
          end if 

          write(489,'(A5, 1X, F12.7)') "unit:", e_unit
         
          write(489,'(A9)') "metaData:"
          if(isreal) then
            write(489,'(2X, A11)') "halfGrid: 1"
          else
            write(489,'(2X, A11)') "halfGrid: 0"
          end if

 
          write(489,'(A17)') "nonZeroCondition:"
          write(489,'(2X, A4)') "all:"
          !Spins
          write(489,'(4X, A2)') "0:"
          write(489,'(6X,A22)') "name: SpinConservation"
          write(489,'(6X, A11)') "properties:"
          write(489,'(8X, A2)') "0:"
          write(489,'(10X, A14)') "property: Spin"
          write(489,'(10X, A12)') "dimension: 1"
          write(489,'(8X, A2)') "1:"
          write(489,'(10X, A14)') "property: Spin"
          write(489,'(10X, A12)') "dimension: 2"
          write(489,'(6X, A9)') "nonZeros:"
          do i_spin=0,n_spin-1
            write(489,'(8X, A1, 1X, A1, I0, A1, I0, A1)') "-", "[", &
                    &i_spin, ",", i_spin, "]"
          end do


          !Crystal-/Transfer-momenta
          !Re-arrange/permute the following 3 dimensions
          !so that the order is 0, 1, 2
          !and accordingly permute the indices within the
          !momentum triples.
          !This will be required in future formats of CC4S
          write(489,'(4X, A2)') "1:"
          write(489,'(6X,A26)') "name: MomentumConservation"
          write(489,'(6X, A11)') "properties:"
          write(489,'(8X, A2)') "0:"
          write(489,'(10X, A25)') "property: CrystalMomentum"
          write(489,'(10X, A12)') "dimension: 0" !"dimension: 1"
          write(489,'(8X, A2)') "1:"
          write(489,'(10X, A25)') "property: CrystalMomentum"
          write(489,'(10X, A12)') "dimension: 1" !"dimension: 2"
          write(489,'(8X, A2)') "2:"
          write(489,'(10X, A25)') "property: CrystalMomentum"
          write(489,'(10X, A12)') "dimension: 2" !"dimension: 0"
          write(489,'(6X, A9)') "nonZeros:"

          do i=1,size(left_momentum_triples_list,2)
            write(489,'(8X, A1, 1X, A1, I0, A1, I0, A1, I0, A1)') "-", "[", &
                    &left_momentum_triples_list(3,i)-1, ",", &
                    &left_momentum_triples_list(1,i)-1, ",", &
                    &left_momentum_triples_list(2,i)-1, "]"
          end do
  
          close(unit=489)
  
  end subroutine CC4S_out_left_coulvertex_yaml









 !*******************************************************
  subroutine CC4S_out_coulvertex_singular_vectors_yaml(version, n_basbas, n_basbas_OAF, &
                  &n_k, e_unit, bin, isreal)
    implicit none
  
    character(len=*), intent(in) :: version
    integer, intent(in) :: n_basbas
    integer, intent(in) :: n_basbas_OAF
    integer, intent(in) :: n_k
    double precision, intent(in) :: e_unit
    logical, intent(in) :: bin
    logical, intent(in) :: isreal
  
    !local
    character(len=25) :: grid
    integer :: i, i_spin, i_k_point
  
    !if(isreal) then
    !  grid='halfGrid'
    !else
    !  grid='fullGrid'
    !end if 
  
    open(unit=490, file='CoulombVertexSingularVectors.yaml')
    write(490,'(A8, 1X, A5)') "version:", version
    write(490,'(A12)') "type: Tensor"
    !FIXME: At this moment, the singular vectors are always exported
    !as complex, even if they actually have a vanishing imaginary part.
    write(490,'(A21)') "scalarType: Complex64"
 
    write(490,'(A11)') "dimensions:"
    write(490,'(A9, 1X, I0)') "- length:", n_basbas
    write(490,'(2X, A14)') "type: Momentum"
    write(490,'(A9, 1X, I0)') "- length:", n_basbas_OAF
    write(490,'(2X, A20)') "type: AuxiliaryField"

    write(490,'(A9)') "elements:"
    if(bin) then
      write(490,'(2X, A20)') "type: IeeeBinaryFile"
    else
      write(490,'(2X, A14)') "type: TextFile" 
    end if 

    write(490,'(A5, 1X, F12.7)') "unit:", e_unit
    
    !write(490,'(A9)') "metaData:"
    !if(isreal) then
    !  write(490,'(2X, A11)') "halfGrid: 1"
    !else
    !  write(490,'(2X, A11)') "halfGrid: 0"
    !end if


    write(490,'(A17)') "nonZeroCondition:"
    write(490,'(2X, A4)') "all:"

    write(490, '(4X, A2)') "0:"
    write(490, '(6X, A26)') "name: MomentumConservation"
    write(490, '(6X, A11)') "properties:"
    write(490, '(8X, A2)') "0:"
    write(490, '(10X, A25)') "property: CrystalMomentum"
    write(490, '(10X, A12)') "dimension: 0"
    write(490, '(8X, A2)') "1:"
    write(490, '(10X, A25)') "property: CrystalMomentum"
    write(490, '(10X, A12)') "dimension: 1"
    write(490, '(6X, A9)') "nonZeros:"

    do i_k_point=0,n_k-1
      write(490,'(8X, A1, 1X, A1, I0, A1, I0, A1)') "-", "[", i_k_point, ",", i_k_point, "]"
    end do
  
    close(unit=490)
  
  end subroutine CC4S_out_coulvertex_singular_vectors_yaml
 !*******************************************************
  subroutine CC4S_out_crystal_momentum_properties_yaml(n_k, n_bb)
    implicit none

    integer, intent(in) :: n_k
    integer, intent(in) :: n_bb

    !local
    integer :: i,j


    open(unit=491, file='Momentum.yaml')
    write(491,'(A12)') "version: 100"
    write(491,'(A23)') "dimensionType: Momentum"
    !Leave out "properties"-section as it is not used anywhere yet
    write(491,'(A16)') "propertyIndices:"
    write(491,'(2X, A16)') "CrystalMomentum:"
    do i=1,n_k
      do j=1,n_bb
        write(491,'(2X, A1, 1X, I0)') "-", i-1
      end do
    end do
    close(491)
    
  end subroutine CC4S_out_crystal_momentum_properties_yaml

!***************************************************************************
  subroutine CC4S_out_coulomb_potential_yaml(version, n_bb, n_k, is_real, e_unit)

    implicit none

    character(len=*), intent(in) :: version
    integer, intent(in) :: n_bb
    integer, intent(in) :: n_k
    logical, intent(in) :: is_real
    double precision, intent(in) :: e_unit

    !local
    integer :: i_k_point

    open(unit=492, file='CoulombPotential.yaml')
    write(492,'(A8, 1X, A5)') "version:", version
    write(492,'(A12)') "type: Tensor"
    write(492,'(A21)') "scalarType: Complex64" 
    write(492,'(A11)') "dimensions:"
    write(492,'(A9, 1X, I0)') "- length:", n_bb*n_k
    write(492,'(2X, A14)') "type: Momentum"
    write(492,'(A9, 1X, I0)') "- length:", n_bb*n_k
    write(492,'(2X, A14)') "type: Momentum"
    write(492,'(A9)') "elements:"
    write(492,'(2X, A20)') "type: IeeeBinaryFile"

    write(492,'(A5, 1X, F12.7)') "unit:", e_unit
    
    write(492,'(A9)') "metaData:"
    if(is_real) then
      write(492,'(2X, A11)') "halfGrid: 1"
    else
      write(492,'(2X, A11)') "halfGrid: 0"
    end if

    write(492,'(A17)') "nonZeroCondition:"
    write(492,'(2X, A4)') "all:"

    write(492, '(4X, A2)') "0:"
    write(492, '(6X, A26)') "name: MomentumConservation"
    write(492, '(6X, A11)') "properties:"
    write(492, '(8X, A2)') "0:"
    write(492, '(10X, A25)') "property: CrystalMomentum"
    write(492, '(10X, A12)') "dimension: 0"
    write(492, '(8X, A2)') "1:"
    write(492, '(10X, A25)') "property: CrystalMomentum"
    write(492, '(10X, A12)') "dimension: 1"
    write(492, '(6X, A9)') "nonZeros:"

    do i_k_point=0,n_k-1
      write(492,'(8X, A1, 1X, A1, I0, A1, I0, A1)') "-", "[", i_k_point, ",", i_k_point, "]"
    end do

    close(unit=492)
  end subroutine CC4S_out_coulomb_potential_yaml
!***************************************************************************
  subroutine CC4S_out_grid_vectors_yaml(version, n_grid_points, e_unit)

  implicit none
  
  character(len=*), intent(in) :: version
  integer, intent(in) :: n_grid_points
  double precision, intent(in) :: e_unit

  !local
  integer :: i

    open(unit=493, file='GridVectors.yaml')
    write(493,'(A8, 1X, A5)') "version:", version
    write(493,'(A12)') "type: Tensor"
    write(493,'(A18)') "scalarType: Real64"
    write(493,'(A11)') "dimensions:"
    write(493,'(A9, 1X, I0)') "- length:", 3
    write(493,'(2X, A12)') "type: Vector"
    write(493,'(A9, 1X, I0)') "- length:", n_grid_points
    write(493,'(2X, A14)') "type: Momentum"
    write(493,'(A9)') "elements:"
    write(493,'(2X, A14)') "type: TextFile"
    write(493,'(A5, 1X, F12.7)') "unit:", e_unit
    write(493,'(A9)') "metaData:"

    write(493, '(2X, A3, 1X, "[", E25.15, ",", E25.15, ",", E25.15, "]")') &
           &"Gi:", CC4S_reciprocal_lattice_vectors(:,1)
    write(493, '(2X, A3, 1X, "[", E25.15, ",", E25.15, ",", E25.15, "]")') &
           &"Gj:", CC4S_reciprocal_lattice_vectors(:,2)
    write(493, '(2X, A3, 1X, "[", E25.15, ",", E25.15, ",", E25.15, "]")') &
           &"Gk:", CC4S_reciprocal_lattice_vectors(:,3)
    close(unit=493)
 
  end subroutine CC4S_out_grid_vectors_yaml
!***************************************************************************
  subroutine CC4S_out_co_densities_yaml(version, n_states, n_spin, n_bb, &
                  &momentum_triples_list, e_unit, bin, isreal)
          implicit none
  
          character(len=*), intent(in) :: version
          integer, intent(in) :: n_states
          integer, intent(in) :: n_spin
          integer, intent(in) :: n_bb
          integer, dimension(:,:), intent(in) :: momentum_triples_list
          double precision, intent(in) :: e_unit
          logical, intent(in) :: bin
          logical, intent(in) :: isreal
  
          !local
          character(len=25) :: grid
          integer :: i, i_spin
  
          !if(isreal) then
          !  grid='halfGrid'
          !else
          !  grid='fullGrid'
          !end if 
  
          open(unit=494, file='RealSpaceCoDensities.yaml')
          write(494,'(A8, 1X, A5)') "version:", version
          write(494,'(A12)') "type: Tensor"
          write(494,'(A21)') "scalarType: Complex64"
          write(494,'(A11)') "dimensions:"
          write(494,'(A9, 1X, I0)') "- length:", n_bb
          write(494,'(2X, A20)') "type: AuxiliaryField"
          write(494,'(A9, 1X, I0)') "- length:", n_states
          write(494,'(2X, A11)') "type: State"
          write(494,'(A9, 1X, I0)') "- length:", n_states
          write(494,'(2X, A11)') "type: State"

          write(494,'(A9)') "elements:"
          if(bin) then
            write(494,'(2X, A20)') "type: IeeeBinaryFile"
          else
            write(494,'(2X, A14)') "type: TextFile" 
          end if 

          write(494,'(A5, 1X, F12.7)') "unit:", e_unit
         
          write(494,'(A9)') "metaData:"
          if(isreal) then
            write(494,'(2X, A11)') "halfGrid: 1"
          else
            write(494,'(2X, A11)') "halfGrid: 0"
          end if

 
          write(494,'(A17)') "nonZeroCondition:"
          write(494,'(2X, A4)') "all:"
          !Spins
          write(494,'(4X, A2)') "0:"
          write(494,'(6X,A22)') "name: SpinConservation"
          write(494,'(6X, A11)') "properties:"
          write(494,'(8X, A2)') "0:"
          write(494,'(10X, A14)') "property: Spin"
          write(494,'(10X, A12)') "dimension: 1"
          write(494,'(8X, A2)') "1:"
          write(494,'(10X, A14)') "property: Spin"
          write(494,'(10X, A12)') "dimension: 2"
          write(494,'(6X, A9)') "nonZeros:"
          do i_spin=0,n_spin-1
            write(494,'(8X, A1, 1X, A1, I0, A1, I0, A1)') "-", "[", &
                    &i_spin, ",", i_spin, "]"
          end do


          !Crystal-/Transfer-momenta
          !Re-arrange/permute the following 3 dimensions
          !so that the order is 0, 1, 2
          !and accordingly permute the indices within the
          !momentum triples.
          !This will be required in future formats of CC4S
          write(494,'(4X, A2)') "1:"
          write(494,'(6X,A26)') "name: MomentumConservation"
          write(494,'(6X, A11)') "properties:"
          write(494,'(8X, A2)') "0:"
          write(494,'(10X, A25)') "property: CrystalMomentum"
          write(494,'(10X, A12)') "dimension: 0" !"dimension: 1"
          write(494,'(8X, A2)') "1:"
          write(494,'(10X, A25)') "property: CrystalMomentum"
          write(494,'(10X, A12)') "dimension: 1" !"dimension: 2"
          write(494,'(8X, A2)') "2:"
          write(494,'(10X, A25)') "property: CrystalMomentum"
          write(494,'(10X, A12)') "dimension: 2" !"dimension: 0"
          write(494,'(6X, A9)') "nonZeros:"

          do i=1,size(momentum_triples_list,2)
            write(494,'(8X, A1, 1X, A1, I0, A1, I0, A1, I0, A1)') "-", "[", &
            !        &momentum_triples_list(1,i)-1, ",", &
            !        &momentum_triples_list(2,i)-1, ",", &
            !        &momentum_triples_list(3,i)-1, "]"
                    &momentum_triples_list(3,i)-1, ",", &
                    &momentum_triples_list(1,i)-1, ",", &
                    &momentum_triples_list(2,i)-1, "]"
          end do
  
          close(unit=494)
  
  end subroutine CC4S_out_co_densities_yaml

 !*******************************************************
  subroutine CC4S_out_pw_transform_yaml(version, n_ri, n_pw, bin, e_unit, isreal)
    implicit none

    character(len=*), intent(in) :: version
    integer, intent(in) :: n_ri !either n_basbas or reduced by OAF
    integer, intent(in) :: n_pw
    logical, intent(in) :: bin
    double precision, intent(in) :: e_unit
    logical, intent(in) :: isreal

    open(unit=495, file="PlaneWaveTransform.yaml")
    write(495,'(A8, 1X, A5)') "version:", version
    write(495,'(A12)') "type: Tensor"
    write(495,'(A21)') "scalarType: Complex64"
    write(495,'(A11)') "dimensions:"
    write(495,'(A9, 1X, I0)') "- length:", n_ri
    write(495,'(2X, A20)') "type: AuxiliaryField"
    write(495,'(A9, 1X, I0)') "- length:", n_pw
    write(495,'(2X, A11)') "type: State"

    write(495,'(A9)') "elements:"
    if(bin) then
      write(495,'(2X, A20)') "type: IeeeBinaryFile"
    else
      write(495,'(2X, A14)') "type: TextFile" 
    end if 

    write(495,'(A5, 1X, F12.7)') "unit:", e_unit
    
    write(495,'(A9)') "metaData:"
    !if(isreal) then
    !  write(495,'(2X, A11)') "halfGrid: 1"
    !else
    !  write(495,'(2X, A11)') "halfGrid: 0"
    !end if

    write(495,'(2X, A11)') "halfGrid: 0"

    close(495)
  end subroutine CC4S_out_pw_transform_yaml

 !*******************************************************
  subroutine CC4S_out_V_G_diagonal(version, n_pw, e_unit)
    implicit none

    character(len=*), intent(in) :: version
    integer, intent(in) :: n_pw
    double precision, intent(in) :: e_unit

          open(unit=496, file='CoulombPotential.yaml')
          write(496,'(A8, 1X, A5)') "version:", version
          write(496,'(A12)') "type: Tensor"
          write(496,'(A18)') "scalarType: Complex64"
          write(496,'(A11)') "dimensions:"
          write(496,'(A9, 1X, I0)') "- length:", n_pw
          write(496,'(2X, A11)') "type: State"
          write(496,'(A9, 1X, I0)') "- length:", n_pw
          write(496,'(2X, A11)') "type: State"
          write(496,'(A9)') "elements:"
          write(496,'(2X, A14)') "type: TextFile"
          write(496,'(A5, 1X, F12.7)') "unit:", e_unit
          close(496)

  end subroutine CC4S_out_V_G_diagonal
 !*******************************************************
  subroutine CC4S_out_coulomb_inverse_sqrt(version, n_ri, n_pw, bin, e_unit, isreal)
    implicit none

    character(len=*), intent(in) :: version
    integer, intent(in) :: n_ri !either n_basbas or reduced by OAF
    integer, intent(in) :: n_pw
    logical, intent(in) :: bin
    double precision, intent(in) :: e_unit
    logical, intent(in) :: isreal

    open(unit=497, file="CoulombInverseSqrt.yaml")
    write(497,'(A8, 1X, A5)') "version:", version
    write(497,'(A12)') "type: Tensor"
    write(497,'(A21)') "scalarType: Complex64"
    write(497,'(A11)') "dimensions:"
    write(497,'(A9, 1X, I0)') "- length:", n_ri
    write(497,'(2X, A20)') "type: AuxiliaryField"
    write(497,'(A9, 1X, I0)') "- length:", n_pw
    write(497,'(2X, A11)') "type: State"

    write(497,'(A9)') "elements:"
    if(bin) then
      write(497,'(2X, A20)') "type: IeeeBinaryFile"
    else
      write(497,'(2X, A14)') "type: TextFile" 
    end if 

    write(497,'(A5, 1X, F12.7)') "unit:", e_unit
    
    write(497,'(A9)') "metaData:"
    if(isreal) then
      write(497,'(2X, A11)') "halfGrid: 1"
    else
      write(497,'(2X, A11)') "halfGrid: 0"
    end if

 
    !      write(503,'(A17)') "nonZeroCondition:"
    !      write(503,'(2X, A4)') "all:"
    !      !Spins
    !      write(503,'(4X, A2)') "0:"
    !      write(503,'(6X,A22)') "name: SpinConservation"
    !      write(503,'(6X, A11)') "properties:"
    !      write(503,'(8X, A2)') "0:"
    !      write(503,'(10X, A14)') "property: Spin"
    !      write(503,'(10X, A12)') "dimension: 1"
    !      write(503,'(8X, A2)') "1:"
    !      write(503,'(10X, A14)') "property: Spin"
    !      write(503,'(10X, A12)') "dimension: 2"
    !      write(503,'(6X, A9)') "nonZeros:"
    !      do i_spin=0,n_spin-1
    !        write(503,'(8X, A1, 1X, A1, I0, A1, I0, A1)') "-", "[", &
    !                &i_spin, ",", i_spin, "]"
    !      end do


          !Crystal-/Transfer-momenta
          !Re-arrange/permute the following 3 dimensions
          !so that the order is 0, 1, 2
          !and accordingly permute the indices within the
          !momentum triples.
          !This will be required in future formats of CC4S
    !      write(503,'(4X, A2)') "1:"
    !      write(503,'(6X,A26)') "name: MomentumConservation"
    !      write(503,'(6X, A11)') "properties:"
    !      write(503,'(8X, A2)') "0:"
    !      write(503,'(10X, A25)') "property: CrystalMomentum"
    !      write(503,'(10X, A12)') "dimension: 0" !"dimension: 1"
    !      write(503,'(8X, A2)') "1:"
    !      write(503,'(10X, A25)') "property: CrystalMomentum"
    !      write(503,'(10X, A12)') "dimension: 1" !"dimension: 2"
    !      write(503,'(8X, A2)') "2:"
    !      write(503,'(10X, A25)') "property: CrystalMomentum"
    !      write(503,'(10X, A12)') "dimension: 2" !"dimension: 0"
    !      write(503,'(6X, A9)') "nonZeros:"

    !      do i=1,size(momentum_triples_list,2)
    !        write(503,'(8X, A1, 1X, A1, I0, A1, I0, A1, I0, A1)') "-", "[", &
    !        !        &momentum_triples_list(1,i)-1, ",", &
    !        !        &momentum_triples_list(2,i)-1, ",", &
    !        !        &momentum_triples_list(3,i)-1, "]"
    !                &momentum_triples_list(3,i)-1, ",", &
    !                &momentum_triples_list(1,i)-1, ",", &
    !                &momentum_triples_list(2,i)-1, "]"
    !      end do
    close(497)
  end subroutine CC4S_out_coulomb_inverse_sqrt

 !*******************************************************
  subroutine CC4S_out_momentum_properties_yaml(sparse_OAF_dim)
    implicit none

    integer, dimension(:), intent(in) :: sparse_OAF_dim

    !local
    integer :: i,j,k

    k = 1

    open(unit=498, file='AuxiliaryField.yaml')
    write(498,'(A12)') "version: 100"
    write(498,'(A29)') "dimensionType: AuxiliaryField"
    !Leave out "properties"-section as it is not used anywhere yet
    write(498,'(A16)') "propertyIndices:"
    write(498,'(2X, A16)') "CrystalMomentum:"
    do i=1,size(sparse_OAF_dim)
      do j=1,sparse_OAF_dim(i)
        !write(498,'(2X, I0, A1, 1X, I0)') k-1,":", i-1
        write(498,'(2X, A1, 1X, I0)') "-", i-1
        k = k + 1
      end do
    end do
    close(498)
    
  end subroutine CC4S_out_momentum_properties_yaml
 !*******************************************************
  subroutine CC4S_out_orbital_properties_yaml(ordered_indices, &
                  &n_k_points, n_states, n_spins)
    implicit none

    integer, dimension(:), intent(in) :: ordered_indices
    integer, intent(in) :: n_k_points, n_states, n_spins

    !local
    integer :: i,j,lb
    integer, dimension(:), allocatable :: unordered_k_indices
    integer, dimension(:), allocatable :: unordered_spin_indices

    allocate(unordered_k_indices(n_k_points*CC4S_nuf_states*n_spins))
    allocate(unordered_spin_indices(n_k_points*CC4S_nuf_states*n_spins))




    !Momentum indices
    do i=1,n_k_points
      lb = (i-1)*CC4S_nuf_states*n_spins + 1
      unordered_k_indices(lb:lb+CC4S_nuf_states*n_spins-1) = i
    end do

    !Spin indices
    do i=1,n_k_points
      do j=1,n_spins
        lb = ((i-1)*n_spins + (j-1))*CC4S_nuf_states + 1
        unordered_spin_indices(lb:lb+CC4S_nuf_states-1) = j
      end do
    end do


    open(unit=499, file='State.yaml')
    write(499,'(A12)') "version: 100"
    write(499,'(A20)') "dimensionType: State"
    write(499,'(A11)') "properties:"
    write(499,'(2X, A5)') "Spin:"

    if(n_spins == 2) then
      write(499,'(4X, A7)') "0: +0.5"
      write(499,'(4X, A7)') "1: -0.5"
    else
      write(499,'(4X, A7)') "0: +0.5"
    end if 

    write(499,'(A16)') "propertyIndices:"
    write(499,'(2X, A16)') "CrystalMomentum:"
    do i=1,size(ordered_indices)
      !write(499,'(2X, I0, A1, 1X, I0)') i-1, ":", unordered_k_indices(ordered_indices(i)) - 1
      write(499,'(2X, A1, 1X, I0)') "-", unordered_k_indices(ordered_indices(i)) - 1
    end do

    write(499,'(2X, A5)') "Spin:"
    do i=1,size(ordered_indices)
      !write(499,'(2X, I0, A1, 1X, I0)') i-1, ":", unordered_spin_indices(ordered_indices(i)) - 1
      write(499,'(2X, A1, 1X, I0)') "-", unordered_spin_indices(ordered_indices(i)) - 1
    end do

    close(499)
  end subroutine CC4S_out_orbital_properties_yaml
 !*******************************************************
  subroutine CC4S_out_spin_yaml(version, n_states, n_spin, n_k_points, e_unit)
        implicit none
  
        !character(len=*), intent(in) :: datafile_name
        character(len=*), intent(in) :: version
        integer, intent(in) :: n_states
        integer, intent(in) :: n_spin
        integer, intent(in) :: n_k_points
        double precision, intent(in) :: e_unit

        !local
        integer :: i_spin, i_k_point
  
        open(unit=500, file='Spins.yaml')
        write(500,'(A8, 1X, A5)') "version:", version
        write(500,'(A8)') "indices:"
        write(500,'(2X, A8)') "orbital:"
        !Assuming this file will only be written out for spin-polarized case
        write(500,'(4X, A10)') "type: spin"
        write(500,'(A11)') "dimensions:"
        write(500,'(2X, A9, 1X, I0)') "- length:", n_states
        write(500,'(4X, A15)') "type: orbital"
        !write(500,'(A5, 1X, A100)') "data:", datafile_name
        write(500,'(A5, 1X, F12.7)') "unit:", e_unit

        write(500,'(A17)') "nonZeroCondition:"
        write(500, '(2X, A4)') "all:"
        !Spin
        write(500, '(4X, A2)') "0:"
        write(500, '(6X, A22)') "name: SpinConservation"
        write(500, '(6X, A11)') "properties:"
        write(500, '(8X, A2)') "0:"
        write(500, '(10X, A14)') "property: Spin"
        write(500, '(10X, A12)') "dimension: 0"
        write(500, '(6X, A9)') "nonZeros:"

        do i_spin=0,n_spin-1
          write(500,'(8X, A1, 1X, A1, I0, A1)') "-", "[", i_spin, "]"
        end do

        !crystal momentum
        write(500, '(4X, A2)') "1:"
        write(500, '(6X, A26)') "name: MomentumConservation"
        write(500, '(6X, A11)') "properties:"
        write(500, '(8X, A2)') "0:"
        write(500, '(10X, A25)') "property: CrystalMomentum"
        write(500, '(10X, A12)') "dimension: 0"
        write(500, '(6X, A9)') "nonZeros:"

        do i_k_point=0,n_k_points-1
          write(500,'(8X, A1, 1X, A1, I0, A1)') "-", "[", i_k_point, "]"
        end do
        close(unit=500)
  end subroutine CC4S_out_spin_yaml
  !******************************************************
  subroutine CC4S_out_eigenenergies_yaml(version, e_unit, e_fermi, scf_energies, &
                  &n_k_points, n_spin)
          implicit none
  
          character(len=*), intent(in) :: version
          double precision, intent(in) :: e_unit, e_fermi
          integer, intent(in) :: n_k_points
          integer, intent(in) :: n_spin
          !Flattened and sorted scf-energies
          double precision, dimension(:) :: scf_energies
  
          !local
          integer :: i_k_point
          integer :: i_spin
          integer :: i_element

  
          open(unit=501, file='EigenEnergies.yaml')
          write(501,'(A8, 1X, A5)') "version:", version
          write(501,'(A12)') "type: Tensor"
          write(501,'(A18)') "scalarType: Real64"
          !write(501,'(A8)') "indices:"
          !write(501,'(2X, A8)') "orbital:"
          !if(n_spin .eq. 2) then
          !  write(501,'(4X, A10)') "type: spin"
          !else
          !  write(501,'(4X, A13)') "type: spatial"
          !end if 
          write(501,'(A11)') "dimensions:"
          write(501,'(A9, 1X, I0)') "- length:", size(scf_energies)
          write(501,'(2X, A11)') "type: State"
          !write(501,'(4X, A13)') "type: orbital"
          !write(501,'(A5, 1X, A100)') "data:", scf_energies_filename
          write(501,'(A9)') "elements:"
          write(501,'(2X, A14)') "type: TextFile"
          write(501,'(A5, 1X, F12.7)') "unit:", e_unit
          write(501, '(A9)' ) "metaData:"
          write(501,'(2X, A12, 1X, F25.16)') "fermiEnergy:", e_fermi
          write(501,'(2X, A9)') "energies:"

          do i_element=1,size(scf_energies)
            write(501,'(2X, A1, 1X, F25.16)') "-", scf_energies(i_element)
          end do
 
          write(501,'(A17)') "nonZeroCondition:"
          write(501, '(2X, A4)') "all:"
          !Spin
          write(501, '(4X, A2)') "0:"
          write(501, '(6X, A22)') "name: SpinConservation"
          write(501, '(6X, A11)') "properties:"
          write(501, '(8X, A2)') "0:"
          write(501, '(10X, A14)') "property: Spin"
          write(501, '(10X, A12)') "dimension: 0"
          write(501, '(6X, A9)') "nonZeros:"

          do i_spin=0,n_spin-1
            write(501,'(8X, A1, 1X, A1, I0, A1)') "-", "[", i_spin, "]"
          end do

          !crystal momentum
          write(501, '(4X, A2)') "1:"
          write(501, '(6X, A26)') "name: MomentumConservation"
          write(501, '(6X, A11)') "properties:"
          write(501, '(8X, A2)') "0:"
          write(501, '(10X, A25)') "property: CrystalMomentum"
          write(501, '(10X, A12)') "dimension: 0"
          write(501, '(6X, A9)') "nonZeros:"

          do i_k_point=0,n_k_points-1
            write(501,'(8X, A1, 1X, A1, I0, A1)') "-", "[", i_k_point, "]"
          end do

          close(unit=501)
  
  end subroutine CC4S_out_eigenenergies_yaml 
  !*************************************************
  subroutine CC4S_out_fock_yaml(version, n_states, n_k_points, n_spin, e_unit, bin, isreal)
          implicit none
  
          !character(len=*), intent(in) :: datafile_name
          character(len=*), intent(in) :: version
          integer, intent(in) :: n_states
          integer, intent(in) :: n_k_points
          integer, intent(in) :: n_spin
          double precision, intent(in) :: e_unit
          logical, intent(in) :: bin
          logical, intent(in) :: isreal
  
          !local
          character(len=20) :: scalartype
          integer :: i_k_point
          integer :: i_spin

          if(isreal) then
            scalartype='Real64'
          else
            scalartype='Complex64'
          end if 
  
          open(unit=502, file='FockOperator.yaml')
          write(502,'(A8, 1X, A5)') "version:", version
           write(502,'(A12)') "type: Tensor"
          write(502,'(A11, 1X, A20)') "scalarType:", scalartype
          !write(502,'(A8)') "indices:"
          !write(502,'(2X, A8)') "orbital:"
          !if(n_spin .eq. 2) then
          !  write(502,'(4X, A10)') "type: spin"
          !else
          !  write(502,'(4X, A13)') "type: spatial"
          !end if 
          write(502,'(A11)') "dimensions:"
          write(502,'(A9, 1X, I0)') "- length:", n_states
          write(502,'(2X, A11)') "type: State"
          write(502,'(A9, 1X, I0)') "- length:", n_states
          write(502,'(2X, A11)') "type: State"

          write(502,'(A9)') "elements:"
          if(bin) then
            write(502,'(2X, A20)') "type: IeeeBinaryFile"
          else
            write(502,'(2X, A14)') "type: TextFile"
          end if 

          !if(bin) then
          !  write(502,'(A9)') "binary: 1"
          !  write(502,'(A5, 1X, A50)') "data:", scf_fockmatrix_filename_bin
          !else
          !  write(502,'(A5, 1X, A50)') "data:", scf_fockmatrix_filename_dat
          !end if 

          write(502,'(A5, 1X, F12.7)') "unit:", e_unit

          write(502,'(A17)') "nonZeroCondition:"
          write(502, '(2X, A4)') "all:"
          !Spin
          write(502, '(4X, A2)') "0:"
          write(502, '(6X, A22)') "name: SpinConservation"
          write(502, '(6X, A11)') "properties:"
          write(502, '(8X, A2)') "0:"
          write(502, '(10X, A14)') "property: Spin"
          write(502, '(10X, A12)') "dimension: 0"
          write(502, '(8X, A2)') "1:"
          write(502, '(10X, A14)') "property: Spin"
          write(502, '(10X, A12)') "dimension: 1"
          write(502, '(6X, A9)') "nonZeros:"

          do i_spin=0,n_spin-1
            write(502,'(8X, A1, 1X, A1, I0, A1, I0, A1)') "-", "[", i_spin, ",", i_spin, "]"
          end do

          !crystal momentum
          write(502, '(4X, A2)') "1:"
          write(502, '(6X, A26)') "name: MomentumConservation"
          write(502, '(6X, A11)') "properties:"
          write(502, '(8X, A2)') "0:"
          write(502, '(10X, A25)') "property: CrystalMomentum"
          write(502, '(10X, A12)') "dimension: 0"
          write(502, '(8X, A2)') "1:"
          write(502, '(10X, A25)') "property: CrystalMomentum"
          write(502, '(10X, A12)') "dimension: 1"
          write(502, '(6X, A9)') "nonZeros:"

          do i_k_point=0,n_k_points-1
            write(502,'(8X, A1, 1X, A1, I0, A1, I0, A1)') "-", "[", i_k_point, ",", i_k_point, "]"
          end do

          close(unit=502)
  
  end subroutine CC4S_out_fock_yaml
  !*******************************************************
  subroutine CC4S_out_coulvertex_yaml(version, n_states, n_spin, n_basbas_OAF, &
                  &momentum_triples_list, e_unit, bin, isreal)
          implicit none
  
          character(len=*), intent(in) :: version
          integer, intent(in) :: n_states
          integer, intent(in) :: n_spin
          integer, intent(in) :: n_basbas_OAF
          integer, dimension(:,:), intent(in) :: momentum_triples_list
          double precision, intent(in) :: e_unit
          logical, intent(in) :: bin
          logical, intent(in) :: isreal
  
          !local
          character(len=25) :: grid
          integer :: i, i_spin
  
          !if(isreal) then
          !  grid='halfGrid'
          !else
          !  grid='fullGrid'
          !end if 
  
          open(unit=503, file='CoulombVertex.yaml')
          write(503,'(A8, 1X, A5)') "version:", version
          write(503,'(A12)') "type: Tensor"
          write(503,'(A21)') "scalarType: Complex64"
          !write(503,'(A8)') "indices:"
          !write(503,'(2X, A9)') "momentum:"
          !write(503,'(4X, A5, 1X, A25)') "type:", grid
          !write(503,'(2X, A8)') "orbital:"
          !if(n_spin .eq. 2) then
          !  write(503,'(4X, A10)') "type: spin"
          !else
          !  write(503,'(4X, A13)') "type: spatial"
          !end if 
          write(503,'(A11)') "dimensions:"
          write(503,'(A9, 1X, I0)') "- length:", n_basbas_OAF
          write(503,'(2X, A20)') "type: AuxiliaryField"
          write(503,'(A9, 1X, I0)') "- length:", n_states
          !write(503,'(2X, A13)') "type: orbital"
          write(503,'(2X, A11)') "type: State"
          write(503,'(A9, 1X, I0)') "- length:", n_states
          !write(503,'(2X, A13)') "type: orbital"
          write(503,'(2X, A11)') "type: State"

          write(503,'(A9)') "elements:"
          if(bin) then
            write(503,'(2X, A20)') "type: IeeeBinaryFile"
          else
            write(503,'(2X, A14)') "type: TextFile" 
          end if 

          !if(bin) then
          !  write(503,'(A9)') "binary: 1"
          !  write(503,'(A5, 1X, A50)') "data:", coulomb_vertex_filename_bin
          !else
          !  write(503,'(A5, 1X, A50)') "data:", coulomb_vertex_filename_dat
          !end if

          write(503,'(A5, 1X, F12.7)') "unit:", e_unit
         
          write(503,'(A9)') "metaData:"
          if(isreal) then
            write(503,'(2X, A11)') "halfGrid: 1"
          else
            write(503,'(2X, A11)') "halfGrid: 0"
          end if

 
          write(503,'(A17)') "nonZeroCondition:"
          write(503,'(2X, A4)') "all:"
          !Spins
          write(503,'(4X, A2)') "0:"
          write(503,'(6X,A22)') "name: SpinConservation"
          write(503,'(6X, A11)') "properties:"
          write(503,'(8X, A2)') "0:"
          write(503,'(10X, A14)') "property: Spin"
          write(503,'(10X, A12)') "dimension: 1"
          write(503,'(8X, A2)') "1:"
          write(503,'(10X, A14)') "property: Spin"
          write(503,'(10X, A12)') "dimension: 2"
          write(503,'(6X, A9)') "nonZeros:"
          do i_spin=0,n_spin-1
            write(503,'(8X, A1, 1X, A1, I0, A1, I0, A1)') "-", "[", &
                    &i_spin, ",", i_spin, "]"
          end do


          !Crystal-/Transfer-momenta
          !Re-arrange/permute the following 3 dimensions
          !so that the order is 0, 1, 2
          !and accordingly permute the indices within the
          !momentum triples.
          !This will be required in future formats of CC4S
          write(503,'(4X, A2)') "1:"
          write(503,'(6X,A26)') "name: MomentumConservation"
          write(503,'(6X, A11)') "properties:"
          write(503,'(8X, A2)') "0:"
          write(503,'(10X, A25)') "property: CrystalMomentum"
          write(503,'(10X, A12)') "dimension: 0" !"dimension: 1"
          write(503,'(8X, A2)') "1:"
          write(503,'(10X, A25)') "property: CrystalMomentum"
          write(503,'(10X, A12)') "dimension: 1" !"dimension: 2"
          write(503,'(8X, A2)') "2:"
          write(503,'(10X, A25)') "property: CrystalMomentum"
          write(503,'(10X, A12)') "dimension: 2" !"dimension: 0"
          write(503,'(6X, A9)') "nonZeros:"

          do i=1,size(momentum_triples_list,2)
            write(503,'(8X, A1, 1X, A1, I0, A1, I0, A1, I0, A1)') "-", "[", &
            !        &momentum_triples_list(1,i)-1, ",", &
            !        &momentum_triples_list(2,i)-1, ",", &
            !        &momentum_triples_list(3,i)-1, "]"
                    &momentum_triples_list(3,i)-1, ",", &
                    &momentum_triples_list(1,i)-1, ",", &
                    &momentum_triples_list(2,i)-1, "]"
          end do
  
          close(unit=503)
  
  end subroutine CC4S_out_coulvertex_yaml

end module CC4S_yaml_output
