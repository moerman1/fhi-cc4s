subroutine CC4S_output_scf_energies(scf_energies, n_states, n_spin, &
                &n_k_points, lowest_state, file_name, bin)

        !PURPOSE: write the eigenvalues of the underlying SCF-method to file called 'file_name'
        !INPUT: 
        !       *scf_energies: A pointer to a three dimensional array of dimension 
        !                      n_states x n_spin x n_k_points
        !                      which contains the scf-eigenvalues for a generally unrestricted
        !                      (n_spin=2) calculation with an arbitrary k-mesh. n_states denotes
        !                      the number of states per k-point (i.e band-index). The array is 
        !                      expected to not be distributed
        !  
        !       *file_name:    Name of the file the scf-energies will be written to
        !
        !       *lowest_state: For frozen-core calculations lowest_state contains the first unfrozen
        !                      band-index. For full-electron calculations specify lowest_state=1
        !
        !       *bin:          Output file formatted (bin=.false.) or as binary (bin=.true.)

        !input variables
        integer, intent(in) :: n_states
        integer, intent(in) :: n_spin
        integer, intent(in) :: n_k_points 
        integer, intent(in) :: lowest_state
        double precision, dimension(n_states, n_spin, n_k_points), intent(in) :: scf_energies
        character(len=*), intent(in) :: file_name
        logical, intent(in) :: bin

        !local variables
        integer :: i_k_point, i_state, i_spin

        if(.not. bin) then
          open(unit=334, file=file_name, action='write')
          do i_k_point=1,n_k_points
            do i_spin=1,n_spin
              do i_state=lowest_state,n_states !        1,n_states-lowest_state+1
                write(334,'(F25.16)') scf_energies(i_state, i_spin, i_k_point)
              end do
            end do
          end do
        else
          open(unit=334, file=file_name, action='write', form='unformatted', access='sequential')
          do i_k_point=1,n_k_points
            do i_spin=1,n_spin
              do i_state=lowest_state, n_states !1,n_states-lowest_state+1
                write(334) scf_energies(i_state, i_spin, i_k_point)
              end do
            end do
          end do

        end if 
        close(unit=334)

end subroutine CC4S_output_scf_energies
