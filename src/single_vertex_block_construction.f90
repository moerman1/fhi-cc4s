module CC4S_vertex_block

        use CC4S_M_k_q, only: CC4S_compute_M_k_q
        use CC4S_VtimesM, only: CC4S_v_times_M_k_q
        use calculation_data, only: CC4S_nuf_states, CC4S_n_basis, CC4S_n_states, CC4S_n_low_state
        use CC4S_blacs_environment, only: CC4S_lbb_row, CC4S_ubb_row, &
                &CC4S_lstates_col, CC4S_ustates_col

        interface CC4S_build_vertex_block
                module procedure CC4S_build_vertex_block_real
                module procedure CC4S_build_vertex_block_cmplx
                module procedure CC4S_build_vertex_block_riv_real
                module procedure CC4S_build_vertex_block_riv_cmplx
        end interface CC4S_build_vertex_block
contains
       !PURPOSE: In order to accurately transform the Coulomb vertex
       !into the compressed OAF-vertex, it will probably be necessary
       !to calculate the non-trivial (i.e non-0) blocks of the vertex
       !MULTIPLE TIMES.
       !Hence, the one-time construction of the single blocks as it is currently
       !done in construct_coulvertex.f90 will be put in the functions here.

       !It should be stressed, that the functions in here WILL NOT perform the actual
       !OAF-transformation but only construct the untruncated Coulomb vertex blocks.

       subroutine CC4S_build_vertex_block_real(aomo_lvl_k_r, &
                                              &aomo_lvl_q_r, &
                                              &scf_eigvecs_k_r, &
                                              &scf_eigvecs_q_r, &
                                              &coulmat_sqrt_r, &
                                              &i_spin, &
                                              &i_k_point, &
                                              &i_q_point, &
                                              &coulomb_vertex_block, &
                                              &save_M_k_q)
         implicit none
         
         !INPUT:
         !  *aomo_lvl_k_r     : RI-LVL-coefficient tensor, where one AO-dimension
         !                      has been transformed to MO with crystal momentum k
         !  *aomo_lvl_q_r     : RI-LVL-coefficient tensor, where one AO-dimension
         !                      has been transformed to MO with crystal momentum q
         !  *scf_eigvecs_k_r  : HF/KS-eigenvectors from scf-calculation for k-point
         !  *scf_eigvecs_q_r  : HF/KS-eigenvectors from scf-calculation for q-point
         !  *coulmat_sqrt_r   : square-root of auxiliary Coulomb matrix V
         !  *i_spin           : spin-channel for which Coulomb vertex block will be
         !                      constructed (i_spin=1 or i_spin=2)
         !  *i_k_point        : First crystal-momentum vector k
         !  *i_q_point        : Second crystal-momentum vector q 

         !input
         double precision, dimension(:,:,:,:), intent(in) :: aomo_lvl_k_r
         double precision, dimension(:,:,:,:), intent(in) :: aomo_lvl_q_r
         double precision, dimension(:,:), intent(in) :: scf_eigvecs_k_r
         double precision, dimension(:,:), intent(in) :: scf_eigvecs_q_r
         double precision, dimension(:,:), intent(in) :: coulmat_sqrt_r
         integer, intent(in) :: i_spin
         integer, intent(in) :: i_k_point
         integer, intent(in) :: i_q_point
         logical, optional, intent(in) :: save_M_k_q

         !output
         double precision, dimension(:,:,:,:), target, intent(inout) :: coulomb_vertex_block

         !local
         !ss = single spin
         double precision, dimension(:,:,:), pointer :: coulomb_vertex_block_ss  => null()
         logical :: internal_save_M_k_q

         if(present(save_M_k_q)) then
           internal_save_M_k_q = save_M_k_q
         else
           internal_save_M_k_q = .false.
         endif

         call CC4S_compute_M_k_q(i_spin, i_k_point, i_q_point, &
                                &scf_eigvecs_k_r, scf_eigvecs_q_r, &
                                &aomo_lvl_k_r, aomo_lvl_q_r, &
                                &coulomb_vertex_block, &
                                &save_co_densities=internal_save_M_k_q)

         coulomb_vertex_block_ss => coulomb_vertex_block(:,:,:,i_spin)

         call CC4S_v_times_M_k_q(coulmat_sqrt_r, coulomb_vertex_block_ss)

       end subroutine CC4S_build_vertex_block_real

!*******************************************************
       subroutine CC4S_build_vertex_block_cmplx(aomo_lvl_k_c, &
                                               &aomo_lvl_q_c, &
                                               &scf_eigvecs_k_c, &
                                               &scf_eigvecs_q_c, &
                                               &coulmat_sqrt_c, &
                                               &i_spin, &
                                               &i_k_point, &
                                               &i_q_point, &
                                               &coulomb_vertex_block, &
                                               &save_M_k_q, &
                                               &transpose_V)
         implicit none
         
         !INPUT:
         !  *aomo_lvl_k_c     : RI-LVL-coefficient tensor, where one AO-dimension
         !                      has been transformed to MO with crystal momentum k
         !  *aomo_lvl_q_c     : RI-LVL-coefficient tensor, where one AO-dimension
         !                      has been transformed to MO with crystal momentum q
         !  *scf_eigvecs_k_c  : HF/KS-eigenvectors from scf-calculation for k-point
         !  *scf_eigvecs_q_c  : HF/KS-eigenvectors from scf-calculation for q-point
         !  *coulmat_sqrt_c   : square-root of auxiliary Coulomb matrix V
         !  *i_spin           : spin-channel for which Coulomb vertex block will be
         !                      constructed (i_spin=1 or i_spin=2)
         !  *i_k_point        : First crystal-momentum vector k
         !  *i_q_point        : Second crystal-momentum vector q 

         !input
         double complex, dimension(:,:,:,:), intent(in) :: aomo_lvl_k_c
         double complex, dimension(:,:,:,:), intent(in) :: aomo_lvl_q_c
         double complex, dimension(:,:), intent(in) :: scf_eigvecs_k_c
         double complex, dimension(:,:), intent(in) :: scf_eigvecs_q_c
         double complex, dimension(:,:), intent(in) :: coulmat_sqrt_c
         integer, intent(in) :: i_spin
         integer, intent(in) :: i_k_point
         integer, intent(in) :: i_q_point
         logical, optional, intent(in) :: save_M_k_q
         logical, optional, intent(in) :: transpose_V

         !output
         double complex, dimension(:,:,:,:), target, intent(inout) :: coulomb_vertex_block

         !local
         !ss = single spin
         double complex, dimension(:,:,:), pointer :: coulomb_vertex_block_ss  => null()
         logical :: internal_save_M_k_q
         logical :: internal_transpose_V

         if(present(save_M_k_q)) then
           internal_save_M_k_q = save_M_k_q
         else
           internal_save_M_k_q = .false.
         endif

         if(present(transpose_V)) then
           internal_transpose_V = transpose_V
         else
           internal_transpose_V = .false.
         end if

         call CC4S_compute_M_k_q(i_spin, i_k_point, i_q_point, &
                                &scf_eigvecs_k_c, scf_eigvecs_q_c, &
                                &aomo_lvl_k_c, aomo_lvl_q_c, &
                                &coulomb_vertex_block, &
                                &save_co_densities=internal_save_M_k_q)

         coulomb_vertex_block_ss => coulomb_vertex_block(:,:,:,i_spin)

         call CC4S_v_times_M_k_q(coulmat_sqrt_c, coulomb_vertex_block_ss, internal_transpose_V)

       end subroutine CC4S_build_vertex_block_cmplx
!*************************************************************
!NEWNEWNEWNEWNEW
       subroutine CC4S_build_vertex_block_riv_real(aomo_v_kq_r, &
                                              &scf_eigvecs_q_r, &
                                              &coulmat_sqrt_r, &
                                              &i_spin, &
                                              &i_k_point, &
                                              &i_q_point, &
                                              &coulomb_vertex_block)
         implicit none
         
         !INPUT:
         !  *aomo_v_kq_r     : RI-V-coefficient tensor, where one AO-dimension
         !                      has been transformed to MO with crystal momentum k
         !  *scf_eigvecs_q_r  : HF/KS-eigenvectors from scf-calculation for q-point
         !  *coulmat_sqrt_r   : square-root of auxiliary Coulomb matrix V
         !  *i_spin           : spin-channel for which Coulomb vertex block will be
         !                      constructed (i_spin=1 or i_spin=2)
         !  *i_k_point        : First crystal-momentum vector k
         !  *i_q_point        : Second crystal-momentum vector q 

         !input
         double precision, dimension(:,:,:,:), intent(in) :: aomo_v_kq_r
         double precision, dimension(:,:), intent(in) :: scf_eigvecs_q_r
         double precision, dimension(:,:), intent(in) :: coulmat_sqrt_r
         integer, intent(in) :: i_spin
         integer, intent(in) :: i_k_point
         integer, intent(in) :: i_q_point

         !output
         double precision, dimension(:,:,:,:), target, intent(inout) :: coulomb_vertex_block

         !local
         !ss = single spin
         double precision, dimension(:,:,:), pointer :: coulomb_vertex_block_ss  => null()
         integer :: i_state
         integer :: loc_bb_dim, loc_states_dim
         double precision, dimension(:,:), allocatable :: temp_aomo_v_r
         double precision, dimension(:,:), allocatable :: temp_coulomb_vertex_block


         !Perform second AO->MO transformation of RI-V coefficients
         loc_bb_dim = CC4S_ubb_row - CC4S_lbb_row + 1
         loc_states_dim = CC4S_ustates_col - CC4S_lstates_col + 1

         allocate(temp_aomo_v_r(size(aomo_v_kq_r,1), size(aomo_v_kq_r,3)))
         allocate(temp_coulomb_vertex_block(size(coulomb_vertex_block,1), size(coulomb_vertex_block,3)))

         do i_state=1,loc_states_dim
           temp_aomo_v_r(:,:) = aomo_v_kq_r(:,i_state,:,i_spin)
           temp_coulomb_vertex_block(:,:) = coulomb_vertex_block(:,i_state,:,i_spin)


           call dgemm('N', 'N', &
                   &loc_bb_dim, CC4S_nuf_states, CC4S_n_basis, 1.0D0, &
                   &temp_aomo_v_r(:,:), loc_bb_dim, &
                   &scf_eigvecs_q_r(:,CC4S_n_low_state:CC4S_n_states), CC4S_n_basis, 0.0D0, &
                   &temp_coulomb_vertex_block(:,:), loc_bb_dim)
 
           coulomb_vertex_block(:,i_state,:,i_spin) = temp_coulomb_vertex_block(:,:)
         end do
         coulomb_vertex_block_ss => coulomb_vertex_block(:,:,:,i_spin)


         call CC4S_v_times_M_k_q(coulmat_sqrt_r, coulomb_vertex_block_ss)

       end subroutine CC4S_build_vertex_block_riv_real
!*************************************************************
!NEWNEWNEWNEWNEW
       subroutine CC4S_build_vertex_block_riv_cmplx(aomo_v_kq_c, &
                                              &scf_eigvecs_q_c, &
                                              &coulmat_sqrt_c, &
                                              &i_spin, &
                                              &i_k_point, &
                                              &i_q_point, &
                                              &coulomb_vertex_block)
         implicit none
         
         !INPUT:
         !  *aomo_v_kq_c     : RI-V-coefficient tensor, where one AO-dimension
         !                      has been transformed to MO with crystal momentum k
         !  *scf_eigvecs_q_c  : HF/KS-eigenvectors from scf-calculation for q-point
         !  *coulmat_sqrt_c   : square-root of auxiliary Coulomb matrix V
         !  *i_spin           : spin-channel for which Coulomb vertex block will be
         !                      constructed (i_spin=1 or i_spin=2)
         !  *i_k_point        : First crystal-momentum vector k
         !  *i_q_point        : Second crystal-momentum vector q 

         !input
         double complex, dimension(:,:,:,:), intent(in) :: aomo_v_kq_c
         double complex, dimension(:,:), intent(in) :: scf_eigvecs_q_c
         double complex, dimension(:,:), intent(in) :: coulmat_sqrt_c
         integer, intent(in) :: i_spin
         integer, intent(in) :: i_k_point
         integer, intent(in) :: i_q_point

         !output
         double complex, dimension(:,:,:,:), target, intent(inout) :: coulomb_vertex_block

         !local
         !ss = single spin
         double complex, dimension(:,:,:), pointer :: coulomb_vertex_block_ss  => null()
         integer :: i_state
         integer :: loc_bb_dim, loc_states_dim
         double complex, dimension(:,:), allocatable :: temp_aomo_v_c
         double complex, dimension(:,:), allocatable :: temp_coulomb_vertex_block


         !Perform second AO->MO transformation of RI-V coefficients
         loc_bb_dim = CC4S_ubb_row - CC4S_lbb_row + 1
         loc_states_dim = CC4S_ustates_col - CC4S_lstates_col + 1

         allocate(temp_aomo_v_c(size(aomo_v_kq_c,1), size(aomo_v_kq_c,3)))
         allocate(temp_coulomb_vertex_block(size(coulomb_vertex_block,1), size(coulomb_vertex_block,3)))

         do i_state=1,loc_states_dim
           temp_aomo_v_c(:,:) = aomo_v_kq_c(:,i_state,:,i_spin)
           temp_coulomb_vertex_block(:,:) = coulomb_vertex_block(:,i_state,:,i_spin)


           call zgemm('N', 'N', &
                   &loc_bb_dim, CC4S_nuf_states, CC4S_n_basis, (1.0D0, 0.0D0), &
                   &temp_aomo_v_c(:,:), loc_bb_dim, &
                   &conjg(scf_eigvecs_q_c(:,:)), CC4S_n_basis, (0.0D0, 0.0D0), &
                   &temp_coulomb_vertex_block(:,:), loc_bb_dim)

           coulomb_vertex_block(:,i_state,:,i_spin) = temp_coulomb_vertex_block(:,:)
         end do
         coulomb_vertex_block_ss => coulomb_vertex_block(:,:,:,i_spin)


         call CC4S_v_times_M_k_q(coulmat_sqrt_c, coulomb_vertex_block_ss)

       end subroutine CC4S_build_vertex_block_riv_cmplx

end module CC4S_vertex_block
