module CC4S_sqrt_coulmat

        interface CC4S_sqrt_scalapack
                module procedure CC4S_sqrt_aux_coulmat_r_sclpck
                module procedure CC4S_sqrt_aux_coulmat_c_sclpck
        end interface CC4S_sqrt_scalapack

        interface CC4S_sqrt_lapack
                module procedure CC4S_sqrt_aux_coulmat_r_lpck
                module procedure CC4S_sqrt_aux_coulmat_c_lpck
        end interface CC4S_sqrt_lapack

contains
subroutine CC4S_sqrt_aux_coulmat_r_sclpck(coulmat, n_bb, coulmat_desc, &
                &loc_shape_coulmat, comm, debug, power)
        use mpi
        use CC4S_blacs_environment, only: CC4S_nprow, CC4S_npcol, CC4S_myid_row, CC4S_myid_col
        implicit none

        !PURPOSE: Compute the matrix-sqrt of the auxiliary coulomb matrix

        !INPUT: *coulmat: 2D-array (matrix) of global dimension
        !                   (number of aux. functions) x (number of aux. functions).
        !                   This is the auxiliary coulomb matrix in recip. space
        !                   and is the fourier transform of 
        !                   V_{\mu\nu}=\int P_{\mu}(\bm{r})\frac{1}{\bm{r}-\bm{r'}}P_{\nu}(\bm{r'})
        !                   A definition where the cut-Coulomb operator is used can be found in
        !                   the publication "Hybrid functions for large periodic systems in an 
        !                   all-electron numeric atom-centered basis framework" by 
        !                   Levchenko et al. in equation (31),(32) and the subsequent, 
        !                   non-enumerated equation.
        !                   The square root of coulmat is saved in coulmat upon return.
        !
        !       *n_bb:      Number of aux. basis functions used. The aux. Coulomb matrix coulmat is 
        !                   (global) shape n_bb x n_bb
        !
        !       *coulmat_descr: Scalapack array descriptor of coulmat. The matrix distributed 
        !                         is expected to be non-cyclicly blocked.
        !
        !       *loc_shape_coulmat: integer 1D array with 4 entries containing the 
        !                           process-local index-boundaries of coulmat:
        !                           loc_shape_coulmat(1)=lowest row-index
        !                           loc_shape_coulmat(2)=highest row-index
        !                           loc_shape_coulmat(3)=lowest column-index
        !                           loc_shape_coulmat(4)=highest column-index
        !
        !       *comm             : MPI-Communicator corresponding to the blacs context
        !
        !       *debug            : .true. for more verbose output of subroutine


        !input
        integer, dimension(4), intent(in) :: loc_shape_coulmat
        double precision, dimension(loc_shape_coulmat(1):loc_shape_coulmat(2), &
                                   &loc_shape_coulmat(3):loc_shape_coulmat(4)), &
                                   &intent(inout) :: coulmat
        integer, intent(in) :: n_bb
        integer, dimension(9), intent(in) :: coulmat_desc
        integer, intent(in) :: comm
        logical, intent(in) :: debug
        double precision, optional, intent(in) :: power

        !local 
        integer, dimension(2) :: coulmat_dims !Local dimensions of coulmat
        integer :: lwork !Scalapack PDSYEV: length of work array
        double precision, dimension(:), allocatable :: work !Scalapack PDSYEV: work array
        integer :: info !Scalapack PDSYEV: error-info
        double precision, dimension(:), allocatable :: eigenvalues !Scalapack PDSYEV: contains eigenvalues
        double precision :: eig_thr !Numerical threshold of small eigenvalues
        integer :: n_nonsingular !Number of acceptable eigenvalues to be kept
        integer :: lc !Counter of column
        double precision, dimension(:,:), allocatable :: coulmat_H    !Stores the herm. adjoint of coulmat
        double precision, dimension(:,:), allocatable :: coulmat_H_cp 
        double precision, dimension(:,:), allocatable :: coulmat_cp, coulmat_cp2   !Only used for debug=.true.
        integer :: ierr !Generic error flag
        integer :: i_eigval 
        double precision :: checkloc, checkglob

        integer :: myid

        double precision :: internal_power

        call MPI_Comm_rank(comm, myid, ierr)

        if(present(power)) then
          internal_power = power
        else
          !default behaviour : take sqrt
          internal_power = 0.50d0
        end if

        if(myid .eq. 0) then
          write(*,*) "Calculating square root using Scalapack"
        end if 
        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1

        !Symmetrize the coulmat beforehand
        allocate(coulmat_H(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_H_cp(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_cp2(coulmat_dims(1), coulmat_dims(2)))

        coulmat_H(:,:)=0.0D0
        coulmat_H_cp(:,:)=0.0D0

        call pdtran(n_bb, n_bb, 1.0D0, coulmat, 1, 1, coulmat_desc, &
                &0.0D0, coulmat_H, 1, 1, coulmat_desc)

        if(debug) then
          checkloc=sum(abs(coulmat-coulmat_H))
          call mpi_allreduce(checkloc, checkglob,1,&
          &MPI_DOUBLE_PRECISION,MPI_SUM,comm,ierr)

          if(myid .eq. 0) then
          write(*,'(A,1X,ES10.3)') "Difference between Coulomb matrix and its hermitian adjoint:",&
              &checkglob
          end if 
        end if 

        coulmat(:,:)=0.5D0*(coulmat(:,:)+coulmat_H(:,:))
        coulmat_cp2(:,:)=coulmat(:,:)
        !Negate symmetrized coulmat, because Scalapack PDSYEV outputs the eigenvalues in ascending order
        !To filter out the low/negative eigenvalues efficiently we reverse the order by negation
        coulmat(:,:)=-coulmat(:,:)

        !Perform eigenvalue decomposition. After return, eigenvalues(:) will contain the negated 
        !eigenvalues in descending order and coulmat_H will contain the corresponding eigenvectors
        !(also referred to as U in the comments)

        allocate(eigenvalues(n_bb))

        lwork=-1
        allocate(work(1))
        call PDSYEV('V', 'U', n_bb, coulmat(:,:), 1, 1, coulmat_desc, &
                &eigenvalues(:), coulmat_H, 1, 1, coulmat_desc, &
                &work, lwork, info)
        lwork=int(work(1))
        deallocate(work)
        allocate(work(lwork))

        call PDSYEV('V', 'U', n_bb, coulmat(:,:), 1, 1, coulmat_desc, &
                &eigenvalues(:), coulmat_H, 1, 1, coulmat_desc, &
                &work, lwork, info)

        !Correct for previous negative sign
        eigenvalues(:)=-eigenvalues(:)

        !Determine smallest acceptable eigenvalue to be square-rooted
        !eig_thr=tiny(0.0D0)

        if(myid .eq. 0) then
          open(unit=554, file='eigvals_V.dat')
          write(554,*) eigenvalues(:)
          close(unit=554)
        end if

        if(internal_power < 0.0d0) then
          !If the power implies inversion, a more conservative threshold is needed
          !TODO: Replace 1d-7 by user-specified value
          eig_thr=1d-5
        else
          eig_thr=tiny(0.0D0)
        end if

        do i_eigval=1,n_bb
          if(eigenvalues(i_eigval) < eig_thr) then
            exit
          end if 
        end do

        n_nonsingular=i_eigval-1

        if(myid .eq. 0) then
          write(*,*) "Of", size(eigenvalues), "eigenvalues,", n_nonsingular, &
            & "are fine"
        end if 

        where(eigenvalues(:) < eig_thr) eigenvalues=0.0D0

        !Calculate U x diag(coulmat)^0.5 x U^H
        !Perform manual left side multiplication: U x diag(coulmat)^0.5

        coulmat_H_cp(:,:)=coulmat_H(:,:)
        lc=0
        do i_eigval=1,n_nonsingular,1
          if(mod((i_eigval-1)/coulmat_desc(6), CC4S_npcol) .eq. CC4S_myid_col) then
            lc=lc+1 
            !coulmat_H(:,lc)=coulmat_H(:,lc)*eigenvalues(i_eigval)**0.5D0
            coulmat_H(:,lc)=coulmat_H(:,lc)*eigenvalues(i_eigval)**internal_power
          end if 
        end do

        !Perform right side multiplication: (U x diag(coulmat)^0.5) x U^H
        call pdgemm('N', 'C', n_bb, n_bb, n_nonsingular, 1.0D0, coulmat_H(:,:), &
                &1, 1, coulmat_desc, coulmat_H_cp(:,:), 1, 1, coulmat_desc, 0.0D0, &
                &coulmat(:,:), 1, 1, coulmat_desc)

        if(debug .and. internal_power==0.50d0) then
          !Determine accuracy of sqrt
          allocate(coulmat_cp(coulmat_dims(1), coulmat_dims(2)))
          coulmat_cp(:,:)=coulmat(:,:)
          coulmat_H(:,:)=0.0D0
          call pdgemm('N', 'N', n_bb, n_bb, n_bb, 1.0D0, coulmat(:,:), &
                  &1, 1, coulmat_desc, coulmat_cp(:,:), 1, 1, coulmat_desc, &
                  &0.0D0, coulmat_H(:,:), 1, 1, coulmat_desc)

          checkglob=0.0D0
          checkloc=0.0D0
          checkloc=sum(abs(coulmat_cp2-coulmat_H))
          call MPI_Allreduce(checkloc, checkglob, 1, MPI_DOUBLE_PRECISION, MPI_SUM, &
                  &comm, ierr)

          if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
            write(*,'(A12, 1X, ES10.3)') "V-sqrt(V)**2", checkglob/(n_bb**2)
          end if 
        end if 

end subroutine CC4S_sqrt_aux_coulmat_r_sclpck
!*********************************************************************************
subroutine CC4S_sqrt_aux_coulmat_r_lpck(coulmat, n_bb, coulmat_desc, &
                &loc_shape_coulmat, comm, debug, power)
        use mpi
        implicit none

        !PURPOSE: Compute the matrix-sqrt of the auxiliary coulomb matrix

        !INPUT: *coulmat: 2D-array (matrix) of global dimension
        !                   (number of aux. functions) x (number of aux. functions).
        !                   This is the auxiliary coulomb matrix in recip. space
        !                   and is the fourier transform of 
        !                   V_{\mu\nu}=\int P_{\mu}(\bm{r})\frac{1}{\bm{r}-\bm{r'}}P_{\nu}(\bm{r'})
        !                   A definition where the cut-Coulomb operator is used can be found in
        !                   the publication "Hybrid functions for large periodic systems in an 
        !                   all-electron numeric atom-centered basis framework" by 
        !                   Levchenko et al. in equation (31),(32) and the subsequent, 
        !                   non-enumerated equation.
        !                   The square root of coulmat is saved in coulmat upon return.
        !
        !       *n_bb:      Number of aux. basis functions used. The aux. Coulomb matrix coulmat is 
        !                   (global) shape n_bb x n_bb
        !
        !       *coulmat_descr: Scalapack array descriptor of coulmat. The matrix distributed 
        !                         is expected to be non-cyclicly blocked.
        !
        !       *loc_shape_coulmat: integer 1D array with 4 entries containing the 
        !                           process-local index-boundaries of coulmat:
        !                           loc_shape_coulmat(1)=lowest row-index
        !                           loc_shape_coulmat(2)=highest row-index
        !                           loc_shape_coulmat(3)=lowest column-index
        !                           loc_shape_coulmat(4)=highest column-index
        !
        !       *comm             : MPI-Communicator corresponding to the blacs context
        !
        !       *debug            : .true. for more verbose output of subroutine


        !input
        integer, dimension(4), intent(in) :: loc_shape_coulmat
        double precision, dimension(loc_shape_coulmat(1):loc_shape_coulmat(2), &
                                   &loc_shape_coulmat(3):loc_shape_coulmat(4)), &
                                   &intent(inout) :: coulmat
        integer, intent(in) :: n_bb
        integer, dimension(9), intent(in) :: coulmat_desc
        integer, intent(in) :: comm
        logical, intent(in) :: debug
        double precision, optional, intent(in) :: power

        !local 
        integer, dimension(2) :: coulmat_dims !Local dimensions of coulmat
        integer :: lwork !Scalapack PDSYEV: length of work array
        double precision, dimension(:), allocatable :: work !Scalapack PDSYEV: work array
        integer :: info !Scalapack PDSYEV: error-info
        double precision, dimension(:), allocatable :: eigenvalues !Scalapack PDSYEV: contains eigenvalues
        double precision :: eig_thr !Numerical threshold of small eigenvalues
        integer :: n_nonsingular !Number of acceptable eigenvalues to be kept
        integer :: lc !Counter of column
        integer :: nprow, npcol !Size of process grid
        integer :: myid_row, myid_col !Coordinates of current process
        double precision, dimension(:,:), allocatable :: coulmat_H    !Stores the herm. adjoint of coulmat
        double precision, dimension(:,:), allocatable :: coulmat_eigvec, coulmat2 
        double precision, dimension(:,:), allocatable :: coulmat_H_cp 
        double precision, dimension(:,:), allocatable :: coulmat_cp   !Only used for debug=.true.
        integer :: ierr !Generic error flag
        integer :: i_eigval 
        double precision :: checkloc, checkglob

        integer :: myid

        double precision :: internal_power

        call MPI_Comm_rank(comm, myid, ierr)

        if(present(power)) then
          internal_power = power
        else
          !default behaviour : take sqrt
          internal_power = 0.50d0
        end if

        write(*,*) "Calculating square root using Lapack"
        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1

        !Symmetrize the coulmat beforehand
        allocate(coulmat_H(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_H_cp(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_cp(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat2(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_eigvec(coulmat_dims(1), coulmat_dims(2)))
        coulmat_H(:,:)=0.0D0
        coulmat_H_cp(:,:)=0.0D0


        call pdtran(n_bb, n_bb, 1.0D0, coulmat, 1, 1, coulmat_desc, &
                &0.0D0, coulmat_H, 1, 1, coulmat_desc)

        coulmat2(:,:)=0.5D0*(coulmat(:,:)+coulmat_H(:,:))
        coulmat_cp(:,:)=coulmat2(:,:)

        allocate(eigenvalues(n_bb))

        coulmat2(:,:) = -coulmat2(:,:)

        lwork=-1
        allocate(work(1))
        call DSYEV('V', 'U', n_bb, coulmat2(:,:), n_bb, eigenvalues, work, lwork, info)

        lwork=int(work(1))
        deallocate(work)
        allocate(work(lwork))

        call DSYEV('V', 'U', n_bb, coulmat2(:,:), n_bb, eigenvalues, work, lwork, info)

        eigenvalues(:) = -eigenvalues(:)
        coulmat_eigvec(:,:)=coulmat2(:,:)

        !Determine smallest acceptable eigenvalue to be square-rooted
        !eig_thr=tiny(0.0D0)
        if(internal_power < 0.0d0) then
          !If the power implies inversion, a more conservative threshold is needed
          !TODO: Replace 1d-7 by user-specified value
          eig_thr=1d-5
        else
          eig_thr=tiny(0.0D0)
        end if

        write(*,*) "Eigenvalues below", eig_thr, "are ignored"
        do i_eigval=1,n_bb,1
          if(eigenvalues(i_eigval) < eig_thr) then
            exit
          end if 
        end do

        where(eigenvalues(:) < 0.0D0) eigenvalues=0.0D0
        n_nonsingular=i_eigval-1

        write(*,*) "Of", size(eigenvalues), "eigenvalues,", n_nonsingular, &
            & "are fine"


        !Calculate U x diag(coulmat)^0.5 x U^H
        !Perform manual left side multiplication: U x diag(coulmat)^0.5

        do i_eigval=1,n_nonsingular,1
          coulmat2(:,i_eigval)=coulmat2(:,i_eigval)*(eigenvalues(i_eigval)**internal_power)
        end do

        !Perform right side multiplication: (U x diag(coulmat)^0.5) x U^H
        coulmat(:,:)=0.0D0
        call dgemm('N', 'C', n_bb, n_bb, n_nonsingular, 1.0D0, coulmat2(:,:), n_bb, &
                &coulmat_eigvec(:,:), n_bb, 0.0D0, coulmat(:,:), n_bb)

        if(debug .and. (myid .eq. 0) .and. internal_power==0.50d0) then
          !Determine accuracy of sqrt
          write(*,'(A12, 1X, ES10.3)') "V-sqrt(V)**2", sum(abs(coulmat_cp-matmul(coulmat,coulmat)))/(n_bb**2)
        end if 
end subroutine CC4S_sqrt_aux_coulmat_r_lpck
!*********************************************************
subroutine CC4S_sqrt_aux_coulmat_c_sclpck(coulmat, n_bb, coulmat_desc, &
                &loc_shape_coulmat, comm, debug, power)
        use mpi
        use CC4S_blacs_environment, only: CC4S_nprow, CC4S_npcol, CC4S_myid_row, CC4S_myid_col
        implicit none

        integer, dimension(4), intent(in) :: loc_shape_coulmat
        double complex, dimension(loc_shape_coulmat(1):loc_shape_coulmat(2), &
                                   &loc_shape_coulmat(3):loc_shape_coulmat(4)), &
                                   &intent(inout) :: coulmat
        integer, intent(in) :: n_bb
        integer, dimension(9), intent(in) :: coulmat_desc
        integer, intent(in) :: comm
        logical, intent(in) :: debug
        double precision, optional, intent(in) :: power

        !local 
        integer, dimension(2) :: coulmat_dims !Local dimensions of coulmat
        integer :: lwork, lrwork !Scalapack PZHEEV: length of work array
        double complex, dimension(:), allocatable :: work, rwork !Scalapack PZHEEV: work array
        integer :: info !Scalapack PZHEEV: error-info
        double precision, dimension(:), allocatable :: eigenvalues !Scalapack PZHEEV: contains eigenvalues
        double precision :: eig_thr !Numerical threshold of small eigenvalues
        integer :: n_nonsingular !Number of acceptable eigenvalues to be kept
        integer :: lc !Counter of column
        double complex, dimension(:,:), allocatable :: coulmat_H    !Stores the herm. adjoint of coulmat
        double complex, dimension(:,:), allocatable :: coulmat_H_cp 
        double complex, dimension(:,:), allocatable :: coulmat_cp, coulmat_cp2   !Only used for debug=.true.
        integer :: ierr !Generic error flag
        integer :: i_eigval 
        double precision :: checkloc, checkglob

        integer :: myid

        double precision :: internal_power

        call MPI_Comm_rank(comm, myid, ierr)

        if(present(power)) then
          internal_power = power
        else
          !default behaviour : take sqrt
          internal_power = 0.50d0 
        end if

        if(myid .eq. 0) then
          write(*,*) "Calculating square root using Scalapack (complex)"
        end if 
        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1

        !Symmetrize the coulmat beforehand
        allocate(coulmat_H(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_H_cp(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_cp2(coulmat_dims(1), coulmat_dims(2)))

        coulmat_H(:,:)=0.0D0
        coulmat_H_cp(:,:)=0.0D0

        call pztranc(n_bb, n_bb, (1.0D0,0.0D0), coulmat, 1, 1, coulmat_desc, &
                &(0.0D0, 0.0D0), coulmat_H, 1, 1, coulmat_desc)

        if(debug) then
          checkloc=sum(abs(coulmat-coulmat_H))
          call mpi_allreduce(checkloc, checkglob,1,&
          &MPI_DOUBLE_PRECISION,MPI_SUM,comm,ierr)
          if(myid .eq. 0) then
            write(*,'(A,1X,ES10.3)') "Difference between Coulomb matrix and its hermitian adjoint:",&
              &checkglob
          end if 
        end if 

        coulmat(:,:)=0.5D0*(coulmat(:,:)+coulmat_H(:,:))

        coulmat_cp2(:,:)=coulmat(:,:)
        !Negate symmetrized coulmat, because Scalapack PDSYEV outputs the eigenvalues in ascending order
        !To filter out the low/negative eigenvalues efficiently we reverse the order by negation
        coulmat(:,:)=-coulmat(:,:)

        !Perform eigenvalue decomposition. After return, eigenvalues(:) will contain the negated 
        !eigenvalues in descending order and coulmat_H will contain the corresponding eigenvectors
        !(also referred to as U in the comments)

        allocate(eigenvalues(n_bb))

        lwork=-1
        lrwork=-1
        allocate(work(1), rwork(1))

        call PZHEEV('V', 'U', n_bb, coulmat(:,:), 1, 1, coulmat_desc, &
                &eigenvalues(:), coulmat_H, 1, 1, coulmat_desc, &
                &work, lwork, rwork, lrwork, info)
        lwork=int(work(1))
        lrwork=int(rwork(1))
        deallocate(work, rwork)
        allocate(work(lwork), rwork(lrwork))

        call PZHEEV('V', 'U', n_bb, coulmat(:,:), 1, 1, coulmat_desc, &
                &eigenvalues(:), coulmat_H, 1, 1, coulmat_desc, &
                &work, lwork, rwork, lrwork, info)

        !Correct for previous negative sign
        eigenvalues(:)=-eigenvalues(:)

        !if(myid .eq. 0) then
        !  open(unit=554, file='eigvals.dat')
        !  write(554,*) eigenvalues(:)
        !  close(unit=554)
        !end if 

        !Determine smallest acceptable eigenvalue to be square-rooted
        if(internal_power < 0.0d0) then
          !If the power implies inversion, a more conservative threshold is needed
          !TODO: Replace 1d-7 by user-specified value
          eig_thr=1d-5
          if(myid .eq. 0) then
            write(*,*) "Negative power(",internal_power, ") of matrix has been requested"
            write(*,*) "Eigenvalue threshold will be adjusted to", eig_thr
          end if
        else
          eig_thr=tiny(0.0D0)
        end if

        do i_eigval=1,n_bb
          if(eigenvalues(i_eigval) < eig_thr) then
            exit
          end if 
        end do

        n_nonsingular=i_eigval-1

        if(myid .eq. 0) then
          write(*,*) "Of", size(eigenvalues), "eigenvalues,", n_nonsingular, &
            & "are fine"
        end if 

        where(eigenvalues(:) < eig_thr) eigenvalues=0.0D0

        !Calculate U x diag(coulmat)^0.5 x U^H
        !Perform manual left side multiplication: U x diag(coulmat)^0.5

        coulmat_H_cp(:,:)=coulmat_H(:,:)
        lc=0
        do i_eigval=1,n_nonsingular,1
          if(mod((i_eigval-1)/coulmat_desc(6), CC4S_npcol) .eq. CC4S_myid_col) then
            lc=lc+1 
            !coulmat_H(:,lc)=coulmat_H(:,lc)*eigenvalues(i_eigval)**0.5D0
            coulmat_H(:,lc)=coulmat_H(:,lc)*eigenvalues(i_eigval)**internal_power
          end if 
        end do

        !Perform right side multiplication: (U x diag(coulmat)^0.5) x U^H
        call pzgemm('N', 'C', n_bb, n_bb, n_nonsingular, (1.0D0, 0.0D0), coulmat_H(:,:), &
                &1, 1, coulmat_desc, coulmat_H_cp(:,:), 1, 1, coulmat_desc, (0.0D0, 0.0D0), &
                &coulmat(:,:), 1, 1, coulmat_desc)

        if(debug .and. internal_power==0.50d0) then
          !Determine accuracy of sqrt
          allocate(coulmat_cp(coulmat_dims(1), coulmat_dims(2)))
          coulmat_cp(:,:)=coulmat(:,:)
          coulmat_H(:,:)=0.0D0
          call pzgemm('N', 'N', n_bb, n_bb, n_bb, (1.0D0, 0.0D0), coulmat(:,:), &
                  &1, 1, coulmat_desc, coulmat_cp(:,:), 1, 1, coulmat_desc, &
                  &(0.0D0, 0.0D0), coulmat_H(:,:), 1, 1, coulmat_desc)

          checkglob=0.0D0
          checkloc=0.0D0
          checkloc=sum(abs(coulmat_cp2-coulmat_H))
          call MPI_Allreduce(checkloc, checkglob, 1, MPI_DOUBLE_PRECISION, MPI_SUM, &
                  &comm, ierr)
    
          if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
            write(*,'(A12, 1X, ES10.3)') "V-sqrt(V)**2", checkglob/(n_bb**2)
          end if
        end if 

end subroutine CC4S_sqrt_aux_coulmat_c_sclpck
!*********************************************************************
subroutine CC4S_sqrt_aux_coulmat_c_lpck(coulmat, n_bb, coulmat_desc, &
                &loc_shape_coulmat, comm, debug, power)
        use mpi
        implicit none

        !input
        integer, dimension(4), intent(in) :: loc_shape_coulmat
        double complex, dimension(loc_shape_coulmat(1):loc_shape_coulmat(2), &
                                   &loc_shape_coulmat(3):loc_shape_coulmat(4)), &
                                   &intent(inout) :: coulmat
        integer, intent(in) :: n_bb
        integer, dimension(9), intent(in) :: coulmat_desc
        integer, intent(in) :: comm
        logical, intent(in) :: debug
        double precision, optional, intent(in) :: power

        !local 
        integer, dimension(2) :: coulmat_dims !Local dimensions of coulmat
        integer :: lwork !Scalapack PDSYEV: length of work array
        double complex, dimension(:), allocatable :: work !Scalapack PDSYEV: work array
        double precision, dimension(:), allocatable :: rwork
        integer :: info !Scalapack PDSYEV: error-info
        double precision, dimension(:), allocatable :: eigenvalues !Scalapack PDSYEV: contains eigenvalues
        double precision :: eig_thr !Numerical threshold of small eigenvalues
        integer :: n_nonsingular !Number of acceptable eigenvalues to be kept
        integer :: lc !Counter of column
        integer :: nprow, npcol !Size of process grid
        integer :: myid_row, myid_col !Coordinates of current process
        double complex, dimension(:,:), allocatable :: coulmat_H    !Stores the herm. adjoint of coulmat
        double complex, dimension(:,:), allocatable :: coulmat_eigvec, coulmat2 
        double complex, dimension(:,:), allocatable :: coulmat_H_cp 
        double complex, dimension(:,:), allocatable :: coulmat_cp   !Only used for debug=.true.
        integer :: ierr !Generic error flag
        integer :: i_eigval 
        double precision :: checkloc, checkglob

        integer :: myid

        double precision :: internal_power

        call MPI_Comm_rank(comm, myid, ierr)

        if(present(power)) then
          internal_power = power
        else
          !default behaviour : take sqrt
          internal_power = 0.50d0
        end if

        write(*,*) "Calculating square root using Lapack (complex)"
        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1

        !Symmetrize the coulmat beforehand
        allocate(coulmat_H(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_H_cp(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_cp(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat2(coulmat_dims(1), coulmat_dims(2)))
        allocate(coulmat_eigvec(coulmat_dims(1), coulmat_dims(2)))
        coulmat_H(:,:)=(0.0D0, 0.0D0)
        coulmat_H_cp(:,:)=(0.0D0, 0.0D0)

        call pztranc(n_bb, n_bb, (1.0D0, 0.0D0), coulmat, 1, 1, coulmat_desc, &
                &(0.0D0, 0.0D0), coulmat_H, 1, 1, coulmat_desc)

        coulmat2(:,:)=0.5D0*(coulmat(:,:)+coulmat_H(:,:))
        coulmat_cp(:,:)=coulmat2(:,:)

        allocate(eigenvalues(n_bb))

        coulmat2(:,:) = -coulmat2(:,:)

        lwork=-1
        allocate(work(1))
        allocate(rwork(3*n_bb-2))
        call ZHEEV('V', 'U', n_bb, coulmat2(:,:), n_bb, eigenvalues, work, lwork, rwork, info)

        lwork=int(work(1))
        deallocate(work)
        allocate(work(lwork))

        call ZHEEV('V', 'U', n_bb, coulmat2(:,:), n_bb, eigenvalues, work, lwork, rwork, info)

        eigenvalues(:) = -eigenvalues(:)
        coulmat_eigvec(:,:)=coulmat2(:,:)

        !Determine smallest acceptable eigenvalue to be square-rooted
        !eig_thr=tiny(0.0D0)
        if(internal_power < 0.0d0) then
          !If the power implies inversion, a more conservative threshold is needed
          !TODO: Replace 1d-7 by user-specified value
          eig_thr=1d-5
        else
          eig_thr=tiny(0.0D0)
        end if

        write(*,*) "Eigenvalues below", eig_thr, "are ignored"
        do i_eigval=1,n_bb,1
          if(eigenvalues(i_eigval) < eig_thr) then
            exit
          end if 
        end do

        where(eigenvalues(:) < 0.0D0) eigenvalues=0.0D0
        n_nonsingular=i_eigval-1


        write(*,*) "Of", size(eigenvalues), "eigenvalues,", n_nonsingular, &
            & "are fine"

        !Calculate U x diag(coulmat)^0.5 x U^H
        !Perform manual left side multiplication: U x diag(coulmat)^0.5

        do i_eigval=1,n_nonsingular,1
          !coulmat2(:,i_eigval)=coulmat2(:,i_eigval)*(eigenvalues(i_eigval)**0.5D0)
          coulmat2(:,i_eigval)=coulmat2(:,i_eigval)*(eigenvalues(i_eigval)**internal_power)
        end do

        !Perform right side multiplication: (U x diag(coulmat)^0.5) x U^H
        coulmat(:,:)=(0.0D0, 0.0D0)
        call zgemm('N', 'C', n_bb, n_bb, n_nonsingular, (1.0D0, 0.0D0), coulmat2(:,:), n_bb, &
                &coulmat_eigvec(:,:), n_bb, (0.0D0, 0.0D0), coulmat(:,:), n_bb)

        if(debug .and. (myid .eq. 0) .and. internal_power==0.50d0) then
          !Determine accuracy of sqrt
          write(*,'(A12, 1X, ES10.3)') "V-sqrt(V)**2", sum(abs(coulmat_cp-matmul(coulmat,coulmat)))/(n_bb**2)
        end if 
end subroutine CC4S_sqrt_aux_coulmat_c_lpck
!*******************************************************
subroutine CC4S_compute_pseudo_inverse_via_svd(n_bb, lbb_row, ubb_row, lbb_col, ubb_col, &
                                               &overlap_at_k, overlap_desc, comm_blacs, debug)

    use mpi
    use CC4S_blacs_environment, only: CC4S_nprow, CC4S_npcol, CC4S_myid_row, CC4S_myid_col
    implicit none

    integer, intent(in) :: n_bb
    integer, intent(in) :: lbb_row, ubb_row, lbb_col, ubb_col
    double complex, dimension(lbb_row:ubb_row,lbb_col:ubb_col), intent(inout) :: overlap_at_k
    integer, dimension(9), intent(in) :: overlap_desc
    integer, intent(in) :: comm_blacs
    logical, optional, intent(in) :: debug

    !local
    double complex, allocatable, dimension(:,:) :: overlap_at_k_symm
    double precision :: checkloc, checkglob
    integer :: mpierr, info
    double precision, allocatable, dimension(:) :: singular_values
    double complex, allocatable, dimension(:,:) :: left_singular_vectors
    double complex, allocatable, dimension(:,:) :: right_singular_vectors
    integer :: lwork, lrwork
    double complex, allocatable, dimension(:) :: work, rwork
    double complex, allocatable, dimensioN(:,:) :: diagonal_singular_value_matrix
    integer :: i,j
    integer :: n_nonsingular
    logical :: internal_debug
    double complex, allocatable, dimension(:,:) :: debug_overlap_cp
    double complex, allocatable, dimension(:,:) :: id_matrix
    double complex, allocatable, dimension(:,:) :: S_times_invS
    integer :: ierr

    if(present(debug)) then
      internal_debug = debug
    else
      internal_debug = .false.
    end if

    if(internal_debug) then
      allocate(debug_overlap_cp(lbb_row:ubb_row,lbb_col:ubb_col))
      debug_overlap_cp(:,:) = overlap_at_k(:,:)
    end if

    !First, perform good ole symmterization of S
    allocate(overlap_at_k_symm(lbb_row:ubb_row,lbb_col:ubb_col))
    overlap_at_k_symm(:,:) = (0.0d0, 0.0d0)

    ! check if matrix is self-adjoint
    call pztranc( n_bb, n_bb, (1.0d0, 0.0d0), overlap_at_k(:,:), 1, 1, &
                &overlap_desc, (0.0d0, 0.0d0), overlap_at_k_symm(:,:), &
                &1, 1, overlap_desc )

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Calculated hermitian-adjoint before inversion"
    end if

    checkloc=sum(abs(overlap_at_k-overlap_at_k_symm))
    call mpi_allreduce(checkloc, checkglob,1,MPI_DOUBLE_PRECISION,MPI_SUM,comm_blacs,mpierr)

    if((checkglob.gt.5e-9).and.((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0))) write (*,'(2X,2A,ES14.4)') &
         'WARNING: input matrix in power_auxmat_scalapack_complex does not seem to be self-adjoint!',&
         ' Checksum: ',checkglob

! averaging the upper and lower triangles
    overlap_at_k_symm(:,:) = 0.5d0* (overlap_at_k(:,:) + overlap_at_k_symm(:,:))

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Symmetrize the to-be-inverted matrix"
    end if  

    allocate(singular_values(n_bb))
    allocate(left_singular_vectors(lbb_row:ubb_row, lbb_col:ubb_col))
    allocate(right_singular_vectors(lbb_row:ubb_row, lbb_col:ubb_col))
    singular_values(:) = 0.0d0
    left_singular_vectors(:,:) = (0.0d0, 0.0d0)
    right_singular_vectors(:,:) = (0.0d0, 0.0d0)

    lwork=-1
    lrwork=-1
    allocate(work(1))
    allocate(rwork(1))

    call pzgesvd('V', 'V', n_bb, n_bb, overlap_at_k_symm(:,:), 1, 1, overlap_desc, singular_values(:), &
                &left_singular_vectors(:,:), 1, 1, overlap_desc, right_singular_vectors(:,:), 1, 1, overlap_desc, &
                &work, lwork, rwork, lrwork, info)


    lwork=int(work(1)) 
    lrwork=int(rwork(1))

    deallocate(work)
    deallocate(rwork)

    allocate(work(lwork))
    allocate(rwork(lrwork))

    call pzgesvd('V', 'V', n_bb, n_bb, overlap_at_k_symm(:,:), 1, 1, overlap_desc, singular_values(:), &
                &left_singular_vectors(:,:), 1, 1, overlap_desc, right_singular_vectors(:,:), 1, 1, overlap_desc, &
                &work, lwork, rwork, lrwork, info)



    !singular_values(:) contains the values in DESCENDING order.
    !Calculate the inverse of the singular values above the threshold. Everything below, leave unchanged.
    !Alternative: set to 0
    do i=1,n_bb
      !if(singular_values(i) < prodbas_threshold) then
      if(singular_values(i) <= 0d0) then
        exit
      else
        singular_values(i) = 1.0d0/singular_values(i)
      end if
    end do

    n_nonsingular = i - 1

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Singular values of RI overlap range from ", 1.0d0/singular_values(1), " to ", &
                 &1.0d0/singular_values(n_nonsingular)
    end if

    allocate(diagonal_singular_value_matrix(lbb_row:ubb_row, lbb_col:ubb_col))
    diagonal_singular_value_matrix(:,:) = (0.0d0, 0.0d0)

    do i=lbb_row,ubb_row
      do j=lbb_col,ubb_col
        if(i==j) then
          diagonal_singular_value_matrix(i,j) = singular_values(i)
        end if
      end do
    end do

    !At this point overlap_at_k_symm is not needed anymore.
    !We can recycle it to store an intermediate result Sigma x U*
    overlap_at_k_symm(:,:) = (0.0d0, 0.0d0)
    call pzgemm('N', 'C', n_bb, n_bb, &
               n_bb, (1.0d0, 0.0d0), &
               diagonal_singular_value_matrix(:,:), &
               1, 1, overlap_desc, &
               left_singular_vectors(:,:), &
               1, 1, overlap_desc, (0.0d0, 0.0d0), &
               overlap_at_k_symm(:,:), &
               1, 1, overlap_desc &
              )

    !Compute pseudo-inverse as V x Sigma x U*
    call pzgemm('C', 'N', n_bb, n_bb, &
               n_bb, (1.0d0, 0.0d0), &
               right_singular_vectors(:,:), &
               1, 1, overlap_desc, &
               overlap_at_k_symm(:,:), &
               1, 1, overlap_desc, (0.0d0, 0.0d0), &
               overlap_at_k(:,:), &
               1, 1, overlap_desc &
              )  

    if(internal_debug) then
      !If debug is specified, test quality of interversion
      allocate(id_matrix(lbb_row:ubb_row, lbb_col:ubb_col))
      id_matrix(:,:) = (0.0d0, 0.0d0)
      do i=lbb_row,ubb_row
        do j=lbb_col,ubb_col
          if(i==j) then
            id_matrix(i,j) = (1.0d0, 0.0d0)
          end if
        end do
      end do

     allocate(S_times_invS(lbb_row:ubb_row, lbb_col:ubb_col))
     S_times_invS(:,:) = (0.0d0, 0.0d0)

     !Compute S * S^{-1}
     call pzgemm('N', 'N', n_bb, n_bb, &
               n_bb, (1.0d0, 0.0d0), &
               debug_overlap_cp(:,:), &
               1, 1, overlap_desc, &
               overlap_at_k(:,:), &
               1, 1, overlap_desc, (0.0d0, 0.0d0), &
               S_times_invS(:,:), &
               1, 1, overlap_desc &
              )

     checkglob=0.0D0
     checkloc=0.0D0
     checkloc=sum(abs(S_times_invS - id_matrix))
     call MPI_Allreduce(checkloc, checkglob, 1, MPI_DOUBLE_PRECISION, MPI_SUM, &
                  &comm_blacs, ierr)
    
          if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
            write(*,'(A14, 1X, ES10.3)') "S * S^{-1} - 1", checkglob/(n_bb**2)
          end if
    end if

  end subroutine CC4S_compute_pseudo_inverse_via_svd

  subroutine CC4S_compute_pseudo_inverse_via_svd_general(n_row_elements, n_col_elements, &
                        &l_row_original, u_row_original, l_col_original, u_col_original, &
                        &l_row_inverted, u_row_inverted, l_col_inverted, u_col_inverted, &
                        &original_matrix, original_matrix_desc, &
                        &inverted_matrix, inverted_matrix_desc, &
                        &n_left_row_opt, n_left_col_opt, n_right_row_opt, n_right_col_opt, original_matrix_opt_desc, &
                        &lefteig_desc, righteig_desc, lefteig_opt_desc, righteig_opt_desc, &
                        &comm_blacs, debug)

    use mpi
    use CC4S_blacs_environment, only: CC4S_nprow, CC4S_npcol, CC4S_myid_row, CC4S_myid_col
    implicit none

    integer, intent(in) :: n_row_elements, n_col_elements !Total number of rows and columns of original matrix (global)
    integer, intent(in) :: l_row_original, u_row_original !First and last row-index of original matrix (local)
    integer, intent(in) :: l_col_original, u_col_original !First and last column-index of original matrix (local)
    integer, intent(in) :: l_row_inverted, u_row_inverted !First and last row-index of inverted matrix (local)
    integer, intent(in) :: l_col_inverted, u_col_inverted !First and last column-index of inverted matrix (local)
    double complex, dimension(l_row_original:u_row_original, l_col_original:u_col_original), intent(in) :: original_matrix
    integer, dimension(9), intent(in) :: original_matrix_desc
    double complex, dimension(l_row_inverted:u_row_inverted, l_col_inverted:u_col_inverted), intent(out) :: inverted_matrix
    integer, dimension(9), intent(in) :: inverted_matrix_desc
    !integer, intent(in) :: l_col_lefteig, u_col_lefteig, l_row_righteig, u_row_righteig
    integer, intent(in) :: n_left_row_opt, n_left_col_opt, n_right_row_opt, n_right_col_opt
    integer, dimension(9), intent(in) :: original_matrix_opt_desc
    integer, dimension(9), intent(in) :: lefteig_desc, righteig_desc
    integer, dimension(9), intent(in) :: lefteig_opt_desc, righteig_opt_desc
    integer, intent(in) :: comm_blacs
    logical, optional, intent(in) :: debug

    !local
    logical :: internal_debug
    double complex, allocatable, dimension(:,:) :: debug_original_matrix_cp
    double complex, allocatable, dimension(:,:) :: original_matrix_symm
    integer :: mpierr
    double precision, allocatable, dimension(:) :: singular_values
    double complex, allocatable, dimension(:,:) :: left_singular_vectors
    double complex, allocatable, dimension(:,:) :: right_singular_vectors
    integer :: lwork, lrwork
    double complex, allocatable, dimension(:) :: work, rwork
    double complex, allocatable, dimension(:,:) :: id_matrix
    double complex, allocatable, dimension(:,:) :: original_times_inverted
    double precision :: checkloc, checkglob
    integer :: info
    integer :: i,j
    integer :: n_nonsingular
    double complex, allocatable, dimension(:,:) :: diagonal_singular_value_matrix

    double complex, allocatable, dimension(:,:) :: original_matrix_opt
    double complex, allocatable, dimension(:,:) :: left_singular_vectors_opt
    double complex, allocatable, dimension(:,:) :: right_singular_vectors_opt
    if(present(debug)) then
      internal_debug = debug
    else
      internal_debug = .false.
    end if

    if(internal_debug) then
      allocate(debug_original_matrix_cp(l_row_original:u_row_original, l_col_original:u_col_original))
      debug_original_matrix_cp(:,:) = original_matrix(:,:)
    end if

    !First, perform good ole symmterization of original matrix
    allocate(original_matrix_symm(l_row_original:u_row_original, l_col_original:u_col_original))
    original_matrix_symm(:,:) = (0.0d0, 0.0d0)


    if(n_row_elements == n_col_elements) then
      !Symmetrization only makes sense for quadratic matrix

      ! check if matrix is self-adjoint
      call pztranc( n_row_elements, n_col_elements, (1.0d0, 0.0d0), original_matrix(:,:), 1, 1, &
                  &original_matrix_desc, (0.0d0, 0.0d0), original_matrix_symm(:,:), &
                  &1, 1, original_matrix_desc )
  
      call MPI_Barrier(comm_blacs, mpierr)
  
      if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
        write(*,*) "Calculated hermitian-adjoint before inversion"
      end if
  
      checkloc=sum(abs(original_matrix-original_matrix_symm))
      call mpi_allreduce(checkloc, checkglob,1,MPI_DOUBLE_PRECISION,MPI_SUM,comm_blacs,mpierr)
  
      if((checkglob.gt.5e-9).and.((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0))) write (*,'(2X,2A,ES14.4)') &
           'WARNING: original input matrix of svd-inversion does not seem to be self-adjoint!',&
           ' Checksum: ',checkglob
  
      ! averaging the upper and lower triangles
      original_matrix_symm(:,:) = 0.5d0* (original_matrix(:,:) + original_matrix_symm(:,:))
  
      call MPI_Barrier(comm_blacs, mpierr)
  
      if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
        write(*,*) "Symmetrize the to-be-inverted matrix"
      end if
    else
      !In case of non-square matrix, do nothing, just re-assign
      if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
        write(*,*) "A non-square matrix was submitted for inversion"
        write(*,*) "No symmetrization will be performed"
      end if
 
      original_matrix_symm(:,:) = original_matrix(:,:)
    end if

    allocate(singular_values(min(n_row_elements, n_col_elements)))
    allocate(left_singular_vectors(l_row_original:u_row_original, l_col_inverted:u_col_inverted))
    allocate(right_singular_vectors(l_row_inverted:u_row_inverted, l_col_original:u_col_original))

    singular_values(:) = 0.0d0
    left_singular_vectors(:,:) = (0.0d0, 0.0d0)
    right_singular_vectors(:,:) = (0.0d0, 0.0d0)

    lwork=-1
    lrwork=-1
    allocate(work(1))
    allocate(rwork(1))

    !write(*,*) "Process (", CC4S_myid_row, ",", CC4S_myid_col, ") original_matrix_des = ", original_matrix_desc
    !write(*,*) "Process (", CC4S_myid_row, ",", CC4S_myid_col, ") shape(original_matrix_symm) = ", shape(original_matrix_symm)
    
    !PZGESVD expects that the matrix-blocks are square (MB=NB)
    !Hence, we need to redistribute original_matrix_symm first.
    allocate(original_matrix_opt(n_left_row_opt, n_right_col_opt))
    original_matrix_opt = (0.0d0, 0.0d0)
    call pzgemr2d(n_row_elements, n_col_elements, original_matrix_symm(:,:), 1, 1, original_matrix_desc, &
                 &original_matrix_opt(:,:), 1, 1, original_matrix_opt_desc, original_matrix_desc(2))

    !Same with left vectors
    allocate(left_singular_vectors_opt(n_left_row_opt, n_left_col_opt))
    left_singular_vectors_opt = (0.0d0, 0.0d0)

    !and right vectors
    allocate(right_singular_vectors_opt(n_right_row_opt, n_right_col_opt))
    right_singular_vectors_opt(:,:) = (0.0d0, 0.0d0)

    call pzgesvd('V', 'V', n_row_elements, n_col_elements, original_matrix_opt(:,:), 1, 1, &
                &original_matrix_opt_desc, singular_values(:), &
                &left_singular_vectors_opt(:,:), 1, 1, lefteig_opt_desc, &
                &right_singular_vectors_opt(:,:), 1, 1, righteig_opt_desc, &
                &work, lwork, rwork, lrwork, info)


    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "First pzgesvd passed"
    end if

    lwork=int(work(1)) 
    lrwork=int(rwork(1))

    deallocate(work)
    deallocate(rwork)

    allocate(work(lwork))
    allocate(rwork(lrwork))

!    call pzgesvd('V', 'V', n_row_elements, n_col_elements, original_matrix_symm(:,:), 1, 1, original_matrix_desc, singular_values(:), &
!                &left_singular_vectors(:,:), 1, 1, lefteig_desc, right_singular_vectors(:,:), 1, 1, righteig_desc, &
!                &work, lwork, rwork, lrwork, info)

    call pzgesvd('V', 'V', n_row_elements, n_col_elements, original_matrix_opt(:,:), 1, 1, &
                &original_matrix_opt_desc, singular_values(:), &
                &left_singular_vectors_opt(:,:), 1, 1, lefteig_opt_desc, &
                &right_singular_vectors_opt(:,:), 1, 1, righteig_opt_desc, &
                &work, lwork, rwork, lrwork, info)

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Second pzgesvd passed"
    end if

    !SVD finished, return back to old ordering
    !call pzgemr2d(n_row_elements, n_col_elements, original_matrix_opt(:,:), 1, 1, original_matrix_opt_desc, &
    !             &original_matrix_symm(:,:), 1, 1, original_matrix_desc, original_matrix_desc(2))



    call pzgemr2d(n_row_elements, n_row_elements, left_singular_vectors_opt(:,:), 1, 1, lefteig_opt_desc, &
                 &left_singular_vectors(:,:), 1, 1, lefteig_desc, original_matrix_desc(2))

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Converted left singular vectors back to old format"
    end if

    call pzgemr2d(n_col_elements, n_col_elements, right_singular_vectors_opt(:,:), 1, 1, righteig_opt_desc, &
                 &right_singular_vectors(:,:), 1, 1, righteig_desc, original_matrix_desc(2))

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Converted right singular vectors back to old format"
    end if

    !singular_values(:) contains the values in DESCENDING order.
    !Calculate the inverse of the singular values above the threshold. Everything below, leave unchanged.
    !Alternative: set to 0
    do i=1,min(n_row_elements, n_col_elements)
      if(singular_values(i) <= 1e-10) then
        exit
      else
        singular_values(i) = 1.0d0/singular_values(i)
      end if
    end do

    n_nonsingular = i - 1

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Singular values of to-be-inverted matrix range from ", 1.0d0/singular_values(1), " to ", &
                 &1.0d0/singular_values(n_nonsingular)
    end if

    call MPI_Barrier(comm_blacs, mpierr)

    allocate(diagonal_singular_value_matrix(l_row_original:u_row_original, l_col_original:u_col_original))
    diagonal_singular_value_matrix(:,:) = (0.0d0, 0.0d0)

    do i=l_row_original,u_row_original
      do j=l_col_original,u_col_original
        if(i==j) then
          diagonal_singular_value_matrix(i,j) = singular_values(i)
        end if
      end do
    end do

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Assigned remaining singular values to diagonal matrix Sigma"
    end if

    !At this point original_matrix_symm is not needed anymore.
    !We can recycle it to store an intermediate result Sigma x U*
    deallocate(original_matrix_symm)
    allocate(original_matrix_symm(l_row_inverted:u_row_inverted, l_col_inverted:u_col_inverted))
    original_matrix_symm(:,:) = (0.0d0, 0.0d0)

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Reallocated original_matrix_symm"
    end if

    call pzgemm('T', 'C', n_col_elements, n_row_elements, &
               n_row_elements, (1.0d0, 0.0d0), &
               diagonal_singular_value_matrix(:,:), &
               1, 1, original_matrix_desc, &
               left_singular_vectors(:,:), &
               1, 1, lefteig_desc, (0.0d0, 0.0d0), &
               original_matrix_symm(:,:), &
               1, 1, inverted_matrix_desc &
              )

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Passed first pzgemm : Simga x U*"
    end if

    !Compute pseudo-inverse as V x Sigma x U*
    call pzgemm('C', 'N', n_col_elements, n_row_elements, &
               n_col_elements, (1.0d0, 0.0d0), &
               right_singular_vectors(:,:), &
               1, 1, righteig_desc, &
               original_matrix_symm(:,:), &
               1, 1, inverted_matrix_desc, (0.0d0, 0.0d0), &
               inverted_matrix(:,:), &
               1, 1, inverted_matrix_desc &
              )

    call MPI_Barrier(comm_blacs, mpierr)

    if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
      write(*,*) "Passed second pzgemm : V x Sigma U*"
    end if

    if(internal_debug) then
      !If debug is specified, test quality of inversion (right-inversion)
      allocate(id_matrix(l_row_original:u_row_original, l_col_inverted:u_col_inverted))
      id_matrix(:,:) = (0.0d0, 0.0d0)
      do i=l_row_original,u_row_original
        do j=l_col_inverted,u_col_inverted
          if(i==j) then
            id_matrix(i,j) = (1.0d0, 0.0d0)
          end if
        end do
      end do

      allocate(original_times_inverted(l_row_original:u_row_original, l_col_inverted:u_col_inverted))
      original_times_inverted(:,:) = (0.0d0, 0.0d0)

      !Compute S * S^{-1}
      call pzgemm('N', 'N', n_row_elements, n_row_elements, &
               n_col_elements, (1.0d0, 0.0d0), &
               debug_original_matrix_cp(:,:), &
               1, 1, original_matrix_desc, &
               inverted_matrix(:,:), &
               1, 1, inverted_matrix_desc, (0.0d0, 0.0d0), &
               original_times_inverted(:,:), &
               1, 1, lefteig_desc &
              )

      checkglob=0.0D0
      checkloc=0.0D0
      checkloc=sum(abs(original_times_inverted - id_matrix))
      call MPI_Allreduce(checkloc, checkglob, 1, MPI_DOUBLE_PRECISION, MPI_SUM, &
                   &comm_blacs, mpierr)
    
      if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
        write(*,'(A55, 1X, ES10.3)') " Quality of right-side pseudo-inverse A * A^{-1} - 1", checkglob/(n_row_elements**2)
      end if

      !Compute S^{-1} * S
      deallocate(id_matrix)
      allocate(id_matrix(l_row_inverted:u_row_inverted, l_col_original:u_col_original))
      id_matrix(:,:) = (0.0d0, 0.0d0)
      do i=l_row_inverted,u_row_inverted
        do j=l_col_original,u_col_original
          if(i==j) then
            id_matrix(i,j) = (1.0d0, 0.0d0)
          end if
        end do
      end do

      deallocate(original_times_inverted)
      allocate(original_times_inverted(l_row_inverted:u_row_inverted, l_col_original:u_col_original))
      original_times_inverted(:,:) = (0.0d0, 0.0d0)

      call pzgemm('N', 'N', n_col_elements, n_col_elements, &
               n_row_elements, (1.0d0, 0.0d0), &
               inverted_matrix(:,:), &
               1, 1, inverted_matrix_desc, &
               debug_original_matrix_cp(:,:), &
               1, 1, original_matrix_desc, (0.0d0, 0.0d0), &
               original_times_inverted(:,:), &
               1, 1, righteig_desc &
              )

      checkglob=0.0D0
      checkloc=0.0D0
      checkloc=sum(abs(original_times_inverted - id_matrix))
      call MPI_Allreduce(checkloc, checkglob, 1, MPI_DOUBLE_PRECISION, MPI_SUM, &
                   &comm_blacs, mpierr)
    
      if((CC4S_myid_col.eq.0) .and. (CC4S_myid_row.eq.0)) then
        write(*,'(A55, 1X, ES10.3)') " Quality of left-side pseudo-inverse A^{-1} * A - 1", checkglob/(n_col_elements**2)
      end if
    end if

  end subroutine CC4S_compute_pseudo_inverse_via_svd_general
end module CC4S_sqrt_coulmat
