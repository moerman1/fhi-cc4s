module CC4S_coul_vertex
use mpi

use calculation_data, only: CC4S_kpq_point_list, CC4S_k_minus_q_vecs, CC4S_isreal, &
        &CC4S_nuf_states, CC4S_n_spin, CC4S_n_k_points, CC4S_n_kq_points, &
        &CC4S_n_basbas, CC4S_n_states, CC4S_n_basis, CC4S_max_n_basis_sp
use CC4S_blacs_environment, only: CC4S_lbb_row,CC4S_ubb_row, CC4S_comm, &
        &CC4S_lstates_col,CC4S_ustates_col, CC4S_lstates2_col, CC4S_ustates2_col, CC4S_myid, &
        &CC4S_lbb_col 
use CC4S_VtimesM, only: CC4S_v_times_M_k_q
use CC4S_OAF, only: CC4S_prune_vertex, CC4S_reshape_vertex_2D
use CC4S_M_k_q, only: CC4S_compute_M_k_q, CC4S_parallel_write_co_densities_block_to_file
use CC4S_parallel_io, only: CC4S_parallel_write, CC4S_debug_write_4d_real, &
        &CC4S_debug_write_2d_real, CC4S_debug_write_3d_real, cc4s_debug_write_4d_cmplx, &
        &CC4S_debug_write_2d_cmplx, CC4S_debug_write_3d_cmplx
use CC4S_bin2format
use CC4S_vertex_block, only: CC4S_build_vertex_block
use CC4S_OAF_transformation, only: CC4S_add_up_squared_block, CC4S_reset_squared_block_sum, &
                                   &CC4S_compute_OAF_trafo_matrix_real, CC4S_compute_OAF_trafo_matrix_cmplx, &
                                   &U_times_Gamma, CC4S_parallel_write_oaf_trafo_matrix_block_to_file
use outputfile_names

contains
        subroutine CC4S_coulvertex_construction_OLD(aomo_lvl_r, aomo_lvl_c, &
                        &scf_eigvecs_real, scf_eigvecs_cmplx, &
                        &sqrt_coulmat_real, sqrt_coulmat_cmplx, CoulombVertex_fh, &
                        &singval_thr, singval_num)

          implicit none

          !input
          double precision, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_r
          double complex, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_c
          double precision, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_real
          double complex, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_cmplx
          double precision, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_real
          double complex, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_cmplx
          integer, intent(in) :: CoulombVertex_fh
          double precision, optional :: singval_thr
          integer, optional :: singval_num

          !local
          integer :: curr_pair
          integer :: i_q_point, i_kq_pair, i_k_point, i_spin
          double precision, dimension(:,:,:,:), target, allocatable :: momo_lvl_r
          double precision, dimension(:,:,:,:), pointer :: momo_lvl_rptr => null()
          double complex, dimension(:,:,:,:), target, allocatable :: momo_lvl_c
          double complex, dimension(:,:,:,:), pointer :: momo_lvl_cptr => null()
          double precision, dimension(:,:,:), pointer :: sr_momo_lvl_rptr => null()
          double complex, dimension(:,:,:), pointer :: sr_momo_lvl_cptr => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_k => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_k => null() 
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_q => null()
          double precision, dimension(:,:), pointer :: KS_rptr_k => null()
          double precision, dimension(:,:), pointer :: KS_rptr_q => null()
          double complex, dimension(:,:), pointer :: KS_cptr_k => null()
          double complex, dimension(:,:), pointer :: KS_cptr_q => null()
          double precision, dimension(:,:), pointer :: sqrt_cm_rptr => null()
          double complex, dimension(:,:), pointer :: sqrt_cm_cptr => null()
          double precision, dimension(:,:,:), target, allocatable :: vertex_reshaped_r
          double precision, dimension(:,:,:), pointer :: vertex_reshaped_rptr => null()
          double complex, dimension(:,:,:), target, allocatable :: vertex_reshaped_c
          double complex, dimension(:,:,:), pointer :: vertex_reshaped_cptr => null()
          double precision, dimension(:,:,:), allocatable :: vertex_rOAF
          double complex, dimension(:,:,:), allocatable :: vertex_cOAF
          double precision, dimension(:,:,:,:), pointer :: coulomb_vertex_rptr => null()
          double complex, dimension(:,:,:,:), pointer :: coulomb_vertex_cptr => null()

          integer :: OAF_dim

          integer :: i_k2_point, i_q2_point, i_spin2

          integer :: max_singval
          integer :: lredbb_row
          integer, dimension(3) :: OAF_sizes, OAF_subsizes, OAF_starts

          !Just for debugging, remove afterwards
          integer :: momo_size
          integer, dimension(4) :: momo_sizes, momo_subsizes, momo_starts
          integer :: reshape_vertex_size
          integer, dimension(3) :: reshape_vertex_sizes, reshape_vertex_subsizes, reshape_vertex_starts 
          integer :: cm_size
          integer, dimension(2) :: cm_sizes, cm_subsizes, cm_starts
          
          character(len=1) :: taskIdString
          logical :: skip_pruning

          if(CC4S_isreal) then
            allocate(momo_lvl_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                &CC4S_nuf_states, CC4S_n_spin))
            momo_lvl_r(:,:,:,:) = 0.0D0
            momo_lvl_rptr => momo_lvl_r(:,:,:,:)
          else
            allocate(momo_lvl_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                &CC4S_nuf_states, CC4S_n_spin))
            momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
            momo_lvl_cptr => momo_lvl_c(:,:,:,:)
          end if

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p


              if(CC4S_isreal) then
                momo_lvl_r(:,:,:,:) = 0.0D0
                aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k_point)
                aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q_point)
                sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
              else
                momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
                aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k_point)
                aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q_point)
                sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
              end if 

              do i_spin=1,CC4S_n_spin
                if(CC4S_isreal) then
                  KS_rptr_k => scf_eigvecs_real(:,:,i_spin,i_k_point)
                  KS_rptr_q => scf_eigvecs_real(:,:,i_spin,i_q_point)


                  call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                              &aomo_lvl_rptr_q, &
                                              &KS_rptr_k, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_r)
                else
                  KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin,i_k_point)
                  KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin,i_q_point)

                  call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                              &aomo_lvl_cptr_q, &
                                              &KS_cptr_k, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_c)
                end if                 
              end do !i_spin
              if(CC4S_isreal) then
                momo_size=CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
                momo_sizes=[CC4S_n_basbas, CC4S_nuf_states, CC4S_nuf_states, CC4S_n_spin]
                momo_subsizes=shape(momo_lvl_r)
                momo_starts=[CC4S_lbb_row-1, CC4S_lstates_col-1, 0, 0]

                
                coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
                allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_r(:,:,:) = 0.0D0
                vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

                call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

                call CC4S_add_up_squared_block(vertex_reshaped_rptr)

                if(allocated(vertex_reshaped_r)) then
                  deallocate(vertex_reshaped_r)
                end if


              else
                momo_size=CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
                momo_sizes=[CC4S_n_basbas, CC4S_nuf_states, CC4S_nuf_states, CC4S_n_spin]
                momo_subsizes=shape(momo_lvl_c)
                momo_starts=[CC4S_lbb_row-1, CC4S_lstates_col-1, 0, 0]


                coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
                allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
                vertex_reshaped_cptr => vertex_reshaped_c(:,:,:)

                call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

                call CC4S_add_up_squared_block(vertex_reshaped_cptr)

                if(allocated(vertex_reshaped_c)) then
                  deallocate(vertex_reshaped_c)
                end if

              end if 


!              curr_pair = curr_pair + 1 
            end do !i_q_point

            !At this point all blocks for a given transfer momentum k-q have been calculated.
            !Hence, here is the point where the OAF-transformation of an entire coulomb-vertex row
            !can be performed

            if(CC4S_isreal) then
              if(present(singval_num) .and. present(singval_thr)) then
                call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, singval_num, singval_thr)
              else if(present(singval_num)) then
                !If only singval_num has been passed
                call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, num_singvals=singval_num)
              else if(present(singval_thr)) then
                !If only singval_thr has been passed
                call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, singval_thr=singval_thr)
              else
                !If none have been passed
                call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim)
              end if 
            else
              !call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, singval_num)
              if(present(singval_num) .and. present(singval_thr)) then
                call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, singval_num, singval_thr)
              else if(present(singval_num)) then
                !If only singval_num has been passed
                call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, num_singvals=singval_num)
              else if(present(singval_thr)) then
                !If only singval_thr has been passed
                call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, singval_thr=singval_thr)
              else
                !If none have been passed
                call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim)
              end if
            end if

            !The following line serves as a safe-guard.
            !At this point it should be guaranteed that OAF_dim <= CC4S_n_basbas
            max_singval = min(OAF_dim, CC4S_n_basbas)

            !After the trafo matrix has been calculated, the sum of squared blocks
            !belonging to the same transfer momentum is no longer needed
            call CC4S_reset_squared_block_sum()

            !Having calculated the (approximately) unitary transformation matrix
            !for the current transfer momentum,
            !it's time to re-compute the Coulomb vertex blocks and apply the transformation matrix

            do i_q2_point=1,CC4S_n_k_points !k_q
              i_k2_point=CC4S_kpq_point_list(i_q2_point, i_kq_pair) !k_p


              if(CC4S_isreal) then
                momo_lvl_r(:,:,:,:) = 0.0D0
                aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k2_point)
                aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q2_point)
                sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
              else
                momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
                aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k2_point)
                aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q2_point)
                sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
              end if 

              do i_spin2 = 1,CC4S_n_spin
                if(CC4S_isreal) then
                  KS_rptr_k => scf_eigvecs_real(:,:,i_spin2,i_k2_point)
                  KS_rptr_q => scf_eigvecs_real(:,:,i_spin2,i_q2_point)


                  call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                              &aomo_lvl_rptr_q, &
                                              &KS_rptr_k, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin2,  i_k2_point, i_q2_point, &
                                              &momo_lvl_r)
                else
                  KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin2,i_k2_point)
                  KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin2,i_q2_point)

                  call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                              &aomo_lvl_cptr_q, &
                                              &KS_cptr_k, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin2,  i_k2_point, i_q2_point, &
                                              &momo_lvl_c)
                end if                 
              end do !i_spin2

              if(CC4S_isreal) then
                coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
                allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_r(:,:,:) = 0.0D0
                vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

                call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

                call U_times_Gamma(vertex_reshaped_rptr, max_singval, vertex_rOAF)

                if(allocated(vertex_reshaped_r)) then
                  deallocate(vertex_reshaped_r)
                end if

                OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
                OAF_subsizes=shape(vertex_rOAF)
                OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]

                call CC4S_parallel_write(vertex_rOAF, CoulombVertex_fh, OAF_sizes, &
                        &OAF_subsizes, OAF_starts, max_singval, curr_pair)
              else
                coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
                allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
                vertex_reshaped_cptr => vertex_reshaped_c(:,:,:) 

                call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

                call U_times_Gamma(vertex_reshaped_cptr, max_singval, vertex_cOAF)

                if(allocated(vertex_reshaped_c)) then
                  deallocate(vertex_reshaped_c)
                end if

                OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
                OAF_subsizes=shape(vertex_cOAF)
                OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]

                call CC4S_parallel_write(vertex_cOAF, CoulombVertex_fh, OAF_sizes, &
                        &OAF_subsizes, OAF_starts, max_singval, curr_pair)
              end if 



              curr_pair = curr_pair + 1
            end do !i_q2_point


          end do !i_kq_pair

         call bin2format(coulomb_vertex_filename_bin, coulomb_vertex_filename_dat, &
           &MPI_COMM_WORLD, &
           &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin*CC4S_n_k_points*CC4S_n_k_points)

        end subroutine CC4S_coulvertex_construction_OLD
!************************************************************************
        subroutine CC4S_coulvertex_construction(aomo_lvl_r, aomo_lvl_c, &
                        &aomo_v_r, aomo_v_c, &
                        &scf_eigvecs_real, scf_eigvecs_cmplx, &
                        &sqrt_coulmat_real, sqrt_coulmat_cmplx, CoulombVertex_fh, &
                        &debug, momentum_triple_list, sparse_OAF_dim, singval_thr, singval_num, &
                        &evaluate_co_densities, do_left_coulomb_vertex, LeftCoulombVertex_fh, &
                        &left_momentum_triple_list)

          use CC4S_M_k_q, only: open_co_densities_filehandle, close_co_densities_filehandle
          use CC4S_OAF_transformation, only : open_oaf_trafo_matrix_filehandle, close_oaf_trafo_matrix_filehandle
          implicit none

          !input
          double precision, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_r
          double complex, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_c
          double precision, dimension(:,:,:,:,:,:), target, intent(in) :: aomo_v_r
          double complex, dimension(:,:,:,:,:,:), target, intent(in) :: aomo_v_c
          double precision, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_real
          double complex, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_cmplx
          double precision, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_real
          double complex, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_cmplx
          integer, intent(in) :: CoulombVertex_fh
          logical, intent(in) :: debug
          integer, dimension(:,:), allocatable, intent(out) :: momentum_triple_list
          integer, dimension(:), allocatable, intent(out) :: sparse_OAF_dim
          double precision, optional :: singval_thr
          integer, optional :: singval_num
          logical, optional, intent(in) :: evaluate_co_densities
          logical, optional, intent(in) :: do_left_coulomb_vertex
          integer, optional, intent(in) :: LeftCoulombVertex_fh
          integer, optional, dimension(:,:), allocatable, intent(out) :: left_momentum_triple_list

          !local
          integer :: CoulombVertex_size
          logical :: internal_evaluate_co_densities

          if(present(evaluate_co_densities)) then
            internal_evaluate_co_densities = evaluate_co_densities
          else
            internal_evaluate_co_densities = .false.
          endif

          if(internal_evaluate_co_densities) then
            call open_co_densities_filehandle()
          endif

          call open_oaf_trafo_matrix_filehandle()

          CoulombVertex_size = 0
          allocate(momentum_triple_list(3,CC4S_n_k_points**2))

          if(present(left_momentum_triple_list)) then
            allocate(left_momentum_triple_list(3,CC4S_n_k_points**2))
          end if

          !allocate(sparse_OAF_dim(CC4S_n_k_points**2))
          allocate(sparse_OAF_dim(CC4S_n_k_points))

          if(present(singval_thr) .or. present(singval_num)) then
            if(CC4S_myid .eq. 0) then
              write(*,*) "Reduced OAF Coulomb vertex will be calculated" 
            end if 
            if(CC4S_isreal) then
              call CC4S_coulvertex_construction_OAF_real(aomo_lvl_r, &
                                                        &aomo_v_r, &
                                                        &scf_eigvecs_real, &
                                                        &sqrt_coulmat_real, &
                                                        &CoulombVertex_fh, &
                                                        &CoulombVertex_size, &
                                                        &momentum_triple_list, &
                                                        &sparse_OAF_dim, &
                                                        &singval_thr=singval_thr, &
                                                        &singval_num=singval_num, &
                                                        &evaluate_co_densities=&
                                                        &internal_evaluate_co_densities)
              
            else
               call CC4S_coulvertex_construction_OAF_cmplx(aomo_lvl_c, &
                                                        &aomo_v_c, &
                                                        &scf_eigvecs_cmplx, &
                                                        &sqrt_coulmat_cmplx, &
                                                        &CoulombVertex_fh, &
                                                        &CoulombVertex_size, &
                                                        &momentum_triple_list, &
                                                        &sparse_OAF_dim, &
                                                        &singval_thr=singval_thr, &
                                                        &singval_num=singval_num, &
                                                        &evaluate_co_densities=&
                                                        &internal_evaluate_co_densities) 
            end if  
          else
            if(CC4S_myid .eq. 0) then
              write(*,*) "Entire Coulomb vertex will be calculated (not OAF)"
            end if 
            if(CC4S_isreal) then
              call CC4S_coulvertex_construction_noOAF_real(aomo_lvl_r, &
                                                          &aomo_v_r, &
                                                          &scf_eigvecs_real, &
                                                          &sqrt_coulmat_real, &
                                                          &CoulombVertex_fh, &
                                                          &CoulombVertex_size, &
                                                          &momentum_triple_list, &
                                                          &sparse_OAF_dim, &
                                                          &evaluate_co_densities=&
                                                          &internal_evaluate_co_densities)
            else
              call CC4S_coulvertex_construction_noOAF_cmplx(aomo_lvl_c, &
                                                          &aomo_v_c, &
                                                          &scf_eigvecs_cmplx, &
                                                          &sqrt_coulmat_cmplx, &
                                                          &CoulombVertex_fh, &
                                                          &CoulombVertex_size, &
                                                          &momentum_triple_list, &
                                                          &sparse_OAF_dim, &
                                                          &evaluate_co_densities=&
                                                          &internal_evaluate_co_densities, &
                                                          &do_left_coulomb_vertex=&
                                                          &do_left_coulomb_vertex, &
                                                          &left_coulomb_vertex_fh=&
                                                          &LeftCoulombVertex_fh, &
                                                          &left_momentum_triple_list=&
                                                          &left_momentum_triple_list)
            end if 
          end if

          !If debugging-mode is desired,
          !translate the binary Coulomb vertex
          !to human-readable format
          if(debug) then
            call bin2format(coulomb_vertex_filename_bin, coulomb_vertex_filename_dat, MPI_COMM_WORLD, CoulombVertex_size)
          end if

          !call bin2format(co_densities_filename_bin, co_densities_filename_dat, &
          !               &MPI_COMM_WORLD, CC4S_n_basbas*CC4S_nuf_states**2)

          if(internal_evaluate_co_densities) then
            call close_co_densities_filehandle(debug)
          endif

          call close_oaf_trafo_matrix_filehandle(debug)

        end subroutine CC4S_coulvertex_construction

!*************************************************************************
        subroutine CC4S_coulvertex_construction_OAF_real(aomo_lvl_r, aomo_v_r, &
                        &scf_eigvecs_real, sqrt_coulmat_real, CoulombVertex_fh, &
                        &CoulombVertex_size, momentum_triple_list, sparse_OAF_dim, &
                        &singval_thr, singval_num, evaluate_co_densities)
          implicit none

          !input
          double precision, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_r
          double precision, dimension(:,:,:,:,:,:), target, intent(in) :: aomo_v_r
          double precision, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_real
          double precision, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_real
          integer, intent(in) :: CoulombVertex_fh
          double precision, optional :: singval_thr
          integer, optional :: singval_num
          integer, intent(out) :: CoulombVertex_size !Number of elements of final Coulomb vertex
          integer, dimension(:,:), intent(out) :: momentum_triple_list
          integer, dimension(:), intent(out) :: sparse_OAF_dim
          logical, optional, intent(in) :: evaluate_co_densities

          !local
          double precision, dimension(:,:,:,:), target, allocatable :: momo_lvl_r
          integer :: curr_pair !current k-q-pair
          integer :: max_singval !(reduced) OAF-dimension for current transfer momentum k-q
          integer :: i_kq_pair, i_q_point, i_k_point, i_spin
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_k => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_q => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_v_rptr_kq => null()
          double precision, dimension(:,:), pointer :: sqrt_cm_rptr => null()
          double precision, dimension(:,:), pointer :: KS_rptr_k => null()
          double precision, dimension(:,:), pointer :: KS_rptr_q => null()
          double precision, dimension(:,:,:,:), pointer :: coulomb_vertex_rptr => null()
          double precision, dimension(:,:,:), target, allocatable :: vertex_reshaped_r
          double precision, dimension(:,:,:), pointer :: vertex_reshaped_rptr => null()
          integer :: lredbb_row
          integer :: OAF_dim
          integer :: i_q2_point, i_k2_point, i_spin2
          integer, dimension(3) :: OAF_sizes, OAF_subsizes, OAF_starts
          double precision, dimension(:,:,:), allocatable :: vertex_rOAF
          !bool variable stating if RI-V is to be assumed
          logical :: is_riv
          logical :: internal_evaluate_co_densities

          if(present(evaluate_co_densities)) then
            internal_evaluate_co_densities = evaluate_co_densities
          else
            internal_evaluate_co_densities = .false.
          endif


          if(size(aomo_v_r) .eq. 1) then
            is_riv = .false.
          else
            is_riv = .true.
            if(CC4S_myid .eq. 0) then
              write(*,*) "Using RI-V in Coulomb vertex construction"
            end if 
          end if 

          CoulombVertex_size = 0


          allocate(momo_lvl_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                  &CC4S_nuf_states, CC4S_n_spin))
          momo_lvl_r(:,:,:,:) = 0.0D0

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p

              if(CC4S_myid .eq. 0) then
                write(*,'(2X, A, I0, A, I0, A, I0)') "Calculating Coulomb vertex block for k=", &
                  &i_k_point, " q=", i_q_point, " k-q=", i_kq_pair
              end if

              momo_lvl_r(:,:,:,:) = 0.0D0
              if(is_riv) then
                aomo_v_rptr_kq => aomo_v_r(:,:,:,:,i_k_point, i_q_point)
              else
                aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k_point)
                aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q_point)
              end if 
              sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
 

              do i_spin=1,CC4S_n_spin
                KS_rptr_k => scf_eigvecs_real(:,:,i_spin,i_k_point)
                KS_rptr_q => scf_eigvecs_real(:,:,i_spin,i_q_point)
                if(is_riv) then
                  call CC4S_build_vertex_block(aomo_v_rptr_kq, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_r)
                else
                  call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                              &aomo_lvl_rptr_q, &
                                              &KS_rptr_k, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_r)
                end if 
              end do !i_spin

              
              coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
              allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_r(:,:,:) = 0.0D0
              vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

              call CC4S_add_up_squared_block(vertex_reshaped_rptr)

              if(allocated(vertex_reshaped_r)) then
                deallocate(vertex_reshaped_r)
              end if

            end do !i_q_point

            !At this point all blocks for a given transfer momentum k-q have been calculated.
            !Hence, here is the point where the OAF-transformation of an entire coulomb-vertex row
            !can be performed

            if(present(singval_num) .and. present(singval_thr)) then
              call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, singval_num, singval_thr)
            else if(present(singval_num)) then
              !If only singval_num has been passed
              call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, num_singvals=singval_num)
            else if(present(singval_thr)) then
              !If only singval_thr has been passed
              call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, singval_thr=singval_thr)
            else
              !If none have been passed
              call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim)
            end if 


            !The following line serves as a safe-guard.
            !At this point it should be guaranteed that OAF_dim <= CC4S_n_basbas
            max_singval = min(OAF_dim, CC4S_n_basbas)

            !After the trafo matrix has been calculated, the sum of squared blocks
            !belonging to the same transfer momentum is no longer needed
            call CC4S_reset_squared_block_sum()


            !Here is a good place, to write the OAF-trafo matrix ("CoulombVertexSingularVectors")
            !for the current k-q to file
            call CC4S_parallel_write_oaf_trafo_matrix_block_to_file(i_kq_pair-1, CC4S_isreal, max_singval)

            !Having calculated the (approximately) unitary transformation matrix
            !for the current transfer momentum,
            !it's time to re-compute the Coulomb vertex blocks and apply the transformation matrix

            do i_q2_point=1,CC4S_n_k_points !k_q
              i_k2_point=CC4S_kpq_point_list(i_q2_point, i_kq_pair) !k_p

              if(CC4S_myid .eq. 0) then
                write(*,'(2X, A, I0, A, I0, A, I0)') "Recomputing reduced OAF Coulomb vertex block for k=", &
                  &i_k_point, " q=", i_q_point, " k-q=", i_kq_pair
              end if

              momentum_triple_list(1:3,curr_pair+1) = [i_k2_point, i_q2_point, i_kq_pair]

              momo_lvl_r(:,:,:,:) = 0.0D0
              if(is_riv) then
                aomo_v_rptr_kq => aomo_v_r(:,:,:,:,i_k2_point, i_q2_point)
              else
                aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k2_point)
                aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q2_point)
              end if 
              sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)


              do i_spin2 = 1,CC4S_n_spin
                KS_rptr_k => scf_eigvecs_real(:,:,i_spin2,i_k2_point)
                KS_rptr_q => scf_eigvecs_real(:,:,i_spin2,i_q2_point)

                if(is_riv) then
                  call CC4S_build_vertex_block(aomo_v_rptr_kq, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin2,  i_k2_point, i_q2_point, &
                                              &momo_lvl_r)
                else
                  call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                              &aomo_lvl_rptr_q, &
                                              &KS_rptr_k, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin2,  i_k2_point, i_q2_point, &
                                              &momo_lvl_r, &
                                              &internal_evaluate_co_densities)
                end if 
                
              end do !i_spin2


              coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
              allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_r(:,:,:) = 0.0D0
              vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

              call U_times_Gamma(vertex_reshaped_rptr, max_singval, vertex_rOAF)

              if(allocated(vertex_reshaped_r)) then
                deallocate(vertex_reshaped_r)
              end if

              OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
              OAF_subsizes=shape(vertex_rOAF)
              OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]

              !sparse_OAF_dim(curr_pair+1) = max_singval

              CoulombVertex_size = CoulombVertex_size + product(OAF_sizes)

              !Multiply Coulomb vertex by 1/sqrt(n_k_points)
              vertex_rOAF(:,:,:) = vertex_rOAF(:,:,:) * (1.0D0/sqrt(dble(CC4S_n_k_points)))

              call CC4S_parallel_write(vertex_rOAF, CoulombVertex_fh, OAF_sizes, &
                      &OAF_subsizes, OAF_starts, max_singval, curr_pair)

              if(internal_evaluate_co_densities) then
                call CC4S_parallel_write_co_densities_block_to_file(curr_pair, CC4S_isreal) !, max_singval)
              end if
 
              curr_pair = curr_pair + 1
            end do !i_q2_point

            sparse_OAF_dim(i_kq_pair) = max_singval

          end do !i_kq_pair
        end subroutine CC4S_coulvertex_construction_OAF_real
!*************************************************************************
        subroutine CC4S_coulvertex_construction_OAF_cmplx(aomo_lvl_c, aomo_v_c, &
                        &scf_eigvecs_cmplx, sqrt_coulmat_cmplx, CoulombVertex_fh, &
                        &CoulombVertex_size, momentum_triple_list, sparse_OAF_dim, &
                        &singval_thr, singval_num, evaluate_co_densities)
          implicit none

          !input
          double complex, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_c
          double complex, dimension(:,:,:,:,:,:), target, intent(in) :: aomo_v_c
          double complex, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_cmplx
          double complex, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_cmplx
          integer, intent(in) :: CoulombVertex_fh
          double precision, optional :: singval_thr
          integer, optional :: singval_num   
          integer, intent(out) :: CoulombVertex_size !Number of elements of final Coulomb vertex      
          integer, dimension(:,:), intent(out) :: momentum_triple_list
          integer, dimension(:), intent(out) :: sparse_OAF_dim
          logical, optional, intent(in) :: evaluate_co_densities

          !local
          double complex, dimension(:,:,:,:), target, allocatable :: momo_lvl_c
          integer :: curr_pair !current k-q-pair
          integer :: max_singval !(reduced) OAF-dimension for current transfer momentum k-q
          integer :: i_kq_pair, i_q_point, i_k_point, i_spin
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_k => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_v_cptr_kq => null()
          double complex, dimension(:,:), pointer :: sqrt_cm_cptr => null()
          double complex, dimension(:,:), pointer :: KS_cptr_k => null()
          double complex, dimension(:,:), pointer :: KS_cptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: coulomb_vertex_cptr => null()
          double complex, dimension(:,:,:), target, allocatable :: vertex_reshaped_c
          double complex, dimension(:,:,:), pointer :: vertex_reshaped_cptr => null()
          integer :: lredbb_row
          integer :: OAF_dim
          integer :: i_q2_point, i_k2_point, i_spin2
          integer, dimension(3) :: OAF_sizes, OAF_subsizes, OAF_starts
          double complex, dimension(:,:,:), allocatable :: vertex_cOAF
          !bool variable stating if RI-V is to be assumed
          logical :: is_riv
          logical :: internal_evaluate_co_densities

          if(present(evaluate_co_densities)) then
            internal_evaluate_co_densities = evaluate_co_densities
          else
            internal_evaluate_co_densities = .false.
          endif

          if(size(aomo_v_c) .eq. 1) then
            is_riv = .false.
          else
            is_riv = .true.
            if(CC4S_myid .eq. 0) then
              write(*,*) "Using RI-V in Coulomb vertex construction"
            end if 
          end if


          CoulombVertex_size = 0

          allocate(momo_lvl_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                  &CC4S_nuf_states, CC4S_n_spin))
          momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p



              momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
              if(is_riv) then
                aomo_v_cptr_kq => aomo_v_c(:,:,:,:,i_k_point, i_q_point)
              else
                aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k_point)
                aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q_point)
              end if 
              sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
 

              do i_spin=1,CC4S_n_spin
                KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin,i_k_point)
                KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin,i_q_point)

                if(is_riv) then
                  call CC4S_build_vertex_block(aomo_v_cptr_kq, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_c)
                else
                  call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                              &aomo_lvl_cptr_q, &
                                              &KS_cptr_k, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_c)
                end if 
              end do !i_spin

              
              coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
              allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
              vertex_reshaped_cptr => vertex_reshaped_c(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

              call CC4S_add_up_squared_block(vertex_reshaped_cptr)

              if(allocated(vertex_reshaped_c)) then
                deallocate(vertex_reshaped_c)
              end if

            end do !i_q_point

            !At this point all blocks for a given transfer momentum k-q have been calculated.
            !Hence, here is the point where the OAF-transformation of an entire coulomb-vertex row
            !can be performed

            if(present(singval_num) .and. present(singval_thr)) then
              call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, singval_num, singval_thr)
            else if(present(singval_num)) then
              !If only singval_num has been passed
              call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, num_singvals=singval_num)
            else if(present(singval_thr)) then
              !If only singval_thr has been passed
              call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, singval_thr=singval_thr)
            else
              !If none have been passed
              call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim)
            end if 


            !The following line serves as a safe-guard.
            !At this point it should be guaranteed that OAF_dim <= CC4S_n_basbas
            max_singval = min(OAF_dim, CC4S_n_basbas)

            !After the trafo matrix has been calculated, the sum of squared blocks
            !belonging to the same transfer momentum is no longer needed
            call CC4S_reset_squared_block_sum()

            !Here is a good place, to write the OAF-trafo matrix ("CoulombVertexSingularVectors")
            !for the current k-q to file
            call CC4S_parallel_write_oaf_trafo_matrix_block_to_file(i_kq_pair-1, CC4S_isreal, max_singval)

            !Having calculated the (approximately) unitary transformation matrix
            !for the current transfer momentum,
            !it's time to re-compute the Coulomb vertex blocks and apply the transformation matrix

            do i_q2_point=1,CC4S_n_k_points !k_q
              i_k2_point=CC4S_kpq_point_list(i_q2_point, i_kq_pair) !k_p

              momentum_triple_list(1:3,curr_pair+1) = [i_k2_point, i_q2_point, i_kq_pair]

              momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
              if(is_riv) then
                aomo_v_cptr_kq => aomo_v_c(:,:,:,:,i_k2_point, i_q2_point)
              else
                aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k2_point)
                aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q2_point)
              end if 
              sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
 

              do i_spin2 = 1,CC4S_n_spin
                KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin2,i_k2_point)
                KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin2,i_q2_point)

                if(is_riv) then
                  call CC4S_build_vertex_block(aomo_v_cptr_kq, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin2,  i_k2_point, i_q2_point, &
                                              &momo_lvl_c)
                else
                  call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                              &aomo_lvl_cptr_q, &
                                              &KS_cptr_k, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin2,  i_k2_point, i_q2_point, &
                                              &momo_lvl_c, &
                                              &internal_evaluate_co_densities)
                end if 
                
              end do !i_spin2

              coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
              allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
              vertex_reshaped_cptr => vertex_reshaped_c(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

              call U_times_Gamma(vertex_reshaped_cptr, max_singval, vertex_cOAF)

              if(allocated(vertex_reshaped_c)) then
                deallocate(vertex_reshaped_c)
              end if

              OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
              OAF_subsizes=shape(vertex_cOAF)
              OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]

              CoulombVertex_size = CoulombVertex_size + product(OAF_sizes)

              !sparse_OAF_dim(curr_pair+1) = max_singval

              !Multiply Coulomb vertex by 1/sqrt(n_k_points)
              vertex_cOAF(:,:,:) = vertex_cOAF(:,:,:) * (1.0D0/sqrt(dble(CC4S_n_k_points)))

              call CC4S_parallel_write(vertex_cOAF, CoulombVertex_fh, OAF_sizes, &
                      &OAF_subsizes, OAF_starts, max_singval, curr_pair)

              if(internal_evaluate_co_densities) then
                call CC4S_parallel_write_co_densities_block_to_file(curr_pair, CC4S_isreal) !, max_singval)
              end if
 
              curr_pair = curr_pair + 1
            end do !i_q2_point

            sparse_OAF_dim(i_kq_pair) = max_singval

          end do !i_kq_pair
        end subroutine CC4S_coulvertex_construction_OAF_cmplx
!*************************************************************************
        subroutine CC4S_coulvertex_construction_noOAF_real(aomo_lvl_r, aomo_v_r, &
                        &scf_eigvecs_real, sqrt_coulmat_real, CoulombVertex_fh, &
                        &CoulombVertex_size, momentum_triple_list, sparse_OAF_dim, &
                        &evaluate_co_densities)
          implicit none

          !input
          double precision, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_r
          double precision, dimension(:,:,:,:,:,:), target, intent(in) :: aomo_v_r
          double precision, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_real
          double precision, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_real
          integer, intent(in) :: CoulombVertex_fh
          integer, intent(out) :: CoulombVertex_size !Number of elements of final Coulomb vertex
          integer, dimension(:,:), intent(out) :: momentum_triple_list
          integer, dimension(:), intent(out) :: sparse_OAF_dim
          logical, optional, intent(in) :: evaluate_co_densities

          !local
          double precision, dimension(:,:,:,:), target, allocatable :: momo_lvl_r
          integer :: curr_pair !current k-q-pair
          integer :: max_singval !(reduced) OAF-dimension for current transfer momentum k-q
          integer :: i_kq_pair, i_q_point, i_k_point, i_spin
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_k => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_q => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_v_rptr_kq => null()
          double precision, dimension(:,:), pointer :: sqrt_cm_rptr => null()
          double precision, dimension(:,:), pointer :: KS_rptr_k => null()
          double precision, dimension(:,:), pointer :: KS_rptr_q => null()
          double precision, dimension(:,:,:,:), pointer :: coulomb_vertex_rptr => null()
          double precision, dimension(:,:,:), target, allocatable :: vertex_reshaped_r
          double precision, dimension(:,:,:), pointer :: vertex_reshaped_rptr => null()
          integer, dimension(3) :: vertex_sizes, vertex_subsizes, vertex_starts
          !bool variable stating if RI-V is to be assumed
          logical :: is_riv
          logical :: internal_evaluate_co_densities

          if(present(evaluate_co_densities)) then
            internal_evaluate_co_densities = evaluate_co_densities
          else
            internal_evaluate_co_densities = .false.
          endif

          if(size(aomo_v_r) .eq. 1) then
            is_riv = .false.
          else
            is_riv = .true.
            if(CC4S_myid .eq. 0) then
              write(*,*) "Using RI-V in Coulomb vertex construction"
            end if 
          end if


          
          CoulombVertex_size = 0

          allocate(momo_lvl_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                  &CC4S_nuf_states, CC4S_n_spin))
          momo_lvl_r(:,:,:,:) = 0.0D0

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p

              if(CC4S_myid .eq. 0) then
                write(*,'(2X, A, I0, A, I0, A, I0)') "Computing Coulomb vertex block for k=", &
                  &i_k_point, " q=", i_q_point, " k-q=", i_kq_pair
              end if

              momentum_triple_list(1:3,curr_pair+1) = [i_k_point, i_q_point, i_kq_pair]

              momo_lvl_r(:,:,:,:) = 0.0D0
              if(is_riv) then
                aomo_v_rptr_kq => aomo_v_r(:,:,:,:,i_k_point, i_q_point)
              else
                aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k_point)
                aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q_point)
              end if 
              sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
 

              do i_spin=1,CC4S_n_spin
                KS_rptr_k => scf_eigvecs_real(:,:,i_spin,i_k_point)
                KS_rptr_q => scf_eigvecs_real(:,:,i_spin,i_q_point)

                if(is_riv) then
                  call CC4S_build_vertex_block(aomo_v_rptr_kq, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_r)
                else
                  call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                              &aomo_lvl_rptr_q, &
                                              &KS_rptr_k, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_r, &
                                              &internal_evaluate_co_densities)
                end if 
              end do !i_spin


              coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
              allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_r(:,:,:) = 0.0D0
              vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

              max_singval = CC4S_n_basbas



              vertex_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
              vertex_subsizes=shape(vertex_reshaped_rptr)
              vertex_starts=[CC4S_lbb_row-1, CC4S_lstates2_col-1, 0]
              
              CoulombVertex_size = CoulombVertex_size + product(vertex_sizes)

              !sparse_OAF_dim(curr_pair+1) = max_singval

              !Multiply Coulomb vertex by 1/sqrt(n_k_points)
              vertex_reshaped_rptr(:,:,:) = vertex_reshaped_rptr(:,:,:) * (1.0D0/sqrt(dble(CC4S_n_k_points)))

              call CC4S_parallel_write(vertex_reshaped_rptr, CoulombVertex_fh, vertex_sizes, &
                      &vertex_subsizes, vertex_starts, max_singval, curr_pair)

              if(internal_evaluate_co_densities) then
                call CC4S_parallel_write_co_densities_block_to_file(curr_pair, CC4S_isreal)
              end if

              if(allocated(vertex_reshaped_r)) then
                deallocate(vertex_reshaped_r)
              end if

              curr_pair = curr_pair + 1

            end do !i_q_point

            sparse_OAF_dim(i_kq_pair) = max_singval

            !Here is a good place, to write the OAF-trafo matrix ("CoulombVertexSingularVectors")
            !for the current k-q to file. In this routine no OAF is performed, so the matrix will be the identity
            call CC4S_parallel_write_oaf_trafo_matrix_block_to_file(i_kq_pair-1, CC4S_isreal)

          end do !i_kq_pair

        end subroutine CC4S_coulvertex_construction_noOAF_real
!*************************************************************************
        subroutine CC4S_coulvertex_construction_noOAF_cmplx(aomo_lvl_c, aomo_v_c, &
                        &scf_eigvecs_cmplx, sqrt_coulmat_cmplx, CoulombVertex_fh, &
                        &CoulombVertex_size, momentum_triple_list, sparse_OAF_dim, &
                        &evaluate_co_densities, do_left_coulomb_vertex, left_coulomb_vertex_fh, &
                        &left_momentum_triple_list)
          implicit none

          !input
          double complex, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_c
          double complex, dimension(:,:,:,:,:,:), target, intent(in) :: aomo_v_c
          double complex, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_cmplx
          double complex, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_cmplx
          integer, intent(in) :: CoulombVertex_fh
          integer, intent(out) :: CoulombVertex_size !Number of elements of final Coulomb vertex
          integer, dimension(:,:), intent(out) :: momentum_triple_list
          integer, dimension(:), intent(out) :: sparse_OAF_dim
          logical, optional, intent(in) :: evaluate_co_densities
          logical, optional, intent(in) :: do_left_coulomb_vertex
          integer, optional, intent(in) :: left_coulomb_vertex_fh
          integer, optional, dimension(:,:), intent(out) :: left_momentum_triple_list

          !local
          double complex, dimension(:,:), target, allocatable :: cc_sqrt_coulmat_cmplx_kq
          double complex, dimension(:,:,:,:), target, allocatable :: momo_lvl_c
          integer :: curr_pair !current k-q-pair
          integer :: max_singval !(reduced) OAF-dimension for current transfer momentum k-q
          integer :: i_kq_pair, i_q_point, i_k_point, i_spin
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_k => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_v_cptr_kq => null()
          double complex, dimension(:,:), pointer :: sqrt_cm_cptr => null()
          double complex, dimension(:,:), pointer :: KS_cptr_k => null()
          double complex, dimension(:,:), pointer :: KS_cptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: coulomb_vertex_cptr => null()
          double complex, dimension(:,:,:), target, allocatable :: vertex_reshaped_c
          double complex, dimension(:,:,:), pointer :: vertex_reshaped_cptr => null()
          integer, dimension(3) :: vertex_sizes, vertex_subsizes, vertex_starts
          !bool variable stating if RI-V is to be assumed
          logical :: is_riv
          logical :: internal_evaluate_co_densities

          !everything for the LeftCoulombVertex. If I understand correctly,
          !only the quantities depending on the transfer momentum k-q need to
          !be adapted. The only quantity of that kind is the sqrt of the Coulomb potential.
          integer :: i_left_kq_pair
          double precision, dimension(3) :: i_kq_pair_vec, i_left_kq_pair_vec
          integer :: i_d, i_kq_diff
          double complex, dimension(:,:), pointer :: left_sqrt_cm_cptr => null()
          double complex, dimension(:,:,:,:), target, allocatable :: left_momo_lvl_c
          double complex, dimension(:,:,:,:), pointer :: left_coulomb_vertex_cptr => null()
          double complex, dimension(:,:,:), target, allocatable :: left_vertex_reshaped_c
          double complex, dimension(:,:,:), pointer :: left_vertex_reshaped_cptr => null()



          !debug
          integer :: i_k_debug, i_state_debug, i_basis_debug
          character(len=50) :: filename_debug

          if(present(evaluate_co_densities)) then
            internal_evaluate_co_densities = evaluate_co_densities
          else
            internal_evaluate_co_densities = .false.
          endif

          if(size(aomo_v_c) .eq. 1) then
            is_riv = .false.
          else
            is_riv = .true.
            if(CC4S_myid .eq. 0) then
              write(*,*) "Using RI-V in Coulomb vertex construction"
            end if 
          end if


          do i_k_debug=1,CC4S_n_kq_points

            write (filename_debug, "(A14,I0)") "sqrt_coulmat_k", i_k_debug

            !call CC4S_debug_write_2d_cmplx(CC4S_comm, sqrt_coulmat_cmplx(:,:,i_k_debug), &
            !  &CC4S_n_basbas*CC4S_n_basbas, [CC4S_n_basbas,CC4S_n_basbas], &
            !  &shape(sqrt_coulmat_cmplx(:,:,i_k_debug)), [CC4S_lbb_row-1, CC4S_lbb_col-1], &
            !  &trim(filename_debug))

            !if(CC4S_myid .eq. 0) then
            !  write(*,*) "Writing KS_eigenvector for k-point", i_k_debug, "to file"
            !  write (filename_debug, "(A14,I0)") "ks_eigenvector", i_k_debug
            !  open(unit=441, file=trim(filename_debug))
            !  do i_state_debug=1,CC4S_n_states    
            !    do i_basis_debug=1,CC4S_n_basis
            !      write(441,*) scf_eigvecs_cmplx(i_basis_debug,i_state_debug,1,i_k_debug)
            !    end do
            !  end do
            !  close(unit=441)

            !end if

          !  write (filename_debug, "(A14,I0)") "lvlri_coeffmat", i_k_debug
          !  call CC4S_debug_write_3d_cmplx(CC4S_comm, aomo_lvl_c(:,:,:,1,i_k_debug), &
          !    &CC4S_n_basbas*CC4S_max_n_basis_sp*CC4S_nuf_states, &
          !    &[CC4S_n_basbas, CC4S_max_n_basis_sp, CC4S_nuf_states], &
          !    &shape(aomo_lvl_c(:,:,:,1,i_k_debug)), [CC4S_lbb_row-1,0,0], trim(filename_debug))

            if(CC4S_myid .eq. 0) then
              write(*,*) "k-q-vector Nr. ", i_k_debug, " : ", CC4S_k_minus_q_vecs(i_k_debug,:)
            end if
          end do

          CoulombVertex_size = 0

          allocate(momo_lvl_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                  &CC4S_nuf_states, CC4S_n_spin))
          momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)

          if(do_left_coulomb_vertex) then
            allocate(left_momo_lvl_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                    &CC4S_nuf_states, CC4S_n_spin))
            left_momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
          end if

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p

              if(do_left_coulomb_vertex) then
                !Determine the index of the k-q-point opposite to i_kq_pair
                i_kq_pair_vec(:) = CC4S_k_minus_q_vecs(i_kq_pair,:)
                i_left_kq_pair_vec(:) = -1.0d0 * i_kq_pair_vec(:)
                !Put it back in the BZ
                do i_d=1,3
                  if(i_left_kq_pair_vec(i_d) < 0.0d0) then
                    i_left_kq_pair_vec(i_d) = i_left_kq_pair_vec(i_d) + 1.0d0
                  elseif(i_left_kq_pair_vec(i_d) > 1.0d0) then
                    i_left_kq_pair_vec(i_d) = i_left_kq_pair_vec(i_d) - 1.0d0
                  end if
                end do
      
                !Get index for negated vector
                do i_kq_diff=1,CC4S_n_kq_points
                  if( all(abs(i_left_kq_pair_vec(:) - CC4S_k_minus_q_vecs(i_kq_diff,:)) < 1e-6) ) then
                    i_left_kq_pair = i_kq_diff
                    if(CC4S_myid .eq. 0) then
                      write(*,*) "Negative of k-q nr.", i_kq_pair, "is k-q nr.", i_left_kq_pair
                    end if
                  end if
                end do
              end if

              if(CC4S_myid .eq. 0) then
                write(*,'(2X, A, I0, A, I0, A, I0)') "Computing Coulomb vertex block for k=", &
                  &i_k_point, " q=", i_q_point, " k-q=", i_kq_pair
              end if

              momentum_triple_list(1:3,curr_pair+1) = [i_k_point, i_q_point, i_kq_pair]

              if(do_left_coulomb_vertex) then
                left_momentum_triple_list(1:3,curr_pair+1) = [i_k_point, i_q_point, i_left_kq_pair]
                !left_momentum_triple_list(1:3,curr_pair+1) = [i_k_point, i_q_point, i_kq_pair]
              end if

              momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)

              if(do_left_coulomb_vertex) then
                left_momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
              end if

              if(is_riv) then
                aomo_v_cptr_kq => aomo_v_c(:,:,:,:,i_k_point, i_q_point)
              else
                aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k_point)
                aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q_point)
              end if 
              sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)

              if(do_left_coulomb_vertex) then
                if(.not. allocated(cc_sqrt_coulmat_cmplx_kq)) then
                  allocate(cc_sqrt_coulmat_cmplx_kq(size(sqrt_coulmat_cmplx(:,:,i_left_kq_pair),1), &
                                                   &size(sqrt_coulmat_cmplx(:,:,i_left_kq_pair),2)))
                end if

                cc_sqrt_coulmat_cmplx_kq(:,:) = (0.0d0, 0.0d0)
                cc_sqrt_coulmat_cmplx_kq(:,:) = conjg( sqrt_coulmat_cmplx(:,:,i_left_kq_pair) )
                !cc_sqrt_coulmat_cmplx_kq(:,:) = sqrt_coulmat_cmplx(:,:,i_left_kq_pair)

                !left_sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_left_kq_pair)
                left_sqrt_cm_cptr => cc_sqrt_coulmat_cmplx_kq(:,:)
              end if 

              do i_spin=1,CC4S_n_spin
                KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin,i_k_point)
                KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin,i_q_point)

                if(is_riv) then
                  call CC4S_build_vertex_block(aomo_v_cptr_kq, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_c)
                else
                  call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                              &aomo_lvl_cptr_q, &
                                              &KS_cptr_k, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_c, &
                                              &internal_evaluate_co_densities)

                  if(do_left_coulomb_vertex) then
                    !Almost the same build_vertex_block-call as above, but
                    !now V(q-k) instead of V(k-q) is used
                    !and saving of the co-densities has been switched off (".false.")
                    !as this has been done already by the call above.
                    call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                                &aomo_lvl_cptr_q, &
                                                &KS_cptr_k, &
                                                &KS_cptr_q, &
                                                &left_sqrt_cm_cptr, &
                                                &i_spin,  i_k_point, i_q_point, &
                                                &left_momo_lvl_c, &
                                                &.false., &
                                                &.false.)
                  end if
                end if 
              end do !i_spin

              
              coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
              allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
              vertex_reshaped_cptr => vertex_reshaped_c(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

              if(do_left_coulomb_vertex) then
                left_coulomb_vertex_cptr => left_momo_lvl_c(:,:,:,:)
                allocate(left_vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                left_vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
                left_vertex_reshaped_cptr => left_vertex_reshaped_c(:,:,:)

                call CC4S_reshape_vertex_2D(left_coulomb_vertex_cptr, left_vertex_reshaped_cptr)
              end if

              max_singval = CC4S_n_basbas



              vertex_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
              vertex_subsizes=shape(vertex_reshaped_cptr)
              vertex_starts=[CC4S_lbb_row-1, CC4S_lstates2_col-1, 0]


              CoulombVertex_size = CoulombVertex_size + product(vertex_sizes)


              !debug
              !write (filename_debug, "(A9,I0,A1,I0,A1,I0)") "coulvert_", i_kq_pair, "_", i_k_point, "_", i_q_point
              !call CC4S_debug_write_3d_cmplx(CC4S_comm, dconjg(vertex_reshaped_cptr(:,:,:)), &
              !  &product(vertex_sizes), vertex_sizes, vertex_subsizes, vertex_starts, trim(filename_debug))

              !sparse_OAF_dim(curr_pair+1) = max_singval

              !Multiply Coulomb vertex by 1/sqrt(n_k_points)
              vertex_reshaped_cptr(:,:,:) = vertex_reshaped_cptr(:,:,:) * (1.0D0/sqrt(dble(CC4S_n_k_points)))
              !vertex_reshaped_cptr(:,:,:) = dconjg( vertex_reshaped_cptr(:,:,:) ) * (1.0D0/sqrt(dble(CC4S_n_k_points)))

              if(do_left_coulomb_vertex) then
                left_vertex_reshaped_cptr(:,:,:) = left_vertex_reshaped_cptr(:,:,:) * (1.0D0/sqrt(dble(CC4S_n_k_points)))
              !  left_vertex_reshaped_cptr(:,:,:) = vertex_reshaped_cptr(:,:,:) * (1.0D0/sqrt(dble(CC4S_n_k_points)))
              end if



              call CC4S_parallel_write(vertex_reshaped_cptr, CoulombVertex_fh, vertex_sizes, &
                      &vertex_subsizes, vertex_starts, max_singval, curr_pair)

              !if(do_left_coulomb_vertex) then
              !  call CC4S_parallel_write(left_vertex_reshaped_cptr, left_coulomb_vertex_fh, vertex_sizes, &
              !        &vertex_subsizes, vertex_starts, max_singval, curr_pair)         
              !end if

              if(internal_evaluate_co_densities) then
                call CC4S_parallel_write_co_densities_block_to_file(curr_pair, CC4S_isreal)
              end if

              if(allocated(vertex_reshaped_c)) then
                deallocate(vertex_reshaped_c)
              end if

              if(do_left_coulomb_vertex .and. allocated(left_vertex_reshaped_c)) then
                deallocate(left_vertex_reshaped_c)
              end if

              curr_pair = curr_pair + 1

            end do !i_q_point

            sparse_OAF_dim(i_kq_pair) = max_singval

            !Here is a good place, to write the OAF-trafo matrix ("CoulombVertexSingularVectors")
            !for the current k-q to file. In this routine no OAF is performed, so the matrix will be the identity
            call CC4S_parallel_write_oaf_trafo_matrix_block_to_file(i_kq_pair-1, CC4S_isreal)

          end do !i_kq_pair

        end subroutine CC4S_coulvertex_construction_noOAF_cmplx

end module CC4S_coul_vertex
