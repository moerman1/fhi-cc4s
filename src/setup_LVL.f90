module CC4S_LVL
        use mpi

        double precision, dimension(:,:,:,:,:), allocatable, target, private :: momo_lvl_rempty
        double complex, dimension(:,:,:,:,:), allocatable, target :: momo_lvl_cempty
        integer :: lvl_shm_win
contains
subroutine CC4S_setup_LVL(cntxt, comm_shared, myid_shared, is_lvl_root, &
                &comm_lvl_root, myid_lvl_root, n_tasks_lvl_root, mpi_comm_shared_row, & 
                &n_tasks_shrow, myid_shrow)

      use CC4S_blacs_environment, only: CC4S_myid_col, CC4S_myid_row, CC4S_myid, CC4S_member
      implicit none

      !PURPOSE: The tensor/array containing the LVL-coefficients
      !         in reciprocal space C_{ij}^{\mu}(\bm{k}) of dimension
      !         (n_basbas x max_n_basis_sp x n_states x n_spin x n_k_points)
      !         is the most memory-consuming object required for the
      !         interface. The dimensions being
      !         - n_basbas: Number of aux. basis functions (RI-functions)
      !         -max_n_basis_sp: Maximum number of atomic basis
      !         functions per element
      !         - n_states: Number of HF/KS/states
      !         - n_spin: Number of spin channels
      !         - n_k_points: Number of considered k-points.
      !
      !         For all but the smallest systems, it is not possible to save this array
      !         on every processor.
      !         In order to resolve this the first dimension, n_basbas, is distributed
      !         over the column of a square process grid. For n_basbas=100 and a 2x2 
      !         process grid, this would look like
      !
      !         -------------------------------------------------
      !         |                       |                       |
      !         |                       |                       |
      !         |  lvl(1:50,:,:,:,:)    |  lvl(1:50,:,:,:,:)    |
      !         |                       |                       |
      !         -------------------------------------------------
      !         |                       |                       |
      !         |                       |                       |
      !         |  lvl(51:100,:,:,:,:)  |  lvl(51:100,:,:,:,:)  |
      !         |                       |                       |
      !         -------------------------------------------------
      
      !         This however still leads to memory issues for not too large 
      !         calculations. As one can see in the illustration, all processes of 
      !         the same row own a copy of the same sub-array.
      !         By only allocating said sub-array on one of processes of a row
      !         and subsequently sharing it with the other row-process via 
      !         MPI shared window one can immensely reduce the memory requirements
 
      !         Use this routine if the LVL-array has been allocated/initialized
      !         on the relevant processes and only the shared window needs to be set up


      !input
      integer, intent(in) :: cntxt
      integer, intent(in) :: comm_shared, myid_shared
      logical, intent(out) :: is_lvl_root
      integer, intent(out) :: comm_lvl_root, myid_lvl_root, n_tasks_lvl_root
      integer, intent(out) :: n_tasks_shrow, myid_shrow
      integer, intent(out) :: mpi_comm_shared_row

      !local
      integer :: n_lvl_root
      integer :: ierr

      is_lvl_root=.false.
      n_lvl_root=0
      if((CC4S_myid_col .eq. 0) .or. (myid_shared .eq. 0)) then
        is_lvl_root=.true. 
        n_lvl_root=1
      end if 
      if(CC4S_member) then
        call MPI_Comm_split(cntxt, n_lvl_root, CC4S_myid, comm_lvl_root, ierr)
        call MPI_Comm_size(comm_lvl_root, n_tasks_lvl_root, ierr)
        call MPI_Comm_rank(comm_lvl_root, myid_lvl_root, ierr)

      !if(CC4S_member) then
        call MPI_Comm_split(comm_shared, CC4S_myid_row, myid_shared, mpi_comm_shared_row, ierr)
        call MPI_Comm_size(mpi_comm_shared_row, n_tasks_shrow, ierr)
        call MPI_Comm_rank(mpi_comm_shared_row, myid_shrow, ierr)
      end if 

end subroutine CC4S_setup_LVL
!*******************************************
subroutine CC4S_mo_trafo_LVL(lvl_trico, KS_eigenvector, &
                &KS_eigenvector_cmplx, is_lvl_root,&
                &cc4s_member, lbb_row, ubb_row, n_ap, cntxt, &
                &lvl_arr)
        use mpi
        use calculation_data
        use CC4S_blacs_environment, only: CC4S_nprow, CC4S_npcol, &
                &CC4S_myid_row, CC4S_myid_col, CC4S_myid, CC4S_n_tasks, &
                &CC4S_blockbb_row
        implicit none

        !PURPOSE:MO-Transforms one of the basis-dimensions of the 
        !LVL-coefficient array  
        !The original LVL-coefficient array is of dimension
        !(max_n_basis_sp, max_n_basis_sp, max_n_basbas_sp, n_k_points, n_ap)
        !where,
        !max_n_basis_sp: Maximum number of AO basis functions per atom
        !max_n_basbas_sp: Maximum number of aux. basis functions per atom
        !n_k_points: Number of k_points
        !n_ap: (local) number of atom-pairs assigned to process; the total
        !set of atom-pairs is distributed over the rows of our process grid

        !input
        integer :: n_ap
        double complex, dimension(CC4S_max_n_basis_sp, CC4S_max_n_basis_sp, &
                &CC4S_max_n_basbas_sp, CC4S_n_k_points, n_ap), &
                &intent(inout) :: lvl_trico

        double precision, dimension(:,:,:,:), intent(in) :: KS_eigenvector
        double complex, dimension(:,:,:,:), intent(in) :: KS_eigenvector_cmplx
        logical, intent(in) :: is_lvl_root !(local) is true for processes
        !which should have an actual copy of the LVL-array
        logical, intent(in) :: cc4s_member !(local) process which is part of 
        !the square grid used for the interface
        integer, intent(in) :: lbb_row, ubb_row
        integer, intent(in) :: cntxt

        double complex, dimension(lbb_row:ubb_row, CC4S_max_n_basis_sp, &
                &CC4S_nuf_states, CC4S_n_spin, CC4S_n_k_points), intent(out) :: lvl_arr


        !local
        integer :: blockbb_row
        integer :: myid_blacs, n_tasks_blacs
        integer(kind=MPI_ADDRESS_KIND) :: nbytes, offset
        integer :: nbytes2
        integer :: dcsize
        integer :: ierr
        integer :: n_c3fn, n_c3fn_task, rest
        integer :: myoffset
        integer :: lb_ap, ub_ap !, n_ap
        integer :: c0pos
        logical :: i_collect
        integer :: ppos, n_exc, i_cell
        integer :: max_atoms, lbi, ubi, atoms
        integer :: win_trico
        double complex, dimension(CC4S_n_basis, CC4S_n_states, CC4S_n_spin) &
                &:: KS_eigenvector_k
        double complex, dimension(:,:,:,:), allocatable :: lvl_tricoeff_tmp
        double complex, dimension(:,:,:,:), allocatable :: trico_tmp

        integer :: i_exc
        integer :: maxatoms, i
        integer :: nprow, npcol, myid_row, myid_col
        integer :: fencecount
        integer :: i_atom_1, i_atom_2, i_species_1, i_species_2
        integer :: basis_off_2
        integer :: n_sp_basis_1, n_sp_basis_2, i_sp_basis_2, i_sp_basis_1
        integer :: bboff_1, n_spbb_1
        integer, dimension(:,:), allocatable :: ap_pos
        integer :: i_task, per_task, i_ap
        integer :: i_k_point, i_spin
        integer :: count_new

        integer, parameter :: i8 = selected_int_kind(18)
 
        !call MPI_Comm_rank(cntxt, myid_blacs, ierr)
        !call MPI_Comm_size(cntxt, n_tasks_blacs, ierr)

        myid_blacs=CC4S_myid
        n_tasks_blacs=CC4S_n_tasks

        nprow=CC4S_nprow
        npcol=CC4S_npcol
        myid_row=CC4S_myid_row
        myid_col=CC4S_myid_col

        blockbb_row=CC4S_blockbb_row

        if(cc4s_member) then
          if(is_lvl_root) then
            allocate(lvl_tricoeff_tmp(lbb_row:ubb_row, CC4S_max_n_basis_sp, &
                    &CC4S_n_basis, CC4S_n_k_points))

            lvl_tricoeff_tmp(:,:,:,:) = (0.0D0, 0.0D0)
          end if 
          dcsize=16
          nbytes=(int(CC4S_max_n_basis_sp,i8)**2)*int(CC4S_max_n_basbas_sp,i8)*&
                  &int(CC4S_n_k_points,i8)*int(n_ap,i8)*int(dcsize,MPI_ADDRESS_KIND)
          call MPI_Win_create(lvl_trico, nbytes, dcsize, MPI_INFO_NULL, &
                  &cntxt, win_trico, ierr)
          call MPI_Win_fence(0, win_trico, ierr)

          n_c3fn=CC4S_n_atoms*CC4S_n_atoms*(CC4S_n_cells+1)
          n_c3fn_task=n_c3fn/n_tasks_blacs
          rest=n_c3fn-n_tasks_blacs*n_c3fn_task
          if(myid_blacs .lt. rest) then
            n_c3fn_task=n_c3fn_task+1
            myoffset=myid_blacs*n_c3fn_task
          else
            myoffset=myid_blacs*n_c3fn_task+rest
          end if 

          lb_ap=myoffset/(CC4S_n_cells+1)
          ub_ap=(myoffset+n_c3fn_task-1)/(CC4S_n_cells+1)
          n_ap=ub_ap-lb_ap+1

          allocate(ap_pos(3,0:CC4S_n_atoms*CC4S_n_atoms-1))
          i_task=0
          if(i_task.lt.rest) then
            per_task=n_c3fn/n_tasks_blacs+1
          else
            per_task=n_c3fn/n_tasks_blacs
          end if

          i_cell=0
          do i_ap=0,CC4S_n_atoms*CC4S_n_atoms-1
            do while(i_cell .ge. per_task)
              i_cell=i_cell-per_task
              i_task=i_task+1
              if(i_task .lt. rest) then
                per_task=n_c3fn/n_tasks_blacs+1
              else
                per_task=n_c3fn/n_tasks_blacs
              end if
            end do
            ap_pos(1,i_ap)=i_task
            ap_pos(2,i_ap)=i_cell/(CC4S_n_cells+1)
            if (ap_pos(2,i_ap)*(CC4S_n_cells+1).lt.i_cell) then
              ap_pos(2,i_ap)=ap_pos(2,i_ap)+1
            end if 
            if (i_ap.gt.0) then
              if (ap_pos(2,i_ap).gt.0) then
                ap_pos(3,i_ap-1)=i_task
              else
                ap_pos(3,i_ap-1)=i_task-1
              end if 
            end if 
            i_cell=i_cell+CC4S_n_cells+1
          end do
          ap_pos(3,CC4S_n_atoms*CC4S_n_atoms-1)=n_tasks_blacs-1

          c0pos=ub_ap*(CC4S_n_cells+1)

          i_collect=(c0pos .ge. myoffset) .and. &
                  &(c0pos .lt. myoffset+n_c3fn_task)

          if(i_collect) then
            allocate(trico_tmp(CC4S_max_n_basis_sp, CC4S_max_n_basis_sp, &
                    &CC4S_max_n_basbas_sp, CC4S_n_k_points))
          end if 

          nbytes2=(CC4S_max_n_basis_sp**2)*CC4S_max_n_basbas_sp*CC4S_n_k_points
          offset=0
          ppos=myid_blacs
          n_exc=(CC4S_n_cells+1)/(n_c3fn/n_tasks_blacs)+1
          i_cell=n_c3fn_task+myoffset-ub_ap*(CC4S_n_cells+1)

          do i_exc=1,n_exc
            if(i_collect .and. (i_cell.lt.(CC4S_n_cells+1))) then
              ppos=ppos+1
              i_cell=i_cell+n_c3fn/n_tasks_blacs
              if(ppos.lt.rest) then
                i_cell=i_cell+1
              end if
              call MPI_get(trico_tmp, nbytes2, MPI_DOUBLE_COMPLEX, &
                      &ppos, offset, nbytes2, MPI_DOUBLE_COMPLEX, &
                      &win_trico, ierr) 
              call MPI_Win_fence(0, win_trico, ierr)
              lvl_trico(:,:,:,:,n_ap)=lvl_trico(:,:,:,:,n_ap)+trico_tmp
            else
              call MPI_Win_fence(0, win_trico, ierr)  
            end if 
          end do

          if(i_collect) then
            deallocate(trico_tmp)
          end if 

          maxatoms=0
          do i=0, nprow-1
            lbi=i*blockbb_row+1
            ubi=min((i+1)*blockbb_row, CC4S_n_basbas)
            atoms = CC4S_basbas_atom(ubi)-CC4S_basbas_atom(lbi)+1
            if(atoms .gt. maxatoms) then
              maxatoms=atoms
            end if 
          end do

          fencecount=0

          if(is_lvl_root) then
            allocate(trico_tmp(CC4S_max_n_basis_sp,CC4S_max_n_basis_sp,&
                    &CC4S_max_n_basbas_sp,CC4S_n_k_points))

            do i_atom_1=CC4S_basbas_atom(lbb_row), CC4S_basbas_atom(ubb_row)
              i_species_1 = CC4S_species(i_atom_1)
              n_sp_basis_1 = CC4S_sp2n_basis_sp(i_species_1)
              bboff_1 = CC4S_atom2basbas_off(i_atom_1)
              n_spbb_1 = CC4S_sp2n_basbas_sp(i_species_1)

              lbi=max(lbb_row-bboff_1,1)
              ubi=min(ubb_row-bboff_1,n_spbb_1)

              do i_atom_2=1,CC4S_n_atoms
                i_species_2 = CC4S_species(i_atom_2)
                basis_off_2 = CC4S_atom2basis_off(i_atom_2)
                n_sp_basis_2 = CC4S_sp2n_basis_sp(i_species_2)

                i_ap=(i_atom_2-1)*CC4S_n_atoms+i_atom_1-1
                offset=ap_pos(2,i_ap)*nbytes2

                call MPI_get(trico_tmp, nbytes2, MPI_DOUBLE_COMPLEX, &
                        &ap_pos(1, i_ap), offset, nbytes2, MPI_DOUBLE_COMPLEX, &
                        &win_trico, ierr)
                call MPI_Win_fence(0, win_trico, ierr)

                fencecount=fencecount+1
                do i_k_point=1,CC4S_n_k_points
                  do i_sp_basis_2=1,n_sp_basis_2
                    do i_sp_basis_1=1,n_sp_basis_1
                      do i=lbi,ubi
                      lvl_tricoeff_tmp(bboff_1+i, i_sp_basis_1,&
                      &basis_off_2+i_sp_basis_2,i_k_point) =&
                      &lvl_tricoeff_tmp(bboff_1+i, i_sp_basis_1,&
                      &basis_off_2+i_sp_basis_2,i_k_point) +&
                      &trico_tmp(i_sp_basis_1,i_sp_basis_2,i,i_k_point)
                      end do
                    end do
                  end do
                end do

              end do !i_atom_1
            end do !i_atom_2
            deallocate(trico_tmp)
          end if 

          do while(fencecount .lt. CC4S_n_atoms*maxatoms)
            call MPI_Win_fence(0, win_trico, ierr)
            fencecount=fencecount+1
          end do

          call MPI_Win_free(win_trico, ierr)
        end if !cc4s_member 
        
        if(is_lvl_root .and. CC4S_member) then
          do i_k_point=1,CC4S_n_k_points
            if(CC4S_isreal) then
              KS_eigenvector_k(:,:,:)=KS_eigenvector(:,:,:,i_k_point)
            else
              KS_eigenvector_k(:,:,:)=KS_eigenvector_cmplx(:,:,:,i_k_point)
            end if

            do i_spin=1,CC4S_n_spin
              count_new=(ubb_row-lbb_row+1)*CC4S_max_n_basis_sp
              call zgemm('N', 'N', count_new, CC4S_nuf_states, CC4S_n_basis, &
                      &(1.0D0, 0.0D0), lvl_tricoeff_tmp(:,:,:,i_k_point), &
                      &count_new, KS_eigenvector_k(:,CC4S_n_low_state:CC4S_n_states,i_spin), &
                      &CC4S_n_basis, (0.0D0, 0.0D0), lvl_arr(lbb_row, 1, 1, &
                      &i_spin, i_k_point), count_new)
            end do         
          end do
        end if 

        if(allocated(lvl_tricoeff_tmp)) then
          deallocate(lvl_tricoeff_tmp)
        end if 

end subroutine CC4S_mo_trafo_LVL
!***************************************************************
subroutine CC4S_shm_LVL(CC4S_member, is_lvl_root, momo_lvl_real, momo_lvl_cmplx, &
                &lvl_shm_real, lvl_shm_cmplx, mpi_comm_shared_row)
        use calculation_data, only: CC4S_isreal, CC4S_max_n_basis_sp, CC4S_n_states, &
                &CC4S_n_spin, CC4S_n_k_points, CC4S_nuf_states
        use CC4S_blacs_environment, only: CC4S_ubb_row, CC4S_lbb_row, CC4S_myid
        use iso_c_binding
        use mpi
        implicit none

        !input
        logical, intent(in) :: CC4S_member, is_lvl_root
        double precision, dimension(:,:,:,:,:), allocatable, intent(inout) :: momo_lvl_real
        double complex, dimension(:,:,:,:,:), allocatable, intent(inout) :: momo_lvl_cmplx
        double precision, dimension(:,:,:,:,:), pointer, contiguous, intent(out) :: lvl_shm_real 
        double complex, dimension(:,:,:,:,:), pointer, contiguous, intent(out) :: lvl_shm_cmplx
        integer, intent(in) :: mpi_comm_shared_row

        !local
        integer(kind=MPI_ADDRESS_KIND) :: wsize
        integer :: mem_unit, disp_unit
        integer :: ierr
        type(c_ptr) :: lvl_shm_cptr
        !integer :: lvl_shm_win
        integer, dimension(5) :: win_lvl_shape

        lvl_shm_real => null()
        lvl_shm_cmplx => null()
        if(CC4S_isreal) then
          mem_unit=8
        else
          mem_unit=16
        end if 

        if(CC4S_member) then
          if(is_lvl_root) then
            wsize=mem_unit*(CC4S_ubb_row-CC4S_lbb_row+1)*CC4S_max_n_basis_sp*&
                    &CC4S_nuf_states*CC4S_n_spin*CC4S_n_k_points
          else
            wsize=0
          end if 


          call MPI_Win_allocate_shared(wsize, mem_unit, MPI_INFO_NULL, mpi_comm_shared_row, &
                  &lvl_shm_cptr, lvl_shm_win, ierr)
          !write(*,*) "Successful MPI_Win_allocate_shared"
          call MPI_Win_shared_query(lvl_shm_win, MPI_PROC_NULL, wsize, disp_unit, &
                  &lvl_shm_cptr, ierr)
          !write(*,*) "Successful MPI_Win_shared_query"
          win_lvl_shape=[(CC4S_ubb_row-CC4S_lbb_row+1), CC4S_max_n_basis_sp, CC4S_nuf_states, &
                  &CC4S_n_spin, CC4S_n_k_points]

          if(CC4S_isreal) then
            call c_f_pointer(lvl_shm_cptr, lvl_shm_real, win_lvl_shape)
            call MPI_Win_lock_all(MPI_MODE_NOCHECK, lvl_shm_win, ierr)
            if(is_lvl_root) then
              lvl_shm_real(:,:,:,:,:) = momo_lvl_real(:,:,:,:,:) 
            end if
            allocate(momo_lvl_cempty(1, 1, 1, 1, 1))
            lvl_shm_cmplx => momo_lvl_cempty(:,:,:,:,:)
          else
            call c_f_pointer(lvl_shm_cptr, lvl_shm_cmplx, win_lvl_shape)
            call MPI_Win_lock_all(MPI_MODE_NOCHECK, lvl_shm_win, ierr)
            if(is_lvl_root) then
              lvl_shm_cmplx(:,:,:,:,:) = momo_lvl_cmplx(:,:,:,:,:)
            end if
            allocate(momo_lvl_rempty(1, 1, 1, 1, 1)) 
            lvl_shm_real => momo_lvl_rempty(:,:,:,:,:)
          end if 

          call MPI_Win_sync(lvl_shm_win, ierr)
          call MPI_Barrier(mpi_comm_shared_row, ierr)
        else
          if(CC4S_isreal) then
            deallocate(momo_lvl_real)
            allocate(momo_lvl_rempty(CC4S_lbb_row:CC4S_ubb_row, 1, 1, 1, CC4S_n_k_points))
            allocate(momo_lvl_cempty(CC4S_lbb_row:CC4S_ubb_row, 1, 1, 1, CC4S_n_k_points))
            lvl_shm_real => momo_lvl_rempty(:,:,:,:,:)
            lvl_shm_cmplx => momo_lvl_cempty(:,:,:,:,:)
          else
            deallocate(momo_lvl_cmplx)
            allocate(momo_lvl_rempty(CC4S_lbb_row:CC4S_ubb_row, 1, 1, 1, CC4S_n_k_points))
            allocate(momo_lvl_cempty(CC4S_lbb_row:CC4S_ubb_row, 1, 1, 1, CC4S_n_k_points))
            lvl_shm_cmplx => momo_lvl_cempty(:,:,:,:,:)
            lvl_shm_real => momo_lvl_rempty(:,:,:,:,:)
          end if 
        end if 

end subroutine CC4S_shm_LVL
!**********************************
subroutine cleanup_shm_LVL(shared_LVL_real, shared_LVL_cmplx)
        use CC4S_blacs_environment, only: CC4S_member
        implicit none

        !input
        double precision, dimension(:,:,:,:,:), pointer, optional :: shared_LVL_real
        double complex, dimension(:,:,:,:,:), pointer, optional :: shared_LVL_cmplx
        !local
        integer :: ierr
        character(len=200) :: func
        character(len=200) :: err_msg

        func='cleanup_shm_LVL'

        if(CC4S_member) then
          call MPI_Win_unlock_all(lvl_shm_win, ierr)
          call MPI_Win_free(lvl_shm_win, ierr)
        end if 
        if(present(shared_LVL_real) .and. present(shared_LVL_cmplx)) then
          shared_LVL_real => null()
          shared_LVL_cmplx => null()
        elseif(present(shared_LVL_real)) then
          shared_LVL_real => null()
        elseif(present(shared_LVL_cmplx)) then
          shared_LVL_cmplx => null()
        else
          err_msg='No shared LVL-pointer submitted to be deallocated'
          call error_stop(func, err_msg)
        end if 
end subroutine cleanup_shm_LVL
end module CC4S_LVL
