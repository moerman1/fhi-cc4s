module CC4S_adapt_data_file_suffix
#ifdef __INTEL_COMPILER
  use ifport
#endif
implicit none

contains
  subroutine adapt_data_file_suffix(debug, data_filename_dat, data_filename_bin, comm)
    !Intel compiler requires ifport-module to use 
    !rename-function

    use mpi
    implicit none
 
    logical :: debug
    character(len=*) :: data_filename_dat, data_filename_bin
    integer :: comm

    !local
    integer :: myid
    integer :: ierr
    character(len=200) :: destination_filename
    integer :: prefix_end_position

    call MPI_Comm_rank(comm, myid, ierr)

    prefix_end_position = scan(trim(data_filename_dat),".", BACK= .true.)
    destination_filename = trim(data_filename_dat(1:prefix_end_position))//"elements"
    if(myid .eq. 0) then
      if(debug) then
        ierr = rename(trim(data_filename_dat), trim(destination_filename))
      else
        ierr = rename(trim(data_filename_bin), trim(destination_filename))
      end if
    end if 

  end subroutine adapt_data_file_suffix
end module CC4S_adapt_data_file_suffix
