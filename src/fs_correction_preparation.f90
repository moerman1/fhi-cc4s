module finite_size_correction

  !quantity, where V^(-0.5) is transformed to PWs in one index
  !and still in RI-basis in the second index. If no OAF-trafo has been requested
  !this is the central finites-size correction-related quantity to be written out
  !for CC4S to read. 
  !If an OAF-trafo has been performed, this object needs to be contracted with the
  !OAF-trafo matrix to obtain the functionally equivalent quantity.
  !Either way, this variable will eventually contain the final quantity for CC4S,
  !either with an additional OAF-transformation or without if not requested.
  double complex, allocatable, dimension(:,:) :: V_inv_sqrt_mu_G
  double complex, allocatable, dimension(:,:) :: V_inv_sqrt_mu_G_OAF
  double complex, allocatable, dimension(:,:) :: V_G_diag_all_kq !(n_pws x n_kq_points)
  double complex, allocatable, dimension(:,:) :: inverted_basis_transform_at_k
  
  integer :: coulomb_inverse_sqrt_fh
  integer :: pw_transform_fh
contains

subroutine invert_basis_transformation_matrix(basis_transform_at_k, basis_transform_desc, inverted_basis_transform_desc, &
                         &n_pws, n_bb, loc_shape_basis_transform, loc_shape_inverted_basis_transform, &
                         &lefteig_desc, righteig_desc, &
                         &comm)

  use CC4S_sqrt_coulmat, only : CC4S_compute_pseudo_inverse_via_svd_general
  use CC4S_blacs_environment, only : CC4S_loc_pw_row_opt, CC4S_loc_pw_col_opt, CC4S_loc_bb_row_opt,  CC4S_loc_bb_col_opt, &
                                     &CC4S_pwsxbb_opt_desc, CC4S_GxGprime_opt_desc, CC4S_bb_opt_desc
  implicit none

  double complex, dimension(:,:), intent(in) :: basis_transform_at_k !<G|mu>
  integer, dimension(9), intent(in) :: basis_transform_desc, inverted_basis_transform_desc
  integer, intent(in) :: n_pws, n_bb
  integer, dimension(4), intent(in) :: loc_shape_basis_transform, loc_shape_inverted_basis_transform
  integer, dimension(9), intent(in) :: lefteig_desc, righteig_desc
  integer :: comm

  !local
  integer :: lG_row, uG_row, lbb_col, ubb_col
  integer :: lbb_row, ubb_row, lG_col, uG_col
  integer :: ierr
  integer :: myid

  call MPI_Comm_rank(comm, myid, ierr)

  lG_row  = loc_shape_basis_transform(1)
  uG_row  = loc_shape_basis_transform(2)
  lbb_col = loc_shape_basis_transform(3)
  ubb_col = loc_shape_basis_transform(4)

  lbb_row = loc_shape_inverted_basis_transform(1)
  ubb_row = loc_shape_inverted_basis_transform(2)
  lG_col  = loc_shape_inverted_basis_transform(3)
  uG_col  = loc_shape_inverted_basis_transform(4)

  if(.not. allocated(inverted_basis_transform_at_k)) then 
    allocate(inverted_basis_transform_at_k(lbb_row:ubb_row, lG_col:uG_col))
  end if 
  inverted_basis_transform_at_k(:,:) = (0.0d0, 0.0d0)

  if(myid .eq. 0) then
    write(*,*) "Inverting <G|mu...>"
  end if

  call CC4S_compute_pseudo_inverse_via_svd_general(n_pws, n_bb, &
                        &lG_row, uG_row, lbb_col, ubb_col, &
                        &lbb_row, ubb_row, lG_col, uG_col, &
                        &basis_transform_at_k, basis_transform_desc, &
                        &inverted_basis_transform_at_k, inverted_basis_transform_desc, &
                        &CC4S_loc_pw_row_opt, CC4S_loc_pw_col_opt, CC4S_loc_bb_row_opt, &
                        &CC4S_loc_bb_col_opt, CC4S_pwsxbb_opt_desc, &
                        &lefteig_desc, righteig_desc, CC4S_GxGprime_opt_desc, &
                        &CC4S_bb_opt_desc, comm, .true.)

end subroutine invert_basis_transformation_matrix

subroutine transform_coulmat_to_pws(V_mu_nu, loc_shape_V_mu_nu, V_mu_nu_desc, &
                                   &ri_to_pw_matrix, loc_shape_ri_to_pw_matrix, ri_to_pw_matrix_desc, &
                                   &n_bb, n_pws, loc_shape_V_G_Q, V_G_Q_desc, comm, current_kq_pair)
  use mpi
  use CC4S_blacs_environment, only: CC4S_member
  use calculation_data, only: CC4S_n_kq_points
  implicit none

  integer, dimension(4), intent(in) :: loc_shape_V_mu_nu
  integer, dimension(4), intent(in) :: loc_shape_ri_to_pw_matrix
  integer, dimension(4), intent(in) :: loc_shape_V_G_Q
  double complex, dimension(loc_shape_V_mu_nu(1):loc_shape_V_mu_nu(2),&
                           &loc_shape_V_mu_nu(3):loc_shape_V_mu_nu(4)), intent(inout) :: V_mu_nu !(n_basbas x n_basbas)
  integer, dimension(9), intent(in) :: V_mu_nu_desc
  double complex, dimension(loc_shape_ri_to_pw_matrix(1):loc_shape_ri_to_pw_matrix(2),&
                           &loc_shape_ri_to_pw_matrix(3):loc_shape_ri_to_pw_matrix(4)), intent(in) :: ri_to_pw_matrix !(n_basbas x n_pws)
  integer, dimension(9), intent(in) :: ri_to_pw_matrix_desc
  integer, intent(in) :: n_bb
  integer, intent(in) :: n_pws
  !double precision, allocatable, dimension(:), intent(out) :: V_G !(n_pws)
  integer, dimension(9), intent(in) :: V_G_Q_desc
  integer, intent(in) :: comm
  integer, intent(in) :: current_kq_pair

  !local
  integer, dimension(2) :: V_mu_nu_dims
  integer, dimension(2) :: ri_to_pw_matrix_dims
  integer, dimension(2) :: V_G_Q_dims
  double complex, dimension(:,:), allocatable :: V_mu_nu_H !hermitian adjoint
  double complex, allocatable, dimension(:,:) :: temp_one_sided_transform
  double complex, allocatable, dimension(:,:) :: V_G_Q 
  integer :: ierr
  integer :: myid
  double precision :: checkloc, checkglob
  integer :: i,j,k

  double complex, allocatable, dimension(:) :: V_G !(n_pws)
  integer :: lpw_row, upw_row, lpw_col, upw_col
  integer :: local_min_pw_idx
  double complex, allocatable, dimension(:) :: local_V_G_diag
  integer :: comm_diag, comm_offdiag
  integer :: myid_diag
  integer, allocatable, dimension(:) :: recvcounts_diag
  integer, allocatable, dimension(:) :: displacements_diag
  integer :: local_number_diag_entries
  integer :: comm_diag_size

  call MPI_Comm_rank(comm, myid, ierr)

  if(myid .eq. 0) then
    write(*,*) "Transforming aux. Coulomb matrix to plane waves."
  end if

  V_mu_nu_dims(1)=loc_shape_V_mu_nu(2)-loc_shape_V_mu_nu(1)+1
  V_mu_nu_dims(2)=loc_shape_V_mu_nu(4)-loc_shape_V_mu_nu(3)+1

  ri_to_pw_matrix_dims(1) = loc_shape_ri_to_pw_matrix(2) - loc_shape_ri_to_pw_matrix(1) + 1
  ri_to_pw_matrix_dims(2) = loc_shape_ri_to_pw_matrix(4) - loc_shape_ri_to_pw_matrix(3) + 1

  V_G_Q_dims(1) = loc_shape_V_G_Q(2) - loc_shape_V_G_Q(1) + 1
  V_G_Q_dims(2) = loc_shape_V_G_Q(4) - loc_shape_V_G_Q(3) + 1

  allocate(V_mu_nu_H(V_mu_nu_dims(1), V_mu_nu_dims(2)))

  V_mu_nu_H(:,:) = (0.0d0, 0.0d0)

  V_G(:) = 0.0d0

  call pztranc(n_bb, n_bb, (1.0D0,0.0D0), V_mu_nu, 1, 1, V_mu_nu_desc, &
                &(0.0D0, 0.0D0), V_mu_nu_H, 1, 1, V_mu_nu_desc) 

  checkloc=sum(abs(V_mu_nu-V_mu_nu_H))
  call mpi_allreduce(checkloc, checkglob,1,&
       &MPI_DOUBLE_PRECISION,MPI_SUM,comm,ierr)
  if(myid .eq. 0) then
    write(*,'(A,1X,ES10.3)') "Difference between Coulomb matrix and its hermitian adjoint:",&
              &checkglob
  end if
  
  !Symmetrize the Coulomb matrix (this should be the only modification of the input matrix)
  V_mu_nu(:,:)=0.5D0*(V_mu_nu(:,:)+V_mu_nu_H(:,:)) 

  call MPI_Barrier(comm, ierr)
  if(myid .eq. 0) then
    write(*,*) "Symmetrized real-space Coulomb matrix one last time before transformation..."
  end if

  !Perform 1st RI->PW transformation: <mu|V|nu> <nu|G> = <mu|V|G>
  allocate(temp_one_sided_transform(ri_to_pw_matrix_dims(1), ri_to_pw_matrix_dims(2)))
  temp_one_sided_transform(:,:) = (0.0d0, 0.0d0)
  call pzgemm('N', 'N', n_bb, n_pws, n_bb, (1.0D0, 0.0D0), V_mu_nu(:,:), &
                &1, 1, V_mu_nu_desc, ri_to_pw_matrix(:,:), 1, 1, ri_to_pw_matrix_desc, (0.0D0, 0.0D0), &
                &temp_one_sided_transform(:,:), 1, 1, ri_to_pw_matrix_desc)

  call MPI_Barrier(comm, ierr)
  if(myid .eq. 0) then
    write(*,*) "Transformed <mu|V|nu> -> <mu|V|G>"
  end if

  !Perform 2nd RI-PW transformation:<Q|mu> <mu|V|G> = <Q|V|G>
  !allocate(V_G_Q(V_G_Q_dims(1),V_G_Q_dims(2)))
  allocate(V_G_Q(loc_shape_V_G_Q(1):loc_shape_V_G_Q(2),loc_shape_V_G_Q(3):loc_shape_V_G_Q(4)))
  V_G_Q(:,:) = (0.0d0, 0.0d0)
  call pzgemm('C', 'N', n_pws, n_pws, n_bb, (1.0D0, 0.0D0), ri_to_pw_matrix(:,:), &
                &1, 1, ri_to_pw_matrix_desc, temp_one_sided_transform(:,:), 1, 1, ri_to_pw_matrix_desc, (0.0D0, 0.0D0), &
                &V_G_Q(:,:), 1, 1, V_G_Q_desc)

  call MPI_Barrier(comm, ierr)
  if(myid .eq. 0) then
    write(*,*) "Transformed <mu|V|G> -> <G'|V|G>"
  end if

  !Write the resulting V_G_Q to stdout
!  do i=loc_shape_V_G_Q(1), loc_shape_V_G_Q(2)
!    do j=loc_shape_V_G_Q(3), loc_shape_V_G_Q(4)
!      !write(*,*) "V_G_Q",i,j, dble(V_G_Q(i,j))
!      write(*,*) "V_G_Q",i,j, V_G_Q(i,j)
!    end do
!  end do

!!!     !Only keep diagonal part of dble(V_G_Q) as approximation of V_G
!!!     !For that collect the diagonal part of V_G_Q on rank 0 
!!!     !As a 1D-quantity with at most ~100000 elements, this should never lead to memory problems.
!!!     !First, create new sub-communicator which only contains tasks of global communicator,
!!!     !which possess diagonal parts of V_G_Q
!!!     lpw_row = loc_shape_V_G_Q(1)
!!!     upw_row = loc_shape_V_G_Q(2)
!!!     lpw_col = loc_shape_V_G_Q(3)
!!!     upw_col = loc_shape_V_G_Q(4)
!!!   
!!!     local_min_pw_idx = minloc(loc_shape_V_G_Q, dim=1)
!!!   
!!!     if(lpw_row == lpw_col .and. upw_row == upw_col .and. CC4S_member) then    
!!!       !If row- and col- borders are identical, there are certainly 
!!!       !diagonal elements in this block
!!!   
!!!       call MPI_Comm_split(comm, 0, lpw_row, comm_diag, ierr)
!!!   
!!!       allocate(local_V_G_diag(lpw_row:upw_row))
!!!       do i=lpw_row, upw_row
!!!         local_V_G_diag(i) = V_G_Q(i,i)
!!!       end do
!!!   
!!!     elseif(local_min_pw_idx == 1 .and. lpw_col <= upw_row .and. CC4S_member) then
!!!       !If the row-range has the lowest index and lower border of 
!!!       !col-idx is below upper border of row-idx, there are
!!!       !diagonal elements in this block
!!!       
!!!       call MPI_Comm_split(comm, 0, lpw_col, comm_diag, ierr)
!!!   
!!!       allocate(local_V_G_diag(lpw_col:upw_row))
!!!       do i=lpw_col,upw_row
!!!         local_V_G_diag(i) = V_G_Q(i,i)
!!!       end do
!!!     elseif(local_min_pw_idx == 3 .and. lpw_row <= upw_col .and. CC4S_member) then
!!!       !Same as above but col-idx is lowest, again diagonal elements
!!!       !must be present
!!!    
!!!       call MPI_Comm_split(comm, 0, lpw_row, comm_diag, ierr)
!!!   
!!!       allocate(local_V_G_diag(lpw_row:upw_col))
!!!       do i=lpw_row,upw_col
!!!         local_V_G_diag(i) = V_G_Q(i,i)
!!!       end do
!!!   
!!!     else
!!!       !Contains only off-diagonal terms, exclude from communicator
!!!       call MPI_Comm_split(comm, MPI_UNDEFINED, MPI_UNDEFINED, comm_offdiag, ierr)
!!!     end if
!!!   
!!!     !At this point comm_diag contains all ranks contributing to the diagonal elements
!!!     !Get ranks of this new communicator
!!!     if(allocated(local_V_G_diag)) then
!!!       call MPI_Comm_rank(comm_diag, myid_diag, ierr)
!!!     else
!!!       myid_diag = -1 !Not part of comm_diag
!!!     end if
!!!   
!!!     if(myid_diag == 0) then
!!!       !myid_diag 0 will be root of MPI_Gather
!!!   
!!!       call MPI_Comm_size(comm_diag, comm_diag_size, ierr)
!!!       allocate(recvcounts_diag(comm_diag_size))
!!!       recvcounts_diag(:) = -1
!!!   
!!!       local_number_diag_entries = size(local_V_G_diag)
!!!    
!!!       call MPI_Gather(local_number_diag_entries, 1, MPI_INTEGER, &
!!!                      &recvcounts_diag, 1, MPI_INTEGER, &
!!!                      &0, comm_diag, ierr)
!!!   
!!!       allocate(displacements_diag(comm_diag_size))
!!!       displacements_diag(:) = 0
!!!       do i=2,comm_diag_size
!!!         displacements_diag(i) = displacements_diag(i-1) + recvcounts_diag(i-1)
!!!       end do 
!!!   
!!!       !allocate(V_G(n_pws))
!!!       !V_G(:) = (0.0d0, 0.0d0)
!!!       if(.not. allocated(V_G_diag_all_kq)) then
!!!         allocate(V_G_diag_all_kq(n_pws, CC4S_n_kq_points))
!!!         V_G_diag_all_kq(:,:) = (0.0d0, 0.0d0)
!!!       else
!!!       end if
!!!   
!!!   !    call MPI_Gatherv(local_V_G_diag, local_number_diag_entries, &
!!!   !                    &MPI_DOUBLE_COMPLEX, V_G, recvcounts_diag, &
!!!   !                    &displacements_diag, MPI_DOUBLE_COMPLEX, 0, comm_diag, ierr) 
!!!   
!!!       call MPI_Gatherv(local_V_G_diag, local_number_diag_entries, &
!!!                       &MPI_DOUBLE_COMPLEX, V_G_diag_all_kq(:,current_kq_pair), recvcounts_diag, &
!!!                       &displacements_diag, MPI_DOUBLE_COMPLEX, 0, comm_diag, ierr)
!!!     elseif(myid_diag > 0) then
!!!       call MPI_Comm_size(comm_diag, comm_diag_size, ierr)
!!!       !dummy
!!!       allocate(recvcounts_diag(1))
!!!       recvcounts_diag(:) = -1
!!!   
!!!       local_number_diag_entries = size(local_V_G_diag)
!!!   
!!!       call MPI_Gather(local_number_diag_entries, 1, MPI_INTEGER, &
!!!                      &recvcounts_diag, 1, MPI_INTEGER, &
!!!                      &0, comm_diag, ierr)
!!!   
!!!       !dummies
!!!       allocate(displacements_diag(1))
!!!       allocate(V_G(1))
!!!   
!!!       call MPI_Gatherv(local_V_G_diag, local_number_diag_entries, &
!!!                       &MPI_DOUBLE_COMPLEX, V_G, recvcounts_diag, &
!!!                       &displacements_diag, MPI_DOUBLE_COMPLEX, 0, comm_diag, ierr)   
!!!     end if
!!!    
!!!     !Sloppy write-out to file of V_G. No real need for parallel write due
!!!     !to small size of object
!!!     if(myid_diag == 0 .and. current_kq_pair == CC4S_n_kq_points) then
!!!       !Only write out on rank 0 after completing calculation for final k-q-point
!!!       open(unit=771, file="CoulombPotential.elements", iostat=ierr)
!!!       !do i=1,n_pws
!!!       !  write(771,*) dble(V_G(i))
!!!       !end do
!!!   
!!!       do k=1,CC4S_n_kq_points
!!!         do i=1,n_pws
!!!           write(771,*) dble(V_G_diag_all_kq(i,k))
!!!         end do
!!!       end do
!!!   
!!!       close(unit=771)
!!!     end if 
end subroutine transform_coulmat_to_pws

subroutine contract_V_inv_sqrt_with_nu_G(V_mu_nu, loc_shape_V_mu_nu, V_mu_nu_desc, &
                                        &ri_to_pw_matrix, loc_shape_ri_to_pw_matrix, &
                                        &ri_to_pw_matrix_desc, n_bb, n_pws, comm, current_kq_pair)
  use mpi
  use CC4S_sqrt_coulmat
  use CC4S_blacs_environment, only: CC4S_n_tasks, CC4S_lbb_row, CC4S_lpw_col, CC4S_myid
  use calculation_data, only: CC4S_n_kq_points
  use CC4S_parallel_io, only: CC4S_parallel_io_V_inv_sqrt_complex 
  implicit none

  double complex, dimension(:,:), intent(in) :: V_mu_nu
  integer, dimension(4), intent(in) :: loc_shape_V_mu_nu
  integer, dimension(9), intent(in) :: V_mu_nu_desc
  double complex, dimension(:,:), intent(in) :: ri_to_pw_matrix
  integer, dimension(4), intent(in) :: loc_shape_ri_to_pw_matrix
  integer, dimension(9), intent(in) :: ri_to_pw_matrix_desc
  integer, intent(in) :: n_bb
  integer, intent(in) :: n_pws
  integer, intent(in) :: comm
  integer, intent(in) :: current_kq_pair

  !PURPOSE: Simply transform V_mu_nu -> V^(-0.5)_mu_nu
  !and then the contraction/matrix multiplication
  ! <mu|V^(-0.5)|nu><nu|G> = <mu|V^(-0.5)|G>

  !local
  integer :: i_k_point
  double complex, allocatable, dimension(:,:) :: V_inv_sqrt_mu_nu
  integer, dimension(2) :: coulomb_inverse_sqrt_sizes
  integer, dimension(2) :: coulomb_inverse_sqrt_subsizes
  integer, dimension(2) :: coulomb_inverse_sqrt_starts

!  call open_V_inv_sqrt_mu_G_filehandle()

  if(.not. allocated(V_inv_sqrt_mu_G)) then
    allocate(V_inv_sqrt_mu_G(loc_shape_V_mu_nu(1):loc_shape_V_mu_nu(2), &
                            &loc_shape_ri_to_pw_matrix(3):loc_shape_ri_to_pw_matrix(4)))
  end if

  V_inv_sqrt_mu_G(:,:) = (0.0d0, 0.0d0)

  allocate(V_inv_sqrt_mu_nu(loc_shape_V_mu_nu(1):loc_shape_V_mu_nu(2), &
                           &loc_shape_V_mu_nu(3):loc_shape_V_mu_nu(4)))


!  do i_k_point=1,CC4S_n_kq_points

    V_inv_sqrt_mu_nu(:,:) = V_mu_nu(:,:)

    !V_mu_nu -> V^(-0.5)_mu_nu (for now, no debug mode)
    if(CC4S_myid .eq. 0) then
      write(*,*) "Calculating inverse sqrt of V : V^(-0.5)"
    end if

    if(CC4S_n_tasks .eq. 1) then
      call CC4S_sqrt_lapack(V_inv_sqrt_mu_nu, n_bb, &
                  &V_mu_nu_desc, loc_shape_V_mu_nu, comm, .false., power=-0.5d0) 
    else
      call CC4S_sqrt_scalapack(V_inv_sqrt_mu_nu, n_bb, &
                  &V_mu_nu_desc, loc_shape_V_mu_nu, comm, .false., power=-0.5d0)
    end if

    ! <mu|V^(-0.5)|nu><nu|G> = <mu|V^(-0.5)|G>
    call pzgemm('N', 'N', n_bb, n_pws, n_bb, (1.0D0, 0.0D0), V_inv_sqrt_mu_nu(:,:), &
                &1, 1, V_mu_nu_desc, ri_to_pw_matrix(:,:), 1, 1, ri_to_pw_matrix_desc, (0.0D0, 0.0D0), &
                &V_inv_sqrt_mu_G(:,:), 1, 1, ri_to_pw_matrix_desc)

    !Result is stored in V_inv_sqrt_mu_G for each k(k-q) point

    coulomb_inverse_sqrt_sizes=[n_bb, n_pws] !N_G x N_n x N_n x 1
    coulomb_inverse_sqrt_subsizes=shape(V_inv_sqrt_mu_G(:,:))
    coulomb_inverse_sqrt_starts=[CC4S_lbb_row-1, CC4S_lpw_col-1]
    call CC4S_parallel_io_V_inv_sqrt_complex(V_inv_sqrt_mu_G(:,:), coulomb_inverse_sqrt_fh, &
             &coulomb_inverse_sqrt_sizes, coulomb_inverse_sqrt_subsizes, &
             &coulomb_inverse_sqrt_starts, n_bb, current_kq_pair) 

!  end do 

 
end subroutine contract_V_inv_sqrt_with_nu_G


subroutine contract_V_inv_sqrt_with_nu_G_OAF(V_mu_nu, loc_shape_V_mu_nu, V_mu_nu_desc, &
                                        &ri_to_pw_matrix, loc_shape_ri_to_pw_matrix, &
                                        &ri_to_pw_matrix_desc, n_bb, n_pws, comm, max_singval_kq, current_kq_pair)
  use mpi
  use CC4S_sqrt_coulmat
  use CC4S_blacs_environment, only: CC4S_n_tasks, CC4S_lredbb_row, CC4S_uredbb_row, CC4S_lpw_col, CC4S_upw_col, &
                                   &CC4S_lbb_row, CC4S_ubb_row, CC4S_lredbb_col, CC4S_uredbb_col, &
                                   &CC4S_bbxredbb_desc, CC4S_redbbxpws_desc, CC4S_myid
  use calculation_data, only: CC4S_n_kq_points, CC4S_isreal
  use CC4S_OAF_transformation, only : OAF_trafo_matrix_r, OAF_trafo_matrix_c
  use CC4S_parallel_io, only: CC4S_parallel_io_V_inv_sqrt_complex 
  implicit none

  double complex, dimension(:,:), intent(in) :: V_mu_nu
  integer, dimension(4), intent(in) :: loc_shape_V_mu_nu
  integer, dimension(9), intent(in) :: V_mu_nu_desc
  double complex, dimension(:,:), intent(in) :: ri_to_pw_matrix
  integer, dimension(4), intent(in) :: loc_shape_ri_to_pw_matrix
  integer, dimension(9), intent(in) :: ri_to_pw_matrix_desc
  integer, intent(in) :: n_bb
  integer, intent(in) :: n_pws
  integer, intent(in) :: comm
  integer, intent(in) :: max_singval_kq
  integer, intent(in) :: current_kq_pair

  !PURPOSE: Simply transform V_mu_nu -> V^(-0.5)_mu_nu
  !and then the contraction/matrix multiplication
  ! <mu|V^(-0.5)|nu><nu|G> = <mu|V^(-0.5)|G>

  !local
  integer :: i_k_point
  double complex, allocatable, dimension(:,:) :: V_inv_sqrt_mu_nu
  double complex, allocatable, dimension(:,:) :: temp_oaf_trafo_real
  integer, dimension(2) :: coulomb_inverse_sqrt_sizes
  integer, dimension(2) :: coulomb_inverse_sqrt_subsizes
  integer, dimension(2) :: coulomb_inverse_sqrt_starts

  if(.not. allocated(V_inv_sqrt_mu_G)) then
    allocate(V_inv_sqrt_mu_G(loc_shape_V_mu_nu(1):loc_shape_V_mu_nu(2), &
                            &loc_shape_ri_to_pw_matrix(3):loc_shape_ri_to_pw_matrix(4)))
  end if

  V_inv_sqrt_mu_G(:,:) = (0.0d0, 0.0d0)

  allocate(V_inv_sqrt_mu_nu(loc_shape_V_mu_nu(1):loc_shape_V_mu_nu(2), &
                           &loc_shape_V_mu_nu(3):loc_shape_V_mu_nu(4)))


!  do i_k_point=1,CC4S_n_kq_points

    V_inv_sqrt_mu_nu(:,:) = V_mu_nu(:,:)

    !V_mu_nu -> V^(-0.5)_mu_nu (for now, no debug mode)
    if(CC4S_myid .eq. 0) then
      write(*,*) "Calculating inverse sqrt of V : V^(-0.5)"
    end if

    if(CC4S_n_tasks .eq. 1) then
      call CC4S_sqrt_lapack(V_inv_sqrt_mu_nu, n_bb, &
                  &V_mu_nu_desc, loc_shape_V_mu_nu, comm, .false., power=-0.5d0) 
    else
      call CC4S_sqrt_scalapack(V_inv_sqrt_mu_nu, n_bb, &
                  &V_mu_nu_desc, loc_shape_V_mu_nu, comm, .false., power=-0.5d0)
    end if

    ! <mu|V^(-0.5)|nu><nu|G> = <mu|V^(-0.5)|G>
    call pzgemm('N', 'N', n_bb, n_pws, n_bb, (1.0D0, 0.0D0), V_inv_sqrt_mu_nu(:,:), &
                &1, 1, V_mu_nu_desc, ri_to_pw_matrix(:,:), 1, 1, ri_to_pw_matrix_desc, (0.0D0, 0.0D0), &
                &V_inv_sqrt_mu_G(:,:), 1, 1, ri_to_pw_matrix_desc)

    !Result is stored in V_inv_sqrt_mu_G for each k(k-q) point

    !Proceed to multiply result with OAF-trafo matrix
    allocate(V_inv_sqrt_mu_G_OAF(CC4S_lredbb_row:CC4S_uredbb_row,CC4S_lpw_col:CC4S_upw_col))
    V_inv_sqrt_mu_G_OAF(:,:) = (0.0d0, 0.0d0)

    ! U_{mu,F}<mu|V^(-0.5)|G> = X_{F,G}
    if(CC4S_isreal) then
      allocate(temp_oaf_trafo_real(CC4S_lbb_row:CC4S_ubb_row, CC4S_lredbb_col:CC4S_uredbb_col))
      temp_oaf_trafo_real(:,:) = cmplx(OAF_trafo_matrix_r(:,:), kind=selected_real_kind(15))
      !do i_k_point=1,CC4S_n_kq_points
        !NOTE: Not 100% sure if U should be transposed ("T") or hermitian adjoint ("C")
        call pzgemm('C', 'N', max_singval_kq, n_pws, n_bb, (1.0D0, 0.0D0), temp_oaf_trafo_real(:,:), &
                    &1, 1, CC4S_bbxredbb_desc, V_inv_sqrt_mu_G(:,:), 1, 1, ri_to_pw_matrix_desc, (0.0D0, 0.0D0), &
                    &V_inv_sqrt_mu_G_OAF(:,:), 1, 1, CC4S_redbbxpws_desc)
      !end do
    else
      !do i_k_point=1,CC4S_n_kq_points
        !NOTE: Not 100% sure if U should be transposed ("T") or hermitian adjoint ("C")
        call pzgemm('C', 'N', max_singval_kq, n_pws, n_bb, (1.0D0, 0.0D0), OAF_trafo_matrix_c(:,:), &
                    &1, 1, CC4S_bbxredbb_desc, V_inv_sqrt_mu_G(:,:), 1, 1, ri_to_pw_matrix_desc, (0.0D0, 0.0D0), &
                    &V_inv_sqrt_mu_G_OAF(:,:), 1, 1, CC4S_redbbxpws_desc)
      !end do
    end if

    coulomb_inverse_sqrt_sizes=[max_singval_kq, n_pws] !N_G x N_n x N_n x 1
    coulomb_inverse_sqrt_subsizes=shape(V_inv_sqrt_mu_G_OAF(:,:))
    coulomb_inverse_sqrt_starts=[CC4S_lredbb_row-1, CC4S_lpw_col-1]
    call CC4S_parallel_io_V_inv_sqrt_complex(V_inv_sqrt_mu_G_OAF(:,:), coulomb_inverse_sqrt_fh, &
                    &coulomb_inverse_sqrt_sizes, coulomb_inverse_sqrt_subsizes, &
                    &coulomb_inverse_sqrt_starts, max_singval_kq, current_kq_pair)



!  end do 

    if(allocated(V_inv_sqrt_mu_G_OAF)) then
      !Deallocate after each k-point calculation,
      !as size of max_singval may change
      deallocate(V_inv_sqrt_mu_G_OAF)
    end if
 
end subroutine contract_V_inv_sqrt_with_nu_G_OAF

!subroutine contract_OAF_trafo_matrix_with_V_inv_sqrt_mu_G()
!  use CC4S_OAF_transformation, only : OAF_trafo_matrix_r, OAF_trafo_matrix_c
!  use CC4S_blacs_environment, only: CC4S_init_blacsOAF_pws_distribution, CC4S_redbbxpws_desc, CC4S_bbxredbb_desc, &
!                                    &CC4S_lredbb_row, CC4S_uredbb_row, CC4S_lbb_col, CC4S_ubb_col, &
!                                    &CC4S_lbb_row, CC4S_ubb_row, CC4S_lredbb_col, CC4S_uredbb_col, &
!                                    &CC4S_max_singval, CC4S_lpw_col, CC4S_upw_col, CC4S_bbxpws_desc
!  use calculation_data, only: CC4S_n_kq_points, CC4S_isreal, CC4S_num_pws, CC4S_n_basbas
!  implicit none
!
!  integer :: i_k_point
!  double complex, allocatable, dimension(:,:) :: temp_oaf_trafo_real
!  
!
!  !Initialize BLACS-discriptor array for result of matrix-product
!  call CC4S_init_blacsOAF_pws_distribution()
!
!  allocate(V_inv_sqrt_mu_G_OAF(CC4S_lredbb_row:CC4S_uredbb_row,CC4S_lpw_col:CC4S_upw_col,CC4S_n_kq_points))
!  V_inv_sqrt_mu_G_OAF(:,:,:) = (0.0d0, 0.0d0)
!
!  ! U_{mu,F}<mu|V^(-0.5)|G> = X_{F,G}
!  if(CC4S_isreal) then
!    allocate(temp_oaf_trafo_real(CC4S_lbb_row:CC4S_ubb_row, CC4S_lredbb_col:CC4S_uredbb_col))
!    temp_oaf_trafo_real(:,:) = dcmplx(OAF_trafo_matrix_r(:,:))
!    do i_k_point=1,CC4S_n_kq_points
!      !NOTE: Not 100% sure if U should be transposed ("T") or hermitian adjoint ("C")
!      call pzgemm('C', 'N', CC4S_max_singval, CC4S_num_pws, CC4S_n_basbas, (1.0D0, 0.0D0), temp_oaf_trafo_real(:,:), &
!                  &1, 1, CC4S_bbxredbb_desc, V_inv_sqrt_mu_G(:,:,i_k_point), 1, 1, CC4S_bbxpws_desc, (0.0D0, 0.0D0), &
!                  &V_inv_sqrt_mu_G_OAF(:,:,i_k_point), 1, 1, CC4S_redbbxpws_desc)
!    end do
!  else
!    do i_k_point=1,CC4S_n_kq_points
!      !NOTE: Not 100% sure if U should be transposed ("T") or hermitian adjoint ("C")
!      call pzgemm('C', 'N', CC4S_max_singval, CC4S_num_pws, CC4S_n_basbas, (1.0D0, 0.0D0), OAF_trafo_matrix_c(:,:), &
!                  &1, 1, CC4S_bbxredbb_desc, V_inv_sqrt_mu_G(:,:,i_k_point), 1, 1, CC4S_bbxpws_desc, (0.0D0, 0.0D0), &
!                  &V_inv_sqrt_mu_G_OAF(:,:,i_k_point), 1, 1, CC4S_redbbxpws_desc)
!    end do
!  end if
!
!  deallocate(V_inv_sqrt_mu_G)
!
!end subroutine contract_OAF_trafo_matrix_with_V_inv_sqrt_mu_G

subroutine open_V_inv_sqrt_mu_G_filehandle()
  use CC4S_blacs_environment, only : CC4S_member, CC4S_comm
  use outputfile_names
  use mpi
  implicit none

  integer :: ierr

  if(CC4S_member) then
    call MPI_File_open(CC4S_comm, coulomb_inverse_sqrt_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, coulomb_inverse_sqrt_fh, ierr)
  end if

end subroutine open_V_inv_sqrt_mu_G_filehandle

subroutine open_pw_transform_filehandle()
  use CC4S_blacs_environment, only : CC4S_member, CC4S_comm
  use outputfile_names
  use mpi
  implicit none

  integer :: ierr

  if(CC4S_member) then
    call MPI_File_open(CC4S_comm, pw_transform_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, pw_transform_fh, ierr)
  end if

end subroutine open_pw_transform_filehandle

subroutine close_V_inv_sqrt_mu_G_filehandle(debug)
  use CC4S_blacs_environment, only : CC4S_member, CC4S_comm
  use outputfile_names
  use CC4S_adapt_data_file_suffix
  use mpi
  implicit none

  logical, intent(in) :: debug

  integer :: ierr

  if(CC4S_member) then
    call MPI_File_close(coulomb_inverse_sqrt_fh, ierr)

    !In the new (release-version) of CC4S, data files carry the suffix ".elements"
    !instead of ".dat" or ".bin".
    !The following function will select the chosen data file 
    !(the .bin- or .dat-file depending on debug-setting)
    !and change the respective suffix to ".elements"
    call adapt_data_file_suffix(debug, coulomb_inverse_sqrt_filename_dat, &
                                &coulomb_inverse_sqrt_filename_bin, MPI_COMM_WORLD)
  end if

end subroutine close_V_inv_sqrt_mu_G_filehandle

subroutine close_pw_transform_filehandle(debug)
  use CC4S_blacs_environment, only : CC4S_member, CC4S_comm
  use outputfile_names
  use CC4S_adapt_data_file_suffix
  use mpi
  implicit none

  logical, intent(in) :: debug

  integer :: ierr

  if(CC4S_member) then
    call MPI_File_close(pw_transform_fh, ierr)

    !In the new (release-version) of CC4S, data files carry the suffix ".elements"
    !instead of ".dat" or ".bin".
    !The following function will select the chosen data file 
    !(the .bin- or .dat-file depending on debug-setting)
    !and change the respective suffix to ".elements"
    call adapt_data_file_suffix(debug, pw_transform_filename_dat, &
                                &pw_transform_filename_bin, MPI_COMM_WORLD)
  end if

end subroutine close_pw_transform_filehandle

subroutine write_current_pw_transform_block_to_file(curr_kq_pair)
  use CC4S_parallel_io, only: CC4S_parallel_io_pw_transform_cmplx
  use calculation_data, only: CC4S_num_pws, CC4S_n_basbas
  use CC4S_blacs_environment, only: CC4S_lbb_row, CC4S_lpw_col
  implicit none

  integer, intent(in) :: curr_kq_pair

  !local
  integer, dimension(2) :: pw_transform_sizes
  integer, dimension(2) :: pw_transform_subsizes
  integer, dimension(2) :: pw_transform_starts


  pw_transform_sizes = [CC4S_n_basbas, CC4S_num_pws]
  pw_transform_subsizes = shape(inverted_basis_transform_at_k)
  pw_transform_starts = [CC4S_lbb_row-1, CC4S_lpw_col-1]

  call CC4S_parallel_io_pw_transform_cmplx(inverted_basis_transform_at_k, pw_transform_fh, &
             &pw_transform_sizes, pw_transform_subsizes, pw_transform_starts, curr_kq_pair)

end subroutine write_current_pw_transform_block_to_file

!subroutine write_V_inv_sqrt_mu_G_to_file(curr_kq_pair)
!  use CC4S_parallel_io, only: CC4S_parallel_io_V_inv_sqrt_complex
!  use CC4S_blacs_environment, only: CC4S_member, CC4S_comm, CC4S_max_singval
!  use outputfile_names
!  implicit none
!
!  integer, intent(in) :: curr_kq_pair
!
!  integer, dimension(2) :: coulomb_inverse_sqrt_sizes
!  integer, dimension(2) :: coulomb_inverse_sqrt_subsizes
!  integer, dimension(2) :: coulomb_inverse_sqrt_starts
!
!  if(allocated(V_inv_sqrt_mu_G_OAF)) then
!    coulomb_inverse_sqrt_sizes=[CC4S_max_singval, CC4S_num_pws] !N_G x N_n x N_n x 1
!    coulomb_inverse_sqrt_subsizes=shape(V_inv_sqrt_mu_G_OAF(:,:,curr_kq_pair))
!    coulomb_inverse_sqrt_starts=[CC4S_lredbb_row-1, CC4S_lpw_col-1]
!    call CC4S_parallel_io_V_inv_sqrt_complex(V_inv_sqrt_mu_G_OAF(:,:,curr_kq_pair), coulomb_inverse_sqrt_fh, &
!                                            &coulomb_inverse_sqrt_sizes, coulomb_inverse_sqrt_subsizes, coulomb_inverse_sqrt_starts, &
!                                            &CC4S_max_singval, curr_kq_pair) 
!  else
!    coulomb_inverse_sqrt_sizes=[CC4S_n_basbas, CC4S_num_pws] !N_G x N_n x N_n x 1
!    coulomb_inverse_sqrt_subsizes=shape(V_inv_sqrt_mu_G(:,:,curr_kq_pair))
!    coulomb_inverse_sqrt_starts=[CC4S_lbb_row-1, CC4S_lpw_col-1]
!    call CC4S_parallel_io_V_inv_sqrt_complex(V_inv_sqrt_mu_G(:,:,curr_kq_pair), coulomb_inverse_sqrt_fh, &
!                                            &coulomb_inverse_sqrt_sizes, coulomb_inverse_sqrt_subsizes, coulomb_inverse_sqrt_starts, &
!                                            &CC4S_n_basbas, curr_kq_pair)
!  end if
!end subroutine write_V_inv_sqrt_mu_G_to_file

end module finite_size_correction
