module RIV
  use mpi
  use calculation_data, only: CC4S_isreal, CC4S_n_basis, CC4S_n_basbas, &
                             &CC4S_nuf_states, CC4S_n_k_points, &
                             &CC4S_isreal, CC4S_n_spin, CC4S_n_low_state, &
                             &CC4S_n_states

  use CC4S_blacs_environment, only: CC4S_lbb_row, CC4S_ubb_row, &
                                   &CC4S_lbasis_col, CC4S_ubasis_col, &
                                   &CC4S_lstates_col, CC4S_ustates_col, &
                                   &comm_col, CC4S_npcol, CC4S_member, CC4S_myid

  interface convert_RIV_distributed_aotomo
    module procedure convert_RIV_distributed_aotomo_real
    module procedure convert_RIV_distributed_aotomo_cmplx
  end interface convert_RIV_distributed_aotomo

  type stackablePointerReal
    double precision, dimension(:,:,:), pointer :: ptr3D
  end type stackablePointerReal

  type stackablePointerCmplx
    double complex, dimension(:,:,:), pointer ::ptr3D
  end type stackablePointerCmplx
contains
  subroutine convert_RIV_distributed_aotomo_real(RIV_aoao, scf_eigvecs_real, RIV_aomo)
    implicit none

    double complex, dimension(:,:,:,:,:), intent(in) :: RIV_aoao 
    double precision, dimension(:,:,:,:), intent(in) :: scf_eigvecs_real
    double precision, dimension(:,:,:,:,:,:), allocatable, intent(out) :: RIV_aomo

    !local
    integer, parameter :: dp = selected_real_kind(15)
    integer :: i_basis
    integer :: i_k_point, i_q_point
    integer :: i_spin
    integer :: local_bb_size, local_basis_size
    double complex, dimension(:,:,:), allocatable :: tmp_RIV_aomo
    integer, dimension(:,:), allocatable :: states_bounds
    integer, dimension(2) :: local_states_bounds
    integer :: myid_col
    integer :: ierr
    integer, dimension(:), allocatable :: sendcount_arr
    integer, dimension(:), allocatable :: displacement_arr
    integer, dimension(:), allocatable :: recvcount_arr
    integer, dimension(:), allocatable :: recvdisplacement_arr
    integer :: i_task
    double complex, dimension(:,:,:,:), allocatable :: recv_buff

    !write(*,*) "RANK", CC4S_myid, 'entered ovlp_3fn->ovlp_3KS conversion'
    if(.not. CC4S_member) then
      allocate(RIV_aomo(1,1,1,1,1,1))
      return
    end if 


    !analogue of aomo_lvl
    allocate(RIV_aomo(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, CC4S_n_basis, &
            &CC4S_n_spin, CC4S_n_k_points, CC4S_n_k_points))

    RIV_aomo(:,:,:,:,:,:) = 0.0D0

    allocate(tmp_RIV_aomo(CC4S_n_spin, CC4S_lbb_row:CC4S_ubb_row, CC4S_nuf_states))
    local_bb_size = CC4S_ubb_row-CC4S_lbb_row+1
    local_basis_size = CC4S_ubasis_col-CC4S_lbasis_col+1


    !Communicate to all tasks in the same process row, lbasis_col and ubasis_col
    !for the scatterv-call
    allocate(states_bounds(2,CC4S_npcol))
    states_bounds(:,:) = 0
    local_states_bounds(:) = [CC4S_lstates_col,CC4S_ustates_col]
    call MPI_Comm_rank(comm_col, myid_col, ierr)

    call MPI_Allgather(local_states_bounds(:), 2, MPI_INTEGER, states_bounds(:,:), 2, MPI_INTEGER, comm_col, ierr)


    !Setting up counts/displacements for MPI_Alltoallv
    allocate(recv_buff(CC4S_n_spin, CC4S_lbb_row:CC4S_ubb_row, &
            &CC4S_lstates_col:CC4S_ustates_col, CC4S_npcol))
    recv_buff(:,:,:,:) = (0.0D0, 0.0D0)

    allocate(sendcount_arr(CC4S_npcol))
    sendcount_arr(:) = (states_bounds(2,:) - states_bounds(1,:) + 1)*local_bb_size*CC4S_n_spin
    allocate(displacement_arr(CC4S_npcol))
    do i_task =1,CC4S_npcol
      displacement_arr(i_task) = CC4S_n_spin*local_bb_size*(states_bounds(1,i_task)-1)
    end do

    allocate(recvcount_arr(CC4S_npcol))
    !Every task of a given process-row needs to get its slice of shape
    !CC4S_n_spin x local_bb_size x local_states_bounds
    recvcount_arr(:) = CC4S_n_spin*local_bb_size*(CC4S_ustates_col - CC4S_lstates_col + 1)
    allocate(recvdisplacement_arr(CC4S_npcol))
    do i_task=1,CC4S_npcol
      if(i_task .eq. 1) then
        recvdisplacement_arr(i_task) = 0
      else
        recvdisplacement_arr(i_task) = recvcount_arr(i_task-1)*(i_task-1)
      end if 
    end do



    do i_k_point=1,CC4S_n_k_points
      do i_q_point=1,CC4S_n_k_points
        do i_basis=1,CC4S_n_basis
          do i_spin=1,CC4S_n_spin
            !Transform every local block of shape (lbb_row:ubb_row,lbasis_col:ubasis_col)
            !to (lbb_row:ubb_row,1:n_states_nuf)
             
            call zgemm('N', 'N', local_bb_size, CC4S_nuf_states, local_basis_size, &
                &(1.0D0, 0.0D0), RIV_aoao(:,:,i_basis,i_k_point,i_q_point), &
                &local_bb_size, &
                &cmplx(scf_eigvecs_real(CC4S_lbasis_col:CC4S_ubasis_col,CC4S_n_low_state:CC4S_n_states, &
                &i_spin, i_k_point), kind=dp), &
                &local_basis_size, (0.0D0, 0.0D0), tmp_RIV_aomo(i_spin,:,:), local_bb_size)
          end do
          !Not sure if necessary, but without the following synchronization, it could happen
          !that recv-buffer is updated already, while the previous slice is still evaluated
          call MPI_Barrier(comm_col, ierr)
          call MPI_Alltoallv(tmp_RIV_aomo(:,:,:), sendcount_arr(:), displacement_arr(:), MPI_DOUBLE_COMPLEX, &
                             &recv_buff(:,:,:,:), recvcount_arr(:), recvdisplacement_arr(:), MPI_DOUBLE_COMPLEX, &
                             &comm_col, ierr)

          
          !Scatterv is a blocking operation: After all slices were exchanged, add them all up
          do i_spin=1,CC4S_n_spin
            RIV_aomo(:,:,i_basis, i_spin, i_k_point,i_q_point) = &
                    &dble(sum(recv_buff(i_spin,:,:,:), 3))
          end do
        
        end do
      end do
    end do
    
    !write(*,*) 'Done with ao->mo conversion'

  end subroutine convert_RIV_distributed_aotomo_real
!***********************************************************************
  subroutine convert_RIV_distributed_aotomo_cmplx(RIV_aoao, scf_eigvecs_cmplx, RIV_aomo)
    implicit none

    double complex, dimension(:,:,:,:,:), intent(in) :: RIV_aoao 
    double complex, dimension(:,:,:,:), intent(in) :: scf_eigvecs_cmplx
    double complex, dimension(:,:,:,:,:,:), allocatable, intent(out) :: RIV_aomo

    !local
    integer :: i_basis
    integer :: i_k_point, i_q_point
    integer :: i_spin
    integer :: local_bb_size, local_basis_size
    double complex, dimension(:,:,:), allocatable :: tmp_RIV_aomo
    integer, dimension(:,:), allocatable :: states_bounds
    integer, dimension(2) :: local_states_bounds
    integer :: myid_col
    integer :: ierr
    integer, dimension(:), allocatable :: sendcount_arr
    integer, dimension(:), allocatable :: displacement_arr
    integer, dimension(:), allocatable :: recvcount_arr
    integer, dimension(:), allocatable :: recvdisplacement_arr
    integer :: i_task
    double complex, dimension(:,:,:,:), allocatable :: recv_buff

    if(.not. CC4S_member) then
      allocate(RIV_aomo(1,1,1,1,1,1))
      return
    end if 


    !analogue of aomo_lvl
    allocate(RIV_aomo(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, CC4S_n_basis, &
            &CC4S_n_spin, CC4S_n_k_points, CC4S_n_k_points))

    RIV_aomo(:,:,:,:,:,:) = (0.0D0, 0.0D0)

    allocate(tmp_RIV_aomo(CC4S_n_spin, CC4S_lbb_row:CC4S_ubb_row, CC4S_nuf_states))
    local_bb_size = CC4S_ubb_row-CC4S_lbb_row+1
    local_basis_size = CC4S_ubasis_col-CC4S_lbasis_col+1


    !Communicate to all tasks in the same process row, lbasis_col and ubasis_col
    !for the scatterv-call
    allocate(states_bounds(2,CC4S_npcol))
    states_bounds(:,:) = 0
    local_states_bounds(:) = [CC4S_lstates_col,CC4S_ustates_col]
    call MPI_Comm_rank(comm_col, myid_col, ierr)

    call MPI_Allgather(local_states_bounds(:), 2, MPI_INTEGER, states_bounds(:,:), 2, MPI_INTEGER, comm_col, ierr)


    !Setting up counts/displacements for MPI_Alltoallv
    allocate(recv_buff(CC4S_n_spin, CC4S_lbb_row:CC4S_ubb_row, &
            &CC4S_lstates_col:CC4S_ustates_col, CC4S_npcol))
    recv_buff(:,:,:,:) = (0.0D0, 0.0D0)

    allocate(sendcount_arr(CC4S_npcol))
    sendcount_arr(:) = (states_bounds(2,:) - states_bounds(1,:) + 1)*local_bb_size*CC4S_n_spin
    allocate(displacement_arr(CC4S_npcol))
    do i_task =1,CC4S_npcol
      displacement_arr(i_task) = CC4S_n_spin*local_bb_size*(states_bounds(1,i_task)-1)
    end do

    allocate(recvcount_arr(CC4S_npcol))
    !Every task of a given process-row needs to get its slice of shape
    !CC4S_n_spin x local_bb_size x local_states_bounds
    recvcount_arr(:) = CC4S_n_spin*local_bb_size*(CC4S_ustates_col - CC4S_lstates_col + 1)
    allocate(recvdisplacement_arr(CC4S_npcol))
    do i_task=1,CC4S_npcol
      if(i_task .eq. 1) then
        recvdisplacement_arr(i_task) = 0
      else
        recvdisplacement_arr(i_task) = recvcount_arr(i_task-1)*(i_task-1)
      end if 
    end do



    do i_k_point=1,CC4S_n_k_points
      do i_q_point=1,CC4S_n_k_points
        do i_basis=1,CC4S_n_basis
          do i_spin=1,CC4S_n_spin
            !Transform every local block of shape (lbb_row:ubb_row,lbasis_col:ubasis_col)
            !to (lbb_row:ubb_row,1:n_states_nuf)
             
            call zgemm('N', 'N', local_bb_size, CC4S_nuf_states, local_basis_size, &
                &(1.0D0, 0.0D0), RIV_aoao(:,:,i_basis,i_k_point,i_q_point), &
                &local_bb_size, &
                &scf_eigvecs_cmplx(CC4S_lbasis_col:CC4S_ubasis_col,CC4S_n_low_state:CC4S_n_states,i_spin, i_k_point), &
                &local_basis_size, (0.0D0, 0.0D0), tmp_RIV_aomo(i_spin,:,:), local_bb_size)
          end do
          !Not sure if necessary, but without the following synchronization, it could happen
          !that recv-buffer is updated already, while the previous slice is still evaluated
          call MPI_Barrier(comm_col, ierr)
          call MPI_Alltoallv(tmp_RIV_aomo(:,:,:), sendcount_arr(:), displacement_arr(:), MPI_DOUBLE_COMPLEX, &
                             &recv_buff(:,:,:,:), recvcount_arr(:), recvdisplacement_arr(:), MPI_DOUBLE_COMPLEX, &
                             &comm_col, ierr)

          
          !Scatterv is a blocking operation: After all slices were exchanged, add them all up
          do i_spin=1,CC4S_n_spin
            RIV_aomo(:,:,i_basis, i_spin, i_k_point,i_q_point) = &
                    &dble(sum(recv_buff(i_spin,:,:,:), 3))
          end do
        
        end do
      end do
    end do
    
    !write(*,*) 'Done with ao->mo conversion cmplx'

  end subroutine convert_RIV_distributed_aotomo_cmplx
!***********************************************************************
!  subroutine convert_RIV_distributed_aotomo_cmplx(RIV_aoao, scf_eigvecs_cmplx, RIV_aomo)
!    implicit none
!
!    double complex, dimension(:,:,:,:,:), intent(in) :: RIV_aoao 
!    double complex, dimension(:,:,:,:), intent(in) :: scf_eigvecs_cmplx
!    double complex, dimension(:,:,:,:,:,:), allocatable, intent(out) :: RIV_aomo
!
!    !local
!    integer :: i_basis
!    integer :: i_k_point, i_q_point
!    integer :: i_spin
!    integer :: local_bb_size, local_basis_size
!    double complex, dimension(:,:,:), allocatable :: tmp_RIV_aomo
!    integer, dimension(:,:), allocatable :: states_bounds
!    integer, dimension(2) :: local_states_bounds
!    integer :: myid_col
!    integer :: ierr
!    integer, dimension(:), allocatable :: sendcount_arr
!    integer, dimension(:), allocatable :: displacement_arr
!    integer :: i_task
!    double complex, dimension(:,:,:,:), allocatable :: recv_buff
!
!    !analogue of aomo_lvl
!    allocate(RIV_aomo(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, CC4S_n_basis, &
!            &CC4S_n_spin, CC4S_n_k_points, CC4S_n_k_points))
!
!    allocate(tmp_RIV_aomo(CC4S_n_spin, CC4S_lbb_row:CC4S_ubb_row, CC4S_nuf_states))
!    local_bb_size = CC4S_ubb_row-CC4S_ubb_row+1
!    local_basis_size = CC4S_ubasis_col-CC4S_lbasis_col+1
!
!    allocate(recv_buff(CC4S_n_spin, CC4S_lbb_row:CC4S_ubb_row, &
!            &CC4S_lstates_col:CC4S_ustates_col, CC4S_npcol))
!    !Communicate to all tasks in the same process row, lbasis_col and ubasis_col
!    !for the scatterv-call
!    allocate(states_bounds(CC4S_npcol,2))
!    local_states_bounds = [CC4S_lstates_col,CC4S_ustates_col]
!    call MPI_Comm_rank(comm_col, myid_col, ierr)
!    call MPI_Alltoall(local_states_bounds, 2, MPI_INTEGER, states_bounds(myid_col+1,:), &
!            &2, MPI_INTEGER, comm_col, ierr)
!
!    allocate(sendcount_arr(CC4S_npcol))
!    sendcount_arr(:) = (states_bounds(:,2) - states_bounds(:,1) + 1)*local_bb_size*CC4S_n_spin
!    allocate(displacement_arr(CC4S_npcol))
!    do i_task =1,CC4S_npcol
!      displacement_arr(i_task) = CC4S_n_spin*local_bb_size*(states_bounds(i_task,1)-1)
!    end do
!
!    do i_k_point=1,CC4S_n_k_points
!      do i_q_point=1,CC4S_n_k_points
!        do i_basis=1,CC4S_n_basis
!          do i_spin=1,CC4S_n_spin
!            !Transform every local block of shape (lbb_row:ubb_row,lbasis_col:ubasis_col)
!            !to (lbb_row:ubb_row,1:n_states_nuf) 
!            call zgemm('N', 'N', local_bb_size, CC4S_nuf_states, local_basis_size, &
!                &(1.0D0, 0.0D0), RIV_aoao(:,:,i_basis,i_k_point,i_q_point), &
!                &local_bb_size, &
!                &scf_eigvecs_cmplx(CC4S_lbasis_col:CC4S_ubasis_col,1:CC4S_nuf_states,i_k_point, i_spin), &
!                &CC4S_nuf_states, (0.0D0, 0.0D0), tmp_RIV_aomo(i_spin,:,:), local_bb_size)
!          end do
!          !Not sure if necessary, but without the following synchronization, it could happen
!          !that recv-buffer is updated already, while the previous slice is still evaluated
!          call MPI_Barrier(comm_col, ierr)
!          !Scatter this transformed slice to every other task of the same process row
!          call MPI_Scatterv(tmp_RIV_aomo(:,:,:), sendcount_arr, displacement_arr, &
!                  &MPI_DOUBLE_COMPLEX, recv_buff(:,:,:,myid_col), size(recv_buff(:,:,:,myid_col)), &
!                  &MPI_DOUBLE_COMPLEX, myid_col, comm_col, ierr)
!
!          !Scatterv is a blocking operation: After all slices were exchanged, add them all up
!          do i_spin=1,CC4S_n_spin
!            RIV_aomo(:,:,i_basis, i_spin, i_k_point,i_q_point) = &
!                    &sum(recv_buff(i_spin,:,:,:), 3)
!          end do
!
!
!        end do
!      end do
!    end do
!  end subroutine convert_RIV_distributed_aotomo_cmplx
!*******************************************************************
!  subroutine shm_RIV_real(RIV_aomo_intermediate_real, CC4S_member, is_riv_root, &
!                  &mpi_comm_shared_row, RIV_shm_real)
!    implicit none
!
!    double precision, dimension(:,:,:), allocatable, intent(inout) :: RIV_aomo_intermediate_real
!    logical, intent(in) :: CC4S_member, is_lvl_root
!    integer, intent(in) ::mpi_comm_shared_row
!    double precision, dimension(:,:,:), pointer, contiguous, intent(out) :: RIV_shm_real
!
!    !local
!    integer(kind=MPI_ADDRESS_KIND) :: wsize, mem_unit, disp_unit
!
!    RIV_shm_real => null()
!
!    !Actually copy (broadcast) the given intermediate 3D-quantity to one task per (shared) node
!    !which takes part in the process row (should be equivalent to tasks with is_lvl_root == .true.)
!    if(CC4S_member) then
!    end if 
!
!  end subroutine shm_RIV_real
end module RIV
