module CC4S_VtimesM
        interface CC4S_v_times_M_k_q
                module procedure CC4S_v_times_M_k_q_real
                module procedure CC4S_v_times_M_k_q_cmplx
        end interface CC4S_v_times_M_k_q

contains
subroutine CC4S_v_times_M_k_q_real(coulmat, M_k_q)

        use CC4S_blacs_environment, only: CC4S_bb_desc, CC4S_bbxstates_desc, &
                &CC4S_lbb_row, CC4S_ubb_row, CC4S_lbb_col, CC4S_ubb_col, &
                &CC4S_lstates_col, CC4S_ustates_col

        use calculation_data, only: CC4S_n_basbas, CC4S_nuf_states
  implicit none

      !PURPOSE: Final algebraic operation to obtain Coulomb vertex.
      !         Multiply the MO-transformed RI-coefficient array in
      !         recip. space with the Coulomb matrix.
      !         THe RI-coefficient array depends on a pair of k-points
      !         denoted by k and q, while the Coulomb matrix depends on
      !         the difference k-q. 

      double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col), &
              &intent(in) :: coulmat
      double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
              &1:CC4S_nuf_states), target, intent(inout) :: M_k_q

      !local
      integer :: i_state
      double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col), &
              &target :: temp_M_k_q
      double precision, dimension(:,:), pointer :: ptr_M_k_q_a => null()
      double precision, dimension(:,:), pointer :: ptr_M_k_q_b => null()

      do i_state=1,CC4S_nuf_states
        temp_M_k_q(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col) = &
                &M_k_q(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, i_state)

        ptr_M_k_q_a => temp_M_k_q(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col)
        ptr_M_k_q_b => M_k_q(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                &i_state)

        call pdgemm('N', 'N', CC4S_n_basbas, CC4S_nuf_states, CC4S_n_basbas, 1.0D0, &
                &coulmat, 1, 1, CC4S_bb_desc, ptr_M_k_q_a, 1, 1, &
                &CC4S_bbxstates_desc, 0.0D0, ptr_M_k_q_b, 1, 1, CC4S_bbxstates_desc)

      end do

end subroutine CC4S_v_times_M_k_q_real
!*************************************************
subroutine CC4S_v_times_M_k_q_cmplx(coulmat, M_k_q, do_V_T)

        use CC4S_blacs_environment, only: CC4S_bb_desc, CC4S_bbxstates_desc, &
                &CC4S_lbb_row, CC4S_ubb_row, CC4S_lbb_col, CC4S_ubb_col, &
                &CC4S_lstates_col, CC4S_ustates_col

        use calculation_data, only: CC4S_n_basbas, CC4S_nuf_states
  implicit none

      !PURPOSE: Final algebraic operation to obtain Coulomb vertex.
      !         Multiply the MO-transformed RI-coefficient array in
      !         recip. space with the Coulomb matrix.
      !         THe RI-coefficient array depends on a pair of k-points
      !         denoted by k and q, while the Coulomb matrix depends on
      !         the difference k-q. 

      double complex, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col), &
              &intent(in) :: coulmat
      double complex, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
              &1:CC4S_nuf_states), target, intent(inout) :: M_k_q
       logical, optional, intent(in) :: do_V_T

      !local
      integer :: i_state
      double complex, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col), &
              &target :: temp_M_k_q
      double complex, dimension(:,:), pointer :: ptr_M_k_q_a => null()
      double complex, dimension(:,:), pointer :: ptr_M_k_q_b => null()

      logical :: internal_do_V_T

      if(present(do_V_T)) then
        internal_do_V_T = do_V_T
      else
        internal_do_V_T = .false.
      end if

      do i_state=1,CC4S_nuf_states
        temp_M_k_q(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col) = &
                &M_k_q(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, i_state)

        ptr_M_k_q_a => temp_M_k_q(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col)
        ptr_M_k_q_b => M_k_q(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                &i_state)

        if(internal_do_V_T) then
          call pzgemm('T', 'N', CC4S_n_basbas, CC4S_nuf_states, CC4S_n_basbas, (1.0D0, 0.0D0), &
                  &coulmat, 1, 1, CC4S_bb_desc, ptr_M_k_q_a, 1, 1, &
                  &CC4S_bbxstates_desc, (0.0D0, 0.0D0), ptr_M_k_q_b, 1, 1, CC4S_bbxstates_desc)
        else
          call pzgemm('N', 'N', CC4S_n_basbas, CC4S_nuf_states, CC4S_n_basbas, (1.0D0, 0.0D0), &
                  &coulmat, 1, 1, CC4S_bb_desc, ptr_M_k_q_a, 1, 1, &
                  &CC4S_bbxstates_desc, (0.0D0, 0.0D0), ptr_M_k_q_b, 1, 1, CC4S_bbxstates_desc)
        end if
      end do

end subroutine CC4S_v_times_M_k_q_cmplx
end module CC4S_VtimesM
