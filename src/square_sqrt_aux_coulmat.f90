module CC4S_square_sqrt_coulmat
        interface CC4S_square_sqrt_scalapack
                module procedure CC4S_square_sqrt_aux_coulmat_r_sclpck
                module procedure CC4S_square_sqrt_aux_coulmat_c_sclpck
        end interface CC4S_square_sqrt_scalapack

        interface CC4S_square_sqrt_lapack
                module procedure CC4S_square_sqrt_aux_coulmat_r_lpck
                module procedure CC4S_square_sqrt_aux_coulmat_c_lpck
        end interface CC4S_square_sqrt_lapack

contains

subroutine CC4S_square_sqrt_aux_coulmat_r_sclpck(coulmat_sqrt, n_bb, coulmat_desc, &
                &loc_shape_coulmat, comm, debug)
        use mpi
        use CC4S_blacs_environment, only: CC4S_nprow, CC4S_npcol, CC4S_myid_row, CC4S_myid_col
        implicit none

        !input
        integer, dimension(4), intent(in) :: loc_shape_coulmat
        double precision, dimension(loc_shape_coulmat(1):loc_shape_coulmat(2), &
                                   &loc_shape_coulmat(3):loc_shape_coulmat(4)), &
                                   &intent(inout) :: coulmat_sqrt
        integer, intent(in) :: n_bb
        integer, dimension(9), intent(in) :: coulmat_desc
        integer, intent(in) :: comm
        logical, intent(in) :: debug

        !local
        integer, dimension(2) :: coulmat_dims !Local dimensions of coulmat
        double precision, dimension(:,:), allocatable :: coulmat_sqrt_cp, coulmat_squared

        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1

        allocate(coulmat_sqrt_cp(coulmat_dims(1), coulmat_dims(2)))
        coulmat_sqrt_cp(:,:) = coulmat_sqrt(:,:)
        allocate(coulmat_squared(coulmat_dims(1), coulmat_dims(2)))
        coulmat_squared(:,:) = 0.0d0

        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1 

        call pdgemm('N', 'N', n_bb, n_bb, n_bb, 1.0D0, coulmat_sqrt(:,:), &
                  &1, 1, coulmat_desc, coulmat_sqrt_cp(:,:), 1, 1, coulmat_desc, &
                  &0.0D0, coulmat_squared(:,:), 1, 1, coulmat_desc)

        coulmat_sqrt(:,:) = coulmat_squared(:,:)

end subroutine CC4S_square_sqrt_aux_coulmat_r_sclpck
!*********************************************************************************
subroutine CC4S_square_sqrt_aux_coulmat_r_lpck(coulmat_sqrt, n_bb, coulmat_desc, &
                &loc_shape_coulmat, comm, debug)
        use mpi
        implicit none

        !input
        integer, dimension(4), intent(in) :: loc_shape_coulmat
        double precision, dimension(loc_shape_coulmat(1):loc_shape_coulmat(2), &
                                   &loc_shape_coulmat(3):loc_shape_coulmat(4)), &
                                   &intent(inout) :: coulmat_sqrt
        integer, intent(in) :: n_bb
        integer, dimension(9), intent(in) :: coulmat_desc
        integer, intent(in) :: comm
        logical, intent(in) :: debug

        !local
        integer, dimension(2) :: coulmat_dims !Local dimensions of coulmat
        double precision, dimension(:,:), allocatable :: coulmat_sqrt_cp, coulmat_squared
 
        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1

        allocate(coulmat_sqrt_cp(coulmat_dims(1), coulmat_dims(2)))
        coulmat_sqrt_cp(:,:) = coulmat_sqrt(:,:)
        allocate(coulmat_squared(coulmat_dims(1), coulmat_dims(2)))
        coulmat_squared(:,:) = 0.0d0

        coulmat_squared(:,:) = matmul(coulmat_sqrt, coulmat_sqrt_cp)

        coulmat_sqrt(:,:) = coulmat_squared(:,:)

end subroutine CC4S_square_sqrt_aux_coulmat_r_lpck
!*********************************************************
subroutine CC4S_square_sqrt_aux_coulmat_c_sclpck(coulmat_sqrt, n_bb, coulmat_desc, &
                &loc_shape_coulmat, comm, debug)
        use mpi
        use CC4S_blacs_environment, only: CC4S_nprow, CC4S_npcol, CC4S_myid_row, CC4S_myid_col
        implicit none

        integer, dimension(4), intent(in) :: loc_shape_coulmat
        double complex, dimension(loc_shape_coulmat(1):loc_shape_coulmat(2), &
                                   &loc_shape_coulmat(3):loc_shape_coulmat(4)), &
                                   &intent(inout) :: coulmat_sqrt
        integer, intent(in) :: n_bb
        integer, dimension(9), intent(in) :: coulmat_desc
        integer, intent(in) :: comm
        logical, intent(in) :: debug

        !local
        integer, dimension(2) :: coulmat_dims !Local dimensions of coulmat
        double complex, dimension(:,:), allocatable :: coulmat_sqrt_cp, coulmat_squared

        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1

        allocate(coulmat_sqrt_cp(coulmat_dims(1), coulmat_dims(2)))
        coulmat_sqrt_cp(:,:) = coulmat_sqrt(:,:)
        allocate(coulmat_squared(coulmat_dims(1), coulmat_dims(2)))
        coulmat_squared(:,:) = (0.0d0, 0.0d0)

        call pzgemm('N', 'N', n_bb, n_bb, n_bb, (1.0D0, 0.0D0), coulmat_sqrt(:,:), &
                &1, 1, coulmat_desc, coulmat_sqrt_cp(:,:), 1, 1, coulmat_desc, &
                &(0.0D0, 0.0D0), coulmat_squared(:,:), 1, 1, coulmat_desc)

        coulmat_sqrt(:,:) = coulmat_squared(:,:)

end subroutine CC4S_square_sqrt_aux_coulmat_c_sclpck
!*********************************************************************
subroutine CC4S_square_sqrt_aux_coulmat_c_lpck(coulmat_sqrt, n_bb, coulmat_desc, &
                &loc_shape_coulmat, comm, debug)
        use mpi
        implicit none

        !input
        integer, dimension(4), intent(in) :: loc_shape_coulmat
        double complex, dimension(loc_shape_coulmat(1):loc_shape_coulmat(2), &
                                   &loc_shape_coulmat(3):loc_shape_coulmat(4)), &
                                   &intent(inout) :: coulmat_sqrt
        integer, intent(in) :: n_bb
        integer, dimension(9), intent(in) :: coulmat_desc
        integer, intent(in) :: comm
        logical, intent(in) :: debug

        !local
        integer, dimension(2) :: coulmat_dims !Local dimensions of coulmat
        double complex, dimension(:,:), allocatable :: coulmat_sqrt_cp, coulmat_squared

        coulmat_dims(1)=loc_shape_coulmat(2)-loc_shape_coulmat(1)+1
        coulmat_dims(2)=loc_shape_coulmat(4)-loc_shape_coulmat(3)+1

        allocate(coulmat_sqrt_cp(coulmat_dims(1), coulmat_dims(2)))
        coulmat_sqrt_cp(:,:) = coulmat_sqrt(:,:)
        allocate(coulmat_squared(coulmat_dims(1), coulmat_dims(2)))
        coulmat_squared(:,:) = (0.0d0)

        coulmat_squared(:,:) = matmul(coulmat_sqrt, coulmat_sqrt_cp)

        coulmat_sqrt(:,:) = coulmat_squared(:,:)

end subroutine CC4S_square_sqrt_aux_coulmat_c_lpck
end module CC4S_square_sqrt_coulmat
