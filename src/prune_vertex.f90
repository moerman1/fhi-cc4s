module CC4S_OAF
        interface CC4S_prune_vertex
                module procedure CC4S_prune_vertex_real
                module procedure CC4S_prune_vertex_cmplx
        end interface CC4S_prune_vertex

        interface CC4S_reshape_vertex_2D
                module procedure CC4S_reshape_vertex_2D_real
                module procedure CC4S_reshape_vertex_2D_cmplx
        end interface CC4S_reshape_vertex_2D
contains
subroutine CC4S_prune_vertex_real(complete_vertex, vertex_OAF, max_singval, &
                &lredbb_row, singval_thr, singval_num)
      use mpi
      use CC4S_blacs_environment, only: CC4S_init_blacsOAF_distribution, &
              &CC4S_states2xbb_desc, CC4S_bb_desc, CC4S_n_tasks, &
              &CC4S_lbb_row, CC4S_ubb_row, CC4S_lbb_col, CC4S_ubb_col, &
              &CC4S_lstates_col, CC4S_ustates_col, CC4S_comm, &
              &CC4S_lstates2_col, CC4S_ustates2_col, &
              &CC4S_nprow, CC4S_npcol, &
              &CC4S_myid_row, CC4S_myid_col
      use calculation_data, only: CC4S_n_spin, CC4S_n_basbas, CC4S_nuf_states

      implicit none 

      !input
      double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates2_col:CC4S_ustates2_col, &
              &CC4S_n_spin), intent(in) :: complete_vertex
      !double precision, dimension(:,:,:), allocatable, intent(out) :: vertex_OAF
      double precision, dimension(:,:,:), allocatable, intent(inout) :: vertex_OAF
      integer, intent(out) :: max_singval
      integer, intent(out) :: lredbb_row
      double precision, optional, intent(in) :: singval_thr
      integer, optional, intent(in) :: singval_num

      !local
      integer :: i_spin, i_basbas
      double precision, dimension(:,:), allocatable, target :: vertex_sq
      double precision, dimension(:), allocatable :: vertex_SV
      integer :: lwork
      double precision, dimension(:), allocatable :: work
      integer :: info
      double precision, dimension(:,:), allocatable :: vertex_eigvec
      double precision, dimension(:,:), allocatable :: proj_id
      double precision, dimension(:,:), allocatable :: vertex_eigvec_OAF
      integer :: i_singval
      integer :: myid, ierr
      integer :: nprow, npcol
      integer :: myid_row, myid_col
      integer :: redbb_block_col
      integer :: bb_block_row
      integer :: loc_row, loc_col

      integer :: uredbb_row !, lredbb_row now in output
      integer :: lredbb_col, uredbb_col
      integer, dimension(9) :: bbxredbb_desc, states2xredbb_desc

      integer, external :: indxg2l

      call MPI_Comm_rank(CC4S_comm, myid, ierr)
      allocate(vertex_sq(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
      allocate(vertex_SV(CC4S_n_basbas))
      allocate(vertex_eigvec(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))

      do i_spin=1,CC4S_n_spin
        !Calculate square of the vertex
        call pdgemm('N', 'C', CC4S_n_basbas, CC4S_n_basbas, CC4S_nuf_states**2, 1.0D0, &
                &complete_vertex(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, &
                &complete_vertex(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, &
                &0.0D0, vertex_sq(:,:), 1, 1, CC4S_bb_desc)

        vertex_sq(:,:)=-vertex_sq(:,:)

        allocate(work(1))
        lwork=-1

        if(CC4S_n_tasks .eq. 1) then
          call dsyev('V', 'U', CC4S_n_basbas, vertex_sq(:,:), CC4S_n_basbas, &
                  &vertex_SV, work, lwork, info)

          lwork=int(work(1))
          deallocate(work)
          allocate(work(lwork))

          call dsyev('V', 'U', CC4S_n_basbas, vertex_sq(:,:), CC4S_n_basbas, &
                  &vertex_SV, work, lwork, info)

          deallocate(work)
          vertex_eigvec(:,:)=vertex_sq(:,:)
        else
          call pdsyev('V', 'U', CC4S_n_basbas, vertex_sq(:,:), 1, 1, CC4S_bb_desc, &
                  &vertex_SV, vertex_eigvec(:,:), 1, 1, CC4S_bb_desc, work, lwork, info)

          lwork=int(work(1))
          deallocate(work)
          allocate(work(lwork))
          call pdsyev('V', 'U', CC4S_n_basbas, vertex_sq(:,:), 1, 1, CC4S_bb_desc, &
                  &vertex_SV, vertex_eigvec(:,:), 1, 1, CC4S_bb_desc, work, lwork, info)

          deallocate(work)
        end if 


        vertex_SV(:)=-vertex_SV(:)

        if(present(singval_thr)) then
          do i_singval=1,size(vertex_SV)
            if(vertex_SV(i_singval) < singval_thr) then
              exit
            end if 
          end do

          max_singval=i_singval-1
        else if(present(singval_num)) then
          max_singval=singval_num
        else
          if(myid .eq. 0) then
            write(*,*) "No SV threshold provided. Using complete vertex"
          end if 
          return 
        end if 

        if(myid .eq. 0) then
          write(*,*) "Of", CC4S_n_basbas, "singular values,", &
                  &max_singval, "will be kept"
        end if 

        call CC4S_init_blacsOAF_distribution(max_singval, lredbb_row, &
                &uredbb_row, lredbb_col, uredbb_col, &
                &bbxredbb_desc, states2xredbb_desc, redbb_block_col, bb_block_row)

        allocate(proj_id(CC4S_lbb_row:CC4S_ubb_row, lredbb_col:uredbb_col))
        proj_id=0.0D0

        !call blacs_gridinfo(CC4S_comm, nprow, npcol, myid_row, myid_col)

        !redbb_block_col=(lredbb_col-1)/myid_col
        !bb_block_row=(CC4S_lbb_row-1)/myid_row


        do i_basbas=1,max_singval
          if((mod((i_basbas-1)/bb_block_row, CC4S_nprow) .eq. CC4S_myid_row) &
                  &.and. (mod((i_basbas-1)/redbb_block_col, CC4S_npcol) &
                  &.eq. CC4S_myid_col)) then

            loc_row=indxg2l(i_basbas, bb_block_row, 0, 0, CC4S_nprow)
            loc_col=indxg2l(i_basbas, redbb_block_col, 1, 1, CC4S_npcol)

            proj_id(CC4S_lbb_row+loc_row-1, lredbb_col+loc_col-1) = 1.0D0
          end if 
        end do
 
       
       allocate(vertex_eigvec_OAF(CC4S_lbb_row:CC4S_ubb_row, lredbb_col:uredbb_col)) 

       call pdgemm('N', 'N', CC4S_n_basbas, max_singval, CC4S_n_basbas, 1.0D0, &
               &vertex_eigvec(:,:), 1, 1, CC4S_bb_desc, proj_id(:,:), 1, 1, &
               &bbxredbb_desc, 0.0D0, vertex_eigvec_OAF(:,:), &
               &1, 1, bbxredbb_desc)


       deallocate(proj_id)

       allocate(vertex_OAF(lredbb_row:uredbb_row, &
               &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
       write(*,*) "(CC4S_prune_vertex_real) shape(vertex_OAF): ", uredbb_row-lredbb_row+1, &
               &CC4S_ustates2_col-CC4S_lstates2_col+1, CC4S_n_spin
       vertex_OAF(:,:,:) = 0.0D0
       call pdgemm('T', 'N', max_singval, CC4S_nuf_states**2, CC4S_n_basbas, 1.0D0, &
               &vertex_eigvec_OAF(:,:), 1, 1, bbxredbb_desc, &
               &complete_vertex(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, 0.0D0, &
               &vertex_OAF(:,:,i_spin), 1, 1, states2xredbb_desc)

      end do !n_spin

      if(allocated(vertex_sq)) then
        deallocate(vertex_sq)
      end if 

      if(allocated(vertex_SV)) then
        deallocate(vertex_SV)
      end if 

      if(allocated(vertex_eigvec)) then
        deallocate(vertex_eigvec)
      end if 



end subroutine CC4S_prune_vertex_real
!*******************************
subroutine CC4S_prune_vertex_cmplx(complete_vertex, vertex_OAF, max_singval, &
                &lredbb_row, singval_thr, singval_num)
      use mpi
      use CC4S_blacs_environment, only: CC4S_init_blacsOAF_distribution, &
              &CC4S_states2xbb_desc, CC4S_bb_desc, CC4S_n_tasks, &
              &CC4S_lbb_row, CC4S_ubb_row, CC4S_lbb_col, CC4S_ubb_col, &
              &CC4S_lstates_col, CC4S_ustates_col, CC4S_comm, &
              &CC4S_lstates2_col, CC4S_ustates2_col, &
              &CC4S_nprow, CC4S_npcol, &
              &CC4S_myid_row, CC4S_myid_col
      use calculation_data, only: CC4S_n_spin, CC4S_n_basbas, CC4S_nuf_states

      implicit none 

      !input
      double complex, dimension(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates2_col:CC4S_ustates2_col, &
              &CC4S_n_spin), intent(in) :: complete_vertex
      double complex, dimension(:,:,:), allocatable, intent(out) :: vertex_OAF
      integer, intent(out) :: max_singval
      integer, intent(out) :: lredbb_row
      double precision, optional, intent(in) :: singval_thr
      integer, optional, intent(in) :: singval_num

      !local
      integer :: i_spin, i_basbas
      double complex, dimension(:,:), allocatable, target :: vertex_sq
      double precision, dimension(:), allocatable :: vertex_SV
      integer :: lwork, lrwork
      double complex, dimension(:), allocatable :: work
      double precision, dimension(:), allocatable :: rwork
      integer :: info
      double complex, dimension(:,:), allocatable :: vertex_eigvec
      double complex, dimension(:,:), allocatable :: proj_id
      double complex, dimension(:,:), allocatable :: vertex_eigvec_OAF
      integer :: i_singval
      integer :: myid, ierr
      integer :: nprow, npcol
      integer :: myid_row, myid_col
      integer :: redbb_block_col
      integer :: bb_block_row
      integer :: loc_row, loc_col

      integer :: uredbb_row !, lredbb_row now in output
      integer :: lredbb_col, uredbb_col
      integer, dimension(9) :: bbxredbb_desc, states2xredbb_desc

      integer, external :: indxg2l

      call MPI_Comm_rank(CC4S_comm, myid, ierr)
      allocate(vertex_sq(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
      allocate(vertex_SV(CC4S_n_basbas))
      allocate(vertex_eigvec(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))

      do i_spin=1,CC4S_n_spin
        !Calculate square of the vertex
        call pzgemm('N', 'C', CC4S_n_basbas, CC4S_n_basbas, CC4S_nuf_states**2, (1.0D0, 0.0D0), &
                &complete_vertex(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, &
                &complete_vertex(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, &
                &(0.0D0, 0.0D0), vertex_sq(:,:), 1, 1, CC4S_bb_desc)

        vertex_sq(:,:)=-vertex_sq(:,:)

        allocate(work(1))
        lwork=-1

        if(CC4S_n_tasks .eq. 1) then
          allocate(rwork(3*CC4S_n_basbas-2))
          call zheev('V', 'U', CC4S_n_basbas, vertex_sq(:,:), CC4S_n_basbas, &
                  &vertex_SV, work, lwork, rwork, info)

          lwork=int(work(1))
          deallocate(work)
          allocate(work(lwork))

          call zheev('V', 'U', CC4S_n_basbas, vertex_sq(:,:), CC4S_n_basbas, &
                  &vertex_SV, work, lwork, rwork, info)

          deallocate(work, rwork)
          vertex_eigvec(:,:)=vertex_sq(:,:)
        else
          allocate(rwork(1))
          lrwork=-1
          call pzheev('V', 'U', CC4S_n_basbas, vertex_sq(:,:), 1, 1, CC4S_bb_desc, &
                  &vertex_SV, vertex_eigvec(:,:), 1, 1, CC4S_bb_desc, &
                  &work, lwork, rwork, lrwork, info)

          lwork=int(work(1))
          deallocate(work)
          allocate(work(lwork))

          lrwork=int(rwork(1))
          deallocate(rwork)
          allocate(rwork(lrwork))

          call pzheev('V', 'U', CC4S_n_basbas, vertex_sq(:,:), 1, 1, CC4S_bb_desc, &
                  &vertex_SV, vertex_eigvec(:,:), 1, 1, CC4S_bb_desc, &
                  &work, lwork, rwork, lrwork, info)

          deallocate(work, rwork)
        end if 

        vertex_SV(:)=-vertex_SV(:)

        if(present(singval_thr)) then
          do i_singval=1,size(vertex_SV)
            if(vertex_SV(i_singval) < singval_thr) then
              exit
            end if 
          end do

          max_singval=i_singval-1
        else if(present(singval_num)) then
          max_singval=singval_num
        else
          if(myid .eq. 0) then
            write(*,*) "No SV threshold provided. Using complete vertex"
          end if 
          return 
        end if 

        if(myid .eq. 0) then
          write(*,*) "Of", CC4S_n_basbas, "singular values,", &
                  &max_singval, "will be kept"
        end if 

        call CC4S_init_blacsOAF_distribution(max_singval, lredbb_row, &
                &uredbb_row, lredbb_col, uredbb_col, &
                &bbxredbb_desc, states2xredbb_desc, redbb_block_col, bb_block_row)

        allocate(proj_id(CC4S_lbb_row:CC4S_ubb_row, lredbb_col:uredbb_col))
        proj_id=(0.0D0, 0.0D0)

       !redbb_block_col=(lredbb_col-1)/myid_col
       !bb_block_row=(CC4S_lbb_row-1)/myid_row

        do i_basbas=1,max_singval
          if((mod((i_basbas-1)/bb_block_row, CC4S_nprow) .eq. CC4S_myid_row) &
                  &.and. (mod((i_basbas-1)/redbb_block_col, CC4S_npcol) &
                  &.eq. CC4S_myid_col)) then

            loc_row=indxg2l(i_basbas, bb_block_row, 0, 0, CC4S_nprow)
            loc_col=indxg2l(i_basbas, redbb_block_col, 1, 1, CC4S_npcol)

            proj_id(CC4S_lbb_row+loc_row-1, lredbb_col+loc_col-1) = (1.0D0, 0.0D0)
          end if 
        end do
        
       allocate(vertex_eigvec_OAF(CC4S_lbb_row:CC4S_ubb_row, lredbb_col:uredbb_col)) 

       call pzgemm('N', 'N', CC4S_n_basbas, max_singval, CC4S_n_basbas, (1.0D0, 0.0D0), &
               &vertex_eigvec(:,:), 1, 1, CC4S_bb_desc, proj_id(:,:), 1, 1, &
               &bbxredbb_desc, (0.0D0, 0.0D0), vertex_eigvec_OAF(:,:), &
               &1, 1, bbxredbb_desc)

       deallocate(proj_id)

       allocate(vertex_OAF(lredbb_row:uredbb_row, &
               &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
       vertex_OAF(:,:,:) = (0.0D0, 0.0D0)
       call pzgemm('T', 'N', max_singval, CC4S_nuf_states**2, CC4S_n_basbas, (1.0D0, 0.0D0), &
               &vertex_eigvec_OAF(:,:), 1, 1, bbxredbb_desc, &
               &complete_vertex(:,:,i_spin), 1, 1, CC4S_states2xbb_desc, (0.0D0, 0.0D0), &
               &vertex_OAF(:,:,i_spin), 1, 1, states2xredbb_desc)

      end do !n_spin

      if(allocated(vertex_sq)) then
        deallocate(vertex_sq)
      end if 

      if(allocated(vertex_SV)) then
        deallocate(vertex_SV)
      end if 

      if(allocated(vertex_eigvec)) then
        deallocate(vertex_eigvec)
      end if 



end subroutine CC4S_prune_vertex_cmplx
!*******************************
subroutine CC4S_reshape_vertex_2D_real(vertex_kq, reshape_vertex_kq)

        use calculation_data, only: CC4S_n_spin, CC4S_nuf_states
        use CC4S_blacs_environment, only: CC4S_lbb_row, CC4S_ubb_row, &
                &CC4S_lstates_col, CC4S_ustates_col, CC4S_lstates2_col, CC4S_ustates2_col
      implicit none

      !input
      !double precision, dimension(CC4S_lbb_row:CC4S_ubb_row,&
      !        &CC4S_lstates_col:CC4S_ustates_col, 1:CC4S_nuf_states, 1:CC4S_n_spin), &
      !        &intent(in) :: vertex_kq
      !double precision, dimension(CC4S_lbb_row:CC4S_ubb_row, &
      !        &CC4S_lstates2_col: CC4S_ustates2_col, CC4S_n_spin) :: reshape_vertex_kq

      double precision, dimension(:,:,:,:), intent(in) :: vertex_kq
      double precision, dimension(:,:,:) :: reshape_vertex_kq

      !local
      integer, dimension(2) :: newshape
      double precision, dimension(:,:,:), allocatable :: vertex_kq_T
      integer :: i_spin, i_basbas


      allocate(vertex_kq_T(size(vertex_kq,1), &
              &size(vertex_kq,3), size(vertex_kq,2)))

      do i_spin=1,CC4S_n_spin
        do i_basbas=1,size(vertex_kq,1)
          vertex_kq_T(i_basbas,:,:)=transpose(vertex_kq(i_basbas,:,:,i_spin))
        end do

        newshape(1)=size(vertex_kq,1)
        newshape(2)=size(reshape_vertex_kq,2)

        reshape_vertex_kq(:,:,i_spin)=reshape(vertex_kq_T(:,:,:), newshape)

      end do
end subroutine CC4S_reshape_vertex_2D_real
!********************************************
subroutine CC4S_reshape_vertex_2D_cmplx(vertex_kq, reshape_vertex_kq)

        use calculation_data, only: CC4S_n_spin, CC4S_nuf_states
        use CC4S_blacs_environment, only: CC4S_lbb_row, CC4S_ubb_row, &
                &CC4S_lstates_col, CC4S_ustates_col, CC4S_lstates2_col, CC4S_ustates2_col
      implicit none

      !input
      double complex, dimension(:,:,:,:), intent(in) :: vertex_kq
      double complex, dimension(:,:,:) :: reshape_vertex_kq

      !local
      integer, dimension(2) :: newshape
      double complex, dimension(:,:,:), allocatable :: vertex_kq_T
      integer :: i_spin, i_basbas


      allocate(vertex_kq_T(size(vertex_kq,1), &
              &size(vertex_kq,3), size(vertex_kq,2)))

      do i_spin=1,CC4S_n_spin
        do i_basbas=1,size(vertex_kq,1)
          vertex_kq_T(i_basbas,:,:)=transpose(vertex_kq(i_basbas,:,:,i_spin))
        end do

        newshape(1)=size(vertex_kq,1)
        newshape(2)=size(reshape_vertex_kq,2)

        reshape_vertex_kq(:,:,i_spin)=reshape(vertex_kq_T(:,:,:), newshape)

      end do
end subroutine CC4S_reshape_vertex_2D_cmplx
end module CC4S_OAF
