module CC4S_parallel_io

        use CC4S_bin2format

        interface CC4S_parallel_write
                module procedure CC4S_parallel_io_real
                module procedure CC4S_parallel_io_cmplx
        end interface CC4S_parallel_write

        interface CC4S_parallel_write_oaf_singular_vectors
                module procedure CC4S_parallel_write_oaf_singular_vectors_real
                module procedure CC4S_parallel_write_oaf_singular_vectors_cmplx
        end interface CC4S_parallel_write_oaf_singular_vectors
contains
subroutine CC4S_parallel_write_oaf_singular_vectors_real(sing_vecs_arr, fh, sing_vecs_sizes, sing_vecs_subsizes, sing_vecs_starts, &
                &max_singval, curr_kq_pair)

      use calculation_data, only: CC4S_n_states, CC4S_n_spin, CC4S_nuf_states, CC4S_n_basbas
      use CC4S_blacs_environment, only: CC4S_myid
      
      use mpi
      implicit none

      !PURPOSE: Primarily to be used to write the OAF singular vectors to
      !file in parallel. It assumes that a file object has already been
      !opened via MPI_File_open.

      !input
      integer, dimension(2), intent(in) :: sing_vecs_sizes, sing_vecs_subsizes, sing_vecs_starts

      double precision, dimension(sing_vecs_subsizes(1), sing_vecs_subsizes(2)), &
              &intent(in)  :: sing_vecs_arr
      integer, intent(in) :: fh !MPI File handle
      integer, intent(in) :: max_singval
      integer, intent(in) :: curr_kq_pair

      !local
      integer :: ierr
      integer :: subarr

      integer, parameter :: dp = selected_real_kind(15)
      integer(kind=MPI_OFFSET_KIND) :: offset
      integer(kind=MPI_OFFSET_KIND) :: unit_size=16
      integer :: nbytes
   
      integer :: i
      character(len=10) :: file_id
      character(len=50) :: file_name
      !CC4S always expects a complex vertex, even if the imaginary part is 0
      call MPI_Type_create_subarray(2, sing_vecs_sizes, sing_vecs_subsizes, sing_vecs_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

      call MPI_Type_commit(subarr, ierr)

      offset=unit_size*max_singval*CC4S_n_basbas*curr_kq_pair

      call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, subarr, 'native', MPI_INFO_NULL, ierr)
      nbytes=size(sing_vecs_arr)


      call MPI_File_write_all(fh, cmplx(sing_vecs_arr, kind=dp), nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

end subroutine CC4S_parallel_write_oaf_singular_vectors_real
!******************************************************
subroutine CC4S_parallel_write_oaf_singular_vectors_cmplx(sing_vecs_arr, fh, &
               &sing_vecs_sizes, sing_vecs_subsizes, sing_vecs_starts, &
               &max_singval, curr_kq_pair)

      use calculation_data, only: CC4S_n_states, CC4S_n_spin, CC4S_nuf_states, CC4S_n_basbas
      use mpi
      implicit none

      !PURPOSE: Primarily to be used to write the OAF singular vectors to
      !file in parallel. It assumes that a file object has already been
      !opened via MPI_File_open.

      !input
      integer, dimension(2), intent(in) :: sing_vecs_sizes, sing_vecs_subsizes, sing_vecs_starts
      double complex, dimension(sing_vecs_subsizes(1), sing_vecs_subsizes(2)), &
              &intent(in)  :: sing_vecs_arr
      integer, intent(in) :: fh !MPI File handle
      integer, intent(in) :: max_singval
      integer, intent(in) :: curr_kq_pair

      !local
      integer :: ierr
      integer :: subarr

      integer(kind=MPI_OFFSET_KIND) :: offset
      integer(kind=MPI_OFFSET_KIND) :: unit_size=16
      integer :: nbytes

      !CC4S always expects a complex vertex, even if the imaginary part is 0
      call MPI_Type_create_subarray(2, sing_vecs_sizes, sing_vecs_subsizes, sing_vecs_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

      call MPI_Type_commit(subarr, ierr)

      offset=unit_size*max_singval*CC4S_n_basbas*curr_kq_pair

      call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, subarr, 'native', MPI_INFO_NULL, ierr)

      nbytes=size(sing_vecs_arr)
      call MPI_File_write_all(fh, sing_vecs_arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

end subroutine CC4S_parallel_write_oaf_singular_vectors_cmplx
!******************************************************

























subroutine CC4S_parallel_io_real(OAF_arr, fh, OAF_sizes, OAF_subsizes, OAF_starts, &
                &max_singval, curr_kq_pair)

      use calculation_data, only: CC4S_n_states, CC4S_n_spin, CC4S_nuf_states
      use CC4S_blacs_environment, only: CC4S_myid
      
      use mpi
      implicit none

      !PURPOSE: Primarily to be used to write the OAF Coulomb vertex to
      !file in parallel. It assumes that a file object has already been
      !opened via MPI_File_open.

      !input
      integer, dimension(3), intent(in) :: OAF_sizes, OAF_subsizes, OAF_starts

      double precision, dimension(OAF_subsizes(1), OAF_subsizes(2), OAF_subsizes(3)), &
              &intent(in)  :: OAF_arr
      integer, intent(in) :: fh !MPI File handle
      integer, intent(in) :: max_singval
      integer, intent(in) :: curr_kq_pair

      !local
      integer :: ierr
      integer :: subarr

      integer, parameter :: dp = selected_real_kind(15)
      integer(kind=MPI_OFFSET_KIND) :: offset
      integer(kind=MPI_OFFSET_KIND) :: unit_size=16
      integer :: nbytes
   
      integer :: i
      character(len=10) :: file_id
      character(len=50) :: file_name
      !CC4S always expects a complex vertex, even if the imaginary part is 0
      call MPI_Type_create_subarray(3, OAF_sizes, OAF_subsizes, OAF_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

      call MPI_Type_commit(subarr, ierr)

      offset=unit_size*max_singval*(CC4S_nuf_states**2)*CC4S_n_spin*curr_kq_pair

      call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, subarr, 'native', MPI_INFO_NULL, ierr)
      nbytes=size(OAF_arr)


      call MPI_File_write_all(fh, cmplx(OAF_arr, kind=dp), nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

end subroutine CC4S_parallel_io_real
!******************************************************
subroutine CC4S_parallel_io_cmplx(OAF_arr, fh, OAF_sizes, OAF_subsizes, OAF_starts, &
                &max_singval, curr_kq_pair)

      use calculation_data, only: CC4S_n_states, CC4S_n_spin, CC4S_nuf_states
      use mpi
      use CC4S_blacs_environment, only: CC4S_myid
      implicit none

      !PURPOSE: Primarily to be used to write the OAF Coulomb vertex to
      !file in parallel. It assumes that a file object has already been
      !opened via MPI_File_open.

      !input
      integer, dimension(3), intent(in) :: OAF_sizes, OAF_subsizes, OAF_starts
      double complex, dimension(OAF_subsizes(1), OAF_subsizes(2), OAF_subsizes(3)), &
              &intent(in)  :: OAF_arr
      integer, intent(in) :: fh !MPI File handle
      integer, intent(in) :: max_singval
      integer, intent(in) :: curr_kq_pair

      !local
      integer :: ierr
      integer :: subarr

      integer(kind=MPI_OFFSET_KIND) :: offset
      integer(kind=MPI_OFFSET_KIND) :: unit_size=16
      integer :: nbytes

      if(CC4S_myid .eq. 0) then
        write(*,*) "Writing tensor block of shape", OAF_sizes, "to file."
      end if

      !CC4S always expects a complex vertex, even if the imaginary part is 0
      call MPI_Type_create_subarray(3, OAF_sizes, OAF_subsizes, OAF_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

      call MPI_Type_commit(subarr, ierr)

      !offset=16*max_singval*(CC4S_nuf_states**2)*CC4S_n_spin*curr_kq_pair
      offset=unit_size*max_singval*(CC4S_nuf_states**2)*CC4S_n_spin*curr_kq_pair

      write(*,'(A,I0,A,I0,A,I0,A,I0)') "From CC4S_parallel_io_cmplx in rank ",CC4S_myid," : max_singval = ", max_singval,&
                                  & ", offset = ", offset, ", nuf_states**2 = ", CC4S_nuf_states**2

      call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, subarr, 'native', MPI_INFO_NULL, ierr)

      nbytes=size(OAF_arr)
      call MPI_File_write_all(fh, OAF_arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

end subroutine CC4S_parallel_io_cmplx
!******************************************************
subroutine CC4S_parallel_io_coulmat(coulmat_arr, fh, &
             &coulmat_sizes, coulmat_subsizes, coulmat_starts, curr_kq_pair)

      use calculation_data, only: CC4S_num_pws, CC4S_n_basbas 
      use mpi
      implicit none

      !PURPOSE: To be used to write the RI-Coulomb matrix to
      !file in parallel. It assumes that a file object has already been
      !opened via MPI_File_open.

      !input
      integer, dimension(2), intent(in) :: coulmat_sizes, coulmat_subsizes, coulmat_starts
      double complex, dimension(coulmat_subsizes(1), coulmat_subsizes(2)), intent(in)  :: coulmat_arr
      integer, intent(in) :: fh !MPI File handle
      integer, intent(in) :: curr_kq_pair

      !local
      integer :: ierr
      integer :: subarr

      integer(kind=MPI_OFFSET_KIND) :: offset
      integer(kind=MPI_OFFSET_KIND) :: unit_size=16
      integer :: nbytes

      !CC4S always expects a complex vertex, even if the imaginary part is 0
      call MPI_Type_create_subarray(2, coulmat_sizes, coulmat_subsizes, coulmat_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

      call MPI_Type_commit(subarr, ierr)

      offset=unit_size*CC4S_n_basbas*CC4S_n_basbas*curr_kq_pair

      call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, subarr, 'native', MPI_INFO_NULL, ierr)

      nbytes=size(coulmat_arr)
      call MPI_File_write_all(fh, coulmat_arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

end subroutine CC4S_parallel_io_coulmat

!******************************************************
subroutine CC4S_parallel_io_pw_transform_cmplx(pw_transform_arr, fh, &
             &pw_transform_sizes, pw_transform_subsizes, pw_transform_starts, curr_kq_pair)

      use calculation_data, only: CC4S_num_pws, CC4S_n_basbas 
      use mpi
      implicit none

      !PURPOSE: Primarily to be used to write the OAF Coulomb vertex to
      !file in parallel. It assumes that a file object has already been
      !opened via MPI_File_open.

      !input
      integer, dimension(2), intent(in) :: pw_transform_sizes, pw_transform_subsizes, pw_transform_starts
      double complex, dimension(pw_transform_subsizes(1), pw_transform_subsizes(2)), intent(in)  :: pw_transform_arr
      integer, intent(in) :: fh !MPI File handle
      integer, intent(in) :: curr_kq_pair

      !local
      integer :: ierr
      integer :: subarr

      integer(kind=MPI_OFFSET_KIND) :: offset
      integer(kind=MPI_OFFSET_KIND) :: unit_size=16
      integer :: nbytes

      !CC4S always expects a complex vertex, even if the imaginary part is 0
      call MPI_Type_create_subarray(2, pw_transform_sizes, pw_transform_subsizes, pw_transform_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

      call MPI_Type_commit(subarr, ierr)

      offset=unit_size*CC4S_num_pws*CC4S_n_basbas*curr_kq_pair

      call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, subarr, 'native', MPI_INFO_NULL, ierr)

      nbytes=size(pw_transform_arr)
      call MPI_File_write_all(fh, pw_transform_arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

end subroutine CC4S_parallel_io_pw_transform_cmplx

!************************************************
subroutine CC4S_parallel_io_V_inv_sqrt_complex(v_inv_sqrt_arr, fh, v_inv_sqrt_sizes, v_inv_sqrt_subsizes, v_inv_sqrt_starts, &
                &max_singval, curr_kq_pair)

      use calculation_data, only: CC4S_num_pws
      use CC4S_blacs_environment, only: CC4S_myid
      
      use mpi
      implicit none

      !PURPOSE: Primarily to be used to write the OAF Coulomb vertex to
      !file in parallel. It assumes that a file object has already been
      !opened via MPI_File_open.

      !input
      integer, dimension(2), intent(in) :: v_inv_sqrt_sizes, v_inv_sqrt_subsizes, v_inv_sqrt_starts

      double complex, dimension(v_inv_sqrt_subsizes(1), v_inv_sqrt_subsizes(2)), &
              &intent(in)  :: v_inv_sqrt_arr
      integer, intent(in) :: fh !MPI File handle
      integer, intent(in) :: max_singval
      integer, intent(in) :: curr_kq_pair

      !local
      integer :: ierr
      integer :: subarr

      integer, parameter :: dp = selected_real_kind(15)
      integer(kind=MPI_OFFSET_KIND) :: offset
      integer(kind=MPI_OFFSET_KIND) :: unit_size=16
      integer :: nbytes
   
      integer :: i
      character(len=10) :: file_id
      character(len=50) :: file_name
      !CC4S always expects a complex vertex, even if the imaginary part is 0
      call MPI_Type_create_subarray(2, v_inv_sqrt_sizes, v_inv_sqrt_subsizes, v_inv_sqrt_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

      call MPI_Type_commit(subarr, ierr)

      !offset=16*max_singval*(CC4S_nuf_states**2)*CC4S_n_spin*curr_kq_pair
      offset=unit_size*max_singval*CC4S_num_pws*curr_kq_pair

      call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, subarr, 'native', MPI_INFO_NULL, ierr)
      nbytes=size(v_inv_sqrt_arr)


      call MPI_File_write_all(fh, v_inv_sqrt_arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

end subroutine CC4S_parallel_io_V_inv_sqrt_complex
!******************************************************
subroutine CC4S_debug_write_5d_cmplx(comm, arr, arr_size, arr_sizes, arr_subsizes, arr_starts, filename, writingTaskID)
        use mpi

        implicit none
        !input
        integer :: comm
        double complex, dimension(:,:,:,:,:), intent(in) :: arr
        integer :: arr_size
        integer, dimension(5) :: arr_sizes, arr_subsizes, arr_starts
        character(len=*) :: filename
        integer, optional :: writingTaskID

        !local
        integer :: subarr
        integer :: ierr

        integer(kind=MPI_OFFSET_KIND) :: offset
        integer :: nbytes
        character(len=200) :: bin_file, dat_file
        integer :: fh

        bin_file=filename // '.bin'
        dat_file=filename // '.dat'
        call MPI_File_open(comm, bin_file, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, fh, ierr)
        call MPI_Type_create_subarray(5, arr_sizes, arr_subsizes, arr_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

        call MPI_Type_commit(subarr, ierr)
        offset=0
        call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, &
                &subarr, 'native', MPI_INFO_NULL, ierr)

        nbytes=size(arr)
        call MPI_File_write_all(fh, arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

        call MPI_File_close(fh, ierr)
        
        if(present(writingTaskID)) then
          call bin2format(bin_file, dat_file, comm, arr_size, writingTaskID)
        else
          call bin2format(bin_file, dat_file, comm, arr_size)
        end if 
end subroutine CC4S_debug_write_5d_cmplx

!************************************************
subroutine CC4S_debug_write_5d_real(comm, arr, arr_size, arr_sizes, arr_subsizes, arr_starts, filename, writingTaskID)
        use mpi

        implicit none
        !input
        integer :: comm
        double precision, dimension(:,:,:,:,:), intent(in) :: arr
        integer :: arr_size
        integer, dimension(5) :: arr_sizes, arr_subsizes, arr_starts
        character(len=*) :: filename
        integer, optional :: writingTaskID

        !local
        integer :: subarr
        integer :: ierr

        integer(kind=MPI_OFFSET_KIND) :: offset
        integer :: nbytes
        character(len=200) :: bin_file, dat_file
        integer :: fh

        bin_file=filename // '.bin'
        dat_file=filename // '.dat'
        call MPI_File_open(comm, bin_file, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, fh, ierr)
        call MPI_Type_create_subarray(5, arr_sizes, arr_subsizes, arr_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarr, ierr)

        call MPI_Type_commit(subarr, ierr)
        offset=0
        call MPI_File_set_view(fh, offset, MPI_DOUBLE_PRECISION, &
                &subarr, 'native', MPI_INFO_NULL, ierr)

        nbytes=size(arr)
        call MPI_File_write_all(fh, arr, nbytes, MPI_DOUBLE_PRECISION, &
              &MPI_STATUS_IGNORE, ierr)

        call MPI_File_close(fh, ierr)

        if(present(writingTaskID)) then
          call bin2format_real(bin_file, dat_file, comm, arr_size, writingTaskID)
        else
          call bin2format_real(bin_file, dat_file, comm, arr_size)
        end if

end subroutine CC4S_debug_write_5d_real
!********************************************
subroutine CC4S_debug_write_3d_real(comm, arr, arr_size, arr_sizes, arr_subsizes, arr_starts, filename, writingTaskID)
        use mpi

        implicit none
        !input
        integer :: comm
        double precision, dimension(:,:,:), intent(in) :: arr
        integer :: arr_size
        integer, dimension(3) :: arr_sizes, arr_subsizes, arr_starts
        character(len=*) :: filename
        integer, optional :: writingTaskID

        !local
        integer :: subarr
        integer :: ierr

        integer(kind=MPI_OFFSET_KIND) :: offset
        integer :: nbytes
        character(len=200) :: bin_file, dat_file
        integer :: fh

        bin_file=filename // '.bin'
        dat_file=filename // '.dat'
        call MPI_File_open(comm, bin_file, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, fh, ierr)
        call MPI_Type_create_subarray(3, arr_sizes, arr_subsizes, arr_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarr, ierr)

        call MPI_Type_commit(subarr, ierr)
        offset=0
        call MPI_File_set_view(fh, offset, MPI_DOUBLE_PRECISION, &
                &subarr, 'native', MPI_INFO_NULL, ierr)

        nbytes=size(arr)
        call MPI_File_write_all(fh, arr, nbytes, MPI_DOUBLE_PRECISION, &
              &MPI_STATUS_IGNORE, ierr)

        call MPI_File_close(fh, ierr)

        if(present(writingTaskID)) then
          call bin2format_real(bin_file, dat_file, comm, arr_size, writingTaskID)
        else
          call bin2format_real(bin_file, dat_file, comm, arr_size)
        end if


end subroutine CC4S_debug_write_3d_real
!******************************************
subroutine CC4S_debug_write_3d_cmplx(comm, arr, arr_size, arr_sizes, arr_subsizes, arr_starts, filename, writingTaskID)
        use mpi

        implicit none
        !input
        integer :: comm
        double complex, dimension(:,:,:), intent(in) :: arr
        integer :: arr_size
        integer, dimension(3) :: arr_sizes, arr_subsizes, arr_starts
        character(len=*) :: filename
        integer, optional :: writingTaskID

        !local
        integer :: subarr
        integer :: ierr

        integer(kind=MPI_OFFSET_KIND) :: offset
        integer :: nbytes
        character(len=200) :: bin_file, dat_file
        integer :: fh

        bin_file=filename // '.bin'
        dat_file=filename // '.dat'
        call MPI_File_open(comm, bin_file, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, fh, ierr)
        call MPI_Type_create_subarray(3, arr_sizes, arr_subsizes, arr_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

        call MPI_Type_commit(subarr, ierr)
        offset=0
        call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, &
                &subarr, 'native', MPI_INFO_NULL, ierr)

        nbytes=size(arr)
        call MPI_File_write_all(fh, arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

        call MPI_File_close(fh, ierr)

        if(present(writingTaskID)) then
          call bin2format(bin_file, dat_file, comm, arr_size, writingTaskID)
        else
          call bin2format(bin_file, dat_file, comm, arr_size)
        end if


end subroutine CC4S_debug_write_3d_cmplx
!******************************************
subroutine CC4S_debug_write_4d_real(comm, arr, arr_size, arr_sizes, arr_subsizes, arr_starts, filename, writingTaskID)
        use mpi

        implicit none
        !input
        integer :: comm
        double precision, dimension(:,:,:,:), intent(in) :: arr
        integer :: arr_size
        integer, dimension(4) :: arr_sizes, arr_subsizes, arr_starts
        character(len=*) :: filename
        integer, optional :: writingTaskID

        !local
        integer :: subarr
        integer :: ierr

        integer(kind=MPI_OFFSET_KIND) :: offset
        integer :: nbytes
        character(len=200) :: bin_file, dat_file
        integer :: fh

        bin_file=filename // '.bin'
        dat_file=filename // '.dat'
        call MPI_File_open(comm, bin_file, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, fh, ierr)
        call MPI_Type_create_subarray(4, arr_sizes, arr_subsizes, arr_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarr, ierr)

        call MPI_Type_commit(subarr, ierr)
        offset=0
        call MPI_File_set_view(fh, offset, MPI_DOUBLE_PRECISION, &
                &subarr, 'native', MPI_INFO_NULL, ierr)

        nbytes=size(arr)
        call MPI_File_write_all(fh, arr, nbytes, MPI_DOUBLE_PRECISION, &
              &MPI_STATUS_IGNORE, ierr)

        call MPI_File_close(fh, ierr)

        if(present(writingTaskID)) then
          call bin2format_real(bin_file, dat_file, comm, arr_size, writingTaskID)
        else
          call bin2format_real(bin_file, dat_file, comm, arr_size)
        end if


end subroutine CC4S_debug_write_4d_real
!*******************************************************
subroutine CC4S_debug_write_4d_cmplx(comm, arr, arr_size, arr_sizes, arr_subsizes, arr_starts, filename, writingTaskID)
        use mpi

        implicit none
        !input
        integer :: comm
        double complex, dimension(:,:,:,:), intent(in) :: arr
        integer :: arr_size
        integer, dimension(4) :: arr_sizes, arr_subsizes, arr_starts
        character(len=*) :: filename
        integer, optional :: writingTaskID

        !local
        integer :: subarr
        integer :: ierr

        integer(kind=MPI_OFFSET_KIND) :: offset
        integer :: nbytes
        character(len=200) :: bin_file, dat_file
        integer :: fh

        bin_file=filename // '.bin'
        dat_file=filename // '.dat'
        call MPI_File_open(comm, bin_file, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, fh, ierr)
        call MPI_Type_create_subarray(4, arr_sizes, arr_subsizes, arr_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

        call MPI_Type_commit(subarr, ierr)
        offset=0
        call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, &
                &subarr, 'native', MPI_INFO_NULL, ierr)

        nbytes=size(arr)
        call MPI_File_write_all(fh, arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

        call MPI_File_close(fh, ierr)
        
        if(present(writingTaskID)) then
          call bin2format(bin_file, dat_file, comm, arr_size, writingTaskID)
        else
          call bin2format(bin_file, dat_file, comm, arr_size)
        end if 
end subroutine CC4S_debug_write_4d_cmplx
!********************************************************
subroutine CC4S_debug_write_2d_real(comm, arr, arr_size, arr_sizes, arr_subsizes, arr_starts, filename, writingTaskID)
        use mpi

        implicit none
        !input
        integer :: comm
        double precision, dimension(:,:), intent(in) :: arr
        integer :: arr_size
        integer, dimension(2) :: arr_sizes, arr_subsizes, arr_starts
        character(len=*) :: filename
        integer, optional :: writingTaskID

        !local
        integer :: subarr
        integer :: ierr

        integer(kind=MPI_OFFSET_KIND) :: offset
        integer :: nbytes
        character(len=200) :: bin_file, dat_file
        integer :: fh

        bin_file=filename // '.bin'
        dat_file=filename // '.dat'
        call MPI_File_open(comm, bin_file, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, fh, ierr)
        call MPI_Type_create_subarray(2, arr_sizes, arr_subsizes, arr_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarr, ierr)

        call MPI_Type_commit(subarr, ierr)
        offset=0
        call MPI_File_set_view(fh, offset, MPI_DOUBLE_PRECISION, &
                &subarr, 'native', MPI_INFO_NULL, ierr)

        nbytes=size(arr)
        call MPI_File_write_all(fh, arr, nbytes, MPI_DOUBLE_PRECISION, &
              &MPI_STATUS_IGNORE, ierr)

        call MPI_File_close(fh, ierr)

        if(present(writingTaskID)) then
          call bin2format_real(bin_file, dat_file, comm, arr_size, writingTaskID)
        else
          call bin2format_real(bin_file, dat_file, comm, arr_size)
        end if


end subroutine CC4S_debug_write_2d_real
!*******************************************************
subroutine CC4S_debug_write_2d_cmplx(comm, arr, arr_size, arr_sizes, arr_subsizes, arr_starts, filename, writingTaskID)
        use mpi

        implicit none
        !input
        integer :: comm
        double complex, dimension(:,:), intent(in) :: arr
        integer :: arr_size
        integer, dimension(2) :: arr_sizes, arr_subsizes, arr_starts
        character(len=*) :: filename
        integer, optional :: writingTaskID

        !local
        integer :: subarr
        integer :: ierr

        integer(kind=MPI_OFFSET_KIND) :: offset
        integer :: nbytes
        character(len=200) :: bin_file, dat_file
        integer :: fh

        bin_file=filename // '.bin'
        dat_file=filename // '.dat'
        call MPI_File_open(comm, bin_file, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, fh, ierr)
        call MPI_Type_create_subarray(2, arr_sizes, arr_subsizes, arr_starts, &
              &MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, subarr, ierr)

        call MPI_Type_commit(subarr, ierr)
        offset=0
        call MPI_File_set_view(fh, offset, MPI_DOUBLE_COMPLEX, &
                &subarr, 'native', MPI_INFO_NULL, ierr)

        nbytes=size(arr)
        call MPI_File_write_all(fh, arr, nbytes, MPI_DOUBLE_COMPLEX, &
              &MPI_STATUS_IGNORE, ierr)

        call MPI_File_close(fh, ierr)
        
        if(present(writingTaskID)) then
          call bin2format(bin_file, dat_file, comm, arr_size, writingTaskID)
        else
          call bin2format(bin_file, dat_file, comm, arr_size)
        end if 
end subroutine CC4S_debug_write_2d_cmplx

end module CC4S_parallel_io
