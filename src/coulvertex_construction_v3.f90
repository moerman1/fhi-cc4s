module CC4S_coul_vertex
use mpi

use calculation_data, only: CC4S_kpq_point_list, CC4S_isreal, &
        &CC4S_nuf_states, CC4S_n_spin, CC4S_n_k_points, CC4S_n_kq_points, &
        &CC4S_n_basbas
use CC4S_blacs_environment, only: CC4S_lbb_row,CC4S_ubb_row, CC4S_comm, &
        &CC4S_lstates_col,CC4S_ustates_col, CC4S_lstates2_col, CC4S_ustates2_col, CC4S_myid, &
        &CC4S_lbb_col 
use CC4S_VtimesM, only: CC4S_v_times_M_k_q
use CC4S_OAF, only: CC4S_prune_vertex, CC4S_reshape_vertex_2D
use CC4S_M_k_q, only: CC4S_compute_M_k_q
use CC4S_parallel_io, only: CC4S_parallel_write, CC4S_debug_write_4d_real, &
        &CC4S_debug_write_2d_real, CC4S_debug_write_3d_real, cc4s_debug_write_4d_cmplx
use CC4S_bin2format
use CC4S_vertex_block, only: CC4S_build_vertex_block
use CC4S_OAF_transformation, only: CC4S_add_up_squared_block, CC4S_reset_squared_block_sum, &
                                   &CC4S_compute_OAF_trafo_matrix_real, CC4S_compute_OAF_trafo_matrix_cmplx, &
                                   &U_times_Gamma

contains
        subroutine CC4S_coulvertex_construction(aomo_lvl_r, aomo_lvl_c, &
                        &scf_eigvecs_real, scf_eigvecs_cmplx, &
                        &sqrt_coulmat_real, sqrt_coulmat_cmplx, CoulombVertex_fh, &
                        &singval_thr, singval_num)

          implicit none

          !input
          double precision, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_r
          double complex, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_c
          double precision, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_real
          double complex, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_cmplx
          double precision, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_real
          double complex, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_cmplx
          integer, intent(in) :: CoulombVertex_fh
          double precision, optional :: singval_thr
          integer, optional :: singval_num

          !local
          integer :: curr_pair
          integer :: i_q_point, i_kq_pair, i_k_point, i_spin
          double precision, dimension(:,:,:,:), target, allocatable :: momo_lvl_r
          double precision, dimension(:,:,:,:), pointer :: momo_lvl_rptr => null()
          double complex, dimension(:,:,:,:), target, allocatable :: momo_lvl_c
          double complex, dimension(:,:,:,:), pointer :: momo_lvl_cptr => null()
          double precision, dimension(:,:,:), pointer :: sr_momo_lvl_rptr => null()
          double complex, dimension(:,:,:), pointer :: sr_momo_lvl_cptr => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_k => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_k => null() 
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_q => null()
          double precision, dimension(:,:), pointer :: KS_rptr_k => null()
          double precision, dimension(:,:), pointer :: KS_rptr_q => null()
          double complex, dimension(:,:), pointer :: KS_cptr_k => null()
          double complex, dimension(:,:), pointer :: KS_cptr_q => null()
          double precision, dimension(:,:), pointer :: sqrt_cm_rptr => null()
          double complex, dimension(:,:), pointer :: sqrt_cm_cptr => null()
          double precision, dimension(:,:,:), target, allocatable :: vertex_reshaped_r
          double precision, dimension(:,:,:), pointer :: vertex_reshaped_rptr => null()
          double complex, dimension(:,:,:), target, allocatable :: vertex_reshaped_c
          double complex, dimension(:,:,:), pointer :: vertex_reshaped_cptr => null()
          double precision, dimension(:,:,:), allocatable :: vertex_rOAF
          double complex, dimension(:,:,:), allocatable :: vertex_cOAF
          double precision, dimension(:,:,:,:), pointer :: coulomb_vertex_rptr => null()
          double complex, dimension(:,:,:,:), pointer :: coulomb_vertex_cptr => null()

          integer :: OAF_dim

          integer :: i_k2_point, i_q2_point, i_spin2

          integer :: max_singval
          integer :: lredbb_row
          integer, dimension(3) :: OAF_sizes, OAF_subsizes, OAF_starts

          !Just for debugging, remove afterwards
          integer :: momo_size
          integer, dimension(4) :: momo_sizes, momo_subsizes, momo_starts
          integer :: reshape_vertex_size
          integer, dimension(3) :: reshape_vertex_sizes, reshape_vertex_subsizes, reshape_vertex_starts 
          integer :: cm_size
          integer, dimension(2) :: cm_sizes, cm_subsizes, cm_starts
          
          character(len=1) :: taskIdString
          logical :: skip_pruning

          if(CC4S_isreal) then
            allocate(momo_lvl_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                &CC4S_nuf_states, CC4S_n_spin))
            momo_lvl_r(:,:,:,:) = 0.0D0
            momo_lvl_rptr => momo_lvl_r(:,:,:,:)
          else
            allocate(momo_lvl_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                &CC4S_nuf_states, CC4S_n_spin))
            momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
            momo_lvl_cptr => momo_lvl_c(:,:,:,:)
          end if

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p
              write(*,*) "(From CC4S_coulvertex_construction) RANK ", CC4S_myid, &
                      &" : Construction vertex for k-/q-/k-q-point", &
                      &i_q_point, i_k_point, i_kq_pair

              if(CC4S_isreal) then
                momo_lvl_r(:,:,:,:) = 0.0D0
                aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k_point)
                aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q_point)
                sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
              else
                momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
                aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k_point)
                aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q_point)
                sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
              end if 

              do i_spin=1,CC4S_n_spin
                if(CC4S_isreal) then
                  KS_rptr_k => scf_eigvecs_real(:,:,i_spin,i_k_point)
                  KS_rptr_q => scf_eigvecs_real(:,:,i_spin,i_q_point)


                  call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                              &aomo_lvl_rptr_q, &
                                              &KS_rptr_k, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_r)
                else
                  KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin,i_k_point)
                  KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin,i_q_point)

                  call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                              &aomo_lvl_cptr_q, &
                                              &KS_cptr_k, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin,  i_k_point, i_q_point, &
                                              &momo_lvl_c)
                end if                 
              end do !i_spin
              if(CC4S_isreal) then
                momo_size=CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
                momo_sizes=[CC4S_n_basbas, CC4S_nuf_states, CC4S_nuf_states, CC4S_n_spin]
                momo_subsizes=shape(momo_lvl_r)
                momo_starts=[CC4S_lbb_row-1, CC4S_lstates_col-1, 0, 0]

                !Divide the vertex by sqrt(N_k)
                !momo_lvl_r = momo_lvl_r/sqrt(real(CC4S_n_k_points))

!                if (i_k_point == 1 .and. i_q_point == 1) then 
!                  call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_1_1')
!                else if (i_k_point == 1 .and. i_q_point == 2) then
!                  call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_1_2')
!                else if (i_k_point == 2 .and. i_q_point == 1) then
!                  call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_2_1')
!                else if (i_k_point == 2 .and. i_q_point == 2) then
!                  call CC4S_debug_write_4d_real(CC4S_comm, momo_lvl_r, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_2_2')
!                else
!                  write(*,*) "UNEXPECTED ITERATION", i_q_point, i_kq_pair
!                end if 


                
                coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
                allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_r(:,:,:) = 0.0D0
                vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

                call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

                call CC4S_add_up_squared_block(vertex_reshaped_rptr)

                if(allocated(vertex_reshaped_r)) then
                  deallocate(vertex_reshaped_r)
                end if


              else
                momo_size=CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
                momo_sizes=[CC4S_n_basbas, CC4S_nuf_states, CC4S_nuf_states, CC4S_n_spin]
                momo_subsizes=shape(momo_lvl_c)
                momo_starts=[CC4S_lbb_row-1, CC4S_lstates_col-1, 0, 0]

                !Divide the vertex by sqrt(N_k)
                !momo_lvl_r = momo_lvl_r/sqrt(real(CC4S_n_k_points))

!                if (i_k_point == 1 .and. i_q_point == 1) then 
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_1_1')
!                else if (i_k_point == 1 .and. i_q_point == 2) then
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_1_2')
!                else if (i_k_point == 1 .and. i_q_point == 3) then
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_1_3')
!                else if (i_k_point == 2 .and. i_q_point == 1) then
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_2_1')
!                else if (i_k_point == 2 .and. i_q_point == 2) then
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_2_2')
!                else if (i_k_point == 2 .and. i_q_point == 3) then
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_2_3')
!                else if (i_k_point == 3 .and. i_q_point == 1) then
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_3_1')
!                else if (i_k_point == 3 .and. i_q_point == 2) then
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_3_2')
!                else if (i_k_point == 3 .and. i_q_point == 3) then
!                  call CC4S_debug_write_4d_cmplx(CC4S_comm, momo_lvl_c, momo_size, &
!                          &momo_sizes, momo_subsizes, momo_starts, 'coul_vertex_r_3_3')
!                else
!                  write(*,*) "UNEXPECTED ITERATION", i_q_point, i_kq_pair
!                end if

                coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
                allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
                vertex_reshaped_cptr => vertex_reshaped_c(:,:,:)

                call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

                call CC4S_add_up_squared_block(vertex_reshaped_cptr)

                if(allocated(vertex_reshaped_c)) then
                  deallocate(vertex_reshaped_c)
                end if

              end if 


!              curr_pair = curr_pair + 1 
            end do !i_q_point

            !At this point all blocks for a given transfer momentum k-q have been calculated.
            !Hence, here is the point where the OAF-transformation of an entire coulomb-vertex row
            !can be performed

            if(CC4S_isreal) then
              if(present(singval_num) .and. present(singval_thr)) then
                call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, singval_num, singval_thr)
              else if(present(singval_num)) then
                !If only singval_num has been passed
                call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, num_singvals=singval_num)
              else if(present(singval_thr)) then
                !If only singval_thr has been passed
                call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, singval_thr=singval_thr)
              else
                !If none have been passed
                call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim)
              end if 
              !call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, singval_num)
            else
              !call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, singval_num)
              if(present(singval_num) .and. present(singval_thr)) then
                call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, singval_num, singval_thr)
              else if(present(singval_num)) then
                !If only singval_num has been passed
                call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, num_singvals=singval_num)
              else if(present(singval_thr)) then
                !If only singval_thr has been passed
                call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, singval_thr=singval_thr)
              else
                !If none have been passed
                call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim)
              end if
            end if

            !max_singval = min(singval_num, CC4S_n_basbas)
            !The following line serves as a safe-guard.
            !At this point it should be guaranteed that OAF_dim <= CC4S_n_basbas
            max_singval = min(OAF_dim, CC4S_n_basbas)

            !After the trafo matrix has been calculated, the sum of squared blocks
            !belonging to the same transfer momentum is no longer needed
            call CC4S_reset_squared_block_sum()

            !Having calculated the (approximately) unitary transformation matrix
            !for the current transfer momentum,
            !it's time to re-compute the Coulomb vertex blocks and apply the transformation matrix

            do i_q2_point=1,CC4S_n_k_points !k_q
              i_k2_point=CC4S_kpq_point_list(i_q2_point, i_kq_pair) !k_p
!              write(*,*) "(From CC4S_coulvertex_construction) RANK ", CC4S_myid, &
!                      &" : Construction vertex for k-/q-/k-q-point", &
!                      &i_q_point, i_k_point, i_kq_pair

              if(CC4S_isreal) then
                momo_lvl_r(:,:,:,:) = 0.0D0
                aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k2_point)
                aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q2_point)
                sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
              else
                momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
                aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k2_point)
                aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q2_point)
                sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
              end if 

              do i_spin2 = 1,CC4S_n_spin
                if(CC4S_isreal) then
                  KS_rptr_k => scf_eigvecs_real(:,:,i_spin2,i_k2_point)
                  KS_rptr_q => scf_eigvecs_real(:,:,i_spin2,i_q2_point)


                  call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                              &aomo_lvl_rptr_q, &
                                              &KS_rptr_k, &
                                              &KS_rptr_q, &
                                              &sqrt_cm_rptr, &
                                              &i_spin2,  i_k2_point, i_q2_point, &
                                              &momo_lvl_r)
                else
                  KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin2,i_k2_point)
                  KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin2,i_q2_point)

                  call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                              &aomo_lvl_cptr_q, &
                                              &KS_cptr_k, &
                                              &KS_cptr_q, &
                                              &sqrt_cm_cptr, &
                                              &i_spin2,  i_k2_point, i_q2_point, &
                                              &momo_lvl_c)
                end if                 
              end do !i_spin2

              if(CC4S_isreal) then
                coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
                allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_r(:,:,:) = 0.0D0
                vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

                call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

                call U_times_Gamma(vertex_reshaped_rptr, max_singval, vertex_rOAF)

                if(allocated(vertex_reshaped_r)) then
                  deallocate(vertex_reshaped_r)
                end if

                OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
                OAF_subsizes=shape(vertex_rOAF)
                OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]

                call CC4S_parallel_write(vertex_rOAF, CoulombVertex_fh, OAF_sizes, &
                        &OAF_subsizes, OAF_starts, max_singval, curr_pair)
              else
                coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
                allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                        &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
                vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
                vertex_reshaped_cptr => vertex_reshaped_c(:,:,:) 

                call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

                call U_times_Gamma(vertex_reshaped_cptr, max_singval, vertex_cOAF)

                if(allocated(vertex_reshaped_c)) then
                  deallocate(vertex_reshaped_c)
                end if

                OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
                OAF_subsizes=shape(vertex_cOAF)
                OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]

                call CC4S_parallel_write(vertex_cOAF, CoulombVertex_fh, OAF_sizes, &
                        &OAF_subsizes, OAF_starts, max_singval, curr_pair)
              end if 



              curr_pair = curr_pair + 1
            end do !i_q2_point


          end do !i_kq_pair

         call bin2format('CoulombVertex.dat', 'CV_formatted.dat', &
           &MPI_COMM_WORLD, &
           &max_singval*(CC4S_nuf_states**2)*CC4S_n_spin*CC4S_n_k_points*CC4S_n_k_points)

        end subroutine CC4S_coulvertex_construction
!*************************************************************************
        subroutine CC4S_coulvertex_construction_OAF_real(aomo_lvl_r, &
                        &scf_eigvecs_real, sqrt_coulmat_real, CoulombVertex_fh, &
                        &singval_thr, singval_num)
          implicit none

          !input
          double precision, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_r
          double precision, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_real
          double precision, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_real
          integer, intent(in) :: CoulombVertex_fh
          double precision, optional :: singval_thr
          integer, optional :: singval_num         

          !local
          double precision, dimension(:,:,:,:), target, allocatable :: momo_lvl_r
          integer :: curr_pair !current k-q-pair
          integer :: max_singval !(reduced) OAF-dimension for current transfer momentum k-q
          integer :: i_kq_pair, i_q_point, i_k_point, i_spin
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_k => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_q => null()
          double precision, dimension(:,:), pointer :: sqrt_cm_rptr => null()
          double precision, dimension(:,:), pointer :: KS_rptr_k => null()
          double precision, dimension(:,:), pointer :: KS_rptr_q => null()
          double precision, dimension(:,:,:,:), pointer :: coulomb_vertex_rptr => null()
          double precision, dimension(:,:,:), target, allocatable :: vertex_reshaped_r
          double precision, dimension(:,:,:), pointer :: vertex_reshaped_rptr => null()
          integer :: lredbb_row
          integer :: OAF_dim
          integer :: i_q2_point, i_k2_point, i_spin2
          integer, dimension(3) :: OAF_sizes, OAF_subsizes, OAF_starts
          double precision, dimension(:,:,:), allocatable :: vertex_rOAF

          allocate(momo_lvl_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                  &CC4S_nuf_states, CC4S_n_spin))
          momo_lvl_r(:,:,:,:) = 0.0D0

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p
              write(*,*) "(From CC4S_coulvertex_construction) RANK ", CC4S_myid, &
                      &" : Construction vertex for k-/q-/k-q-point", &
                      &i_q_point, i_k_point, i_kq_pair

              momo_lvl_r(:,:,:,:) = 0.0D0
              aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k_point)
              aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q_point)
              sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
 

              do i_spin=1,CC4S_n_spin
                KS_rptr_k => scf_eigvecs_real(:,:,i_spin,i_k_point)
                KS_rptr_q => scf_eigvecs_real(:,:,i_spin,i_q_point)

                call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                            &aomo_lvl_rptr_q, &
                                            &KS_rptr_k, &
                                            &KS_rptr_q, &
                                            &sqrt_cm_rptr, &
                                            &i_spin,  i_k_point, i_q_point, &
                                            &momo_lvl_r)
              end do !i_spin

              !momo_size=CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
              !momo_sizes=[CC4S_n_basbas, CC4S_nuf_states, CC4S_nuf_states, CC4S_n_spin]
              !momo_subsizes=shape(momo_lvl_r)
              !momo_starts=[CC4S_lbb_row-1, CC4S_lstates_col-1, 0, 0]

              
              coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
              allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_r(:,:,:) = 0.0D0
              vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

              call CC4S_add_up_squared_block(vertex_reshaped_rptr)

              if(allocated(vertex_reshaped_r)) then
                deallocate(vertex_reshaped_r)
              end if

            end do !i_q_point

            !At this point all blocks for a given transfer momentum k-q have been calculated.
            !Hence, here is the point where the OAF-transformation of an entire coulomb-vertex row
            !can be performed

            if(present(singval_num) .and. present(singval_thr)) then
              call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, singval_num, singval_thr)
            else if(present(singval_num)) then
              !If only singval_num has been passed
              call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, num_singvals=singval_num)
            else if(present(singval_thr)) then
              !If only singval_thr has been passed
              call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim, singval_thr=singval_thr)
            else
              !If none have been passed
              call CC4S_compute_OAF_trafo_matrix_real(lredbb_row, OAF_dim)
            end if 


            !max_singval = min(singval_num, CC4S_n_basbas)
            !The following line serves as a safe-guard.
            !At this point it should be guaranteed that OAF_dim <= CC4S_n_basbas
            max_singval = min(OAF_dim, CC4S_n_basbas)

            !After the trafo matrix has been calculated, the sum of squared blocks
            !belonging to the same transfer momentum is no longer needed
            call CC4S_reset_squared_block_sum()

            !Having calculated the (approximately) unitary transformation matrix
            !for the current transfer momentum,
            !it's time to re-compute the Coulomb vertex blocks and apply the transformation matrix

            do i_q2_point=1,CC4S_n_k_points !k_q
              i_k2_point=CC4S_kpq_point_list(i_q2_point, i_kq_pair) !k_p

              momo_lvl_r(:,:,:,:) = 0.0D0
              aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k2_point)
              aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q2_point)
              sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
 

              do i_spin2 = 1,CC4S_n_spin
                KS_rptr_k => scf_eigvecs_real(:,:,i_spin2,i_k2_point)
                KS_rptr_q => scf_eigvecs_real(:,:,i_spin2,i_q2_point)


                call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                            &aomo_lvl_rptr_q, &
                                            &KS_rptr_k, &
                                            &KS_rptr_q, &
                                            &sqrt_cm_rptr, &
                                            &i_spin2,  i_k2_point, i_q2_point, &
                                            &momo_lvl_r)
                
              end do !i_spin2

              coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
              allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_r(:,:,:) = 0.0D0
              vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

              call U_times_Gamma(vertex_reshaped_rptr, max_singval, vertex_rOAF)

              if(allocated(vertex_reshaped_r)) then
                deallocate(vertex_reshaped_r)
              end if

              OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
              OAF_subsizes=shape(vertex_rOAF)
              OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]

              call CC4S_parallel_write(vertex_rOAF, CoulombVertex_fh, OAF_sizes, &
                      &OAF_subsizes, OAF_starts, max_singval, curr_pair)
 
              curr_pair = curr_pair + 1
            end do !i_q2_point

          end do !i_kq_pair
        end subroutine CC4S_coulvertex_construction_OAF_real
!*************************************************************************
        subroutine CC4S_coulvertex_construction_OAF_cmplx(aomo_lvl_c, &
                        &scf_eigvecs_cmplx, sqrt_coulmat_cmplx, CoulombVertex_fh, &
                        &singval_thr, singval_num)
          implicit none

          !input
          double complex, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_c
          double complex, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_cmplx
          double complex, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_cmplx
          integer, intent(in) :: CoulombVertex_fh
          double precision, optional :: singval_thr
          integer, optional :: singval_num         

          !local
          double complex, dimension(:,:,:,:), target, allocatable :: momo_lvl_c
          integer :: curr_pair !current k-q-pair
          integer :: max_singval !(reduced) OAF-dimension for current transfer momentum k-q
          integer :: i_kq_pair, i_q_point, i_k_point, i_spin
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_k => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_q => null()
          double complex, dimension(:,:), pointer :: sqrt_cm_cptr => null()
          double complex, dimension(:,:), pointer :: KS_cptr_k => null()
          double complex, dimension(:,:), pointer :: KS_cptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: coulomb_vertex_cptr => null()
          double complex, dimension(:,:,:), target, allocatable :: vertex_reshaped_c
          double complex, dimension(:,:,:), pointer :: vertex_reshaped_cptr => null()
          integer :: lredbb_row
          integer :: OAF_dim
          integer :: i_q2_point, i_k2_point, i_spin2
          integer, dimension(3) :: OAF_sizes, OAF_subsizes, OAF_starts
          double complex, dimension(:,:,:), allocatable :: vertex_cOAF

          allocate(momo_lvl_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                  &CC4S_nuf_states, CC4S_n_spin))
          momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p
              write(*,*) "(From CC4S_coulvertex_construction) RANK ", CC4S_myid, &
                      &" : Construction vertex for k-/q-/k-q-point", &
                      &i_q_point, i_k_point, i_kq_pair

              momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
              aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k_point)
              aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q_point)
              sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
 

              do i_spin=1,CC4S_n_spin
                KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin,i_k_point)
                KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin,i_q_point)

                call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                            &aomo_lvl_cptr_q, &
                                            &KS_cptr_k, &
                                            &KS_cptr_q, &
                                            &sqrt_cm_cptr, &
                                            &i_spin,  i_k_point, i_q_point, &
                                            &momo_lvl_c)
              end do !i_spin

              !momo_size=CC4S_n_basbas*CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin
              !momo_sizes=[CC4S_n_basbas, CC4S_nuf_states, CC4S_nuf_states, CC4S_n_spin]
              !momo_subsizes=shape(momo_lvl_r)
              !momo_starts=[CC4S_lbb_row-1, CC4S_lstates_col-1, 0, 0]

              
              coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
              allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
              vertex_reshaped_cptr => vertex_reshaped_c(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

              call CC4S_add_up_squared_block(vertex_reshaped_cptr)

              if(allocated(vertex_reshaped_c)) then
                deallocate(vertex_reshaped_c)
              end if

            end do !i_q_point

            !At this point all blocks for a given transfer momentum k-q have been calculated.
            !Hence, here is the point where the OAF-transformation of an entire coulomb-vertex row
            !can be performed

            if(present(singval_num) .and. present(singval_thr)) then
              call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, singval_num, singval_thr)
            else if(present(singval_num)) then
              !If only singval_num has been passed
              call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, num_singvals=singval_num)
            else if(present(singval_thr)) then
              !If only singval_thr has been passed
              call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim, singval_thr=singval_thr)
            else
              !If none have been passed
              call CC4S_compute_OAF_trafo_matrix_cmplx(lredbb_row, OAF_dim)
            end if 


            !max_singval = min(singval_num, CC4S_n_basbas)
            !The following line serves as a safe-guard.
            !At this point it should be guaranteed that OAF_dim <= CC4S_n_basbas
            max_singval = min(OAF_dim, CC4S_n_basbas)

            !After the trafo matrix has been calculated, the sum of squared blocks
            !belonging to the same transfer momentum is no longer needed
            call CC4S_reset_squared_block_sum()

            !Having calculated the (approximately) unitary transformation matrix
            !for the current transfer momentum,
            !it's time to re-compute the Coulomb vertex blocks and apply the transformation matrix

            do i_q2_point=1,CC4S_n_k_points !k_q
              i_k2_point=CC4S_kpq_point_list(i_q2_point, i_kq_pair) !k_p

              momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
              aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k2_point)
              aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q2_point)
              sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
 

              do i_spin2 = 1,CC4S_n_spin
                KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin2,i_k2_point)
                KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin2,i_q2_point)


                call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                            &aomo_lvl_cptr_q, &
                                            &KS_cptr_k, &
                                            &KS_cptr_q, &
                                            &sqrt_cm_cptr, &
                                            &i_spin2,  i_k2_point, i_q2_point, &
                                            &momo_lvl_c)
                
              end do !i_spin2

              coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
              allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
              vertex_reshaped_cptr => vertex_reshaped_c(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

              call U_times_Gamma(vertex_reshaped_cptr, max_singval, vertex_cOAF)

              if(allocated(vertex_reshaped_c)) then
                deallocate(vertex_reshaped_c)
              end if

              OAF_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
              OAF_subsizes=shape(vertex_cOAF)
              OAF_starts=[lredbb_row-1, CC4S_lstates2_col-1, 0]

              call CC4S_parallel_write(vertex_cOAF, CoulombVertex_fh, OAF_sizes, &
                      &OAF_subsizes, OAF_starts, max_singval, curr_pair)
 
              curr_pair = curr_pair + 1
            end do !i_q2_point

          end do !i_kq_pair
        end subroutine CC4S_coulvertex_construction_OAF_cmplx
!*************************************************************************
        subroutine CC4S_coulvertex_construction_noOAF_real(aomo_lvl_r, &
                        &scf_eigvecs_real, sqrt_coulmat_real, CoulombVertex_fh)
          implicit none

          !input
          double precision, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_r
          double precision, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_real
          double precision, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_real
          integer, intent(in) :: CoulombVertex_fh

          !local
          double precision, dimension(:,:,:,:), target, allocatable :: momo_lvl_r
          integer :: curr_pair !current k-q-pair
          integer :: max_singval !(reduced) OAF-dimension for current transfer momentum k-q
          integer :: i_kq_pair, i_q_point, i_k_point, i_spin
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_k => null()
          double precision, dimension(:,:,:,:), pointer :: aomo_lvl_rptr_q => null()
          double precision, dimension(:,:), pointer :: sqrt_cm_rptr => null()
          double precision, dimension(:,:), pointer :: KS_rptr_k => null()
          double precision, dimension(:,:), pointer :: KS_rptr_q => null()
          double precision, dimension(:,:,:,:), pointer :: coulomb_vertex_rptr => null()
          double precision, dimension(:,:,:), target, allocatable :: vertex_reshaped_r
          double precision, dimension(:,:,:), pointer :: vertex_reshaped_rptr => null()
          integer, dimension(3) :: vertex_sizes, vertex_subsizes, vertex_starts

          allocate(momo_lvl_r(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                  &CC4S_nuf_states, CC4S_n_spin))
          momo_lvl_r(:,:,:,:) = 0.0D0

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p
              write(*,*) "(From CC4S_coulvertex_construction) RANK ", CC4S_myid, &
                      &" : Construction vertex for k-/q-/k-q-point", &
                      &i_q_point, i_k_point, i_kq_pair

              momo_lvl_r(:,:,:,:) = 0.0D0
              aomo_lvl_rptr_k => aomo_lvl_r(:,:,:,:,i_k_point)
              aomo_lvl_rptr_q => aomo_lvl_r(:,:,:,:,i_q_point)
              sqrt_cm_rptr => sqrt_coulmat_real(:,:,i_kq_pair)
 

              do i_spin=1,CC4S_n_spin
                KS_rptr_k => scf_eigvecs_real(:,:,i_spin,i_k_point)
                KS_rptr_q => scf_eigvecs_real(:,:,i_spin,i_q_point)

                call CC4S_build_vertex_block(aomo_lvl_rptr_k, &
                                            &aomo_lvl_rptr_q, &
                                            &KS_rptr_k, &
                                            &KS_rptr_q, &
                                            &sqrt_cm_rptr, &
                                            &i_spin,  i_k_point, i_q_point, &
                                            &momo_lvl_r)
              end do !i_spin

              
              coulomb_vertex_rptr => momo_lvl_r(:,:,:,:)
              allocate(vertex_reshaped_r(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_r(:,:,:) = 0.0D0
              vertex_reshaped_rptr => vertex_reshaped_r(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_rptr, vertex_reshaped_rptr)

              max_singval = CC4S_n_basbas

              if(allocated(vertex_reshaped_r)) then
                deallocate(vertex_reshaped_r)
              end if

              vertex_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
              vertex_subsizes=shape(vertex_reshaped_rptr)
              vertex_starts=[CC4S_lbb_row-1, CC4S_lstates2_col-1, 0]

              call CC4S_parallel_write(vertex_reshaped_rptr, CoulombVertex_fh, vertex_sizes, &
                      &vertex_subsizes, vertex_starts, max_singval, curr_pair)

              curr_pair = curr_pair + 1

            end do !i_q_point
          end do !i_kq_pair

        end subroutine CC4S_coulvertex_construction_noOAF_real
!*************************************************************************
        subroutine CC4S_coulvertex_construction_noOAF_cmplx(aomo_lvl_c, &
                        &scf_eigvecs_cmplx, sqrt_coulmat_cmplx, CoulombVertex_fh)
          implicit none

          !input
          double complex, dimension(:,:,:,:,:), target, intent(in) :: aomo_lvl_c
          double complex, dimension(:,:,:,:), target, intent(in) :: scf_eigvecs_cmplx
          double complex, dimension(:,:,:), target, allocatable, intent(in) :: sqrt_coulmat_cmplx
          integer, intent(in) :: CoulombVertex_fh

          !local
          double complex, dimension(:,:,:,:), target, allocatable :: momo_lvl_c
          integer :: curr_pair !current k-q-pair
          integer :: max_singval !(reduced) OAF-dimension for current transfer momentum k-q
          integer :: i_kq_pair, i_q_point, i_k_point, i_spin
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_k => null()
          double complex, dimension(:,:,:,:), pointer :: aomo_lvl_cptr_q => null()
          double complex, dimension(:,:), pointer :: sqrt_cm_cptr => null()
          double complex, dimension(:,:), pointer :: KS_cptr_k => null()
          double complex, dimension(:,:), pointer :: KS_cptr_q => null()
          double complex, dimension(:,:,:,:), pointer :: coulomb_vertex_cptr => null()
          double complex, dimension(:,:,:), target, allocatable :: vertex_reshaped_c
          double complex, dimension(:,:,:), pointer :: vertex_reshaped_cptr => null()
          integer, dimension(3) :: vertex_sizes, vertex_subsizes, vertex_starts

          allocate(momo_lvl_c(CC4S_lbb_row:CC4S_ubb_row, CC4S_lstates_col:CC4S_ustates_col, &
                  &CC4S_nuf_states, CC4S_n_spin))
          momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)

          curr_pair=0

          max_singval = 0 !For tasks with CC4S_member == .false.

          !Make i_kq_pair (k-q) outer loop to facilitate OAF-transformation of all
          !blocks with equal k-q
          do i_kq_pair=1,CC4S_n_kq_points !0 for CC4S_member=.false. !k_F = k_p-k_q
            do i_q_point=1,CC4S_n_k_points !k_q
              i_k_point=CC4S_kpq_point_list(i_q_point, i_kq_pair) !k_p
              write(*,*) "(From CC4S_coulvertex_construction) RANK ", CC4S_myid, &
                      &" : Construction vertex for k-/q-/k-q-point", &
                      &i_q_point, i_k_point, i_kq_pair

              momo_lvl_c(:,:,:,:) = (0.0D0, 0.0D0)
              aomo_lvl_cptr_k => aomo_lvl_c(:,:,:,:,i_k_point)
              aomo_lvl_cptr_q => aomo_lvl_c(:,:,:,:,i_q_point)
              sqrt_cm_cptr => sqrt_coulmat_cmplx(:,:,i_kq_pair)
 

              do i_spin=1,CC4S_n_spin
                KS_cptr_k => scf_eigvecs_cmplx(:,:,i_spin,i_k_point)
                KS_cptr_q => scf_eigvecs_cmplx(:,:,i_spin,i_q_point)

                call CC4S_build_vertex_block(aomo_lvl_cptr_k, &
                                            &aomo_lvl_cptr_q, &
                                            &KS_cptr_k, &
                                            &KS_cptr_q, &
                                            &sqrt_cm_cptr, &
                                            &i_spin,  i_k_point, i_q_point, &
                                            &momo_lvl_c)
              end do !i_spin

              
              coulomb_vertex_cptr => momo_lvl_c(:,:,:,:)
              allocate(vertex_reshaped_c(CC4S_lbb_row:CC4S_ubb_row, &
                      &CC4S_lstates2_col:CC4S_ustates2_col, CC4S_n_spin))
              vertex_reshaped_c(:,:,:) = (0.0D0, 0.0D0)
              vertex_reshaped_cptr => vertex_reshaped_c(:,:,:)

              call CC4S_reshape_vertex_2D(coulomb_vertex_cptr, vertex_reshaped_cptr)

              max_singval = CC4S_n_basbas

              if(allocated(vertex_reshaped_c)) then
                deallocate(vertex_reshaped_c)
              end if

              vertex_sizes=[max_singval, CC4S_nuf_states**2, CC4S_n_spin] !N_G x N_n x N_n x 1
              vertex_subsizes=shape(vertex_reshaped_cptr)
              vertex_starts=[CC4S_lbb_row-1, CC4S_lstates2_col-1, 0]

              call CC4S_parallel_write(vertex_reshaped_cptr, CoulombVertex_fh, vertex_sizes, &
                      &vertex_subsizes, vertex_starts, max_singval, curr_pair)

              curr_pair = curr_pair + 1

            end do !i_q_point
          end do !i_kq_pair

        end subroutine CC4S_coulvertex_construction_noOAF_cmplx

end module CC4S_coul_vertex
