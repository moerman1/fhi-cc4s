subroutine CC4S_output_spins(n_states, n_k_points, n_spin, file_name, bin)
      implicit none

      !PURPOSE: Print out the Sz-eigenvalues for the calculated
      !scf-states. For restricted calculations (n_spin=1) the final file
      !will contain only zeroes. 

      !INPUT:
      !       *n_states: Number of states per k_point, i.e maximal band
      !                  index.
      !
      !       *n_k_points: Total number of k-points (no exploitation of
      !       irreducible BZ/symmetry possible at the moment)
      !
      !       *n_spin: number of spin channels. 
      !                n_spin=1 => restricted calculation
      !                n_spin=2 => unrestricted calculation

      !input variables
      integer, intent(in) :: n_states
      integer, intent(in) :: n_k_points
      integer, intent(in) :: n_spin
      character(len=*), intent(in) :: file_name
      logical, intent(in) :: bin

      !local variables
      integer :: i_state, i_k_point, i_spin

      if(.not. bin) then
        open(unit=335, file=file_name, action='write')
        if(n_spin .eq. 1) then
          do i_k_point=1,n_k_points
            do i_state=1,n_states
              write(335,'(F20.16)') 0.0
            end do
          end do
        else
          do i_k_point=1,n_k_points
            do i_spin=1,n_spin
              do i_state=1,n_states
                write(335,'(F20.16)') -dble(i_spin)+1.5D0
              end do
            end do
          end do

          !write(*,*) "At this point the format of the spin file for &
          !        &unrestricted calculations is yet to be determined"
        end if 
      else
        open(unit=335, file=file_name, action='write', &
                &form='unformatted', access='sequential')
        if(n_spin .eq. 1) then
          do i_k_point=1,n_k_points
            do i_state=1,n_states
              write(335) 0.0
            end do
          end do
        else
          do i_k_point=1,n_k_points
            do i_spin=1,n_spin
              do i_state=1,n_states
                write(335) -dble(i_spin)+1.5D0
              end do
            end do
          end do
          !write(*,*) "At this point the format of the spin file for &
          !        &unrestricted calculations is yet to be determined"
        end if 
      end if 

      close(335)


end subroutine CC4S_output_spins
