module CC4S_out_fock

contains
subroutine output_fockmatrix_real(scf_eigvals, bin, ishf, fockmatrix_real)
      use calculation_data, only: CC4S_k_eigvec_loc, CC4S_n_states, CC4S_nuf_states, &
              &CC4S_n_low_state, CC4S_n_spin, CC4S_n_k_points
      use CC4S_bin2format
      use outputfile_names, only: scf_fockmatrix_filename_dat, scf_fockmatrix_filename_bin
      use mpi
      implicit none

      !PURPOSE: CC4S requires the MO-transformed Fock-matrix/Fock-hamiltonian. irrespective
      !of the SCF-method being Hartree-Fock (diagonal Fock-matrix) or a KS-functional
      !(dense Fock-matrix).

      !Similarly to the case of the scf-eigenvectors, the Fockmatrix of a given k-point
      !is to be found according to the mapping array CC4S_k_eigvec_loc

      double precision, dimension(:,:,:,:), optional, intent(in) :: fockmatrix_real
      double precision, dimension(CC4S_n_states, CC4S_n_spin, CC4S_n_k_points) :: scf_eigvals
      !character(len=*), intent(in) :: filename
      logical, intent(in) :: bin
      logical, intent(in) :: ishf

      !local
      integer :: i_k_point, i_state, i_spin
      double precision, dimension(:,:), allocatable :: sparse_HF_fockmat
      integer :: myid, ierr
      integer :: fock_fh
      integer(kind=MPI_OFFSET_KIND) :: offset
      !integer(kind=MPI_ADDRESS_KIND) :: nbytes
      integer :: nbytes

      !for idle MPI-IO
      integer, dimension(:), allocatable :: zero_fock_arr

      call MPI_Comm_rank(MPI_COMM_WORLD, myid, ierr)

      if(.not. bin) then
        if(ishf) then
          if(myid .eq. 0) then
            open(unit=339, file=scf_fockmatrix_filename_dat, status='replace', action='write')
            allocate(sparse_HF_fockmat(CC4S_n_low_state:CC4S_n_states, &
                    &CC4S_n_low_state:CC4S_n_states))
            sparse_HF_fockmat(:,:) = 0.0D0
            do i_k_point=1, CC4S_n_k_points
              do i_spin=1,CC4S_n_spin
                do i_state=CC4S_n_low_state, CC4S_n_states
                  sparse_HF_fockmat(i_state,i_state) = scf_eigvals(i_state, i_spin, i_k_point)
                end do
                write(339,'(F20.16)') sparse_HF_fockmat(:,:)
              end do
            end do
            deallocate(sparse_HF_fockmat)
            close(335)
          end if 
        end if 
      end if 

      if(bin .or. (.not. ishf)) then!bin=.true.; if KS was used, only bin is possible
        if(ishf) then
          if(myid .eq. 0) then
            open(unit=339, file=scf_fockmatrix_filename_bin, status='replace', action='write', &
                    &form='unformatted', access='sequential')
            allocate(sparse_HF_fockmat(CC4S_n_low_state:CC4S_n_states, &
                    &CC4S_n_low_state:CC4S_n_states))
            sparse_HF_fockmat(:,:) = 0.0D0
            do i_k_point=1, CC4S_n_k_points
              do i_spin=1,CC4S_n_spin
                do i_state=CC4S_n_low_state, CC4S_n_states
                  sparse_HF_fockmat(i_state,i_state) = scf_eigvals(i_state, i_spin, i_k_point)
                end do
                write(339) sparse_HF_fockmat(:,:)
              end do
            end do
            deallocate(sparse_HF_fockmat)
          end if 
        else
          call MPI_File_open(MPI_COMM_WORLD, scf_fockmatrix_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
                  &MPI_INFO_NULL, fock_fh, ierr)
          do i_k_point=1,CC4S_n_k_points
            if(myid .eq. (CC4S_k_eigvec_loc(1, i_k_point))) then
              
              offset=8*CC4S_nuf_states*CC4S_nuf_states*(i_k_point-1)*CC4S_n_spin
              call MPI_File_set_view(fock_fh, offset, MPI_DOUBLE_PRECISION, MPI_DOUBLE_PRECISION, &
                      &'native', MPI_INFO_NULL, ierr)
              

              nbytes=size(fockmatrix_real(CC4S_n_low_state:CC4S_n_states, &
                      &CC4S_n_low_state:CC4S_n_states,:, CC4S_k_eigvec_loc(2, i_k_point))) 

              call MPI_File_write(fock_fh, fockmatrix_real(CC4S_n_low_state:CC4S_n_states,&
                      &CC4S_n_low_state:CC4S_n_states,:, CC4S_k_eigvec_loc(2, i_k_point)),&
                      &nbytes, MPI_DOUBLE_PRECISION, &
                      &MPI_STATUS_IGNORE, ierr)

              
            end if
          end do

          if( .not. (any(CC4S_k_eigvec_loc(1,:)==myid)) ) then

            !It can happen that some task owns none of the fock-matrices of any k-point.
            !(e.g if number of tasks > number of k-points)
            !These idle tasks shall write nothing to the FockOperator.dat file
            offset=0
            call MPI_File_set_view(fock_fh, offset, MPI_DOUBLE_PRECISION, MPI_DOUBLE_PRECISION, &
                      &'native', MPI_INFO_NULL, ierr)


            nbytes=0
            allocate(zero_fock_arr(0))
            call MPI_File_write(fock_fh, zero_fock_arr,&
                   &nbytes, MPI_DATATYPE_NULL, &
                   &MPI_STATUS_IGNORE, ierr)

            !write(*,*) 'Rank', myid, 'idling during fockmatrix-IO'
          end if
          !if bin=.false is requested, read in binary file, and write it to a 2nd, formatted file
          if(.not. bin) then
            call bin2format_real(scf_fockmatrix_filename_bin, scf_fockmatrix_filename_dat, MPI_COMM_WORLD, &
              &CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin*CC4S_n_k_points)
          end if
          call MPI_File_close(fock_fh, ierr)
        end if 

      end if 


end subroutine output_fockmatrix_real
!*************************************************
subroutine output_fockmatrix_cmplx(scf_eigvals, bin, ishf, fockmatrix_cmplx)
      use calculation_data, only: CC4S_k_eigvec_loc, CC4S_n_states, CC4S_nuf_states, &
              &CC4S_n_low_state, CC4S_n_spin, CC4S_n_k_points
      use CC4S_bin2format
      use outputfile_names, only: scf_fockmatrix_filename_dat, scf_fockmatrix_filename_bin
      use mpi
      implicit none

      !PURPOSE: CC4S requires the MO-transformed Fock-matrix/Fock-hamiltonian. irrespective
      !of the SCF-method being Hartree-Fock (diagonal Fock-matrix) or a KS-functional
      !(dense Fock-matrix).

      !Similarly to the case of the scf-eigenvectors, the Fockmatrix of a given k-point
      !is to be found according to the mapping array CC4S_k_eigvec_loc

      double complex, dimension(:,:,:,:), optional, intent(in) :: fockmatrix_cmplx
      double precision, dimension(CC4S_n_states, CC4S_n_spin, CC4S_n_k_points) :: scf_eigvals
      !character(len=*), intent(in) :: filename
      logical, intent(in) :: bin
      logical, intent(in) :: ishf

      !local
      integer :: i_k_point, i_state, i_spin
      double precision, dimension(:,:), allocatable :: sparse_HF_fockmat
      integer :: myid, ierr
      integer :: fock_fh
      integer(kind=MPI_OFFSET_KIND) :: offset
      !integer(kind=MPI_ADDRESS_KIND) :: nbytes
      integer :: nbytes

      !for idle MPI-IO
      integer, dimension(:), allocatable :: zero_fock_arr


      call MPI_Comm_rank(MPI_COMM_WORLD, myid, ierr)

      if(.not. bin) then
        if(ishf) then
          if(myid .eq. 0) then
            open(unit=339, file=scf_fockmatrix_filename_dat, status='replace', action='write')
            !allocate(sparse_HF_fockmat(CC4S_n_low_state:CC4S_nuf_states, &
            !        &CC4S_n_low_state:CC4S_nuf_states))
            allocate(sparse_HF_fockmat(CC4S_n_low_state:CC4S_n_states, &
                    &CC4S_n_low_state:CC4S_n_states))
            sparse_HF_fockmat(:,:) = 0.0D0
            do i_k_point=1, CC4S_n_k_points
              do i_spin=1,CC4S_n_spin
                do i_state=CC4S_n_low_state, CC4S_n_states
                  sparse_HF_fockmat(i_state,i_state) = scf_eigvals(i_state, i_spin, i_k_point)
                end do
                write(339,'(F20.16)') sparse_HF_fockmat(:,:)
              end do
            end do
            deallocate(sparse_HF_fockmat)
            close(335)
          end if 
        end if 
      end if 

      if(bin .or. (.not. ishf)) then!bin=.true.; if KS was used, only bin is possible
        if(ishf) then
          if(myid .eq. 0) then
            open(unit=339, file=scf_fockmatrix_filename_bin, status='replace', action='write', &
                    &form='unformatted', access='sequential')
            allocate(sparse_HF_fockmat(CC4S_n_low_state:CC4S_n_states, &
                    &CC4S_n_low_state:CC4S_n_states))
            sparse_HF_fockmat(:,:) = 0.0D0
            do i_k_point=1, CC4S_n_k_points
              do i_spin=1,CC4S_n_spin
                do i_state=CC4S_n_low_state, CC4S_n_states
                  sparse_HF_fockmat(i_state,i_state) = scf_eigvals(i_state, i_spin, i_k_point)
                end do
                write(339) sparse_HF_fockmat(:,:)
              end do
            end do
            deallocate(sparse_HF_fockmat)
          end if 
        else
          
          call MPI_File_open(MPI_COMM_WORLD, scf_fockmatrix_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
                  &MPI_INFO_NULL, fock_fh, ierr)
          
          do i_k_point=1,CC4S_n_k_points
            if(myid .eq. (CC4S_k_eigvec_loc(1, i_k_point))) then

              offset=16*CC4S_nuf_states*CC4S_nuf_states*(i_k_point-1)*CC4S_n_spin
              call MPI_File_set_view(fock_fh, offset, MPI_DOUBLE_COMPLEX, MPI_DOUBLE_COMPLEX, &
                      &'native', MPI_INFO_NULL, ierr)

              nbytes=size(fockmatrix_cmplx(CC4S_n_low_state:CC4S_n_states, &
                      &CC4S_n_low_state:CC4S_n_states,:,CC4S_k_eigvec_loc(2, i_k_point))) 

              call MPI_File_write(fock_fh, fockmatrix_cmplx(CC4S_n_low_state:CC4S_n_states, &
                      &CC4S_n_low_state:CC4S_n_states,:, CC4S_k_eigvec_loc(2, i_k_point)), &
                      &nbytes, MPI_DOUBLE_COMPLEX, &
                      &MPI_STATUS_IGNORE, ierr)
            end if 
          end do

          if( .not. (any(CC4S_k_eigvec_loc(1,:)==myid)) ) then

            !It can happen that some task owns none of the fock-matrices of any k-point.
            !(e.g if number of tasks > number of k-points)
            !These idle tasks shall write nothing to the FockOperator.dat file
            offset=0
            call MPI_File_set_view(fock_fh, offset, MPI_DOUBLE_COMPLEX, MPI_DOUBLE_COMPLEX, &
                      &'native', MPI_INFO_NULL, ierr)


            nbytes=0
            allocate(zero_fock_arr(0))
            call MPI_File_write(fock_fh, zero_fock_arr,&
                   &nbytes, MPI_DATATYPE_NULL, &
                   &MPI_STATUS_IGNORE, ierr)

            !write(*,*) 'Rank', myid, 'idling during fockmatrix-IO'
          end if

          !if bin=.false is requested, read in binary file, and write it to a 2nd, formatted file
          if(.not. bin) then
            call bin2format(scf_fockmatrix_filename_bin, scf_fockmatrix_filename_dat, MPI_COMM_WORLD, &
              &CC4S_nuf_states*CC4S_nuf_states*CC4S_n_spin*CC4S_n_k_points)
          end if
          call MPI_File_close(fock_fh, ierr)
        end if 

      end if 


end subroutine output_fockmatrix_cmplx
end module CC4S_out_fock
