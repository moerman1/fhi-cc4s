module CC4S_main_interface
  use mpi


  double precision, dimension(:,:,:,:), allocatable, target, private :: KS_shm_rempty
  double complex, dimension(:,:,:,:), allocatable, target, private :: KS_shm_cempty
contains
  subroutine CC4S_interface_driver(coulmat, lvl_trico, v_trico, scf_eigvecs_cmplx, &
                  &scf_eigvecs_real, scf_eigvals, comm, debug, singval_thr, singval_num, &
                  &scf_fock_real, scf_fock_cmplx, &
                  &ri_pw_trafo_matrix)
    use CC4S_blacs_environment
    use calculation_data
    use CC4S_shm_KS, only: CC4S_setup_inter_comm, CC4S_share_eigenvec, cleanup_shm_KS
    use CC4S_LVL, only: CC4S_setup_LVL, CC4S_mo_trafo_LVL, CC4S_shm_LVL, cleanup_shm_LVL 
    use CC4S_sqrt_coulmat, only: CC4S_sqrt_lapack, CC4S_sqrt_scalapack
    use CC4S_square_sqrt_coulmat, only: CC4S_square_sqrt_lapack, CC4S_square_sqrt_scalapack
    use CC4S_coul_vertex, only: CC4S_coulvertex_construction
    use CC4S_out_fock, only: output_fockmatrix_real, output_fockmatrix_cmplx
    use CC4S_parallel_io, only: CC4S_debug_write_5d_real, CC4S_debug_write_3d_real, CC4S_debug_write_3d_cmplx, &
                               &CC4S_parallel_io_coulmat
    use outputfile_names
    use CC4S_yaml_output, only: output_yaml_files
    !For compatibility with RI-V
    use RIV, only: convert_RIV_distributed_aotomo_real, convert_RIV_distributed_aotomo_cmplx
    !To convert relevant datafiles' suffix to ".elements"
    use CC4S_adapt_data_file_suffix, only: adapt_data_file_suffix
    use CC4S_bin2format
    use finite_size_correction, only: transform_coulmat_to_pws, contract_V_inv_sqrt_with_nu_G, &
                                      &open_V_inv_sqrt_mu_G_filehandle, contract_V_inv_sqrt_with_nu_G_OAF, &
                                      &close_V_inv_sqrt_mu_G_filehandle, invert_basis_transformation_matrix, &
                                      &inverted_basis_transform_at_k, write_current_pw_transform_block_to_file, &
                                      &open_pw_transform_filehandle, close_pw_transform_filehandle
    implicit none

    !PURPOSE: This subroutine is supposed to gather all the
    !functionality of this interface in order to calculate/output all
    !quantitites necessary for CC4S.
      
    !This subroutine assumes that at the point of invokation the following
    !quantitites have already been calculated, presumably within a HF-SCF
    !calculation:

    !SCF-eigenvalues: For every k-point of the k-mesh the respective 
    !energy eigenvalues should have been calculated

    !SCF-states/eigenvectors: The number of these would maximally 
    !correspond to the number of AO basis functions. Due to near-linear
    !depencies and the frozen-core approximation, the number of states
    !is usually smaller.

    !RI-LVL coefficients: As the CC4S code is primarily designed for 
    !larger, periodic calculations, this interface assumes the 
    !utilisation of the localiced RI-V scheme (i.e RI-LVL), which
    !allows a linear growth of aux. basis functions with system size.
    !In this scheme, the expansion of an AO-basis-product only includes
    !the aux. basis functions which are located on one of the two atoms
    !involved. Therefore, the dimensionality of the LVL-array, this 
    !interface expects is:
    !(max_n_basis_sp x max_n_basis_sp x max_n_basbas_sp x n_k_points x n_atompairs),
    !with:
    !max_n_basis_sp: (global) Max. number of AO-basis functions per atom/element
    !max_n_basbas_sp: (global) Max. number of aux. (RI-) basis functions per atom/element
    !n_k_points: (global) Number of k-points
    !n_atompairs (local) Number of atompairs assigned to the given task. Starting 
    !from the square process grid, required for this interface, this dimension will
    !be distributed across the process rows/vertically. 
    !
    !OR
    !
    !RI-V coefficients (experimental): 
    !This input array is added to increase the range of codes, which can use CCaims
    !This array will be used instead of the RI-LVL array (lvl_trico) if
    !lvl_trico is of dimension (1,1,1,1,1)
    !If the RI-V array (v_trico) is of shape (1,1,1,1,1) as well, the interface will 
    !return control to the calling function.
    !If both arrays, RI-V and RI-LVL, passed here are of non-trivial shape,
    !the RI-LVL will be used.
    !If used, the RI-V array should be of shape
    !(n_basis x n_basis x n_basbas x n_k_points x n_k_points)
    !with:
    !n_basis: number of AO basis functions
    !n_basbas: number of auxiliary/RI basis functions
    !n_k_points: number of k-points
    !As one can see, one disadvantage of the RI-V scheme in recip. space
    !is the double k-dependence

    !Furthermore, this routine assumes that the module variables of 
    !- CC4S_blacs_environment and
    !- calculation data
    !have been initialized already

    !input     
    double complex, dimension(:,:,:), allocatable, target, intent(inout) :: coulmat
    double complex, dimension(:,:,:,:,:), allocatable, intent(inout) :: lvl_trico
    double complex, dimension(:,:,:,:,:), allocatable, intent(inout) :: v_trico
    double complex, dimension(:,:,:,:), intent(in) :: scf_eigvecs_cmplx
    double precision, dimension(:,:,:,:), intent(in) :: scf_eigvecs_real
    double precision, dimension(:,:,:,:), optional, intent(in) :: scf_fock_real
    double complex, dimension(:,:,:,:), optional, intent(in) ::scf_fock_cmplx
    double precision, dimension(:,:,:), intent(in)  :: scf_eigvals
    integer, intent(in) :: comm !Communicator 
    double precision, optional, intent(in) :: singval_thr
    integer, optional, intent(in) :: singval_num
    logical, intent(in) :: debug
    double complex, dimension(:,:,:), optional, intent(in) :: ri_pw_trafo_matrix

    !local
    integer :: comm_shared, comm_inter
    integer :: n_tasks_shared, n_tasks_inter
    integer :: myid_shared, myid_inter
    integer :: myid_world, ierr
    !For real case
    double precision, dimension(:,:,:,:), pointer, contiguous :: KS_shm_real => null()
    !For complex case
    double complex,  dimension(:,:,:,:), pointer, contiguous :: KS_shm_cmplx => null()

    logical :: is_lvl_root
    integer :: comm_lvl_root, myid_lvlroot
    integer :: n_tasks_lvlroot, mpi_comm_shared_row, n_tasks_shrow, myid_shrow
    double complex, dimension(:,:,:,:,:), allocatable :: aomo_lvl
    double precision, dimension(:,:,:,:,:), allocatable :: aomo_lvl_real
    double precision, dimension(:,:,:,:,:), pointer, contiguous :: aomo_lvl_rshm => null()
    !To avoid memory leak
    double precision, dimension(:,:,:,:,:), target, allocatable :: aomo_lvl_rshm_empty
    double complex, dimension(:,:,:,:,:), pointer, contiguous :: aomo_lvl_cshm => null()
    double complex, dimension(:,:,:,:,:), target, allocatable :: aomo_lvl_cshm_empty

    double precision, dimension(:,:,:), allocatable :: coulmat_real
    integer, dimension(4) :: loc_shape_coulmat
    integer :: i_k_point

    integer :: CoulombVertex_fh, LeftCoulombVertex_fh

    !for debugging only, remove afterwards
    integer, dimension(5) :: aomo_sizes, aomo_subsizes, aomo_starts
    !integer, dimension(3) :: coulmat_sizes, coulmat_subsizes, coulmat_starts

    !counter for debugging
    integer :: i
    double complex, dimension(:), allocatable :: flat_aomolvl, flat_trico
    double precision, dimension(:), allocatable :: flat_KS, flat_KS_shm

    !flag for RI-scheme
    character :: RIscheme

    !List of k,q,k-q in the order as they are written to
    !CoulombVertex.dat
    !Necessary for CoulombVertex.yaml
    integer, dimension(:,:), allocatable :: momentum_triple_list, left_momentum_triple_list
    integer, dimension(:), allocatable :: sparse_OAF_dim

    !RI-V specific
    double complex, dimension(:,:,:,:,:,:), allocatable :: aomo_v !analogue of aomo_lvl
    double precision, dimension(:,:,:,:,:,:), allocatable :: aomo_v_real !analogue of aomo_lvl_real

    !flag if hf-calculation has been performed or not
    logical :: is_calc_hf
    logical :: print_binary

    !flag if quantities for finite-size correction are requested
    !logical :: prepare_finite_size_correction

    !double precision, allocatable, dimension(:,:) :: diagonal_coulmat_G
    integer, allocatable, dimension(:) :: loc_shape_ri_pw_trafo_matrix
    integer, allocatable, dimension(:) :: loc_shape_inverted_ri_pw_trafo_matrix
    integer, allocatable, dimension(:) :: loc_shape_V_G_Q_matrix
    double complex, dimension(:,:), pointer :: cm_ptr => null()
    double complex, allocatable, dimension(:,:), target :: coulmat_complex_single_k
    double complex, allocatable, dimension(:,:,:) :: coulmat_copy

    integer :: coulmat_fh
    integer :: i_kq_point
    integer, dimension(2) :: coulmat_sizes, coulmat_subsizes, coulmat_starts

    !debug: 
    integer :: i_k_debug, i_ap_debug, i_basis_debug, i_basis_debug_2, i_basbas_debug, i_state_debug
    character(len=20) :: debug_filename, filename_debug

    call MPI_Comm_rank(MPI_COMM_WORLD, myid_world, ierr)
    if (myid_world == 0) then
      call print_logo
    end if

!    if(present(ri_pw_trafo_matrix) .and. CC4S_num_pws > 0) then
    if(CC4S_num_pws > 0) then
      !Passing a basis transformation from the RI-functions to plane waves
      !is interpreted as a request to prepare everything for a finite-size
      !correction estimation in cc4s.
      !In that case 2 additional quantities will be calculated:

      !1) The representation of the aux. Coulomb matrix in PWs 
      !   V_{\mu.\nu} -> V_{G,G'}
      !   and subsequent approximation as diagonal
      !   V_{G,G'} -> V_G (need to see how valid this is)
      !2) A transformation matrix which can convert the RI-Coulomb vertex
      !   \Gamma_{p}^{q\mu} directly into the co-densities represented in PWs.
      !   In the general case of a compressed OAF vertex, this transformation matrix
      !   can be evaluated as:
      !   U_{F}^{\mu} * ( V^{1/2} )_{\mu\nu} <\nu|G>
      !   , where U is the OAF-trafo matrix, V is the aux. Coulomb matrix (RI-representation)
      !   and <\nu|G> is the RI->PW trafo matrix
      prepare_finite_size_correction = .true.
      if(myid_world .eq. 0) then
        write(*,*) "RI->PW transformation matrix has been passed"
        write(*,*) "Preparing calculation of quantities for finite-size correction"
      end if
    elseif(present(ri_pw_trafo_matrix) .or. CC4S_num_pws > 0) then
      if(myid_world .eq. 0) then
        write(*,*) "Either RI->PW transformation has not been passed,"
        write(*,*) "or number of PWs has not been specified. Both are needed."
        write(*,*) "It seems you forgot to provide the necessary data. Leaving ..."
      end if
     
      return
    else
      prepare_finite_size_correction = .false.
    end if

    loc_shape_coulmat=[CC4S_lbb_row, CC4S_ubb_row, CC4S_lbb_col, CC4S_ubb_col]

!    if(prepare_finite_size_correction .and. CC4S_member) then
!      !For the pair-energy approach we will need the RI-Coulomb matrix for every k-q point.
!      !Hence, it will be written to file here
!      
!      call MPI_File_open(CC4S_comm, coulmat_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
!            &MPI_INFO_NULL, coulmat_fh, ierr)
! 
!      do i_kq_point=1,CC4S_n_kq_points
!        coulmat_sizes = [CC4S_n_basbas, CC4S_n_basbas]
!        coulmat_subsizes=shape(coulmat(:,:,i_kq_point))
!        coulmat_starts=[CC4S_lbb_row-1, CC4S_lbb_col-1] 
!        call CC4S_parallel_io_coulmat(coulmat(:,:,i_kq_point), coulmat_fh, coulmat_sizes, &
!                                     &coulmat_subsizes, coulmat_starts, i_kq_point-1)  
!      end do
!
!      call MPI_File_close(coulmat_fh, ierr)
!
!      !In the new (release-version) of CC4S, data files carry the suffix ".elements"
!      !instead of ".dat" or ".bin".
!      !The following function will select the chosen data file 
!      !(the .bin- or .dat-file depending on debug-setting)
!      !and change the respective suffix to ".elements"
!      call adapt_data_file_suffix(debug, coulmat_filename_dat, &
!                              &coulmat_filename_bin, MPI_COMM_WORLD)
!
!      !call bin2format(coulmat_filename_bin, coulmat_filename_dat, CC4S_comm, &
!      !               &CC4S_n_basbas*CC4S_n_basbas*CC4S_n_kq_points)
!
!
!    end if


!      if(CC4S_member) then
!        coulmat_sizes = [CC4S_n_basbas, CC4S_n_basbas, CC4S_n_kq_points]
!        coulmat_subsizes = shape(coulmat)
!        coulmat_starts = [CC4S_lbb_row-1, CC4S_lbb_col-1,0]
!        call CC4S_debug_write_3d_real(CC4S_comm, dble(coulmat(:,:,:)), CC4S_n_basbas*CC4S_n_basbas*CC4S_n_kq_points, &
!                                      &coulmat_sizes, coulmat_subsizes, coulmat_starts, "coulmat_initial")
!      end if

    !Perform calculation of PW-transformed aux. Coulomb matrix right here, because:
    !1) Everything necessary is already in place (communicators, distribution of matrices, etc.)
    !2) In contrast to all later uses of the aux. Coulomb matrix, the complex-valued
    !   version is actually useful even in the case of CC4S_isreal, because
    !   RI->PW-trafo matrix is always complex
    !if(prepare_finite_size_correction .and. .not. (present(singval_num) .or. present(singval_thr))) then
!    if(prepare_finite_size_correction) then
!      !If no OAF-trafo has been requested perform everything finite-size correction related here
!      allocate(loc_shape_ri_pw_trafo_matrix(4))
!      allocate(loc_shape_inverted_ri_pw_trafo_matrix(4))
!      allocate(loc_shape_V_G_Q_matrix(4))
!      !allocate(diagonal_coulmat_G(CC4S_num_pws, CC4S_n_kq_points))
!      !diagonal_coulmat_G(:,:) = 0.0d0
!
!      !loc_shape_ri_pw_trafo_matrix=[CC4S_lbb_row, CC4S_ubb_row, CC4S_lpw_col, CC4S_upw_col]
!      loc_shape_ri_pw_trafo_matrix=[CC4S_lpw_row, CC4S_upw_row, CC4S_lbb_col, CC4S_ubb_col]
!      loc_shape_inverted_ri_pw_trafo_matrix=[CC4S_lbb_row, CC4S_ubb_row, CC4S_lpw_col, CC4S_upw_col]
!      loc_shape_V_G_Q_matrix = [CC4S_lpw_row, CC4S_upw_row, CC4S_lpw_col, CC4S_upw_col]
!      !write(*,'(A,I0,A,A,I0,A,A,I0, 4(1X,A,I0))') "(proc-row: ",CC4S_myid_row,",","proc-col: ",CC4S_myid_col,") "," Rank: ",myid_world,&
!      !          &"CC4S_lpw_row =",CC4S_lpw_row, &
!      !          &"CC4S_upw_row =",CC4S_upw_row,&
!      !          &"CC4S_lpw_col =",CC4S_lpw_col, &
!      !          &"CC4S_upw_col =",CC4S_upw_col
!
!      !call open_V_inv_sqrt_mu_G_filehandle()
!      call open_pw_transform_filehandle() 
!
!
!
!      do i_k_point=1,CC4S_n_kq_points
!
!        !We start with <G|mu> for some k-point i_k_point. First calculate the inverse of this basis tranform
!        call invert_basis_transformation_matrix(ri_pw_trafo_matrix(:,:,i_k_point), CC4S_pwsxbb_desc, CC4S_bbxpws_desc, &
!                         &CC4S_num_pws, CC4S_n_basbas, loc_shape_ri_pw_trafo_matrix, loc_shape_inverted_ri_pw_trafo_matrix, &
!                         &CC4S_GxGprime_desc, CC4S_bb_desc, &
!                         &CC4S_comm)
!
!        !call transform_coulmat_to_pws(coulmat(:,:,i_k_point), loc_shape_coulmat, CC4S_bb_desc, &
!        !                             &ri_pw_trafo_matrix(:,:,i_k_point), loc_shape_ri_pw_trafo_matrix, CC4S_bbxpws_desc, &
!        !                             &CC4S_n_basbas, CC4S_num_pws, &
!        !                             &loc_shape_V_G_Q_matrix, CC4S_GxGprime_desc, CC4S_comm, i_k_point)
! 
!        call transform_coulmat_to_pws(coulmat(:,:,i_k_point), loc_shape_coulmat, CC4S_bb_desc, &
!                                     &inverted_basis_transform_at_k(:,:), loc_shape_inverted_ri_pw_trafo_matrix, CC4S_bbxpws_desc, &
!                                     &CC4S_n_basbas, CC4S_num_pws, &
!                                     &loc_shape_V_G_Q_matrix, CC4S_GxGprime_desc, CC4S_comm, i_k_point)
! 
!        !call contract_V_inv_sqrt_with_nu_G(coulmat(:,:,i_k_point), loc_shape_coulmat, CC4S_bb_desc, &
!        !                              &ri_pw_trafo_matrix(:,:,i_k_point), loc_shape_ri_pw_trafo_matrix, &
!        !                              &CC4S_bbxpws_desc, CC4S_n_basbas, CC4S_num_pws, CC4S_comm, i_k_point)
!
!        call write_current_pw_transform_block_to_file(i_k_point-1)
!      end do
!
!      !call bin2format(pw_transform_filename_bin, pw_transform_filename_dat, &
!      !               &MPI_COMM_WORLD, CC4S_num_pws*CC4S_n_basbas*CC4S_n_kq_points)
!
!      !call close_V_inv_sqrt_mu_G_filehandle(debug)
!      call close_pw_transform_filehandle(debug)
!    end if

    if(debug) then
      print_binary = .false.
    else
      print_binary = .true.
    end if 

 

!    if(CC4S_member) then
      if(all(shape(lvl_trico) .eq. [1,1,1,1,1]) .and. all(shape(v_trico) .eq. [1,1,1,1,1])) then
          if(myid_world == 0) then
            write(*,*) "No RI-coefficients have been provided."
            write(*,*) "Leaving CCaims..."
          end if
  
          return
      else if(all(shape(lvl_trico) .eq. [1,1,1,1,1])) then
        if(myid_world == 0) then
          write(*,*) "The RI-V coefficients will be used."
        end if 
        RIscheme = 'V'
      else
        if(myid_world == 0) then
          write(*,*) "The RI-LVL coefficients will be used."
        end if 
        RIscheme = 'L'
      end if
!    else
!      !Non-participating tasks can be disregarded
!      RIscheme = 'X' 
!    end if 

    if(allocated(coulmat)) then
      if(myid_world == 0) then
        write(*,*) "aux. Coulomb matrix is allocated"
      end if 
    end if

    if(allocated(lvl_trico)) then
      if(myid_world == 0) then
        write(*,*) "LVL-coefficient tensor is allocated"
      end if 
    else
      if(myid_world == 0) then
        write(*,*) "LVL-coefficient tensor not allocated"
      end if 
      allocate(lvl_trico(1, 1, 1, 1, 1))
    end if 

    if(present(scf_fock_real)) then
      if(myid_world == 0) then
        write(*,*) "real KS-transformed Hartree-Fock-matrix has been submitted"
      end if
      is_calc_hf = .false. 
    elseif(present(scf_fock_cmplx)) then
      if(myid_world == 0) then
        write(*,*) "complex KS-transformed Hartree-Fock-matrix has been submitted"
      end if 
      is_calc_hf = .false.
    else
      if(myid_world == 0) then
        write(*,*) "No (transformed) Hartree-Fock-matrix has been explicitly submitted"
      end if 
      is_calc_hf = .true.
    end if 

    KS_shm_real => null()
    KS_shm_cmplx => null()
    !1.Step: set up shared window for the scf_eigvecs, as they will be used
    !at multiple points, but not themselves modified

    !First use of the following routine sets up inter- and intra communicators for
    !the ENTIRE set of tasks, those which take part in the interface and those which don't
    if(myid_world == 0) then
      write(*,*) "Setting up inter- and intra-communicator for global MPI communicator..."
    end if 
    write(*,*) "RANK", myid_world, "entering CC4S_setup_inter_comm()"
    call CC4S_setup_inter_comm(MPI_COMM_WORLD, comm_shared, n_tasks_shared, myid_shared, &
            &comm_inter, n_tasks_inter, myid_inter)

    if(myid_world == 0) then
      write(*,*) "Opening shared memory window for KS-eigenvectors..."
    end if   
    if(CC4S_isreal) then
      call CC4S_share_eigenvec(comm_shared, n_tasks_inter, myid_inter, n_tasks_shared, &
              &myid_shared, myid_world, scf_eigvecs_real, KS_shm_real)
      allocate(KS_shm_cempty(1, 1, 1, 1))
      KS_shm_cmplx => KS_shm_cempty
    else
      call CC4S_share_eigenvec(comm_shared, n_tasks_inter, myid_inter, n_tasks_shared, &
              &myid_shared, myid_world, scf_eigvecs_cmplx, KS_shm_cmplx)

      allocate(KS_shm_rempty(1, 1, 1, 1))
      KS_shm_real => KS_shm_rempty
    end if
    !write(*,*) "Rank", myid_world, "The SCF eigenvectors/coefficient matrix has been successfully shared"

    !do i_k_debug=1,CC4S_n_kq_points
    !  if(CC4S_myid .eq. 0) then
    !    write(*,*) "Writing KS_eigenvector for k-point", i_k_debug, "to file"
    !    write (filename_debug, "(A14,I0)") "ks_eigenvector", i_k_debug
    !    open(unit=441, file=trim(filename_debug))
    !    do i_state_debug=1,CC4S_n_states    
    !      do i_basis_debug=1,CC4S_n_basis
    !        write(441,*) KS_shm_cmplx(i_basis_debug,i_state_debug,1,i_k_debug)
    !      end do
    !    end do
    !    close(unit=441)

    !  end if
    !end do


    if(myid_world == 0) then
      write(*,*) "Recycling inter- and intra-communicator..."
    end if    
    !To reuse the intra/inter communicator variables, free the MPI-objects first
    call MPI_Comm_free(comm_shared, ierr)
    call MPI_Comm_free(comm_inter, ierr)
    !2.Step: Transform one of the lvl-array dimensions from AO->MO by multiplying with 
    !the (now shared) scf-eigenvectors

    if(CC4S_member) then
      call CC4S_setup_inter_comm(CC4S_comm, comm_shared, n_tasks_shared, myid_shared, &
              &comm_inter, n_tasks_inter, myid_inter)
    else
      myid_shared=-1
      myid_inter=-1
    end if

    if(RIscheme .eq. 'L') then 
      if(myid_world == 0) then
        write(*,*) "...using prior inter- and intra-communicator for RI-LVL-coefficient distribution..."
      end if
      call CC4S_setup_LVL(CC4S_comm, comm_shared, myid_shared, is_lvl_root, comm_lvl_root, &
              &myid_lvlroot, n_tasks_lvlroot, mpi_comm_shared_row, n_tasks_shrow, myid_shrow)
      !write(*,*) "Rank", myid_world,"Communicators and infrastructure for sharing LVL coefficients successful"
      if(is_lvl_root) then
        allocate(aomo_lvl(CC4S_lbb_row:CC4S_ubb_row, CC4S_max_n_basis_sp, &
                &CC4S_n_low_state:CC4S_n_states, &
                &CC4S_n_spin, CC4S_n_k_points))
      else
        allocate(aomo_lvl(CC4S_lbb_row:CC4S_ubb_row, 1, 1, 1, CC4S_n_k_points))
      end if 
  
      aomo_lvl(:,:,:,:,:) = (0.0D0, 0.0D0)
 
      !debug: write out AO-AO-iRI-LVL-coeffs
    !  if(CC4S_member) then
    !    do i_k_debug=1, CC4S_n_k_points
    !      write(debug_filename, "(A7,I0,A2,I0)") "lvl_cc_", i_k_debug, "_p", CC4S_myid
    !       open(unit=100+CC4S_myid, file=trim(debug_filename))
    !       do i_ap_debug=1,size(lvl_trico(:,:,:,:,:),5)
    !         do i_basis_debug=1, CC4S_max_n_basis_sp
    !           do i_basis_debug_2=1, CC4S_max_n_basis_sp
    !             do i_basbas_debug=1, CC4S_max_n_basbas_sp
    !               write(100+CC4S_myid,*) &
    !                 &lvl_trico(i_basis_debug,i_basis_debug_2,i_basbas_debug,i_k_debug,i_ap_debug)
    !             end do
    !           end do
    !         end do
    !       end do
    !    end do
    !    close(unit=100+CC4S_myid)
    !  end if
 
      !if(CC4S_member) then
      if(myid_world == 0) then
        write(*,*) "Transforming first AO-dimension of RI-LVL-coefficients to MOs..."
      end if
      call CC4S_mo_trafo_LVL(lvl_trico, KS_shm_real, KS_shm_cmplx, is_lvl_root, &
              &CC4S_member, CC4S_lbb_row, CC4S_ubb_row, size(lvl_trico, 5), &
              &CC4S_comm, aomo_lvl)
      !end if 

        !debug
      ! do i_k_debug=1,CC4S_n_kq_points
      !   write(*,*) "rank-nr. ", CC4S_myid, ": CC4S_n_basbas = ", CC4S_n_basbas
      !   write(*,*) "rank-nr. ", CC4S_myid, ": CC4S_max_n_basis_sp = ", CC4S_max_n_basis_sp
      !   write(*,*) "rank-nr. ", CC4S_myid, ": CC4S_nuf_states = ", CC4S_nuf_states
      !   write(*,*) "rank-nr. ", CC4S_myid, ": CC4S_lbb_row = ", CC4S_lbb_row
      !   write(*,*) "rank-nr. ", CC4S_myid, ": CC4S_ubb_row = ", CC4S_ubb_row

      !   write (filename_debug, "(A14,I0)") "lvlri_coeffmat", i_k_debug
      !   call CC4S_debug_write_3d_cmplx(CC4S_comm, aomo_lvl(:,:,:,1,i_k_debug), &
      !     &CC4S_n_basbas*CC4S_max_n_basis_sp*CC4S_nuf_states, &
      !     &[CC4S_n_basbas, CC4S_max_n_basis_sp, CC4S_nuf_states], &
      !     &shape(aomo_lvl(:,:,:,1,i_k_debug)), [CC4S_lbb_row-1,0,0], trim(filename_debug))
      ! end do

      !debug: write out AO-AO-iRI-LVL-coeffs
      !if(CC4S_member .and. is_lvl_root) then
      !  do i_k_debug=1, CC4S_n_k_points
      !    write(debug_filename, "(A12,I0,A2,I0)") "aomolvl_cc_k", i_k_debug, "_p", CC4S_myid
      !    open(unit=120+CC4S_myid, file=trim(debug_filename))
      !    do i_state_debug=CC4S_n_low_state,CC4S_n_states
      !      do i_basis_debug=1,CC4S_max_n_basis_sp
      !        do i_basbas_debug=CC4S_lbb_row,CC4S_ubb_row
      !          write(120+CC4S_myid,*) aomo_lvl(i_basbas_debug,i_basis_debug,i_state_debug,1,i_k_debug)
      !        end do
      !      end do
      !    end do
      !    close(unit=120+CC4S_myid)
      !  end do        
      !end if

  
      if(allocated(lvl_trico)) then
        deallocate(lvl_trico)
      end if 
  
      if(CC4S_isreal) then
        if(is_lvl_root) then
          allocate(aomo_lvl_real(CC4S_lbb_row:CC4S_ubb_row, CC4S_max_n_basis_sp, &
                  &CC4S_n_low_state:CC4S_n_states, CC4S_n_spin, CC4S_n_k_points))
          aomo_lvl_real(:,:,:,:,:)=dble(aomo_lvl(:,:,:,:,:))
          deallocate(aomo_lvl)
        else
          allocate(aomo_lvl_real(CC4S_lbb_row:CC4S_ubb_row, 1, 1, 1, CC4S_n_k_points))
          deallocate(aomo_lvl)
        end if 
      end if

      !Dummy-array for RI-V
      !if(CC4S_isreal) then
      !  allocate(aomo_v_real(1,1,1,1,1,1))
      !  aomo_v_real(:,:,:,:,:,:) = 0.0D0
      !else
      !  allocate(aomo_v(1,1,1,1,1,1))
      !  aomo_v(:,:,:,:,:,:) = (0.0D0, 0.0D0)
      !end if 
      allocate(aomo_v_real(1,1,1,1,1,1))
      aomo_v_real(:,:,:,:,:,:) = 0.0D0
      allocate(aomo_v(1,1,1,1,1,1))
      aomo_v(:,:,:,:,:,:) = (0.0D0, 0.0D0)

      if(myid_world == 0) then
        write(*,*) "Opening shared memory window for AO->MO-transformed RI-LVL-coefficients..."
      end if
      call CC4S_shm_LVL(CC4S_member, is_lvl_root, aomo_lvl_real, aomo_lvl, &
              &aomo_lvl_rshm, aomo_lvl_cshm, mpi_comm_shared_row)

      if(.not. CC4S_member) then
        allocate(aomo_lvl_rshm_empty(1,1,1,1,1))
        aomo_lvl_rshm_empty(:,:,:,:,:) = 0.0D0
        aomo_lvl_rshm(1:1,1:1,1:1,1:1,1:1) => aomo_lvl_rshm_empty(:,:,:,:,:)
  
        allocate(aomo_lvl_cshm_empty(1,1,1,1,1))
        aomo_lvl_cshm_empty(:,:,:,:,:) = (0.0D0, 0.0D0)
        aomo_lvl_cshm(1:1,1:1,1:1,1:1,1:1) => aomo_lvl_cshm_empty(:,:,:,:,:)
      end if  
   
      !if(associated(aomo_lvl_rshm)) then
      !  write(*,*) 'In Rank', myid_world, 'aomo_lvl_rshm is associated'
      !else
      !  write(*,*) 'In Rank', myid_world, 'aomo_lvl_rshm is NOT associated'
      !end if  
    else if((RIscheme .eq. 'V') .and. CC4S_member) then
      !Experimental: Convert one of AO-dimensions (the distributed one) of the 
      !RI-V-coefficients to MO.

      if(myid_world == 0) then
        write(*,*) "Transforming first AO-dimension of RI-V-coefficients to MOs..."
      end if

      if(CC4S_isreal) then
        !write(*,*) 'shape(KS_shm_real): ', shape(KS_shm_real)
        !write(*,*) ''
        
        call convert_RIV_distributed_aotomo_real(v_trico, KS_shm_real, aomo_v_real)
        !call MPI_Barrier(MPI_COMM_WORLD, ierr)
        !write(*,*) 'RANK ', myid_world, 'left ao->mo conversion of ovlp_3fn'

        !Allocate complex dummy
        allocate(aomo_v(1,1,1,1,1,1))
        aomo_v(:,:,:,:,:,:) = (0.0D0, 0.0D0)
      else
        call convert_RIV_distributed_aotomo_cmplx(v_trico, KS_shm_cmplx, aomo_v)
        !call MPI_Barrier(MPI_COMM_WORLD, ierr)

        !Allocate real dummy
        allocate(aomo_v_real(1,1,1,1,1,1))
        aomo_v_real(:,:,:,:,:,:) = 0.0D0
      end if 

      allocate(aomo_lvl_rshm_empty(1,1,1,1,1))
      aomo_lvl_rshm_empty(:,:,:,:,:) = 0.0D0
      aomo_lvl_rshm(1:1,1:1,1:1,1:1,1:1) => aomo_lvl_rshm_empty(:,:,:,:,:)

      allocate(aomo_lvl_cshm_empty(1,1,1,1,1))
      aomo_lvl_cshm_empty(:,:,:,:,:) = (0.0D0, 0.0D0)
      aomo_lvl_cshm(1:1,1:1,1:1,1:1,1:1) => aomo_lvl_cshm_empty(:,:,:,:,:)
    else
      if(.not. CC4S_member) then
        allocate(aomo_lvl_rshm_empty(1,1,1,1,1))
        aomo_lvl_rshm_empty(:,:,:,:,:) = 0.0D0
        aomo_lvl_rshm(1:1,1:1,1:1,1:1,1:1) => aomo_lvl_rshm_empty(:,:,:,:,:)
  
        allocate(aomo_lvl_cshm_empty(1,1,1,1,1))
        aomo_lvl_cshm_empty(:,:,:,:,:) = (0.0D0, 0.0D0)
        aomo_lvl_cshm(1:1,1:1,1:1,1:1,1:1) => aomo_lvl_cshm_empty(:,:,:,:,:)

        allocate(aomo_v_real(1,1,1,1,1,1))
        aomo_v_real(:,:,:,:,:,:) = 0.0D0
        allocate(aomo_v(1,1,1,1,1,1))
        aomo_v(:,:,:,:,:,:) = (0.0D0, 0.0D0)
      end if
    end if 

    !if(prepare_finite_size_correction .and. (present(singval_num) .or. present(singval_thr))) then
    !  !In the following code blocks, coulmat/coulmat_real will be modified to contain the sqrt of 
    !  !the Coulomb matrix instead of the matrix itself. If we want to obtain
    !  !finite-size correction related objects in combination with OAF, we, however, need to 
    !  !save the original Coulomb matrix for that purpose. 
    !  !NOTE: Recovering the Coulomb matrix from its sqrt by taking its power of -0.5,
    !  !didn't yield good results. Hence, we need to save a copy of coulmat/coulmat_real instead
    !   allocate(coulmat_copy(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col, &
    !           &CC4S_n_kq_points))
    !   coulmat_copy(:,:,:) = coulmat(:,:,:)
    !end if

    !3.Step: Calculate the sqrt of the aux. Coulomb matrix ("coulmat")
    if(CC4S_isreal) then
      allocate(coulmat_real(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col, &
              &CC4S_n_kq_points))
      coulmat_real(:,:,:) = dble(coulmat(:,:,:))
      deallocate(coulmat)
    end if



    !write(*,*) "(From interface_driver) CC4S_isreal = ", CC4S_isreal
    !write(*,*) "(From interface_driver) shape(coulmat) = ", shape(coulmat)

    if(myid_world == 0) then
      write(*,*) "Calculating square-root of auxliary Coulomb matrix V..."
    end if
    if(CC4S_n_tasks .eq. 1) then
      if(CC4S_isreal) then
        do i_k_point=1,CC4S_n_kq_points
          if(myid_world == 0) then
            write(*,'(A,1X,I0)') "Performing V->V^(0.5) for k-q-index:", i_k_point
          end if 
          call CC4S_sqrt_lapack(coulmat_real(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
                &loc_shape_coulmat, CC4S_comm, debug)
        end do
      else
        do i_k_point=1,CC4S_n_kq_points
          if(myid_world == 0) then
            write(*,'(A,1X,I0)') "Performing V->V^(0.5) for k-q-index:", i_k_point
          end if
          call CC4S_sqrt_lapack(coulmat(:,:,i_k_point), CC4S_n_basbas, &
                  &CC4S_bb_desc, loc_shape_coulmat, CC4S_comm, debug)
        end do
      end if 
    else
      if(CC4S_isreal) then
        do i_k_point=1,CC4S_n_kq_points
          if(myid_world == 0) then
            write(*,'(A,1X,I0)') "Performing V->V^(0.5) for k-q-index:", i_k_point
          end if
          call CC4S_sqrt_scalapack(coulmat_real(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
                &loc_shape_coulmat, CC4S_comm, debug)
        end do
      else
        do i_k_point=1,CC4S_n_kq_points
          if(myid_world == 0) then
            write(*,'(A,1X,I0)') "Performing V->V^(0.5) for k-q-index:", i_k_point
          end if
          call CC4S_sqrt_scalapack(coulmat(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
                &loc_shape_coulmat, CC4S_comm, debug)
        end do
      end if  
    end if 


    !write(*,*) "Rank", myid_world, "has left the sqrt calc"

    
    if( (.not. CC4S_isreal) .and. (.not. present(singval_thr)) .and. (.not. present(singval_num)) ) then
      !the left Coulomb vertex is only relevant for the complex case. Also, (for now) we won't
      !compute the left Coulomb vertex if any kind of OAF-truncation is requested.
      compute_left_coul_vertex = .true.
    end if

    !3.5.Step: Open File-handle for Coulomb-vertex
    if(CC4S_member) then
      call MPI_File_open(CC4S_comm, coulomb_vertex_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
              &MPI_INFO_NULL, CoulombVertex_fh, ierr)

      if(compute_left_coul_vertex) then
        call MPI_File_open(CC4S_comm, left_coulomb_vertex_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
                &MPI_INFO_NULL, LeftCoulombVertex_fh, ierr)
      end if
    end if 

    !4.Step: Perform second AO->MO transformation of lvl-array
    !+
    !5.Step: Mulyiply sqrt(coulmat) with doubly transformed lvl-array to obtain Coulomb vertex
    !+
    !6.Step: (optional but recommended) Prune Coulomb vertex by Principal Component Analysis to 
    !obtain Optimized Auxilliary Field vertex (OAF)
    !+
    !7.Step: Write Coulomb vertex/OAF vertex to file

    !write(*,*) "Rank", myid_world, "entering coulvertex_construction"


    !write(*,*) "AO-MO-LVL (SHM) subsizes: ", shape(aomo_lvl_rshm)
    if(CC4S_myid_col .eq. 0) then
            aomo_sizes=[CC4S_n_basbas, CC4S_max_n_basis_sp, &
                &CC4S_n_states, CC4S_n_spin, CC4S_n_k_points]
            aomo_subsizes=shape(aomo_lvl_rshm)
            aomo_starts=[CC4S_lbb_row-1, 0, 0, 0, 0]

    end if

  

    !write(*,*) "(From interface driver) Writing coulmat real to file"
    if(myid_world == 0) then
      write(*,*) "Calculating Coulomb vertex/Optimized Auxiliary Field (OAF) Coulomb vertex..."
    end if

    !it SEEMS that until here all relevant quantities, 
    !aomo_lvl_rshm, KS_shm_real, &coulmat_real
    !are correct
    !write(*,*) "(From interface driver) Entering CC4S_coulvertex_construction"
    if(present(singval_thr) .and. present(singval_num)) then
      call CC4S_coulvertex_construction(aomo_lvl_rshm, aomo_lvl_cshm, &
            &aomo_v_real, aomo_v, KS_shm_real, KS_shm_cmplx, &
            &coulmat_real, coulmat, CoulombVertex_fh, debug, momentum_triple_list, sparse_OAF_dim, &
            &singval_thr, singval_num, prepare_finite_size_correction)
    else if(present(singval_num)) then
      !If only singval_num has been passed
      call CC4S_coulvertex_construction(aomo_lvl_rshm, aomo_lvl_cshm, &
            &aomo_v_real, aomo_v, KS_shm_real, KS_shm_cmplx, &
            &coulmat_real, coulmat, CoulombVertex_fh, debug, momentum_triple_list, sparse_OAF_dim, &
            &singval_num=singval_num, evaluate_co_densities=prepare_finite_size_correction)
    else if(present(singval_thr)) then
      !If only singval_thr has been passed
      call CC4S_coulvertex_construction(aomo_lvl_rshm, aomo_lvl_cshm, &
            &aomo_v_real, aomo_v, KS_shm_real, KS_shm_cmplx, &
            &coulmat_real, coulmat, CoulombVertex_fh, debug, momentum_triple_list, sparse_OAF_dim, &
            &singval_thr=singval_thr, evaluate_co_densities=prepare_finite_size_correction)
    else 
      !If neither singval_num nor singval_thr has been provided, it is understood
      !that no OAF-transformation of the Coulomb vertex is desired
      if(compute_left_coul_vertex) then 
        call CC4S_coulvertex_construction(aomo_lvl_rshm, aomo_lvl_cshm, &
              &aomo_v_real, aomo_v, KS_shm_real, KS_shm_cmplx, &
              &coulmat_real, coulmat, CoulombVertex_fh, debug, momentum_triple_list, sparse_OAF_dim, &
              &evaluate_co_densities=prepare_finite_size_correction, &
              &do_left_coulomb_vertex=compute_left_coul_vertex, &
              &LeftCoulombVertex_fh=LeftCoulombVertex_fh, &
              &left_momentum_triple_list=left_momentum_triple_list)
      else
        call CC4S_coulvertex_construction(aomo_lvl_rshm, aomo_lvl_cshm, &
              &aomo_v_real, aomo_v, KS_shm_real, KS_shm_cmplx, &
              &coulmat_real, coulmat, CoulombVertex_fh, debug, momentum_triple_list, sparse_OAF_dim, &
              &evaluate_co_densities=prepare_finite_size_correction)
      end if
    end if

    

    !Rebuild positive(semi)definite Coulomb matrix from sqrt
    if(myid_world == 0) then
      write(*,*) "Rebuilding Coulomb matrix V from its square-root..."
    end if
    if(CC4S_n_tasks .eq. 1) then
      if(CC4S_isreal) then
        do i_k_point=1,CC4S_n_kq_points
          if(myid_world == 0) then
            write(*,'(A,1X,I0)') "Performing V^(0.5)->V for k-q-index:", i_k_point
          end if 
          call CC4S_square_sqrt_lapack(coulmat_real(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
                &loc_shape_coulmat, CC4S_comm, debug)
        end do
      else
        do i_k_point=1,CC4S_n_kq_points
          if(myid_world == 0) then
            write(*,'(A,1X,I0)') "Performing V^(0.5)->V for k-q-index:", i_k_point
          end if
          call CC4S_square_sqrt_lapack(coulmat(:,:,i_k_point), CC4S_n_basbas, &
                  &CC4S_bb_desc, loc_shape_coulmat, CC4S_comm, debug)
        end do
      end if 
    else
      if(CC4S_isreal) then
        do i_k_point=1,CC4S_n_kq_points
          if(myid_world == 0) then
            write(*,'(A,1X,I0)') "Performing V^(0.5)-> for k-q-index:", i_k_point
          end if
          call CC4S_square_sqrt_scalapack(coulmat_real(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
                &loc_shape_coulmat, CC4S_comm, debug)
        end do
      else
        do i_k_point=1,CC4S_n_kq_points
          if(myid_world == 0) then
            write(*,'(A,1X,I0)') "Performing V^(0.5)->V for k-q-index:", i_k_point
          end if

          call CC4S_square_sqrt_scalapack(coulmat(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
                &loc_shape_coulmat, CC4S_comm, debug)
        end do
      end if  
    end if

    if(prepare_finite_size_correction .and. CC4S_member) then
      !For the pair-energy approach we will need the RI-Coulomb matrix for every k-q point.
      !Hence, it will be written to file here
      
      call MPI_File_open(CC4S_comm, coulmat_filename_bin, MPI_MODE_WRONLY+MPI_MODE_CREATE, &
            &MPI_INFO_NULL, coulmat_fh, ierr)
 
      do i_kq_point=1,CC4S_n_kq_points
        coulmat_sizes = [CC4S_n_basbas, CC4S_n_basbas]
        if(CC4S_isreal) then
          coulmat_subsizes=shape(coulmat_real(:,:,i_kq_point))
        else
          coulmat_subsizes=shape(coulmat(:,:,i_kq_point))
        end if

        coulmat_starts=[CC4S_lbb_row-1, CC4S_lbb_col-1]
        if(CC4S_isreal) then
          call CC4S_parallel_io_coulmat(dcmplx(coulmat_real(:,:,i_kq_point)), coulmat_fh, coulmat_sizes, &
                                     &coulmat_subsizes, coulmat_starts, i_kq_point-1)
        else
          call CC4S_parallel_io_coulmat(coulmat(:,:,i_kq_point), coulmat_fh, coulmat_sizes, &
                                     &coulmat_subsizes, coulmat_starts, i_kq_point-1)  
        end if
      end do

      call MPI_File_close(coulmat_fh, ierr)

      !In the new (release-version) of CC4S, data files carry the suffix ".elements"
      !instead of ".dat" or ".bin".
      !The following function will select the chosen data file 
      !(the .bin- or .dat-file depending on debug-setting)
      !and change the respective suffix to ".elements"
      call adapt_data_file_suffix(debug, coulmat_filename_dat, &
                              &coulmat_filename_bin, MPI_COMM_WORLD)

      !call bin2format(coulmat_filename_bin, coulmat_filename_dat, CC4S_comm, &
      !               &CC4S_n_basbas*CC4S_n_basbas*CC4S_n_kq_points)


    end if








!    if(prepare_finite_size_correction .and. (present(singval_num) .or. present(singval_thr))) then
      !Complete the construction of U^{mu}_{F} * ( V^(-0.5) )_{mu,nu} & <nu|G> exactly here
      !after the construction of the Coulomb vertex is completed, because
      !at this point OAF_trafo_matrix_r/OAF_trafo_matrix_c (the U) is still allocated and
      !correctly initialized.

!      if(myid_world == 0) then
!        write(*,*) "Finite-size correction quantities have been requested together with OAF-compression"
!      end if
!
!      if(CC4S_n_k_points == 1) then
!        if(myid_world == 0) then
!          write(*,*) "OAF-transformation will be applied to finite-size correction quantities"
!        end if
!
!        !call contract_OAF_trafo_matrix_with_V_inv_sqrt_mu_G()
!      else
!        if(myid_world == 0) then
!          write(*,*) "Finite-size correction with OAF-compression is not implemented for multiple k-points."
!          write(*,*) "No finite-size correction related output will be generated in this run."
!        end if
!     
!      end if

!      !At this point the original coulmat-object (or coulmat_real), which once contained the Coulomb matrix,
!      !has been modified to contain the sqrt of the Coulomb matrix.
!      !As we need the original Coulomb matrix for our finite-size quantities,
!      !we recover V by calculating the inverse square root.
!      !As this involves potentially dividing by small numbers, it remains to be seen
!      !how well we can recover V.
!
!
!      if(myid_world == 0) then
!        write(*,*) "Recovering Coulomb matrix V from its square root..."
!      end if
!      if(CC4S_n_tasks .eq. 1) then
!        if(CC4S_isreal) then
!          do i_k_point=1,CC4S_n_kq_points
!            if(myid_world == 0) then
!              write(*,'(A,1X,I0)') "Performing V^(0.5)->V for k-q-index:", i_k_point
!            end if 
!            call CC4S_sqrt_lapack(coulmat_real(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
!                  &loc_shape_coulmat, CC4S_comm, debug, power=-0.5d0)
!          end do
!        else
!          do i_k_point=1,CC4S_n_kq_points
!            if(myid_world == 0) then
!              write(*,'(A,1X,I0)') "Performing V^(0.5)->V for k-q-index:", i_k_point
!            end if
!            call CC4S_sqrt_lapack(coulmat(:,:,i_k_point), CC4S_n_basbas, &
!                    &CC4S_bb_desc, loc_shape_coulmat, CC4S_comm, debug, power=-0.5d0)
!          end do
!        end if 
!      else
!        if(CC4S_isreal) then
!          do i_k_point=1,CC4S_n_kq_points
!            if(myid_world == 0) then
!              write(*,'(A,1X,I0)') "Performing V^(0.5)->V for k-q-index:", i_k_point
!            end if
!            call CC4S_sqrt_scalapack(coulmat_real(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
!                  &loc_shape_coulmat, CC4S_comm, debug, power=-0.5d0)
!          end do
!        else
!          do i_k_point=1,CC4S_n_kq_points
!            if(myid_world == 0) then
!              write(*,'(A,1X,I0)') "Performing V^(0.5)->V for k-q-index:", i_k_point
!            end if
!            call CC4S_sqrt_scalapack(coulmat(:,:,i_k_point), CC4S_n_basbas, CC4S_bb_desc, &
!                  &loc_shape_coulmat, CC4S_comm, debug, power=-0.5d0)
!          end do
!        end if  
!      end if


      !If OAF-trafo has been requested perform everything finite-size correction related here


!      allocate(loc_shape_ri_pw_trafo_matrix(4))
!      allocate(loc_shape_V_G_Q_matrix(4))
!      !allocate(diagonal_coulmat_G(CC4S_num_pws, CC4S_n_kq_points))
!      !diagonal_coulmat_G(:,:) = 0.0d0
!
!      loc_shape_ri_pw_trafo_matrix=[CC4S_lbb_row, CC4S_ubb_row, CC4S_lpw_col, CC4S_upw_col]
!      loc_shape_V_G_Q_matrix = [CC4S_lpw_row, CC4S_upw_row, CC4S_lpw_col, CC4S_upw_col]

!      !Temporarily re-allocate the original, complex-values coulmat
!      if(CC4S_isreal) then
!        allocate(coulmat_complex_single_k(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col))
!        coulmat_complex_single_k(:,:) = (0.0d0, 0.0d0)
!      end if

!      call open_V_inv_sqrt_mu_G_filehandle()
!
!      do i_k_point=1,CC4S_n_kq_points
!        !blacs-environment needs to be re-calculated for every k-q point as max_singval
!        !may differ between transfer momenta
!        if(CC4S_member) then
!          call CC4S_init_blacsOAF_pws_distribution(sparse_OAF_dim(i_k_point))
!        end if
!        !if(CC4S_isreal) then
!        !  coulmat_complex_single_k(:,:) = dcmplx(coulmat_real(:,:,i_k_point))
!        !  cm_ptr => coulmat_complex_single_k(:,:)
!        !else
!        !  cm_ptr => coulmat(:,:,i_k_point)
!        !end if
!        call transform_coulmat_to_pws(coulmat_copy(:,:,i_k_point), loc_shape_coulmat, CC4S_bb_desc, &
!                                     &ri_pw_trafo_matrix(:,:,i_k_point), loc_shape_ri_pw_trafo_matrix, CC4S_bbxpws_desc, &
!                                     &CC4S_n_basbas, CC4S_num_pws, &
!                                     &loc_shape_V_G_Q_matrix, CC4S_GxGprime_desc, CC4S_comm, i_k_point)
!
!        call contract_V_inv_sqrt_with_nu_G_OAF(coulmat_copy(:,:,i_k_point), loc_shape_coulmat, CC4S_bb_desc, &
!                                        &ri_pw_trafo_matrix(:,:,i_k_point), loc_shape_ri_pw_trafo_matrix, &
!                                        &CC4S_bbxpws_desc, CC4S_n_basbas, CC4S_num_pws, CC4S_comm, &
!                                        &sparse_OAF_dim(i_k_point), i_k_point)
!
!      end do
! 
!      call close_V_inv_sqrt_mu_G_filehandle(debug) 
    
!    end if
    
    !write(*,*) "(From interface driver) Left CC4S_coulvertex_construction"
    if(CC4S_member) then
      call MPI_File_close(CoulombVertex_fh, ierr)
      !In the new (release-version) of CC4S, data files carry the suffix ".elements"
      !instead of ".dat" or ".bin".
      !The following function will select the chosen data file 
      !(the .bin- or .dat-file depending on debug-setting)
      !and change the respective suffix to ".elements"
      call adapt_data_file_suffix(debug, coulomb_vertex_filename_dat, &
                                  &coulomb_vertex_filename_bin, MPI_COMM_WORLD)

      if(compute_left_coul_vertex) then
        call MPI_File_close(LeftCoulombVertex_fh, ierr)
        call adapt_data_file_suffix(debug, left_coulomb_vertex_filename_dat, &
                                  &left_coulomb_vertex_filename_bin, MPI_COMM_WORLD)
      end if
    end if

    if(myid_world == 0) then
      write(*,*) "Coulomb vertex/OAF Coulomb vertex has been written to file CoulombVertex.dat/.bin."
    end if

    !write(*,*) "Rank", myid_world, "left coulvertex_construction"
    !8.Step: Write the other quantitites (scf-eigenvalues, spins, Fock matrix) to files as well
    if(CC4S_n_spin > 1) then
      if(myid_world == 0) then
        write(*,'(A,1X,A,A)') "Writing spin-information to file", trim(scf_spins_filename), "..."
      end if
      call CC4S_output_spins(CC4S_nuf_states, CC4S_n_k_points, CC4S_n_spin, scf_spins_filename, .false.)
    end if 

    if(myid_world == 0) then
      write(*,'(A,1X,A,A)') "Writing SCF-eigenenergies to file", trim(scf_energies_filename), "..."
    end if
    call CC4S_output_scf_energies(scf_eigvals, CC4S_n_states, CC4S_n_spin, CC4S_n_k_points, &
            &CC4S_n_low_state, scf_energies_filename, .false.)
 

    if(.not. is_calc_hf) then
      if(myid_world == 0) then
        write(*,'(A,1X,A,A)') "Writing Fockmatrix to file ", trim(scf_fockmatrix_filename_bin), "..."
      end if
      !Writing out Fockmatrix is only useful if a KS-calculation has been performed. Otherwise,
      !it's just a diagonal matrix containing the scf-eigenenergies (=redundant)
      if(CC4S_isreal) then
        !write(*,*) "Rank", myid_world, "entering output of fockmatrix (real)"
        call output_fockmatrix_real(scf_eigvals, print_binary, .false., &
                &scf_fock_real)
        !write(*,*) "Rank", myid_world, "left output of fockmatrix (real)"
      else
        call output_fockmatrix_cmplx(scf_eigvals, print_binary, .false., &
                &scf_fock_cmplx)
      end if
      call adapt_data_file_suffix(debug, scf_fockmatrix_filename_dat, &
                                  &scf_fockmatrix_filename_bin, MPI_COMM_WORLD)
    end if 


    if(myid_world == 0) then
      write(*,*) "Generating yaml files for the parsed quantities ..." 
    end if 

    if(CC4S_myid .eq. 0) then
      if(compute_left_coul_vertex) then
        call output_yaml_files(scf_eigvals, momentum_triple_list, sparse_OAF_dim, CC4S_n_spin, CC4S_n_basbas, &
                              &print_binary, is_calc_hf, prepare_finite_size_correction, &
                              &present(singval_num) .or. present(singval_thr), &
                              &left_momentum_triples=left_momentum_triple_list)
      else
        call output_yaml_files(scf_eigvals, momentum_triple_list, sparse_OAF_dim, CC4S_n_spin, CC4S_n_basbas, &
                              &print_binary, is_calc_hf, prepare_finite_size_correction, &
                              &present(singval_num) .or. present(singval_thr))
      end if
    end if 



    if(myid_world == 0) then
      write(*,*) "Cleaning up ..."
    end if 
    !9.Step: Deallocate/Clean up
    call cleanup_shm_KS(KS_shm_real, KS_shm_cmplx)
    !write(*,*) 'Rank', myid_world, 'has cleaned up KS'
    if(RIscheme .eq. 'L') then
      call cleanup_shm_LVL(aomo_lvl_rshm, aomo_lvl_cshm)
      !write(*,*) 'Rank', myid_world, 'has cleaned up LVL'
    end if 

    if(myid_world == 0) then
      write(*,*) "Cleaning up completed. All shared memory windows were closed."
    end if

  end subroutine CC4S_interface_driver
!*******************************************************************************************
  subroutine CC4S_init_interface(n_states, n_basbas, n_basis, n_atoms, n_species, &
                  &n_k_points, n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, n_cells, &
                  &isreal, n_low_state, basbas_atom, species, sp2n_basis_sp, atom2basbas_off, &
                  &sp2n_basbas_sp, atom2basis_off, k_eigvec_loc, kpq_point_list, k_minus_q_vecs, &
                  &lb_atom, ub_atom, comm, fermi_energy, num_pws, &
                  &lbb_row, ubb_row, lbb_col, ubb_col, loc_bb_row, loc_bb_col, &
                  &myid_col, nprow, npcol, myid, member, n_tasks, interface_comm, row_comm, &
                  &lb_ap, ub_ap, n_ap, lbasis_col, ubasis_col, &
                  &bbxbb_desc, bbxbasis_desc, cntxt, blockbb_row, &
                  &lpw_row, upw_row, lpw_col, upw_col, bbxpw_desc, pwxpw_desc, &
                  &reciprocal_lattice_vectors, lr_row, ur_row, lr_col, ur_col, bbxr_desc, rxr_desc)

          use CC4S_blacs_environment
          use calculation_data
          implicit none

          !PURPOSE: This routine needs to be called before one enters the actual driver,
          !as this routine both initializes the fundamental quantitites in the 
          !calculation_data-module and determines the blacs-grid and related quantitites
          !which the interface will use for the computation.

          !input
          integer, intent(in) :: n_states, n_basbas, n_basis, n_atoms, n_species
          integer, intent(in) :: n_k_points, n_spin, max_n_basis_sp, max_n_basbas_sp, n_cells
          integer, intent(in) :: n_kq_points
          logical, intent(in) :: isreal
          integer, intent(in) :: n_low_state
          integer, dimension(:), intent(in) :: basbas_atom, species
          integer, dimension(:), intent(in) :: sp2n_basis_sp, atom2basbas_off
          integer, dimension(:), intent(in) :: sp2n_basbas_sp, atom2basis_off
          integer, dimension(:,:), intent(in) :: k_eigvec_loc
          integer, dimension(:,:), intent(in) :: kpq_point_list
          double precision, dimension(:,:), intent(in) :: k_minus_q_vecs
          integer, dimension(:), intent(in) :: lb_atom, ub_atom
          integer, intent(in) :: comm
          double precision, intent(in) :: fermi_energy
          integer, intent(in) :: num_pws
          !integer, intent(in) :: num_r

          integer, intent(out) :: lbb_row, ubb_row, lbb_col, ubb_col
          integer, intent(out) :: loc_bb_row, loc_bb_col
          integer, intent(out) :: myid_col, npcol, nprow 
          integer, intent(out) :: myid
          logical, intent(out) :: member
          integer, intent(out) :: n_tasks
          integer, intent(out) :: interface_comm
          integer, intent(out) :: row_comm
          integer, intent(out) :: lb_ap
          integer, intent(out) :: ub_ap
          integer, intent(out) :: n_ap
          integer, intent(out) :: lbasis_col, ubasis_col
          integer, dimension(9), intent(out) :: bbxbb_desc
          integer, dimension(9), intent(out) :: bbxbasis_desc
          integer, intent(out) :: cntxt
          integer, intent(out) :: blockbb_row
          integer, optional, intent(out) :: lpw_row, upw_row
          integer, optional, intent(out) :: lpw_col, upw_col
          integer, dimension(9), optional, intent(out) :: bbxpw_desc
          integer, dimension(9), optional, intent(out) :: pwxpw_desc
          double precision, dimension(3,3), optional, intent(in) :: reciprocal_lattice_vectors
          integer, optional, intent(out) :: lr_row, ur_row
          integer, optional, intent(out) :: lr_col, ur_col
          integer, dimension(9), optional, intent(out) :: bbxr_desc
          integer, dimension(9), optional, intent(out) :: rxr_desc

          !local
          character(len=200) :: func_name='init_interface'
          character(len=400) :: err_msg
          integer :: n_c3fn, n_c3fn_task, rest, myoffset
          !To be removed. Just a crutch...
          integer :: num_r

          !debug
          integer :: m, n

          num_r = 10000

          !Check dimensions of mapping arrays
          if(size(basbas_atom) .lt. n_basbas) then
            err_msg='Argument no. 14 must have the dimension of aux. basis functions'
            call error_stop(func_name, err_msg)
          end if 

          if(size(species) .ne. n_atoms) then
            err_msg='Argument no. 15 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 

          if(size(sp2n_basis_sp) .ne. n_species) then
            err_msg='Argument no. 16 must have the dimension of number of elements'
            call error_stop(func_name, err_msg)
          end if 

          if(size(atom2basbas_off) .ne. n_atoms) then
            err_msg='Argument no. 17 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 

          if(size(sp2n_basbas_sp) .ne. n_species) then
            err_msg='Argument no. 18 must have the dimension of number of elements'
            call error_stop(func_name, err_msg)
          end if 

          if(size(atom2basis_off) .ne. n_atoms) then
            err_msg='Argument no. 19 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 
          
          if((size(k_eigvec_loc,1) .ne. 2) .or. (size(k_eigvec_loc,2) .ne. n_k_points)) then
            err_msg='Argument no. 20 must have the dimension of 2 x number of k_points'
            call error_stop(func_name, err_msg)
          end if 

          if((size(kpq_point_list,1) .ne. n_k_points) &
                  &.or. (size(kpq_point_list,2) .ne. n_k_points)) then
                  err_msg='Argument no. 21 must have the dimension of number of &
                          &k_points x number of k_points'
                  call error_stop(func_name, err_msg)
          end if

          if((size(k_minus_q_vecs,1) .ne. n_k_points) .or. (size(k_minus_q_vecs,2) .ne. 3)) then
            err_msg='k-q vector list must have the dimension of number of &
                          &k_points x 3'
            call error_stop(func_name, err_msg)
          end if
 

          if(size(lb_atom) .ne. n_atoms) then
            err_msg='Argument no. 22 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 

          if(size(ub_atom) .ne. n_atoms) then
            err_msg='Argument no. 23 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 


          call initialize_data(n_states, n_basbas, n_basis, n_atoms, n_species, &
                  &n_k_points, n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, &
                  &n_cells, isreal, n_low_state, basbas_atom, species, &
                  &sp2n_basis_sp, atom2basbas_off, sp2n_basbas_sp, &
                  &atom2basis_off, k_eigvec_loc, kpq_point_list, k_minus_q_vecs, lb_atom, ub_atom, &
                  &fermi_energy, num_pws, reciprocal_lattice_vectors, num_r)

          !If n_low_state > 1 --> reduce number of relevant states from n_states
          !to n_states - n_low_state + 1
          if(num_pws > 0 .and. num_r > 0) then
            call CC4S_init_blacs_distribution(n_states-n_low_state+1, n_basbas, n_basis, comm, num_pws, num_r)
          else
            call CC4S_init_blacs_distribution(n_states-n_low_state+1, n_basbas, n_basis, comm)
          end if

          lbb_row = CC4S_lbb_row 
          ubb_row = CC4S_ubb_row 
          lbb_col = CC4S_lbb_col 
          ubb_col = CC4S_ubb_col

          loc_bb_row = CC4S_loc_bb_row 
          loc_bb_col = CC4S_loc_bb_col

          blockbb_row = CC4S_blockbb_row

          myid_col = CC4S_myid_col 
          npcol = CC4S_npcol
          nprow = CC4S_nprow

          myid = CC4S_myid
          member = CC4S_member
          n_tasks = CC4S_n_tasks
          interface_comm=CC4S_comm
          !row_comm = comm_col
          row_comm = comm_row

          if(present(lpw_col)) then
            lpw_col = CC4S_lpw_col 
          end if

          if(present(upw_col)) then
            upw_col = CC4S_upw_col
          end if

          if(present(lpw_row)) then
            lpw_row = CC4S_lpw_row
          end if

          if(present(upw_row)) then
            upw_row = CC4S_upw_row
          end if

          if(present(lr_row)) then
            lr_row = CC4S_lr_row
          end if

          if(present(ur_row)) then
            ur_row = CC4S_ur_row
          end if

          if(present(lr_col)) then
            lr_col = CC4S_lr_col
          end if

          if(present(ur_col)) then
            ur_col = CC4S_ur_col
          end if

          !Calculate number and ranges of atom pairs assigned to an mpi-task
          if(CC4S_member) then
            n_c3fn=n_atoms*n_atoms*(n_cells+1)
            n_c3fn_task=n_c3fn/CC4S_n_tasks
            rest=n_c3fn-CC4S_n_tasks*n_c3fn_task
            if(CC4S_myid .lt. rest) then
              n_c3fn_task=n_c3fn_task+1
              myoffset=CC4S_myid*n_c3fn_task
            else
              myoffset=CC4S_myid*n_c3fn_task+rest
            end if 

            lb_ap=myoffset/(n_cells+1)
            ub_ap=(myoffset+n_c3fn_task-1)/(n_cells+1)
          else
            lb_ap = 1
            ub_ap = 1
          end if 

          n_ap=ub_ap-lb_ap+1

          !Return ubasis_col and lbasis_col, which is required
          !if RI-V is used
          lbasis_col = CC4S_lbasis_col
          ubasis_col = CC4S_ubasis_col

          !Return Scalapack descriptor arrays, for potential re-ordering
          !of relevant arrays
          bbxbb_desc = CC4S_bb_desc
          bbxbasis_desc = CC4S_bbxbasis_desc

          if(present(bbxpw_desc)) then
            bbxpw_desc = CC4S_bbxpws_desc
          end if

          if(present(pwxpw_desc)) then
            pwxpw_desc = CC4S_GxGprime_desc
          end if

          if(present(bbxr_desc)) then
            bbxr_desc = CC4S_bbxr_desc
          end if

          if(present(rxr_desc)) then
            rxr_desc = CC4S_rxr_desc
          end if

          call get_cc4s_context(cntxt)


  end subroutine CC4S_init_interface
!*******************************************************************************************
  subroutine CC4S_init_interface_nopbc(n_states, n_basbas, n_basis, n_atoms, n_species, &
                  &n_spin, max_n_basis_sp, max_n_basbas_sp, &
                  &isreal, n_low_state, basbas_atom, species, sp2n_basis_sp, atom2basbas_off, &
                  &sp2n_basbas_sp, atom2basis_off, &
                  &lb_atom, ub_atom, comm, fermi_energy, &
                  &lbb_row, ubb_row, lbb_col, ubb_col, loc_bb_row, loc_bb_col, &
                  &myid_col, npcol, myid, member, n_tasks, interface_comm, &
                  &lb_ap, ub_ap, n_ap, lbasis_col, ubasis_col, &
                  &bbxbb_desc, bbxbasis_desc, cntxt)

          use CC4S_blacs_environment
          use calculation_data
          implicit none

          !PURPOSE: This routine needs to be called before one enters the actual driver,
          !as this routine both initializes the fundamental quantitites in the 
          !calculation_data-module and determines the blacs-grid and related quantitites
          !which the interface will use for the computation.

          !input
          integer, intent(in) :: n_states, n_basbas, n_basis, n_atoms, n_species
          integer, intent(in) :: n_spin, max_n_basis_sp, max_n_basbas_sp
          logical, intent(in) :: isreal
          integer, intent(in) :: n_low_state
          integer, dimension(:), intent(in) :: basbas_atom, species
          integer, dimension(:), intent(in) :: sp2n_basis_sp, atom2basbas_off
          integer, dimension(:), intent(in) :: sp2n_basbas_sp, atom2basis_off
          integer, dimension(:), intent(in) :: lb_atom, ub_atom
          integer, intent(in) :: comm
          double precision, intent(in) :: fermi_energy

          integer, intent(out) :: lbb_row, ubb_row, lbb_col, ubb_col
          integer, intent(out) :: loc_bb_row, loc_bb_col
          integer, intent(out) :: myid_col, npcol 
          integer, intent(out) :: myid
          logical, intent(out) :: member
          integer, intent(out) :: n_tasks
          integer, intent(out) :: interface_comm
          integer, intent(out) :: lb_ap
          integer, intent(out) :: ub_ap
          integer, intent(out) :: n_ap
          integer, intent(out) :: lbasis_col, ubasis_col
          integer, dimension(9), intent(out) :: bbxbb_desc
          integer, dimension(9), intent(out) :: bbxbasis_desc
          integer, intent(out) :: cntxt

          !local
          integer :: n_k_points
          integer :: n_kq_points
          integer :: n_cells
          !"save" necessary, to keep array alive after leaving function
          integer, dimension(2,1), save :: k_eigvec_loc_map
          integer, dimension(1,1), save :: kpq_point_list
          double precision, dimension(1,3) :: k_minus_q_vecs

          character(len=200) :: func_name='init_interface'
          character(len=400) :: err_msg
          integer :: n_c3fn, n_c3fn_task, rest, myoffset

          !debug
          integer :: m, n

          n_k_points = 1
          n_kq_points = 1
          n_cells = 1
          k_eigvec_loc_map(1,1) = 0
          k_eigvec_loc_map(2,1) = 1
          kpq_point_list(1,1) = 1
          k_minus_q_vecs(:,:) = 0.0d0

          !Check dimensions of mapping arrays
          if(size(basbas_atom) .lt. n_basbas) then
            err_msg='Argument no. 14 must have the dimension of aux. basis functions'
            call error_stop(func_name, err_msg)
          end if 

          if(size(species) .ne. n_atoms) then
            err_msg='Argument no. 15 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 

          if(size(sp2n_basis_sp) .ne. n_species) then
            err_msg='Argument no. 16 must have the dimension of number of elements'
            call error_stop(func_name, err_msg)
          end if 

          if(size(atom2basbas_off) .ne. n_atoms) then
            err_msg='Argument no. 17 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 

          if(size(sp2n_basbas_sp) .ne. n_species) then
            err_msg='Argument no. 18 must have the dimension of number of elements'
            call error_stop(func_name, err_msg)
          end if 

          if(size(atom2basis_off) .ne. n_atoms) then
            err_msg='Argument no. 19 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 
          
          if((size(k_eigvec_loc_map,1) .ne. 2) .or. (size(k_eigvec_loc_map,2) .ne. n_k_points)) then
            err_msg='Argument no. 20 must have the dimension of 2 x number of k_points'
            call error_stop(func_name, err_msg)
          end if 

          if((size(kpq_point_list,1) .ne. n_k_points) &
                  &.or. (size(kpq_point_list,2) .ne. n_k_points)) then
                  err_msg='Argument no. 21 must have the dimension of number of &
                          &k_points x number of k_points'
                  call error_stop(func_name, err_msg)
          end if 

          if((size(k_minus_q_vecs,1) .ne. n_k_points) .or. (size(k_minus_q_vecs) .ne. 3)) then
            err_msg='k-q vector list must have the dimension of number of &
                          &k_points x 3'
            call error_stop(func_name, err_msg)
          end if

          if(size(lb_atom) .ne. n_atoms) then
            err_msg='Argument no. 22 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 

          if(size(ub_atom) .ne. n_atoms) then
            err_msg='Argument no. 23 must have the dimension of number of atoms'
            call error_stop(func_name, err_msg)
          end if 


          call initialize_data(n_states, n_basbas, n_basis, n_atoms, n_species, &
                  &n_k_points, n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, &
                  &n_cells, isreal, n_low_state, basbas_atom, species, &
                  &sp2n_basis_sp, atom2basbas_off, sp2n_basbas_sp, &
                  &atom2basis_off, k_eigvec_loc_map, kpq_point_list, k_minus_q_vecs, lb_atom, ub_atom, &
                  &fermi_energy)

          !If n_low_state > 1 --> reduce number of relevant states from n_states
          !to n_states - n_low_state + 1
          call CC4S_init_blacs_distribution(n_states-n_low_state+1, n_basbas, n_basis, comm)

          lbb_row = CC4S_lbb_row 
          ubb_row = CC4S_ubb_row 
          lbb_col = CC4S_lbb_col 
          ubb_col = CC4S_ubb_col

          loc_bb_row = CC4S_loc_bb_row 
          loc_bb_col = CC4S_loc_bb_col

          myid_col = CC4S_myid_col 
          npcol = CC4S_npcol

          myid = CC4S_myid
          member = CC4S_member
          n_tasks = CC4S_n_tasks
          interface_comm=CC4S_comm

          !Calculate number and ranges of atom pairs assigned to an mpi-task
          if(CC4S_member) then
            n_c3fn=n_atoms*n_atoms*(n_cells+1)
            n_c3fn_task=n_c3fn/CC4S_n_tasks
            rest=n_c3fn-CC4S_n_tasks*n_c3fn_task
            if(CC4S_myid .lt. rest) then
              n_c3fn_task=n_c3fn_task+1
              myoffset=CC4S_myid*n_c3fn_task
            else
              myoffset=CC4S_myid*n_c3fn_task+rest
            end if 

            lb_ap=myoffset/(n_cells+1)
            ub_ap=(myoffset+n_c3fn_task-1)/(n_cells+1)
          else
            lb_ap = 1
            ub_ap = 1
          end if 

          n_ap=ub_ap-lb_ap+1

          !Return ubasis_col and lbasis_col, which is required
          !if RI-V is used
          lbasis_col = CC4S_lbasis_col
          ubasis_col = CC4S_ubasis_col

          !Return Scalapack descriptor arrays, for potential re-ordering
          !of relevant arrays
          bbxbb_desc = CC4S_bb_desc
          bbxbasis_desc = CC4S_bbxbasis_desc
          call get_cc4s_context(cntxt)


  end subroutine CC4S_init_interface_nopbc

end module CC4S_main_interface
