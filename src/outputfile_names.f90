module outputfile_names
  implicit none

  character(len=200), parameter :: scf_energies_filename = "EigenEnergies.elements"
  character(len=200) :: scf_fockmatrix_filename_dat = "FockOperator.dat"
  character(len=200) :: scf_fockmatrix_filename_bin = "Fockoperator.bin"
  character(len=200) :: scf_spins_filename = "Spins.elements"
  character(len=200) :: coulomb_vertex_filename_dat = "CoulombVertex.dat"
  character(len=200) :: coulomb_vertex_filename_bin = "CoulombVertex.bin"
  character(len=200) :: coulomb_inverse_sqrt_filename_dat = "CoulombInverseSqrt.dat"
  character(len=200) :: coulomb_inverse_sqrt_filename_bin = "CoulombInverseSqrt.bin"
  character(len=200) :: pw_transform_filename_dat = "PlaneWaveTransform.dat"
  character(len=200) :: pw_transform_filename_bin = "PlaneWaveTransform.bin"
  character(len=200) :: co_densities_filename_dat = "RealSpaceCoDensities.dat"
  character(len=200) :: co_densities_filename_bin = "RealSpaceCoDensities.bin"
  character(len=200) :: coulmat_filename_dat = "CoulombPotential.dat"
  character(len=200) :: coulmat_filename_bin = "CoulombPotential.bin"
  character(len=200) :: oaf_trafo_matrix_filename_dat = "CoulombVertexSingularVectors.dat"
  character(len=200) :: oaf_trafo_matrix_filename_bin = "CoulombVertexSingularVectors.bin"
  character(len=200) :: left_coulomb_vertex_filename_dat = "LeftCoulombVertex.dat"
  character(len=200) :: left_coulomb_vertex_filename_bin = "LeftCoulombVertex.bin"

  !In the new cc4s (the first published version) 
  !.dat and .bin-files have the same name 
  !(the difference is specified in the respective .yaml file).
  !The new convention is that the yaml- and the data-file have the same name,
  !and the .yaml-file ends with ".yaml", while the data-file ends with ".elements"
  !i.e CoulombVertex.yaml <-> CoulombVertex.elements
  !character(len=200), parameter :: scf_energies_filename = "EigenEnergies.elements"
  !character(len=200) :: scf_fockmatrix_filename = "FockOperator.elements"
  !character(len=200) :: scf_spins_filename = "Spins.elements"
  !character(len=200) :: coulomb_vertex_filename = "CoulombVertex.elements"

end module outputfile_names
