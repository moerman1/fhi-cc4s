#!/bin/bash

#This code will
#1) pull the newest versions of the relevant codes (FHI-aims, CC4S, and CC-aims)
#2) compile them (currently via gcc/gfortran)
#3) run all the tests
#4) document the results of the tests
#5) determine if tests have been satisfactorily passed

#Currently this script should be run via the 'remote-testlauncher.sh'-wrapper

#Check that the right number of command line arguments have been passed (currently: 4)
if [ $# -ne 4 ]; then
  echo "Incorrect numbers of arguments passed to script"
  echo "Invokation of script should look like ./run-testsuite.sh <YES/NO> <n> <test-input-directory> <test-list>"
  echo "<YES/NO> referring to optional update of relevant codes (FHI-aims/CC4S/CC-aims)"
  echo "<n> referring to number of MPI-tasks to be used in tests"
  echo "<test-input-directory> referring to the directory where inputs for the tests can be found"
  echo "<test-list> text-file which contains the names and reference-results of the tests"
  exit 3
fi

if [ "$1" != "YES" ] && [ "$1" != "NO" ] ; then
  echo "First argument is either YES or NO"
  exit 3
fi

if ! [[ "$2" =~ ^[0-9]+$ ]]; then
  echo "Second argument needs to be an integer"
  exit 3
fi


do_update_code=$1
number_tasks=$2 #number of MPI-tasks for test runs
test_input_dir=$3
test_list_file=$4

if [ -z "$CC4AIMS_TEST_SUITE" ]; then
  echo "CC4AIMS_TEST_SUITE is empty or not set"
  exit 3
fi

aux_files_dir=$CC4AIMS_TEST_SUITE
#
#if [ -z "$CC4AIMS_INTERFACE_CODEPATHS" ]; then
#  echo "CC4AIMS_INTERFACE_CODEPATHS is empty or not set"
#  exit 3
#fi
#
#if [ -z "$CC4AIMS_CODEPATH" ]; then
#  echo "CC4AIMS_CODEPATH is empty or not set"
#  exit 3
#fi

#testing_dir=$CC4AIMS_TEST_SUITE
#codes_dir=$CC4AIMS_INTERFACE_CODEPATHS
#cc4aims_dir=$CC4AIMS_CODEPATH

gitlab_root_dir=$(pwd)

fhi_aims_repo_subdir='FHIaims' 
cc4s_repo_subdir='cc4s'
#cc_aims_repo_subdir='fhi-cc4s'

#Make sure the repo-directories exist and are not empty
#if [ -z "$(ls -A $codes_dir/$fhi_aims_repo_subdir)" ]
#then
#  echo "FHI-aims repository is empty"
#  #exit 1
#else
#  echo "FHI-aims repository is not empty"
#fi 
#
#if [ -z "$(ls -A $codes_dir/$cc4s_repo_subdir)" ]
#then
#  echo "CC4S repository is empty"
#  #exit 1
#else
#  echo "CC4S repository is not empty"
#fi

#if [ -z "$(ls -A $cc4aims_dir/$cc_aims_repo_subdir)" ]
#then
#  echo "CC-aims repository is empty"
#  exit 1
#else
#  echo "CC-aims repository is not empty"
#fi

codes_dir=$gitlab_root_dir/'interfaced_codes'
mkdir $codes_dir

#full_path_cc_aims=$cc4aims_dir/$cc_aims_repo_subdir
full_path_fhi_aims=$codes_dir/$fhi_aims_repo_subdir
full_path_cc4s=$codes_dir/$cc4s_repo_subdir

if [ "$do_update_code" = "YES" ]; then
  #Pull and (re-)compile the newest versions of the codes
  #codes=( "$full_path_cc_aims" "$full_path_fhi_aims" "$full_path_cc4s" )
  codes=( "$full_path_fhi_aims" "$full_path_cc4s" )
  for code in "${codes[@]}"
  do
    #cd $code
    #git pull

    #make clean
    echo "Compiling" $code
    if [ "$code" = "$full_path_fhi_aims" ]; then
      rm -rf $full_path_fhi_aims
      cd $codes_dir
      #git clone -b use_external_cc4s_interface git@aims-git.rz-berlin.mpg.de:aims/FHIaims.git
      git clone https://moerman:$(cat ~/.fhi-aims-token.txt)@aims-git.rz-berlin.mpg.de/aims/FHIaims.git
      cd $full_path_fhi_aims
      mkdir -p $full_path_fhi_aims/build

      cd build &&
      cp $aux_files_dir/aux_files/$fhi_aims_repo_subdir/cmake.initial . &&
      cmake -C cmake.initial ../ &&
      make -j4 &&

      echo "Successfully compiled FHI-aims"
    elif [ "$code" = "$full_path_cc4s" ]; then
      rm -rf $full_path_cc4s
      cd $codes_dir
      git clone -b new_test_branch_for_ccaims https://emoerman:$(cat ~/.cc4s-token.txt)@gitlab.cc4s.org/cqc/cc4s.git
      #git clone https://emoerman:$(cat ~/.cc4s-token.txt)@gitlab.cc4s.org/cqc/cc4s.git
      cd $full_path_cc4s
      #make clean &&
      make CONFIG=gcc-oblas-ompi clean
      #make -j4 deps &&
      make CONFIG=gcc-oblas-ompi extern -j4 &&
      make CONFIG=gcc-oblas-ompi cc4s -j4 &&
      echo "Successfully compiled CC4S"

   #   cd $code
   #   git pull
   #   make clean &&
   #   make -j10 deps &&
   #   make -j10 &&
   #   echo "Successfully compiled CC4S"
    fi
    cd $gitlab_root_dir
  done
else
 echo "Codes will not be updated."
 echo "Going straight to the tests."
fi

echo "Codes have been successfully updated and compiled"
echo "Final directory" $(pwd)
############################################################################
#At this point all relevant codes should have been updated and compiled
#Time to go through the tests
############################################################################

#test_input_dir="test-inputs"
testing_dir=$(pwd)/tests
test_list="test-list.txt"
test_range_dir="test-range"

#path_to_aims_exe=$full_path_fhi_aims/build/aims*.x 
path_to_aims_exe=$(find $full_path_fhi_aims/build -name "aims*.x")


if [ $? -eq 0 ]; then
  echo "An FHI-aims executable was found: " $path_to_aims_exe
else
  echo "Failed to find FHI-aims executable...Aborting!"
  exit 2
fi

path_to_cc4s_exe=$full_path_cc4s/build/gcc-oblas-ompi/bin/Cc4s
if [ -f $path_to_cc4s_exe ]; then
  echo "An CC4S executable was found: " $path_to_cc4s_exe
else
  echo "Failed to find CC4S executable...Aborting!"
  exit 2
fi

#Create testing range
mkdir $testing_dir/$test_range_dir

echo "Looking for tests..."
if [ -f $test_list_file ]; then
  echo "The following tests will be attempted:"
  cat $test_list_file
  #Read test-list.txt into array (requires bash 4.0+)
  readarray -t test_arr < $test_list_file
else
  echo "No list of tests found...Abort!"
  exit 2
fi
echo '###############################################################'
echo 'Preparation is completed. Starting test range.'
echo '###############################################################'

test_report=$testing_dir/test-report.txt
echo '#############################################################' | tee $test_report
echo '| Test |   Test-result   | Reference | Difference | Status  |' | tee -a $test_report

#while IFS='' read -r LINE || [ -n "${LINE}" ]; do 
#This associative array (i.e a dict) will store the SUCCESS or FAILURE
#of every test
declare -A success_dict

for test_calc in "${test_arr[@]}"; do
  test_name=$(echo "${test_calc}" | cut -d: -f2 | tr -d ' ')
  reference_result=$(echo "${test_calc}" | cut -d: -f3 | tr -d ' ') 

  echo "Initiating test: " $test_name
  echo "Reference test result: " $reference_result

  #Create and enter sub-directory in testing range
  mkdir -p $testing_dir/$test_range_dir/$test_name
  cd $testing_dir/$test_range_dir/$test_name

  if [ $? -ne 0 ]; then
    echo "Could not enter testing directory for some reason...Aborting!"
    exit 4
  fi

  #Copy input files to testing-range-subdir
  cp $test_input_dir/$test_name/control.in .
  cp $test_input_dir/$test_name/geometry.in .

  #Execute test as remote job
  #salloc -N1 -n"$number_tasks" mpiexec -np "$number_tasks" $path_to_aims_exe > out.cc4aims
  salloc -N1 -n"$number_tasks" mpiexec $path_to_aims_exe > out.cc4aims
  
  #Test for success-string
  if grep -q "Have a nice day" out.cc4aims; then
    #FHI-aims + CC-aims finished successfully
    mkdir CC4S-MP2
    cp *yaml CC4S-MP2
    #cp EigenEnergies.dat CC4S-MP2
    #cp CoulombVertex.dat CC4S-MP2
    #In release-version all datafiles are suffixed by ".elements"
    cp EigenEnergies.elements CC4S-MP2
    cp CoulombVertex.elements CC4S-MP2

    if [ -f "FockOperator.elements" ]; then
      #If the test was a non-canonical (non-HF) calculation
      #do the following
      #cp FockOperator.dat CC4S-MP2
      cp FockOperator.elements CC4S-MP2
      cp $aux_files_dir/aux_files/cc4s/pt2.yaml CC4S-MP2

      cd CC4S-MP2

      salloc -N1 -n"$number_tasks" mpiexec "$path_to_cc4s_exe" -i pt2.yaml
    else
      cp $aux_files_dir/aux_files/cc4s/mp2.yaml CC4S-MP2

      cd CC4S-MP2

      salloc -N1 -n"$number_tasks" mpiexec "$path_to_cc4s_exe" -i mp2.yaml
    fi

    #cd CC4S-MP2
    #salloc -N1 -n"$number_tasks" mpiexec -np "$number_tasks" "$path_to_cc4s_exe" -i mp2.yaml
    #salloc -N1 -n"$number_tasks" mpiexec "$path_to_cc4s_exe" -i mp2.yaml
    #grep MP2-energy from cc4s.out
    if grep -q "correlation" cc4s.out.yaml; then
      current_EMP2=$(grep 'correlation' cc4s.out.yaml | cut -d: -f2 | tr -d ' ')
      deviation=$(echo "$current_EMP2 - $reference_result" | bc -l | tr -d "-")
      if (( $(echo "$deviation<10^-6" | bc -l) )); then
        test_status='SUCCESS'
      else
        test_status='FAILED'
      fi
    else
      current_EMP2='NaN'
      deviation='NaN'
      test_status='FAILED'
    fi

  else
    echo "Test failed...Skipping."
    current_EMP2='NaN'
    deviation='NaN'
    test_status='FAILED'
  fi 

  #Append test-report
  echo '|' $test_name '|' $current_EMP2 '|' $reference_result '|' $deviation '|' $test_status '|' | tee -a $test_report

  #Append to success array
  success_dict["$test_name"]+="$test_status"
  cd $testing_dir

done

#Write short summary and determine if
#all together the tests were passed
#satisfactorily
failCount=0
successCount=0
for i in "${!success_dict[@]}"; do
  echo "Test result of ${i}: " ${success_dict[$i]}
  if [ "${success_dict[$i]}" = "SUCCESS" ]; then
    successCount=$((successCount+1)) 
  else
    failCount=$((failCount+1))
  fi 
done

echo "Tests passed successfully: " $successCount
echo "Tests failed:              " $failCount

if [ $failCount -gt 0 ]; then
  #CI/CD not passed
  exit 5
fi 

##Clean up
#rm -r $testing_dir/$test_range_dir/


