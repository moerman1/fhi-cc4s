xc                       hf
frozen_core_postSCF      1
RI_method                LVL
k_grid                   3  1  1
restart                  restart
symmetry_reduced_k_grid  .false.
output cc4s
relativistic             none
occupation_type          gaussian 0.001
mixer                    pulay
KS_method                parallel
n_max_pulay              10
charge_mix_param         0.2
sc_accuracy_rho          1E-6
sc_accuracy_eev          1E-4
sc_accuracy_etot         1E-6
sc_iter_limit            200
override_illconditioning .true.
charge                  0
################################################################################
#
#  FHI-aims code project
#
#  Igor Ying Zhang, Fritz Haber Institute Berlin, 2014
#
#  Suggested "loc-NAO-VCC-nZ" defaults for Li atom (to be pasted into control.in file)
#
################################################################################
  species        Li
#     global species definitions
    nucleus             3
    mass                6.941
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-4
#     
    radial_base         29 7.0
    radial_multiplier   2
    angular_grids       specified
      division   0.4484  110
      division   0.5659  194
      division   0.6315  302
      division   0.6662  434
      division   0.8186  590
#      division   0.9037  770
#      division   6.2760  974
#      outer_grid   974
      outer_grid  770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   1.
#     ion occupancy
    ion_occ      1  s   2.
################################################################################

 basis_acc              1e-05
#================================================
# Optimization in     Li for       loc-NAO-VCC-2Z
# RI-V prodbas generation parameter in Opt.: 
#     1) "prodbas_threshold       1.e-5"
#     2) "default_prodbas_acc     1.e-4"
# RPA total energy :                -xxxx.xxxx eV
#================================================
 for_aux hydro 4 f 3.0
# (sp) correlation set
 hydro     1     s      2.85927373
 hydro     2     p      1.30350218
# polarization set
 hydro     3     d      2.50781471
################################################################################
#
#  FHI-aims code project
#
#  Igor Ying Zhang, Fritz Haber Institute Berlin, 2014
#
#  Suggested "loc-NAO-VCC-nZ" defaults for F atom (to be pasted into control.in file)
#
################################################################################
  species        F
#     global species definitions
    nucleus             9
    mass                18.9984032
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-4
#
    radial_base         37 7.0
    radial_multiplier   2
    angular_grids       specified 
      division   0.4014  110
      division   0.5291  194
      division   0.6019  302
      division   0.6814  434
      division   0.7989  590
#      division   0.8965  770
#      division   1.3427  974
#      outer_grid  974
      outer_grid  770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   5.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   4.
################################################################################

 basis_acc              1e-05
#================================================
# Optimization in      F for       loc-NAO-VCC-2Z
# RI-V prodbas generation parameter in Opt.: 
#     1) "prodbas_threshold       1.e-5"
#     2) "default_prodbas_acc     1.e-4"
# RPA total energy :                -xxxx.xxxx eV
#================================================
 for_aux hydro 4 f 3.0
# (sp) correlation set
 hydro     1     s      1.01362478
 hydro     2     p      2.95599721
# polarization set
 hydro     3     d      9.89078447
################################################################################
#
#  FHI-aims code project
#
#  Igor Ying Zhang, Fritz Haber Institute Berlin, 2014
#
#  Suggested "loc-NAO-VCC-nZ" defaults for H atom (to be pasted into control.in file)
#
################################################################################
  species        H
#     global species definitions
    nucleus             1
    mass                1.00794
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-4
#     
    radial_base         24 7.0
    radial_multiplier   2
    angular_grids       specified
      division   0.1930   50
      division   0.3175  110
      division   0.4293  194
      division   0.5066  302
      division   0.5626  434
      division   0.5922  590
#      division   0.6227  974
#      division   0.6868 1202
      outer_grid  770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      1  s   1.
#     ion occupancy
    ion_occ      1  s   0.5
################################################################################


 basis_acc              1e-05
#================================================
# Optimization in      H for       loc-NAO-VCC-2Z
# RI-V prodbas generation parameter in Opt.: 
#     1) "prodbas_threshold       1.e-5"
#     2) "default_prodbas_acc     1.e-4"
# RPA total energy :                -xxxx.xxxx eV
#================================================
 for_aux hydro 4 f 3.0
# (sp) correlation set
 hydro     1     s      1.23411101
 hydro     2     p      2.94900728
# polarization set
