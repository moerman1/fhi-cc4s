---
title: 'CC4aims: An interface to high-performance periodic coupled-cluster theory calculations with atom-centered, localized basis functions'
tags:
  - Fortran
  - Materials science
  - Quantum chemistry
  - High-performance
  - Periodic Coupled Cluster
authors:
  - name: Evgeny Moerman
    affiliation: 1
  - name: Matthias Scheffler 
    affiliation: 1
affiliations:
 - name: Fritz Haber Institute of the Max Plank Society, Berlin, Germany
   index: 1
date: 10 February 2021
bibliography: paper.bib
---

# Summary

A major part of computational materials science and 
computational chemistry concerns calculations of total energies 
and energy differences of poly-atomic systems. 
Currently, the most prevalent method for such 
computations is density-functional theory [@Hohenberg:1964] 
(DFT) based on the Kohn-Sham formalism [@Kohn:1965] (KS)
together with one of the numerous exchange-correlation (xc) 
approximations [@Sousa:2007]. 
The trade-off between comparably low 
computational cost and often reliably accurate results 
render it the dominant method in the field.
Often, however, the accuracy of the xc approximations 
is not sufficent or uncertain, 
in particular when electronic correlations play a 
decisive role [@Bulik:2015; @Zhang:2016; @Zhang:2019].  
Coupled-cluster [@Cizek:1966] (CC) methods, while substantially more 
computationally expensive have proven to be significantly more 
accurate and reliable, at least for molecules and non-metallic solids.
Indeed, in molecular quantum chemistry the CC approach is considered
the gold standard for a theoretical description of binding energies and
electronic properties [@Chan:2019]. It is typically used (at least)
for benchmark studies.
The approach is limited, however, to systems with 50 electrons [@Feller:2001] or less.
The extension of CC to periodic systems [@Hirata:2004] has been explored to a great degree
by the research groups 
of [Andreas Grüneis](http://cqc.itp.tuwien.ac.at/code.html) and [Garnet Chan](https://pyscf.org/).

In the work of Andreas Grüneis the periodic formulation of CC is often applied to 
study the adsorption behaviour and surface chemistry of 2-dimensional materials 
including graphene [@Al:2017] and boron-nitride [@Brandenburg:2019] and surfaces, 
were it was repeatedly shown that CC yields consistently accurate 
adsorption energies and reaction energetics. 
The work of Garnet Chan et al. in this respect addresses the electronic properties of 2- and 3-dimensional
materials. By applying the periodic equation-of-motion (EOM) CC formalism, band paths, optical spectra and band gaps
of various materials (diamond, silicon, nickel oxide and others) have been investigated.

It is important to not that all the periodic CC calculations published so far 
utilized pseudopotentials [@Goedecker:1996] to approximate the effect of core electrons. 
In most cases, the inclusion of core electrons in a standard CC calculation does not have a notable
effect on the result. The mere approximation of core electrons during the preceding 
Hartree-Fock (HF) or KS-DFT calculation, however, can lead to significant errors in the resulting 
energies and states [@Kiejna:2006] used for the subsequent CC calculation.
One notable distinction between the works of the two groups, is the type of basis set used. 
While the [CC4S](http://cqc.itp.tuwien.ac.at/code.html) code of Andreas Grüneis is interfaced to VASP [@Kresse:1993] and therefore uses a plane-wave basis,
the group of Garnet Chan perform their calculations using PySCF [@Sun:2018], 
which is based on gaussian basis sets. While the plane-wave approach is well-established in calculations
of extended, periodic systems, localized atom-centred basis sets like gaussian orbitals or 
numerical atomic orbitals (NAO) [@Blum:2009] can potentially decrease the computational cost. This is mostly
due to the locality and sparsity of the overlap between basis functions, which decreases the
number of basis functions necessary for the description of the system. 

The ``CC4aims`` interface provides the link between all-electron 
electronic-structure codes with atom-centered
localized basis functions and the "Coupled Cluster theory for solids" code
([CC4S](http://cqc.itp.tuwien.ac.at/code.html)) developed by the group of
Andreas Grüneis.
The interface is formulated in a general way and demonstrated here for 
the FHI-aims code [@Blum:2009]. A generalization to other codes with atom-centered 
basis functions is straight forward, as long as the code uses a resolution-of-identity
approach to represent four-orbital Coulomb integrals [@Ren:2012], and also for the plane 
wave approach the generalization is possible which has been formulated separately [@Hummel:2017]. 
This interface expands the power of the mentioned electronic-structure theory codes to a variety of 
correlated methods for periodic systems, in particular. 
These include, amongst others, Møller-Plesset perturbation theory to second order (MP2), coupled-cluster theory
including single and double excitations (CCSD) and CCSD including the perturbative treatment of triple excitations
(CCSD(T)).
The central computationally involved task provided by the interface is the calculation of the 
Coulomb vertex [@Hummel:2017]. This quantity constitutes a 3-dimensional tensor and is a 
low-rank approximation of the 4-dimensional tensor of four-orbital Coulomb integrals.
For CC methods, as for HF or many other correlated methods, Coulomb integrals are a central quantity.
In contrast to HF which only requires the evaluation of occupied orbitals, CC theories additionally 
rely on a accurate description of unoccupied orbitals, of which there are usually 10 times more than 
occupied orbitals. Hence, the number of Coulomb integrals necessary to yield accurate CC results, 
grows rapidly with the system size and therefore the Coulomb integrals usually constitute the memory
bottleneck of a CC calculation. By approximating the 4-dimensional tensor using the Coulomb vertex, 
the memory overhead can be greatly reduced while loosing virtually no accuracy. 
``CC4aims`` can be used by any software package as long
as it uses a localized basis set and employs a localized
resolution-of-identity scheme [@Ren:2012] (RI) for the computation
of co-densities, which includes programs like the ADF [@Forster:2020] and
ABACUS [@Lin:2020] package. 
For other codes, which employ a different, possibly non-local RI scheme, the 
Coulomb vertex can still be calculated using the same fundamental algorithm 
as is implemented in ``CC4aims``. However, as for HF (or other non-local xc functionals)
FHI-aims only allows periodic calculations in combination with a localized RI scheme (RI-LVL),
``CC4aims`` naturally expects quantities calculated with RI-LVL.
This requirement imposed by FHI-aims is motivated by the fact that standard, non-local RI schemes 
feature a quadratic computational scaling with system-size. As periodic calculations typically 
take more atoms into account than cluster calculations, the utilization of non-local RI-schemes would
lead to a major bottleneck with respect to memory and computation. While in a localized RI-scheme only pairs
of basis functions need to be considered which are close to each other, in a non-local scheme every single 
pair of two basis functions needs to be taken into account.

# Statement of need
Currently, the only other publicly available implementation of periodic 
CC methods can be found in PySCF [@Sun:2020]. 
Hence, the benefit of a standalone 
interface to CC4S is twofold. For once, CC4S is indifferent to the 
source of HF or KS-DFT quantitites it requires. Most codes using either 
a plane-wave basis or a localized basis set can interface to CC4S. 
In the former case, a different approach to calculate the Coulomb 
vertex has to be taken, which is described in [@Hummel:2017]. 
For Quantum chemistry programs employing a localized basis additionally 
the use of an RI scheme has to be ensured. Depending on the RI 
approximation used, these programs can either use CC4aims 
directly or adapt it slightly. CC4aims and thereof derived interfaces 
allow software packages which either lack certain Quantum chemistry 
algorithms completely or which only offer insufficiently optimized 
implementations easy access to these methods. In addition to that only 
a handful of articles on periodic Coupled Cluster theory on chemical 
[@Brandenburg:2019; @Tsatsoulis:2018]
and electronic properties [@Gao:2020; @Mcclain:2017] of solids 
and surfaces have been published. 
Interfaces like CC4aims will accelerate the research done in that area 
substantially by allowing many electronic structure codes to participate 
in these investigations without the time-consuming effort of implementing
these methods. 
In addition, CC is routinely used to benchmark other approximate methods.
While there the focus has been on molecules, periodic CC can be used analogously 
to assess the performance with respect to extended systems. 
Clearly, the access to benchmark approximate treatments by periodic CC would advance
the field significantly as such benchmarks are urgently needed for surface reconstructions, 
energy barriers of diffusion processes and novel materials in general. 

# References

