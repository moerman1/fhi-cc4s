---
title: 'CC4aims: An interface to high-performance periodic Coupled Cluster calculations with localized basis functions'
tags:
  - Fortran
  - Materials science
  - Quantum chemistry
  - High-performance
  - Periodic Coupled Cluster
authors:
  - name: Evgeny Moerman
    affiliation: 1
affiliations:
 - name: Matthias Scheffler, Fritz-Haber-Institute
date: 10 February 2021
bibliography: paper.bib
---

# Summary

Chemical and physical processes are fundamentally governed by 
the energy differences between the initial system, potential 
transition states and the end point of the process. For a 
chemical reaction or the adsorption of a molecule on a surface, 
knowledge of these quantitites allows to infer the kinetics, 
the likelihood and the efficiency of such a transformation. 
Hence, a major part of Computational materials science and 
Computational chemistry is the calculation of these energies 
and energy differences based on theoretical 
models, approximating the quantum-mechanical nature of atoms 
and molecules. Currently, the most popular method for such 
computations is density functional theory (DFT) based on the 
Kohn-Sham formalism (KS). The trade-off between comparably low 
computational cost and often qualitatively accurate results 
renders it the dominant method in the field.
Often, however, the accuracy of KS-DFT [@Hohenberg:1964] is not sufficent, 
particularly when long-range dispersion interactions or strong 
electronic correlation are present. Especially in the age of big 
data and machine learning, which is increasingly used in Materials 
Science to predict properties of novel materials, accurate data is 
crucial to ensure the reliability of the trained model. 
The Coupled Cluster (CC) methods, while substantially more 
computationally expensive have proven to be significantly more 
accurate and reliable than DFT. 

``CC4aims`` is an interface between the "Fritz-Haber-Institute 
ab-initio molecular simulation" package (FHI-aims) and the 
"Coupled Cluster theory for solids" code (CC4S) developed by Felix 
Hummel and Andreas Grüneis. The interface gives access to a variety 
of explicitely correlated methods for periodic systems. Massively 
parallelized, highly efficient implementations of methods including 
MP2, drCCD, CCSD, CCSD(T) can be used for calculations on top of a 
periodic Hartree-Fock (HF) or a KS-DFT calculation in FHI-aims, 
without any additional implementation effort of these methods. 
The central task of CC4aims is to calculate the Coulomb vertex, 
a low-rank factorization of the Coulomb integrals array, needed by 
most correlated methods. Even though the input the interface expects 
is geared towards the quantities and formats used by FHI-aims, CC4aims 
can be used by any other quantum chemistry software package as long 
as it uses a localized basis set and employs a localized 
resolution-of-identity scheme (RI) for the computation of co-densities, 
which includes programs like the ADF and ABACUS package. The  latter 
requirement is neccesary due to the quadratic computational scaling of 
non-local RI approaches, which become a major bottleneck for big 
and/or periodic calculations. Finally, CC4aims can be considered a 
template which can be used by other codes for the implementation of 
an own CC4S interface. As long as the electronic structure code utilises 
a localized basis set and a RI scheme, the fundamental algorithm for 
the computation of the Coulomb vertex in CC4aims can be transferred 
with minor modifications.

# Statement of need
Currently, the only other publicly available implementation of periodic 
CC methods can be found in PySCF. Hence, the benefit of a standalone 
interface to CC4S is twofold. For once, CC4S is indifferent to the 
source of HF or KS-DFT quantitites it requires. Most code using either 
a plane-wave basis or a localized basis set can interface to CC4S. 
In the former case, a different approach to calculate the Coulomb 
vertex has to be taken, which is described in "INSERT LOW FACTORIZATION". 
For Quantum chemistry programs employing a localized basis additionally 
the use of an RI scheme has to be ensured. Depending on the RI 
approximation used, these programs can either use CC4aims 
directly or adapt it slightly. CC4aims and thereof derived interfaces 
allow software packages which either lack certain Quantum chemistry 
algorithms completely or which only offer insufficiently optimized 
implementations easy access to these methods. In addition to that only 
a handful of articles on periodic Coupled Cluster theory on chemical 
and electronic properties of solids and surfaces hav been published. 
Interfaces like CC4aims will accelerate the research done in that area 
substantially by allowing many electronic structure codes to participate 
in these investigations without the time-consuming effort of implementing
these methods. 

# Acknowledgements

We acknowledge contributions from Brigitta Sipocz, Syrtis Major, and Semyeong
Oh, and support from Kathryn Johnston during the genesis of this project.

# References

