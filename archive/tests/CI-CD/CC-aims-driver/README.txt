This directory should contain a .f90-file, which initiates/interfaces CC-aims.
In this respect the program/script should

1) Read in the matrices/arrays usually obtained from a FHI-aims calculation
2) Read in other relevant quantities from some kind of test-specific input file
3) Launch CC-aims to obtain the CoulombVertex and other files necessary for CC4S
