program driver
  use mpi
  use CC4S_main_interface, only: CC4S_interface_driver, CC4S_init_interface
  implicit none
  
  character(len=200) :: testName
  character(len=200) :: FhiFilesPath
  integer :: testInputStatus
  integer :: inputFileUnit
  integer :: ioerr

  !input relevant for CC-aims/output of parseInput-subroutine
  integer :: n_states, n_basbas, n_basis, n_atoms, n_species, n_k_points
  integer :: n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, n_cells
  logical :: real_eigenvectors
  integer :: n_low_state
  integer, dimension(:), allocatable :: basbas_atom 
  integer, dimension(:), allocatable :: species
  integer, dimension(:), allocatable :: sp2n_basis_sp
  integer, dimension(:), allocatable :: atom2basbas_off
  integer, dimension(:), allocatable :: sp2n_basbas_sp
  integer, dimension(:), allocatable :: atom2basis_off
  integer, dimension(:,:), allocatable :: k_point_loc
  integer, dimension(:,:), allocatable :: kpq_point_list
  integer, dimension(:), allocatable :: lb_atom
  integer, dimension(:), allocatable :: ub_atom
  integer :: singval_num

  !output-quantities of CC4S_init_interface
  integer :: CC4S_lbb_row, CC4S_ubb_row, CC4S_lbb_col, CC4S_ubb_col
  integer :: CC4S_loc_bb_row, CC4S_loc_bb_col, CC4S_myid_col, CC4S_npcol
  integer :: CC4S_myid
  logical :: CC4S_member
  integer :: CC4S_n_tasks, CC4S_comm

  !MPI
  integer :: mpierr, global_mpi_id

  double precision, dimension(:,:,:,:), allocatable :: KS_eigenvector
  double complex, dimension(:,:,:,:), allocatable :: KS_eigenvector_cmplx
  double precision, dimension(:,:,:), allocatable :: KS_eigenvalues
  double complex, dimension(:,:,:,:,:), allocatable :: RI_LVL_coefficients
  double complex, dimension(:,:,:), allocatable :: auxiliaryCoulombMatrix
  double precision, dimension(:,:), allocatable :: fockMatrix
  double complex, dimension(:,:), allocatable :: fockMatrix_cmplx

  double precision, dimension(:,:,:,:), allocatable :: KS_eigenvector_loc
  double complex, dimension(:,:,:,:), allocatable :: KS_eigenvector_cmplx_loc
  double complex, dimension(:,:,:), allocatable :: auxiliaryCoulombMatrix_loc

  integer :: i_k_point, i_spin, i_state, i_basis, i_basbas, i_basbas_2
  integer :: i_basis_sp1, i_basis_sp2
  !To calculate the number of atom-pairs a given task is responsible for
  integer :: n_c3fn, n_c3fn_task, rest, myoffset
  integer :: lb_ap, ub_ap, n_ap, i_ap

  integer :: n_k_points_loc
  double complex, dimension(:,:,:,:,:), allocatable :: RI_V_coefficients
  logical :: debug
  integer :: j

  call MPI_Init(mpierr)

  call get_command_argument(1, testName)

  testName = adjustl(trim(testName))
  write(*,*) 'Initiating test: ', testName

  FhiFilesPath = "../FHI-aims-Input/"//testName

  write(*,*) 'Reading in FHI-aims input from: ',  FhiFilesPath

  !The calculation data necessary for CC4S_init_interface
  !is expected to be in a keyword-based input-file called
  !input-test.in


  write(*,*) trim(FhiFilesPath)//'/input-test.in'
  inputFileUnit = 123
  open(unit=inputFileUnit, file=trim(FhiFilesPath)//'/input-test.in', action='read', status='old', iostat=testInputStatus)

  if(testInputStatus .ne. 0) then
    write(*,*) 'No input-test.in file found in ', FhiFilesPath
  else
    call parseInput(inputFileUnit, &
                  &n_states, n_basbas, n_basis, n_atoms, n_species, n_k_points, &
                  &n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, &
                  &n_cells, real_eigenvectors, n_low_state, basbas_atom, &
                  &species, sp2n_basis_sp, atom2basbas_off, sp2n_basbas_sp, &
                  &atom2basis_off, k_point_loc, kpq_point_list, lb_atom, ub_atom, &
                  &singval_num)
  end if 

  close(123)

  call CC4S_init_interface(n_states, n_basbas, n_basis, n_atoms, n_species, n_k_points, &
          &n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, n_cells, real_eigenvectors, &
          &n_low_state, basbas_atom, species, sp2n_basis_sp, atom2basbas_off, sp2n_basbas_sp, &
          &atom2basis_off, k_point_loc, kpq_point_list, lb_atom, ub_atom, &
          &MPI_COMM_WORLD, CC4S_lbb_row, CC4S_ubb_row, CC4S_lbb_col, CC4S_ubb_col, &
          &CC4S_loc_bb_row, CC4S_loc_bb_col, CC4S_myid_col, CC4S_npcol, &
          &CC4S_myid, CC4S_member, CC4S_n_tasks, CC4S_comm)


  !Read Kohn-Sham eigenvectors from file Eigenvectors.dat
  if(real_eigenvectors) then
    allocate(KS_eigenvector(n_basis, n_states, n_spin, n_k_points))
    allocate(KS_eigenvector_cmplx(1,1,1,1))
    KS_eigenvector(:,:,:,:) = 0.0D0
    open(unit=443, file=trim(FhiFilesPath)//'/Eigenvectors.dat', &
            &form='unformatted', action='read', status='old')
    
!    do i_k_point=1,n_k_points
!      do i_spin=1,n_spin
!        do i_state=1,n_states
!          do i_basis=1,n_basis
!            read(443) KS_eigenvector(i_basis, i_state, i_spin, i_k_point)
!          end do
!        end do
!      end do
!    end do
    read(443) KS_eigenvector
  else
    allocate(KS_eigenvector_cmplx(n_basis, n_states, n_spin, n_k_points))
    allocate(KS_eigenvector(1,1,1,1))
    KS_eigenvector_cmplx(:,:,:,:) = (0.0D0, 0.0D0)
    open(unit=443, file=trim(FhiFilesPath)//'/Eigenvectors.dat', &
            &form='unformatted', action='read', status='old')

!    do i_k_point=1,n_k_points
!      do i_spin=1,n_spin
!        do i_state=1,n_states
!          do i_basis=1,n_basis
!            read(443) KS_eigenvector_cmplx(i_basis, i_state, i_spin, i_k_point)
!          end do
!        end do
!      end do
!    end do
    read(443) KS_eigenvector_cmplx
  end if
  close(443) 

  !Read Kohn-Sham eigenvalues from file EigenEnergies.dat
  allocate(KS_eigenvalues(n_states, n_spin, n_k_points))
  open(unit=444, file=trim(FhiFilesPath)//'/EigenEnergies.dat', &
          &form='formatted', action='read', status='old')
  write(*,*) 'n_k_points : ', n_k_points
  write(*,*) 'n_spin : ', n_spin
  write(*,*) 'n_states : ', n_states
  do i_k_point=1,n_k_points
    do i_spin=1,n_spin
      do i_state=1,n_states
        read(444,*) KS_eigenvalues(i_state, i_spin, i_k_point)
      end do
    end do
  end do

  !Read RI-Coefficients from file RILVLCoefficients.dat
  n_c3fn=n_atoms*n_atoms*(n_cells+1)
  n_c3fn_task=n_c3fn/CC4S_n_tasks
  rest=n_c3fn-CC4S_n_tasks*n_c3fn_task
  if(CC4S_myid .lt. rest) then
    n_c3fn_task=n_c3fn_task+1
    myoffset=CC4S_myid*n_c3fn_task
  else
    myoffset=CC4S_myid*n_c3fn_task+rest
  end if

  !corresponding lower and upper bound for atom pairs for this task
  lb_ap=myoffset/(n_cells+1)
  ub_ap=(myoffset+n_c3fn_task-1)/(n_cells+1)
  n_ap=ub_ap-lb_ap+1

  allocate(RI_LVL_coefficients(max_n_basis_sp, max_n_basis_sp, max_n_basbas_sp, &
          &n_k_points, n_ap))
  open(unit=445, file=trim(FhiFilesPath)//"/RILVLCoefficients.dat", &
          &form='unformatted', action='read', status='old')

  write(*,*) 'Reading', n_ap*n_k_points*max_n_basbas_sp*max_n_basis_sp*max_n_basis_sp, &
          &'elements from RILVLCoefficients.dat'
!  do i_ap=1,n_ap
!    do i_k_point=1,n_k_points
!      do i_basbas=1,max_n_basbas_sp
!        do i_basis_sp1=1,max_n_basis_sp
!          do i_basis_sp2=1,max_n_basis_sp
!            read(445,iostat=ioerr) RI_LVL_coefficients(i_basis_sp2, i_basis_sp1, i_basbas, i_k_point, i_ap)
!            if(ioerr < 0) then
!             write(*,*) 'EOF at element', i_ap*i_k_point*i_basbas*i_basis_sp1*i_basis_sp2
!            else if(ioerr > 0) then
!             write(*,*) 'ioerr = ', ioerr
!            end if 
!          end do
!        end do
!      end do
!    end do
!  end do 
  read(445) RI_LVL_coefficients
  close(445)
  !Read auxliary Coulomb Matrix from file AuxiliaryCoulombMatrix.dat
  allocate(auxiliaryCoulombMatrix(n_basbas, n_basbas, n_kq_points))
  open(unit=446, file=trim(FhiFilesPath)//"/AuxiliaryCoulombMatrix.dat", &
          &form='unformatted', action='read', status='old')
!  do i_k_point=1,n_k_points
!    do i_basbas=1,n_basbas
!      do i_basbas_2=1,n_basbas
!        read(446) auxiliaryCoulombMatrix(i_basbas_2, i_basbas, i_k_point)  
!      end do
!    end do
!  end do
  read(446) auxiliaryCoulombMatrix
  close(446)


  !Assign the respective blocks of auxiliaryCoulombMatrix, and KS_eigenvector(_cmplx)
  !auxiliaryCoulombMatrix -> auxiliaryCoulombMatrix_loc
  !KS_eigenvector         -> KS_eigenvector_loc
  !KS_eigenvector_cmplx   -> KS_eigenvector_cmplx_loc

  allocate(auxiliaryCoulombMatrix_loc(CC4S_ubb_row-CC4S_lbb_row+1, &
          &CC4S_ubb_col-CC4S_lbb_col+1, n_kq_points))

  write(*,*) 'shape(auxiliaryCoulombMatrix_loc) : ', shape(auxiliaryCoulombMatrix_loc)
  write(*,*) 'shape(auxiliaryCoulombMatrix) : ', shape(auxiliaryCoulombMatrix)
  auxiliaryCoulombMatrix_loc(:,:,:) = auxiliaryCoulombMatrix(CC4S_lbb_row:CC4S_ubb_row, CC4S_lbb_col:CC4S_ubb_col,:)
  deallocate(auxiliaryCoulombMatrix)

  !Redistribute KS_eigenvector(_cmplx) via k_point_loc-mapping
  !Determine number of k-points at a given mpi task
  call MPI_Comm_rank(MPI_COMM_WORLD, global_mpi_id, mpierr)
  n_k_points_loc = 0
  do i_k_point=1,n_k_points
    if(k_point_loc(1,i_k_point) .eq. global_mpi_id) then
      n_k_points_loc = n_k_points_loc + 1
    end if 
  end do

  
  do j=1,2
    do i_k_point=1,n_k_points
      write(*,*) 'k_point_loc(', j,',',i_k_point,') = ', k_point_loc(j,i_k_point)
    end do
  end do
  if(real_eigenvectors) then
    allocate(KS_eigenvector_loc(n_basis, n_states, n_spin, n_k_points_loc))
    do i_k_point=1,n_k_points
      if(global_mpi_id .eq. k_point_loc(1,i_k_point)) then
        KS_eigenvector_loc(:,:,:,k_point_loc(2,i_k_point)) = &
              &KS_eigenvector(:,:,:,i_k_point)
      end if 
    end do
    allocate(KS_eigenvector_cmplx_loc(1,1,1,1))
    KS_eigenvector_cmplx_loc(:,:,:,:) = (0.0D0, 0.0D0)
  else
    allocate(KS_eigenvector_cmplx_loc(n_basis, n_states, n_spin, n_k_points_loc))
    do i_k_point=1,n_k_points
      if(global_mpi_id .eq. k_point_loc(1,i_k_point)) then
        KS_eigenvector_cmplx_loc(:,:,:,k_point_loc(2,i_k_point)) = &
                &KS_eigenvector_cmplx(:,:,:,i_k_point)
      end if
    end do
    allocate(KS_eigenvector_loc(1,1,1,1))
    KS_eigenvector_loc(:,:,:,:) = 0.0D0
  end if

  deallocate(KS_eigenvector)
  deallocate(KS_eigenvector_cmplx) 


  !At this point all relevant array quantities should be distributed
  !like they are in FHI-aims
  !Time to launch the interface
  allocate(RI_V_coefficients(1,1,1,1,1))
  debug = .true.
  call CC4S_interface_driver(auxiliaryCoulombMatrix_loc, RI_LVL_coefficients, &
          &RI_V_coefficients, KS_eigenvector_cmplx_loc, KS_eigenvector_loc, KS_eigenvalues, &
          &MPI_COMM_WORLD, debug, singval_thr=-20.0D0, singval_num=singval_num)

  call MPI_Finalize(mpierr)
contains
  subroutine parseInput(fileUnit, &
                  &n_states, n_basbas, n_basis, n_atoms, n_species, n_k_points, &
                  &n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, &
                  &n_cells, real_eigenvectors, n_low_state, basbas_atom, &
                  &species, sp2n_basis_sp, atom2basbas_off, sp2n_basbas_sp, &
                  &atom2basis_off, k_point_loc, kpq_point_list, lb_atom, ub_atom, &
                  &singval_num)

    implicit none

    integer, intent(in) :: fileUnit
    integer, intent(out) :: n_states, n_basbas, n_basis, n_atoms, n_species, n_k_points
    integer, intent(out) :: n_kq_points, n_spin, max_n_basis_sp, max_n_basbas_sp, n_cells
    logical, intent(out) :: real_eigenvectors
    integer, intent(out) :: n_low_state
    integer, dimension(:), allocatable, intent(out) :: basbas_atom
    integer, dimension(:), allocatable, intent(out) :: species
    integer, dimension(:), allocatable, intent(out) :: sp2n_basis_sp
    integer, dimension(:), allocatable, intent(out) :: atom2basbas_off
    integer, dimension(:), allocatable, intent(out) :: sp2n_basbas_sp
    integer, dimension(:), allocatable, intent(out) :: atom2basis_off
    integer, dimension(:,:), allocatable, intent(out) :: k_point_loc
    integer, dimension(:,:), allocatable, intent(out) :: kpq_point_list
    integer, dimension(:), allocatable, intent(out) :: lb_atom
    integer, dimension(:), allocatable, intent(out) :: ub_atom
    integer, intent(out) :: singval_num

    integer :: key
    character(len=1000) :: inputLine
    character(len=100) :: inputKeyWord
    integer :: errcode
    integer :: i_basbas

    do key=1,1000
      read(fileUnit, "(A)", iostat=errcode) inputLine
      read(inputLine,*) inputKeyWord
      if (errcode < 0) then
        exit
      end if

      write(*,*) inputKeyWord
      select case(inputKeyWord)
        case("n_states")
          read(inputLine,*) inputKeyWord, n_states
          write(*,*) "n_states (from input) : ", n_states
        case("n_basbas")
          read(inputLine,*) inputKeyWord, n_basbas
        case("n_basis")
          read(inputLine,*) inputKeyWord, n_basis
        case("n_atoms")
          read(inputLine,*) inputKeyWord, n_atoms
        case("n_species")
          read(inputLine,*) inputKeyWord, n_species
        case("n_k_points")
          read(inputLine,*) inputKeyWord, n_k_points
        case("n_kq_points")
          read(inputLine,*) inputKeyWord, n_kq_points
        case("n_spin")
          read(inputLine,*) inputKeyWord, n_spin
        case("max_n_basis_sp")
          read(inputLine,*) inputKeyWord, max_n_basis_sp
        case("max_n_basbas_sp")
          read(inputLine,*) inputKeyWord, max_n_basbas_sp
        case("n_cells")
          read(inputLine,*) inputKeyWord, n_cells
        case("real_eigenvectors")
          read(inputLine,*) inputKeyWord, real_eigenvectors
        case("n_low_state")
          read(inputLine,*) inputKeyWord, n_low_state
        case("basbas_atom")
          allocate(basbas_atom(n_basbas))
          write(*,*) "size(basbas_atom) = ", size(basbas_atom)
          write(*,*) "inputLine = ", inputLine
          !read(fileUnit,*) inputKeyWord
          do i_basbas = 1, n_basbas
            read(fileUnit,*) basbas_atom(i_basbas)
          end do
          write(*,*) "basbas_atom (from input) : ", basbas_atom
        case("species")
          allocate(species(n_atoms))
          read(inputLine,*) inputKeyWord, species
          write(*,*) "species(:) = ", species
        case("sp2n_basis_sp")
          allocate(sp2n_basis_sp(n_species))
          read(inputLine,*) inputKeyWord, sp2n_basis_sp
        case("atom2basbas_off")
          allocate(atom2basbas_off(n_atoms))
          read(inputLine,*) inputKeyWord, atom2basbas_off
        case("sp2n_basbas_sp")
          allocate(sp2n_basbas_sp(n_species))
          read(inputLine,*) inputKeyWord, sp2n_basbas_sp
          write(*,*) "sp2n_basbas_sp(:) = ", sp2n_basbas_sp
        case("atom2basis_off")
          allocate(atom2basis_off(n_atoms))
          read(inputLine,*) inputKeyWord, atom2basis_off
        case("k_point_loc")
          allocate(k_point_loc(2,n_k_points))
          read(inputLine,*) inputKeyWord, k_point_loc
        case("kpq_point_list")
          allocate(kpq_point_list(n_k_points, n_k_points))
          read(inputLine,*) inputKeyWord, kpq_point_list
        case("lb_atom")
          allocate(lb_atom(n_atoms))
          read(inputLine,*) inputKeyWord, lb_atom
        case("ub_atom")
          allocate(ub_atom(n_atoms))
          read(inputLine,*) inputKeyWord, ub_atom
        case("singval_num")
          read(inputLine,*) inputKeyWord, singval_num
      end select
    end do


  end subroutine
end program driver
