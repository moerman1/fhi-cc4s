program out_energies

        use mpi
        implicit none

      integer :: ierr
      integer :: myid

      integer :: n_states, n_k_points, n_spin
      double precision, dimension(:,:,:), allocatable :: scf_e

      call MPI_Init(ierr)
      call MPI_Comm_rank(MPI_COMM_WORLD, myid, ierr)

      n_states=2021
      n_k_points=8
      n_spin=2
      if(myid .eq. 0) then
        allocate(scf_e(n_states, n_spin, n_k_points))
        call random_number(scf_e)
        call CC4S_output_scf_energies(scf_e, n_states, &
                &n_spin, n_k_points, 1, 'cc4s_energies', .false.)
      end if 
      call MPI_Finalize(ierr)

end program out_energies
