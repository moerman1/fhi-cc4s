program test_sqrt
      use mpi
      use iso_c_binding, only: c_ptr, c_f_pointer 
      implicit none

      integer :: ierr
      integer :: n_tasks
      integer :: myid
      integer, parameter :: n_basbas=153

      double precision, dimension(:,:), allocatable :: matr
      integer(kind=MPI_ADDRESS_KIND) :: size_bytes
      type(c_ptr) :: cptr_matr
      double precision, dimension(:,:), pointer, contiguous :: fptr_matr => null()
      integer :: win_shm_matr
      integer :: disp_unit
      integer, dimension(2) :: win_shape

      integer, dimension(9) :: matr_desc
      integer :: lr, ur, lc, uc 

      double precision, dimension(:,:), allocatable :: matr_blocked
      integer, dimension(4) :: loc_matr_block_shape
      call MPI_Init(ierr)
      call MPI_Comm_size(MPI_COMM_WORLD, n_tasks, ierr)
      call MPI_Comm_rank(MPI_COMM_WORLD, myid, ierr)

      !Let one process read in matrix
      if(myid .eq. 0) then
        allocate(matr(n_basbas, n_basbas))

        open(unit=211, file='data-files/coulmat.dat')
        read(211,'(E23.16)') matr
        close(unit=211)
      end if 
      
      !Create shared memory window 
      if(myid .eq. 0) then
        size_bytes=8*n_basbas**2
      else
        size_bytes=0
      end if 

      call MPI_Win_allocate_shared(size_bytes, 8, MPI_INFO_NULL, MPI_COMM_WORLD, &
              &cptr_matr, win_shm_matr, ierr)
      call MPI_Win_shared_query(win_shm_matr, 0, size_bytes, disp_unit, cptr_matr, ierr)
      win_shape=[n_basbas, n_basbas]
      call c_f_pointer(cptr_matr, fptr_matr, win_shape)
      call MPI_Win_lock_all(MPI_MODE_NOCHECK, win_shm_matr, ierr)

      if(myid .eq. 0) then
        fptr_matr=matr(:,:)
      end if 

      call mpi_win_sync(win_shm_matr,ierr)
      call MPI_Barrier(MPI_COMM_WORLD, ierr)

      !Initialize scalapack-like non-cyclic distribution of matrix matr
      call init_blacs(matr_desc, lr, ur, lc, uc)


      allocate(matr_blocked(lr:ur, lc:uc))
      matr_blocked(:,:)=fptr_matr(lr:ur, lc:uc)


      loc_matr_block_shape(1)=lr
      loc_matr_block_shape(2)=ur
      loc_matr_block_shape(3)=lc
      loc_matr_block_shape(4)=uc

      !Calculate square root and save it in matr_blocked
      if(n_tasks .eq. 1) then
        call CC4S_sqrt_aux_coulmat_real_lapack(matr_blocked, n_basbas, matr_desc, loc_matr_block_shape, MPI_COMM_WORLD, .true.)
      else
        call CC4S_sqrt_aux_coulmat_real_scalapack(matr_blocked, n_basbas, matr_desc, loc_matr_block_shape, MPI_COMM_WORLD, .true.)
      end if 

      call mpi_win_unlock_all(win_shm_matr, ierr)
      call mpi_win_free(win_shm_matr, ierr)

      call MPI_Finalize(ierr)
contains
      subroutine init_blacs(bb_desc, lbb_row, ubb_row, lbb_col, ubb_col)
              implicit none

              integer, dimension(9), intent(out) :: bb_desc
              integer, intent(out) :: lbb_row, ubb_row
              integer, intent(out) :: lbb_col, ubb_col

              integer :: ierr
              integer :: n_procs
              integer :: i
              integer :: nprow, npcol
              integer, dimension(2) :: cart2D_dims
              logical, dimension(2) :: cart2D_periodic
              integer :: cart2D_comm
              integer :: blockbb_row, blockbb_col
              integer :: myid_row, myid_col
              integer :: info
              integer :: loc_bb_row, loc_bb_col

              integer, external :: numroc

              !Find most square-like process grid

              call MPI_Comm_size(MPI_COMM_WORLD, n_procs, ierr)

              do i=int(sqrt(real(n_procs))), 1, -1
                if(mod(n_procs,i) .eq. 0) then
                        nprow=i
                        npcol=n_procs/nprow                 
                        exit       
                end if 
              end do

              !Initialize 2D cartesian communicator 
              cart2D_dims=[nprow, npcol]
              cart2D_periodic=[.false., .false.]
              call MPI_Cart_create(MPI_COMM_WORLD, 2, cart2D_dims, cart2D_periodic, .false., cart2D_comm, ierr)
              if(mod(n_basbas, nprow) .eq. 0) then
                blockbb_row=n_basbas/nprow
              else
                blockbb_row=n_basbas/nprow+1
              end if 

              if(mod(n_basbas, npcol) .eq. 0) then
                blockbb_col=n_basbas/npcol
              else
                blockbb_col=n_basbas/npcol+1
              end if 
      

              !Initialize blacs process grid and context
              call blacs_gridinit(cart2D_comm, 'r', nprow, npcol)
              call blacs_gridinfo(cart2D_comm, nprow, npcol, myid_row, myid_col)

              loc_bb_row=numroc(n_basbas, blockbb_row, myid_row, 0, nprow)
              loc_bb_col=numroc(n_basbas, blockbb_col, myid_col, 0, npcol)  

              call descinit(bb_desc, n_basbas, n_basbas, blockbb_row, blockbb_col, 0, 0, &
                      &cart2D_comm, max(1,loc_bb_row), info)

              lbb_row=myid_row*blockbb_row+1
              ubb_row=min(myid_row*blockbb_row+loc_bb_row, n_basbas)

              lbb_col=myid_col*blockbb_col+1
              ubb_col=min(myid_col*blockbb_col+loc_bb_col, n_basbas)
      end subroutine init_blacs

end program test_sqrt
