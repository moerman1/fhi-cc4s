## Dependencies
* A FORTRAN compiler (tested with gfortran version>7.5 and ifort version>19.1)
* A Message Passing Interface (MPI) implementation compatible with the compiler 
(MPI version>3.1). 
* Scalapack (including underlying Lapack and Blas)

## Installation
* Create a new directory: `mkdir cc-aims-dir`
* Enter directory: `cd cc-aims-dir`
* Clone repository: `git clone git@gitlab.com:moerman1/fhi-cc4s.git`
* Enter source directory: `cd src/`
* Compile CC-aims either using gfortran or ifort: `make` for gfortran or `make CONFIG=INTEL` for ifort
* The compilation should take a couple of seconds, after which both a shared library `libcc4aims.so` and a
static library `libcc4aims.a` can be found in the `src`-directory

