As CC-aims was originally developed to interface FHI-aims and CC4S, users of FHI-aims can
directly use CC-aims after linking it to FHI-aims during compilation.

## Compiling FHI-aims with CC-aims
Upon compilation of FHI-aims, include the path to CC-aims. If compiled via cmake-cache file 
(as is recommended), include the path to CC-aims in the LIBPATHS and INCPATHS environment variables:

* `set(LIBPATHS "/path/to/CC-aims" CACHE STRING "")`
* `set(INCPATHS "/path/to/CC-aims" CACHE STRING "")`

Add `cc4aims` to the LIBS environment variable:<br>
`set(LIBS "cc4aims name-of-other-lib-1 name-of-other-lib-2" CACHE STRING "")`

Finally, enable the linking to CC-aims:<br>
`set(USE_CC4AIMS ON CACHE BOOL "")`

## Running CC-aims in FHI-aims
In order to generate CC4S input after an FHI-aims run the `output cc4s` keyword must be added
to control.in. This alone will make CC-aims calculate and output all the data and yaml files required for
a CC4S calculation. 

In order to reduce the size of the Coulomb vertex, which can become quite big, 
additionally the keyword `cc4s_screen_thresh <thr>` can be added to control.in, 
where `<thr>` is a decimal number. This will invoke a principal component analysis (PCA) 
of the Coulomb vertex, during which eigenvalues smaller than `<thr>` will be discarded, 
thus reducing the size of the Coulomb vertex. 
By choosing the threshold carefully, it has been found that often over 50% of 
the eigenvalues can be thrown out without a relevant loss in accuracy.

Alternatively, the PCA can be enabled by adding the `cc4s_screen_num <n>`-keyword to control.in.
Here, `n` is an integer and will be the absolute number of eigenvalues kept during the PCA. 
If both, `cc4s_screen_thresh` and `cc4s_screen_num` are specified in control.in, `cc4s_screen_num` will
precede and `cc4s_screen_thresh` will be ignored.
