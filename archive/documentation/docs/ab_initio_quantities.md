## Quantities input to CC-aims

### The Fock matrix
The Fock matrix $\mathbf{F}$ denotes the matrix-representation of single particle
Hartree-Fock operator

$$
  f_{i}(\mathbf{r}) = -\frac{1}{2}\nabla_{i}^2 - \sum_{I}\frac{Z_{I}}{r_{i}-r_{I}}
  + \hat{J} - \hat{K}
$$

($\hat{J}$ and $\hat{K}$ being the Coulomb and exchange operator), in the 
space of converged molecular orbitals (MO) or - in the periodic case - 
Bloch states (BS). Hence, the Fock matrix elements are of the form

$$
  (\mathbf{F})_{jk} = 
  \langle \psi_{j}^{MO}(\mathbf{r_{i}})|f_{i}|\psi_{j}^{MO}(\mathbf{r_{i}})\rangle 
$$

or

$$
  (\mathbf{F})_{jk} =
  \langle \psi_{j}^{BS}(\mathbf{r_{i}})|f_{i}|\psi_{j}^{BS}(\mathbf{r_{i}})\rangle.
$$

If the self-consistent calculation performed is a HF calculation, the 
Fock matrix becomes diagonal, featuring the single-particle HF-energies
of the diagonal. If, however, the underlying SCF method is KS-DFT is not
diagonal anymore.

CC4S only requires the Fock matrix if a KS-DFT calculation has been performed,
as otherwise the information in the Fock matrix is already given by the
energy eigenvalues themselves.

### HF/KS-DFT energy eigenvalues and eigenvectors
Similar to the Fock matrix, the energy eigenvalues and eigenvectors 
of the SCF-calculation are part of the regular output of any established 
quantum chemistry code. The
eigenvalues are the solutions of the generalized eigenvalue problem, which 
needs to be solved in the HF or KS-DFT method:

$$
  \mathbf{H}\mathbf{C} = \mathbf{S}\mathbf{C}\mathbf{\epsilon}, 
$$ 

where $\mathbf{H}$ is the Fock matrix in the representation of
atomic orbitals (AO), $\mathbf{C}$ the eigenvector matrix, 
$\mathbf{S}$ the overlap matrix of the AOs and $\mathbf{\epsilon}$
the diagonal energy eigenvalue matrix.

###Fourier-transformed RI-expansion coefficents
The representation of a product of Bloch basis functions as an expansion of 
RI-basis functions is given by:

$$
  \phi_{i\mathbf{k}}^{*}(\mathbf{r})\phi_{k\mathbf{q}}^{*}(\mathbf{r}) = 
  \sum_{\mu} C_{ik}^{\mu}(\mathbf{k}, \mathbf{q})P_{\mu}^{\mathbf{q}-\mathbf{k}}
$$

If a calculation with a non-local RI-scheme is requested by CC-aims, the RI-expansion
coefficients $C_{ik}^{\mu}(\mathbf{k}, \mathbf{q})$ need to be submitted directly
to the interface.

However, in the case of the local RI-LVL scheme, 
it has been shown by [Levchenko et al.](https://www.sciencedirect.com/science/article/abs/pii/S001046551500079X) that $C_{ik}^{\mu}(\mathbf{k}, \mathbf{q})$ become separable in
reciprocal space:

$$
  C_{ik}^{\mu}(\mathbf{k}, \mathbf{q}) = 
    C_{ik}^{\mu}(-\mathbf{k}, \mathbf{R_{0}}) + C_{ki}^{\mu}(\mathbf{q}, \mathbf{R_{0}}),
$$

where

$$
  C_{ik}^{\mu}(\mathbf{k}) 
  = C_{ik}^{\mu}(\mathbf{k}, \mathbf{R_{0}}) 
  = \sum_{\mathbf{R}} e^{i\mathbf{k}\mathbf{R}}C_{i(\mathbf{R})
  k(\mathbf{R_{0}})}^{\mu(\mathbf{R_{0}})}
$$

Here, $C_{i(\mathbf{R})k(\mathbf{R_{0}})}^{\mu(\mathbf{R_{0}})}$ denotes
the expansion coefficient of the RI-basis function $P_{\mu}(\mathbf{r})$ of the
central unit cell (signified by the real-space lattice vector $\mathbf{R_{0}}$)
in the representation of the product of the $i$-th atomic basis function in
unit cell $\mathbf{R}$ and the $k$-th atomic basis function in the central unit cell
$\mathbf{R_{0}}$. Due to the translational symmetry of the general, periodic case,
it sufficies to only look at basis function overlap between the central unit cell
and adjacent unit cells.

Hence, if starting from a RI-LVL scheme, it suffices to provide CC-aims with
C_{ik}^{\mu}(\mathbf{k}), which can save significant amounts of memory.

### The auxiliary Coulomb matrix
Finally, CC-aims requires information of the interaction/overlap between
RI-basis functions, which is provided by the auxiliary Coulomb matrix 

$$
  V_{\mu\nu}^{\mathbf{q}-\mathbf{k}} = 
  \int d\mathbf{r} d\mathbf{r'} 
\frac{P_{\mu}^{\mathbf{q}}(\mathbf{r})
P_{\nu}^{\mathbf{k}}(\mathbf{r'})}{|\mathbf{r}-\mathbf{r'}|}.
$$

## Quantities output by CC-aims
The purpose of CC-aims is to calculate the input
quantities of CC4S using results obtained from
a SCF calculation of an ab-initio quantum
chemistry code, FHI-aims in particular.
In addition to the Fock matrix and the SCF-eigenvalues, 
the quantities, calculated and/or output 
by CC-aims are:

### Spin eigenvalues
If a spin-polarized SCF-calculation was performed, CC4S requires the 
$S_z$-eigenvalues as multiples of $\hbar$, meaning an $\alpha$-state is 
assigned a value of $0.5$, and a $\beta$-state $-0.5$.

### The Coulomb vertex
This is the pivotal quantity, CC-aims computes, which is required by CC4S.
In the conventional formulation of most correlated quantum-chemistry methods,
like MP2 and CC, the evaluation of the two-electron Coulomb integrals

$$
  V_{pqrs} = \int d\mathbf{r} d\mathbf{r'} \psi_{p}(\mathbf{r})^{*}
             \psi_{q}(\mathbf{r'})\frac{1}{|\mathbf{r} - \mathbf{r'}|}
             \psi_{r}(\mathbf{r})\psi_{s}(\mathbf{r'})
$$

is necessary, $\psi_{p}$, $\psi_{q}$, $\psi_{r}$ and $\psi_{s}$ being either MO 
or Bloch states. This tensor growths with the fourth order with system size
and constitutes the major memory bottleneck of post-SCF methods.
This is particularly true for CC methods where all interactions between 
unoccupied/virtual and occupied orbials need to be taken into account.

The Coulomb vertex $\Gamma_{pq}^{G}$ is a rank-3 tensor, which can be defined
by its relation to the tensor of two-electron Coulomb integrals

$$
  V_{pqrs} = \sum_{G} \mathbf{\Gamma}_{pr}^{G}(\mathbf{\Gamma}_{qs}^{G})^{\dagger}.
$$

By virtue of its reduced rank (in comparison to the Coulomb integrals tensor) the
Coulomb vertex constitutes a significantly more memory efficient approach of
storing the Coulomb integrals, allowing for the calculation of significantly bigger 
systems.

For the formula to calculate the Coulomb vertex in a plain-waves basis, see the 
[paper by the Grüneis group](https://doi.org/10.1063/1.4977994). In a localized atomic
basis framework, the Coulomb vertex can be obtained via

$$
  \Gamma_{ik}^{\nu}(\mathbf{k}_{i}, \mathbf{k}_k) =
  \sum_{\mu}\sum_{\alpha\gamma} c_{i}^{\alpha*}(\mathbf{k}_{i})
  c_{k}^{\gamma}(\mathbf{k}_{k})C_{\gamma\alpha}^{\mu*}(\mathbf{k}_{k}, \mathbf{k}_{i})
  (V^{\mathbf{k}_{i}-\mathbf{k}_{k}})^{\frac{1}{2}}_{\mu\nu}.
$$

Here, $\Gamma_{ik}^{\nu}(\mathbf{k}_{i}, \mathbf{k}_k)$ denotes the 
generalized Coulomb vertex for periodic systems, where $i$ and $k$ denote
the band-index and $\mathbf{k}_{i}$ and $\mathbf{k}_{k}$ the k-vector of two
HF-/KS-Bloch states. The $\nu$-index goes over all RI/auxiliary basis function
with momentum $\mathbf{k}_{i}-\mathbf{k}_{k}$, 
$P_{\nu}^{\mathbf{k}_{i}-\mathbf{k}_{k}}(\mathbf{r})$.

Furthermore, in that equation $\alpha$ and $\gamma$ denote Bloch-like basis functions,
and $\mu$ (just as $\nu$) denote the index of RI basis functions. 
$c_{i}^{\alpha*}(\mathbf{k}_{i})$ is the $\alpha$-th element of the $i$-th HF- or 
KS-eigenvector. 

$C_{\gamma\alpha}^{\mu}(\mathbf{k}_{k}, \mathbf{k}_{i})$ denotes the RI-expansion 
coefficient of the $\mu$-th RI-function to approximate the product of the
$\gamma$-th and the $\alpha$-th Bloch-like basis function 
$\phi_{\gamma,\mathbf{k}_{k}}\phi_{\alpha,\mathbf{k}_{i}}$.

$(V^{\mathbf{k}_{i}-\mathbf{k}_{k}}){\mu\nu}$ denotes the auxiliary Coulomb matrix 
element $\int d\mathbf{r} d\mathbf{r'} 
\frac{P_{\mu}^{\mathbf{k}_{i}}(\mathbf{r})
P_{\nu}^{\mathbf{k}_{k}}(\mathbf{r'})}{|\mathbf{r}-\mathbf{r'}|}$.


